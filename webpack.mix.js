let mix = require('laravel-mix');
mix.webpackConfig({
    resolve: {
        alias: { 'vuejs-datatable': 'vuejs-datatable/dist/vuejs-datatable.esm.js' }
    }
});
mix.setPublicPath('');
mix.js('./assets/src/js/app.js', './assets/src/dist/js/app.js')
.sass('./assets/sass/cart.scss', './assets/src/dist/css/cart.css');

mix.js('./assets/src/js/admin.js', './assets/src/dist/js/admin.js')