$(document).ready(function(){

	if($(window).width() <= 767) {
		setImageTag();
	} else {
		var card_img_height = $('.display-card-image img[data-id="1"]').height();
		$('.display-card-image').closest('.image-container').find('.after').css({ 'height': card_img_height + 'px' });
	}

	$(window).on('resize', function(){
		if($(window).width() <= 767) {
			setImageTag();
			return;
		}

		$('.display-card-image').closest('.image-container').find('.after').removeAttr('style');
		$('.display-card-image .datestamp_tag').removeAttr('style');
		$('.display-card-image .planeswalker_tag').removeAttr('style');
		$('.display-card-image .language_tag').removeAttr('style');

		var card_img_height = $('.display-card-image img[data-id="1"]').height();
		$('.display-card-image').closest('.image-container').find('.after').css({ 'height': card_img_height + 'px' });
	});

	function setImageTag(){
		var card_img_width = $('.display-card-image img[data-id="1"]').width();
		var card_img_height = $('.display-card-image img[data-id="1"]').height();
		var card_img_left = $('.display-card-image img[data-id="1"]').offset().left - $('.card-image-container .image-container').offset().left - $('.card-image-container').offset().left;
		$('.display-card-image .language_tag').css({ 
			'left': (card_img_left + 30) + 'px'
		});		

		$('.display-card-image').closest('.image-container').find('.after').css({ 'left' : (card_img_left + 15 ) + 'px', 'height': card_img_height ,'width' : card_img_width + 'px' });

		card_img_left = $('.display-card-image img[data-id="1"]').offset().left - $('.card-image-container .image-container').offset().left - $('.card-image-container').offset().left - $('.display-card-image .planeswalker_tag').width();

		$('.display-card-image .planeswalker_tag').css({ 
			'left': (card_img_left + card_img_width - 5) + 'px', 
			'right': 'unset'
		});

		card_img_left = $('.display-card-image img[data-id="1"]').offset().left - $('.card-image-container .image-container').offset().left - $('.card-image-container').offset().left - $('.display-card-image .datestamp_tag').width();
		$('.display-card-image .datestamp_tag').css({ 
			'left': (card_img_left + card_img_width - 5) + 'px', 
			'right': 'unset'
		});
	}

	function load_manacost(data){
		if(!data) return;
		var manacost_disp = '';
		var newStr = data.replace(/[}{]/g, '');

		for(var count = 0; count < newStr.length; count++) {
			manacost_disp += '<i class="mana-'+ newStr[count].toLowerCase() +'"></i>';
		}
		return manacost_disp;
	}
	var manacost = $('.mana_cost').html();
	if(manacost != ''){
		$('.mana_cost').html(load_manacost(manacost));
	}
 
	function related_card_section(){
		$(".related-card-section").not('.slick-initialized').slick({
			lazyLoad: 'ondemand',
	    autoplay: true,
	    dots: true,
	    slidesToShow: 4,
	  	slidesToScroll: 4,
	    responsive: [{ 
	        breakpoint: 767,
	        settings: {
	        arrows: false,
	        infinite: false,
	        slidesToShow: 2,
	        slidesToScroll: 2
	        } 
	    }]
		});	
	}

	$('.image-container img').on("error", function () {
		$(this).attr('src', base_url + "assets/images/no-image-slide.jpg");
	});

	$(document).on('click', '.additional-card-image img', function(){
		var data_id = $(this).data('id');
		$('.display-card-image img').removeClass('active');
		$('.display-card-image img[data-id="'+data_id+'"]').addClass('active');

		if(data_id == 1) {
			$('.display-card-image').closest('.image-container').find('.after').show();
			$('.display-card-image .card_img_tag').show();
		} else {
			$('.display-card-image').closest('.image-container').find('.after').hide();
			$('.display-card-image .card_img_tag').hide();
		}
	});

	$(document).on('change', '.condition_list', function(){
		var value = $(this).val();
		var lang = '';
		if($('.condition_list option:selected').data('lang') != '') {
			lang = '[' + $('.condition_list option:selected').data('lang') + '] ';
		}
		$('.card-title span').html(lang);
		$('.add_to_cart_container').hide();
		$('.price-container').hide();
		$('.add_to_cart_container[data-condition="'+ value +'"]').show();
		$('.price-container[data-condition="'+ value +'"]').show();
	});

	var card_ability = convertSymbol($('.card-ability').html());
	$('.card-ability').html(card_ability);

	$(document).on('click', '.add_item_to_wishlist', function(){
		var id = $(this).data('id');

		if($(this).hasClass('btn-success')){
			$.ajax({
				type: 'POST',
				url: base_url + 'api/add_item_to_wishlist',
				data: {
					token: getCookie('csrf_cookie'),
					id: id,
					language: c_language
				},
				success: function(data){

				}
			});

			$(this).html('Remove from Watchlist');
			$(this).removeClass('btn-success');
			$(this).addClass('btn-danger');
			return;
		}

		$.ajax({
			type: 'POST',
			url: base_url + 'api/remove_item_to_wishlist',
			data: {
				token: getCookie('csrf_cookie'),
				id: id
			},
			success: function(data){

			}
		});
		$(this).html($(this).data('default-text'));
		$(this).addClass('btn-success');
		$(this).removeClass('btn-danger');
	});

	related_card_section();
	//Now it will not throw error, even if called multiple times.
	$(window).on( 'resize', related_card_section );

	var category_tree = [];

	card_category.forEach(function(cat){
		var subcategory = cat.subcategory;
		var sub_category_code = JSON.parse(cat.sub_category_code);
		if(cat.subcategory){
			var subcat_list = [];

			subcategory.forEach(function(subcat, index){
				var sub_code = sub_category_code[index] + '';
				if(sub_code != ""){
					subcat_list.push({
						text: get_language(subcat),
						href: base_url + 'cards/?cat=' + sub_code.toLowerCase()
					});
				}
				
			});
			
			category_tree.push({
				text: '<span style="color:#333">'+ get_language(cat.main_category) +'</span>',
			  state: {
			    expanded: false
			  },
				nodes: subcat_list
			});
		}
	});

	$('#card-tree-view').treeview({
		data: category_tree,
		enableLinks: true, 
		multiSelect: false
	});

	$(document).on('mouseup', '.node-card-tree-view', function(){
		$(this).find('.expand-icon').trigger('click');
	});

});