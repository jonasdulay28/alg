$(document).ready(function(){
    var fee_type = 'international';
    var main_currency_sign = 'NT';

    set_country('.shipping_address ');
    set_state('.shipping_address ');
    set_checkout();

    function set_checkout() {
      if(purrcoins > 0) $('.purrcoin_used').html(purrcoins);
      if(coupon_value > 0 || coupon_discount > 0){
        var coupon_used = coupon_discount ? coupon_discount + '%' : coupon_value;
        $('.coupon_used').html(coupon_used);
      }
      $('.discount').html((discount * 100));
      check_country();
      check_state(); 
      set_total_amount();
    }

    function set_total_amount() {
      var fee = 0;
      var payment_method = $('input[name="payment_method"]:checked').val() + '';

      $('.checkout_type p').hide();
      $('.checkout_type').show();

      if(fee_type == 'international') {
       fee = parseFloat($('.int_checkout .payment_method:checked').data('fee'));
       free_int_shipping_fee = parseFloat($('.int_checkout .payment_method:checked').data('free'));
       var check_price = parseFloat(sub_total) - parseFloat(purrcoins) - parseFloat(sub_total * discount) - parseFloat(sub_total * coupon_discount) - coupon_value;
       if(check_price >= parseFloat(free_int_shipping_fee)) {
        fee = 0;
        $('.checkout_type .type_free').show();
       } else {
        $('.checkout_type .type_int').show();
        $('.checkout_type .type_int small').html(fee);
       }
      } else {
        fee = parseFloat($('.local_checkout .payment_method:checked').data('fee'));
        free_shipping_fee = parseFloat($('.local_checkout .payment_method:checked').data('free'));
        var check_price = parseFloat(sub_total) - parseFloat(purrcoins) - parseFloat(sub_total * discount) - parseFloat(sub_total * coupon_discount) - coupon_value;
        if(check_price >= parseFloat(free_shipping_fee)) {
          fee = 0;
          $('.checkout_type .type_free').show();
        } else {
          $('.checkout_type .type_local').show();
          $('.checkout_type .type_local small').html(fee);
       }
      }
      // } else {
      //   $('.checkout_type p.type_free').show();
      // }

      $('.shipping_fee_amount').html(fee);
      $('.sub_total_amount').html(sub_total);

      total_amount = parseFloat(sub_total) - parseFloat(purrcoins) - parseFloat(sub_total * discount) - parseFloat(sub_total * coupon_discount) - coupon_value + parseFloat(fee);
      
      $('.total_amount').html(total_amount);
    }

  	$(".checkout_page .billing_address .country").on("change",function(){
  		$(".checkout_page .billing_address .state").val("");
  		set_state('.checkout_page .billing_address ');
  	});

    $(".checkout_page input[type='radio'].payment_method").on("change",function(){
      set_total_amount();
    });

    $(".checkout_page .shipping_address .country").on("change",function(){
      $(".checkout_page .shipping_address .state").val("");
      set_state('.checkout_page .shipping_address ');
      check_country();
      if($(this).val().toLowerCase() != 'Taiwan'){
        $('.int_checkout').show();
        $('.local_checkout').hide();
      } else {
        $('.int_checkout').hide();
        $('.local_checkout').show();
      }
    });

    $(".checkout_page .shipping_address .state").on("change",function(){
      check_state();
    });

    $('.shipping_checkbox').on('change', function(){
      if(!$(this).prop('checked')) {
        $('.checkout_page .billing_address').show();
        return;        
      }
      $('.checkout_page .billing_address').hide();
    });

    function check_country() {
      var value = $(".checkout_page .shipping_address .country option:selected").data('value');
      value = value ? value : '';

      if(value.toLowerCase() == 'taiwan') {
        if( total_amount >= parseInt(free_shipping_fee)) {
          $('.shipping_fee').html(shipping_fee);
        }else {
          $('.shipping_fee').html(shipping_fee);
        }
        fee_type = 'local';
      } else {
        if( total_amount >= parseInt(free_int_shipping_fee)) {
          $('.shipping_fee').html(int_shipping_fee);
        }else {
          $('.shipping_fee').html(int_shipping_fee);
        }
        fee_type = 'international';
      }
      set_total_amount();
    }

    function check_required(billing_info, shipping_info) {
      var is_pass = true;
      for(i in billing_info) {
        if(billing_info[i] == '') {
          is_pass = false;
          break;
        }
      }

      if(!is_pass) return is_pass;
      for(i in shipping_info) {
        if(shipping_info[i] == '') {
          is_pass = false;
          break;
        }
      }
      return is_pass;
    }

    function check_state() {
      var value = $(".checkout_page .shipping_address .state option:selected").html();
      value = value ? value : '';

      $('.checkout_subtype').hide();

      if(value.toLowerCase() == 'bangkok') {
        //$('.checkout_subtype').show();
      }
      set_total_amount();
    }

    $('.btn_checkout').on('click', function(){
      var checkout_btn = $(this);
      var selected_opt = 0;
      if(fee_type == 'international') {
        for(var i = 0; i < $('.int_checkout .payment_method').length; i++) {
          if($('.int_checkout .payment_method').eq(i).prop('checked') === true) {
            selected_opt = i;
            break;
          }
        }
      } else {
        for(var i = 0; i < $('.local_checkout .payment_method').length; i++) {
          if($('.local_checkout .payment_method').eq(i).prop('checked') === true) {
            selected_opt = i;
            break;
          }
        }
      }

      $.ajax({
        type: "POST",
        url: base_url + "api/check_cart",
        data: {
          token: getCookie('csrf_cookie'),
          id: ''
        },
        dataType:"json",
        success: function(data){
          if(data.length == 0) {
            $.ajax({
              type: "POST",
              url: base_url + "api/get_cart_total",
              data: {
                token: getCookie('csrf_cookie'),
                id: '',
                fee_type: fee_type,
                selected_opt: selected_opt
              },
              dataType:"json",
              beforeSend: function() {
                
              },
              success: function(data){
                if(data.status != 'success'){
                  toastr.error(data.message);
                  return;
                }
                var payment_method = fee_type == 'international' ? $('input[name="payment_method_int"]:checked').val() + '' : $('input[name="payment_method"]:checked').val() + '';

                base_total = data.data.base_total;
                sub_total = data.data.sub_total;
                shipping_fee = data.data.shipping_fee;
                int_shipping_fee = data.data.int_shipping_fee;
                free_shipping_fee = data.data.free_shipping_fee;
                free_int_shipping_fee = data.data.free_int_shipping_fee;

                purrcoins = parseFloat(data.data.purrcoins);
                coupon_discount = parseFloat(data.data.coupon_discount);
                coupon_value = parseFloat(data.data.coupon_value);

                single_card_rebate = parseFloat(data.data.single_card_rebate);
                other_card_rebate = parseFloat(data.data.other_card_rebate);

                $('#confirmationModal .cart_table tbody').html('');
                for(var i in data.cart_item){
                  var data_item = data.cart_item[i];
                  var tr_items = '<tr>'
                  + '<td>'+data_item.card_name+'</td>'
                  + '<td>'+data_item.card.set+'</td>'
                  + '<td><p>'+main_currency_sign + ' ' +data_item.card_price+'</p></td>'
                  + '<td>'+data_item.quantity+'</td>'
                  + '<td><p>'+main_currency_sign + ' ' +data_item.card_total_amount+'</p></td>'
                  + '</tr>'
                  ;
                  $('#confirmationModal .cart_table tbody').append(tr_items);
                }

                var sub_amount = data.total_amount;
                sub_total = sub_amount;

                var fee = 0;
                if(fee_type == 'international') {
                 fee = parseFloat(int_shipping_fee);
                 var check_price = parseFloat(sub_amount) - parseFloat(purrcoins) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount) - coupon_value;
                 if(check_price >= parseFloat(free_int_shipping_fee)) {
                  fee = 0;
                 }
                } else {
                  fee = parseFloat(shipping_fee);
                  var check_price = parseFloat(sub_amount) - parseFloat(purrcoins) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount) - coupon_value;
                  if(check_price >= parseFloat(free_shipping_fee)) {
                    fee = 0;
                  }
                }
                total_amount = parseFloat(sub_amount) - parseFloat(purrcoins) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount) - coupon_value + parseFloat(fee);
                var points_earned = 0;
                var payment_details_sub_total = parseFloat(sub_amount) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount);
                var payment_details_coupon = parseFloat(sub_amount * coupon_discount) +  coupon_value;

                $('.shipping_fee_amount').html(fee);
                $('.sub_total_amount').html(sub_total);
                $('.total_amount').html(total_amount);

                $('#confirmationModal').modal('show');
                if(fee == 0) {
                  $('.confirmation_order').hide();
                  $('.confirmation_pickup').show();
                  $('#confirmationModal .tr-shipping-fee').hide();
                } else {
                  $('.confirmation_order').show();
                  $('.confirmation_pickup').hide();
                  $('#confirmationModal .tr-shipping-fee').show();
                }
                var email = $('.confirmation_order .email').val();
                var billing_info = {
                  'firstname' : $('.checkout_page .billing_address .first_name').val(),
                  'lastname' : $('.checkout_page .billing_address .last_name').val(),
                  'country' : $('.checkout_page .billing_address .country').val(),
                  'state' : $('.checkout_page .billing_address .state').val(),
                  'street' : $('.checkout_page .billing_address .street_address').val(),
                  'city' : $('.checkout_page .billing_address .town_city').val(),
                  'zip' : $('.checkout_page .billing_address .postal_zip').val(),
                  'phone' : $('.checkout_page .billing_address .phone').val()
                };

                var shipping_info = {
                  'firstname' : $('.checkout_page .shipping_address .first_name').val(),
                  'lastname' : $('.checkout_page .shipping_address .last_name').val(),
                  'country' : $('.checkout_page .shipping_address .country').val(),
                  'state' : $('.checkout_page .shipping_address .state').val(),
                  'street' : $('.checkout_page .shipping_address .street_address').val(),
                  'city' : $('.checkout_page .shipping_address .town_city').val(),
                  'zip' : $('.checkout_page .shipping_address .postal_zip').val(),
                  'phone' : $('.checkout_page .shipping_address .phone').val()
                };

                if($('.shipping_checkbox').prop('checked')) billing_info = shipping_info;

                $('.confirmation_order .billing_address .c_first_name').html(billing_info.firstname);
                $('.confirmation_order .billing_address .c_last_name').html(billing_info.lastname);
                $('.confirmation_order .billing_address .c_country').html(billing_info.country);
                $('.confirmation_order .billing_address .c_state').html(billing_info.state);
                $('.confirmation_order .billing_address .c_street_address').html(billing_info.street);
                $('.confirmation_order .billing_address .c_town_city').html(billing_info.city);
                $('.confirmation_order .billing_address .c_postal_zip').html(billing_info.zip);
                $('.confirmation_order .billing_address .c_phone').html(billing_info.phone);

                $('.confirmation_order .shipping_address .c_first_name').html(shipping_info.firstname);
                $('.confirmation_order .shipping_address .c_last_name').html(shipping_info.lastname);
                $('.confirmation_order .shipping_address .c_country').html(shipping_info.country);
                $('.confirmation_order .shipping_address .c_state').html(shipping_info.state);
                $('.confirmation_order .shipping_address .c_street_address').html(shipping_info.street);
                $('.confirmation_order .shipping_address .c_town_city').html(shipping_info.city);
                $('.confirmation_order .shipping_address .c_postal_zip').html(shipping_info.zip);
                $('.confirmation_order .shipping_address .c_phone').html(shipping_info.phone);

                if($('.shipping_checkbox').prop('checked')) billing_info = shipping_info;

                var checkout_type = '';

                $('.hidden_checkout_btn').attr('data-mode', checkout_type);
              }
            });            
          } else {
            $('#outOfStockModal').modal('show');

            $('#outOfStockModal .table tbody').html('');
            data.forEach(function(el){
              var append_html = '<tr>'
                + '<td style="color:red">' + el.card_name + '</td>'
                + '<td>' + el.card.set_name + '</td>'
                + '<td>' + el.quantity + '</td>'
                + '<td>' + el.card.stock + '</td>'
                + '</tr>';
              $('#outOfStockModal .table tbody').append(append_html);
            });
          }
        }
      });
    });

    $('.hidden_checkout_btn').on('click', function(){
      var checkout_btn = $(this);
      var email = $('.checkout_page .email').val();
      var billing_info = {
        'firstname' : $('.checkout_page .billing_address .first_name').val(),
        'lastname' : $('.checkout_page .billing_address .last_name').val(),
        'country' : $('.checkout_page .billing_address .country').val(),
        'state' : $('.checkout_page .billing_address .state').val(),
        'street' : $('.checkout_page .billing_address .street_address').val(),
        'city' : $('.checkout_page .billing_address .town_city').val(),
        'zip' : $('.checkout_page .billing_address .postal_zip').val(),
        'phone' : $('.checkout_page .billing_address .phone').val()
      };

      var shipping_info = {
        'firstname' : $('.checkout_page .shipping_address .first_name').val(),
        'lastname' : $('.checkout_page .shipping_address .last_name').val(),
        'country' : $('.checkout_page .shipping_address .country').val(),
        'state' : $('.checkout_page .shipping_address .state').val(),
        'street' : $('.checkout_page .shipping_address .street_address').val(),
        'city' : $('.checkout_page .shipping_address .town_city').val(),
        'zip' : $('.checkout_page .shipping_address .postal_zip').val(),
        'phone' : $('.checkout_page .shipping_address .phone').val()
      };

      if($('.shipping_checkbox').prop('checked')) billing_info = shipping_info;
      var payment_method = $('input[name="payment_method"]:checked').val() + '';

      if(!check_required(billing_info, shipping_info) && payment_method.toLowerCase() != 'pickup') {
        toastr.error('Please fillup all required fields');
        return;
      }
      loading()
      $.ajax({
        type: "POST",
        url: base_url + "api/get_cart_total",
        data: {
          token: getCookie('csrf_cookie'),
          id: ''
        },
        dataType:"json",
        beforeSend: function() {
        	
        },
        success: function(data){
          if(data.status != 'success'){
            toastr.error(data.message);
            return;
          }

          base_total = data.data.base_total;
          sub_total = data.data.sub_total;
          shipping_fee = data.data.shipping_fee;
          int_shipping_fee = data.data.int_shipping_fee;
          free_shipping_fee = data.data.free_shipping_fee;
          free_int_shipping_fee = data.data.free_int_shipping_fee;

          purrcoins = parseFloat(data.data.purrcoins);
          coupon_discount = parseFloat(data.data.coupon_discount);
          coupon_value = parseFloat(data.data.coupon_value);

          single_card_rebate = parseFloat(data.data.single_card_rebate);
          other_card_rebate = parseFloat(data.data.other_card_rebate);

          var sub_amount = data.total_amount;
          var fee = 0;
          if(payment_method.toLowerCase() != 'pickup'){
            if(fee_type == 'international') {
             fee = parseFloat(int_shipping_fee);
             var check_price = parseFloat(sub_amount) - parseFloat(purrcoins) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount) - coupon_value;
             if(check_price >= parseFloat(free_int_shipping_fee)) {
              fee = 0;
             }
            } else {
              fee = parseFloat(shipping_fee);
              var check_price = parseFloat(sub_amount) - parseFloat(purrcoins) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount) - coupon_value;
              if(check_price >= parseFloat(free_shipping_fee)) {
                fee = 0;
              }
            }
          }
          total_amount = parseFloat(sub_amount) - parseFloat(purrcoins) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount) - coupon_value + parseFloat(fee);
          var points_earned = 0;
          var payment_details_sub_total = parseFloat(sub_amount) - parseFloat(sub_amount * discount) - parseFloat(sub_amount * coupon_discount);
          var payment_details_coupon = parseFloat(sub_amount * coupon_discount) +  coupon_value;

          data.items.forEach(function(item){
            if(item.card.category == 'single cards') {
              var price = item.card.regular_price ? item.card.regular_price : item.card.foil_price;
              price = price * item.quantity;

              var rebate = parseFloat(price) * parseFloat(single_card_rebate);
            } else {
              var price = item.card.regular_price ? item.card.regular_price : item.card.foil_price;
              price = price * item.quantity;

              var rebate = parseFloat(price) * parseFloat(other_card_rebate);
            }

            points_earned += rebate;
          });

          points_earned = (points_earned - purrcoins) - coupon_value;
          points_earned = points_earned < 0 ? 0 : points_earned;
          var shipping_mode = '';
          if(fee_type == 'international'){ 
            shipping_mode = 'International Shipping';
          } else {
            var value = $(".checkout_page .shipping_address .state option:selected").html();
            value = value ? value : '';
            shipping_mode = 'Local Shipping';
          }

          var payment_details = {
            shipping_fee: parseFloat(fee),
            sub_total: payment_details_sub_total,
            coupon_value: payment_details_coupon,
            total_amount: total_amount
          };

          let dataParam = {
            token: getCookie('csrf_cookie'),
            cart_list: JSON.stringify(data.items),
            billing_info: JSON.stringify(billing_info),
            shipping_info: JSON.stringify(shipping_info),
            amount: total_amount,
            coupon_discount: coupon_discount,
            coupon_value: coupon_value,
            points_earned: points_earned,
            shipping_mode: shipping_mode,
            total_base_price: base_total,
            payment_details: JSON.stringify(payment_details)
          };

          $.ajax({
            type: "POST",
            url: base_url + "api/register_guest",
            data: {
              token: getCookie('csrf_cookie'),
              billing_info: JSON.stringify(billing_info).replace(/'/g, "\\'"),
              shipping_info: JSON.stringify(shipping_info).replace(/'/g, "\\'"),
              email: email
            },
            dataType:"json",
            success: function(data){
              if(data.status != 'success') {
                toastr.error(data.message);
                return;
              }

              dataParam['payment_mode'] = $('[name="payment_method"]:checked').val();

              $.ajax({
                type: "POST",
                url: base_url + "functions/order_product_bt",
                data: dataParam,
                dataType:"json",
                success: function(data){
                  var output = data;
                  if(output.status == "success"){
                    window.location = output.approval_link;
                  } else {
                    toastr.error(output.error_msg);
                  }
                  close_loading();
                }
              });
              return;

            }
          });
          
        }
      });
    });

  	function set_user_billing_address(){
  		$.ajax({
        type: "GET",
        url: base_url+"api/get_user_billing_address",
        data:{},
        dataType: "json",
        success: function(data) {
        	if(data.billing_address.length > 0) {
        		var tmp_billing_address = data.billing_address;
          	var billing_address = tmp_billing_address[0];
          	for(var i in billing_address){
          		if(i != "id" && i != "state"){
          			$(".billing_address ."+i).val(billing_address[i]);
          		}
  					}

  					set_state('.billing_address ');
  					setTimeout(function(){
  						$(".billing_address .state").val(billing_address['state']);
  					},300)
  					
        	}else {
        		set_country('.checkout_page');
      			set_state('.checkout_page ');
        	}
        },
        error: function(err) {
            console.log(err);
        }
    	});
  	}

  	function set_country(page) {
        if($(page + ".country").val() == '')
  			$(page + ".country").val("Taiwan");
  		}
    function set_state(page) {
      $(page + ".state option").remove();
			$.ajax({
        type: "GET",
        url: base_url+"api/get_states",
        data:{'q':$(page + ".country option:selected").data('id')},
        dataType: "json",
        success: function(data) {
        	var states = data.states;
        	var options = $(page + ".state");
        	options.find('option')
			    .remove()
			    .end()
			    //don't forget error handling!
			    $.each(states, function(index,item) {
			    	options.append($("<option />").val(item.id).attr('data-value', item.name).text(item.name));
			    });
			    $(page + ".state").prop("disabled",false);
          if(page == '.checkout_page .shipping_address ') check_state();
        },
        error: function(err) {
            console.log(err);
        }
    	});
		}
  });