$(document).ready(function () {

	var cards_list_group;
	var cards_list;
	var card_list_all;
	var gallery_count = 0;
	var gallery_max_count = 24;
	var gallery_load = true;
	var show_stock = true;
	var main_currency_sign = 'NT';


	var card_table_html = '<table class="table table-hover table-responsive table-card-list cards_table_list">' +
		'<thead>' +
		'<th>'+ get_language('Image', site_language) + '</th>' +
		'<th>'+ get_language('Name', site_language) + '</th>' +
		'<th>'+ get_language('Edition', site_language) + '</th>'
		// +		'<th>Cost</th>'
		+
		'<th>'+ get_language('Condition', site_language) + '</th>' +
		'<th style="color:black !important;padding-right:20px !important;">'+ get_language('Price', site_language) + '</th>' +
		'<th></th>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>';

	load_all_cards();

	function load_all_cards() {
		$('.loading-container').show();
		if (display_type != 'all') {
			$('.filter-variation [data-value="regular"]').prop('checked', true);
			$('.mobile-filter-variation [data-value="regular"]').addClass('active');
			$('.filter-language [data-value=""]').prop('checked', true);
			$('.mobile-filter-language [data-value=""]').addClass('active');
		}
		for (var i in session_sort['sort']) {
			$('.checkbox-group[data-value="' + i + '"] input[data-value="' + session_sort['sort'][i] + '"]').prop('checked', true);
			$('.mobile-group-filter[data-value="' + i + '"] [data-value="' + session_sort['sort'][i] + '"]').addClass('active');
			if (i == 'promo' && session_sort['sort'][i] == 'planeswalker') {
				$('[name="variation"]').prop('checked', false);
				$('[data-name="variation"]').removeClass('active');
			}
		}

		sort_product();
		return;

		if (search_name || cat_code) {
			$.ajax({
				type: 'POST',
				url: base_url + 'api/get_product',
				data: {
					search_name: search_name,
					cat_code: cat_code
				},
				success: function (data) {
					console.log(data)
					$('.loading-container').hide();
					card_list_all = JSON.parse(data);
					cards_list = card_list_all;
					load_datatable(cards_list);
					load_galleryview(cards_list);
				}
			});
			return;
		}

		$.ajax({
			type: 'POST',
			url: base_url + 'api/get_all_products',
			success: function (data) {

				console.log(data)
				$('.loading-container').hide();
				card_list_all = JSON.parse(data);
				cards_list = card_list_all;
				$('.checkbox-group [data-value="all"]').prop('checked', true);
				load_datatable(cards_list);
				load_galleryview(cards_list);
			}
		});
	}
	$(window).on('orientationchange', function () {
		if ($('#card-list-container').html() != '') {
			load_datatable(cards_list);
		}
	});

	function truncate_name(input) {
		if (input.length > 18)
			return input.substring(0, 18) + '...';
		else
			return input;
	};

	function load_galleryview(list) {
		$('.load_gallery_view').hide();
		var disp = '';
		if (gallery_count == 0) disp += '<div style="margin-bottom: 10px;    display: block;width: 100%;"><input type="checkbox" name="show_stock" class="show_stock" style="margin-left: 10px"/> ' + get_language('Show In Stock Items Only', site_language) + '</div>';
		if (gallery_count >= list.length) return;
		while (gallery_count < list.length && gallery_count != gallery_max_count) {
			var cards = list[gallery_count];
			var card = cards[0];
			var img_src = getCardImage(card);
			var card_name = '';
			var price = card.regular_price ? card.regular_price : card.foil_price;
			var is_planeswalker = false;
			var card_datestamp = false;
			var name = card.name;

			if(!card.regular_price) {
				name = name.replace("[FOIL] ", "");
				name = '[FOIL] ' + name.replace(" - Foil", "");
			}

			if (card.rarity == 'T') {
				if (card.type.includes('Basic Land')) {
					card_name = name + ' (' + card.collector_number + ') (' + card.set_code + ')';
				} else {
					card_name = name;
				}
			} else {
				if (card.type.includes('Basic Land')) {
					card_name = name + '(' + card.collector_number + ') (' + card.set_code + ')';
				} else {
					card_name = name;
				}
			}
			var card_language = card.language.toUpperCase();
			var card_name_lang = '';
			
			if(card_language == 'TW' || card_language == 'ENG'){
				card_name_lang = card.name_tw ? '<p title="' + card.name_tw + '">' + card.name_tw + '</p>' : '';
			} else if(card_language == 'JP'){
				card_name_lang = card.name_jp ? '<p title="' + card.name_jp + '">' + card.name_jp + '</p>' : '';
			} else if(card_language == 'KO'){
				card_name_lang = card.name_ko ? '<p title="' + card.name_ko + '">' + card.name_ko + '</p>' : '';
			}

			var tmp_card_name = card_name;
			card_name = truncate_name(card_name);
			var card_language_tag = card.category == 'single cards' ? getCardLanguage(card_language) : '';
			var card_label = card.category == 'single cards' ? getCardLabel(card.tag, card_language) : '';
			var tmp_lang = card_language;
			if(tmp_lang == 'ENG') tmp_lang = 'EN';
			var card_language_disp = tmp_lang ? '[' + tmp_lang + '] ' : '';
			var datestamp_tag = '';
			var planeswalker_tag = '';
			var tag = card.tag;

			var condition_list = [];
			condition_list.push(card.fully_handled);

			var condition_add_cart_disp = '';
			var condition_add_watchlist_disp = '';
			var other_card_disp = '';
			var other_card_condition_disp = '';

			if (!watchlist_data[card.id]) {
				condition_add_watchlist_disp += '<button class="btn add_item_to_wishlist active btn-success" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + card.id + '" data-condition="' + card.fully_handled + '">'+ get_language('Add to Wishlist', site_language) +'</button>';
			} else {
				condition_add_watchlist_disp += '<button class="btn add_item_to_wishlist btn-danger active" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + card.id + '" data-condition="' + card.fully_handled + '">Remove from Wishlist</button>';
			}

			for (var ctr = 1; ctr < cards.length; ctr++) {
				var tmp_card = cards[ctr];
				var card_price = tmp_card.regular_price ? tmp_card.regular_price : tmp_card.foil_price;
				if (!_.contains(condition_list, tmp_card.fully_handled)) {
					condition_list.push(tmp_card.fully_handled);
					fully_handled = tmp_card.fully_handled.toUpperCase();
					// switch (fully_handled) {
					// 	case 'nm':
					// 		fully_handled = 'Near Mint';
					// 		break;
					// 	case 'm':
					// 		fully_handled = 'Mint';
					// 		break;
					// 	case 'ex':
					// 		fully_handled = 'Excellent';
					// 		break;
					// 	case 'vg':
					// 		fully_handled = 'Very Good';
					// 		break;
					// 	case 'g':
					// 		fully_handled = 'Good';
					// 		break;
					// 	case 'used':
					// 		fully_handled = 'Used';
					// 		break;
					// 	case 'p':
					// 		fully_handled = 'Played';
					// 		break;
					// 	case 'd':
					// 		fully_handled = 'Damaged';
					// 		break;
					// 	default:
					// 		break;
					// }
					other_card_condition_disp += '<option value="' + tmp_card.fully_handled + '">' + fully_handled + '</option>';
					if (rate) {
						other_card_disp += '<div class="card-text-price" style="display: none" data-condition="' + tmp_card.fully_handled + '">' + main_currency_sign + ' ' + card_price + ' <br><span class="sub_currency">(' + parseFloat(card_price * rate).toFixed(2) + ' ' + currency + ')</span></div>'
					} else {
						other_card_disp += '<div class="card-text-price" style="display: none" data-condition="' + tmp_card.fully_handled + '">' + main_currency_sign + ' ' + card_price + '</div>'
					}

					condition_add_cart_disp += '<div class="btn-add-cart-container" data-condition="' + tmp_card.fully_handled + '" style="display: none">';

					if (!watchlist_data[tmp_card.id]) {
						condition_add_watchlist_disp += '<button class="btn add_item_to_wishlist btn-success" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + tmp_card.id + '" data-condition="' + tmp_card.fully_handled + '">'+ get_language('Add to Wishlist', site_language) +'</button>';
					} else {
						condition_add_watchlist_disp += '<button class="btn add_item_to_wishlist btn-danger" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + tmp_card.id + '" data-condition="' + tmp_card.fully_handled + '">Remove from Wishlist</button>';
					}

					if (tmp_card.stock > 0) {
						if (cart_data[tmp_card.id]) {
							condition_add_cart_disp += '<div class="add-cart-wrapper" data-id="' + tmp_card.id + '" data-index="' + gallery_count + '"><button class="btn btn-default add_to_cart" data-id="' + tmp_card.id + '" data-target="card-gallery-quantity-content-' + tmp_card.id + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="top" data-html="true">' + cart_data[tmp_card.id].quantity + ' in Cart</button></div>';
						} else {
							condition_add_cart_disp += '<div class="add-cart-wrapper" data-id="' + tmp_card.id + '" data-index="' + gallery_count + '"><button class="btn btn-primary add_to_cart" data-id="' + tmp_card.id + '" data-target="card-gallery-quantity-content-' + tmp_card.id + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="top" data-html="true">'+ get_language('Add to Cart', site_language) +'</button></div>';
						}

						condition_add_cart_disp += '<div id="card-gallery-quantity-content-' + tmp_card.id + '" class="hide add-cart-wrapper">';
						condition_add_cart_disp += '<div class="text-center"> <span class="label-quantity">Select Qty</span> </div>';
						condition_add_cart_disp += '<div class="cart-quantity-content" data-container="card-quantity-content-' + tmp_card.id + '" data-index="' + gallery_count + '" data-id="' + tmp_card.id + '" style="width: 150px; min-height: 100px;">';
						for (var count = 0; count <= tmp_card.stock; count++) {
							if (tmp_card.rarity == 'U' || tmp_card.rarity == 'R' || tmp_card.rarity == 'M') {
								if (count <= 8)
									condition_add_cart_disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
							} else {
								if (count <= 24)
									condition_add_cart_disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
							}

						}
						condition_add_cart_disp += '</div>';
						condition_add_cart_disp += '</div>';
					} else {
						condition_add_cart_disp += '<div class="out-of-stock-container"><span class="out-of-stock">Out of Stock</span></div>';
					}

					condition_add_cart_disp += '</div>';
				}
			}

			if (card.promo == 'planeswalker') {
				is_planeswalker = true;
				planeswalker_tag = '<img class="planeswalker_tag img_tag" src="' + base_url + 'assets/images/planeswalker.png">';
			} else if (card.promo == 'datestamp') {
				datestamp_tag = card.datestamp ? '<label class="datestamp_tag img_tag">' + card.datestamp + '</label>' : '';
				card_datestamp = card.datestamp;
			}
			disp += '<div class=" col-xs-3 col-md-2 col-sm-3" data-name="' + card.name + '">';
			disp += '<div class="gallery_view_container">';
			if (card.regular_price) {
				disp += '<div class="card-image image-container"><a href="javascript:void(0);">' +
					card_language_tag +
					card_label +
					datestamp_tag +
					'<img class="lazy" data-tag="' + tag + '" data-category="'+ card.category +'" data-lang="' + card_language + '" data-planeswalker="' + is_planeswalker + '" data-datestamp="' + card_datestamp + '" data-cardtype="regular" data-original="' + img_src + '" style="width:100%;" alt="' + card_name + '" >' +
					planeswalker_tag +
					'<div class="middle"><div class="preview_button"><i class="fa fa-search"></i></div></div>' +
					'<div class="card-watchlist-container">' + condition_add_watchlist_disp + '</div>' +
					'</a></div>';
			} else {
				disp += '<div class="card-image image-container"><a href="javascript:void(0);">' +
					card_language_tag +
					card_label +
					datestamp_tag +
					'<img class="lazy" data-tag="' + tag + '" data-category="'+ card.category +'" data-lang="' + card_language + '" data-planeswalker="' + is_planeswalker + '" data-datestamp="' + card_datestamp + '" data-cardtype="foil" data-original="' + img_src + '" style="width:100%;" alt="' + card_name + '" >' +
					planeswalker_tag +
					'<div class="middle"><div class="preview_button"><i class="fa fa-search"></i></div></div>' +
					'<div class="card-watchlist-container">' + condition_add_watchlist_disp + '</div>' +
					'</a><div class="after"></div></div>';
			}
			disp += '<div class="tab-content">';
			disp += '<div class="tab-pane tab-pane-border active card-price text-center">';
			disp += '<div class="card-name"><a href="' + base_url + 'cards/view/' + card.id + '"><span title="' + tmp_card_name + '">' + card_language_disp + card_name + '</span>' + card_name_lang + '</a></div>'
			disp += '<div class="card-detail-container">';
			disp += '<div class="card-set-code">' + card.set_code + '</div>';
			if (rate) {
				disp += '<div class="card-text-price" data-condition="' + card.fully_handled + '">' + main_currency_sign + ' ' + price + '<br><span class="sub_currency">(' + parseFloat(price * rate).toFixed(2) + ' ' + currency + ')</span></div>'
			} else {
				disp += '<div class="card-text-price" data-condition="' + card.fully_handled + '">' + main_currency_sign + ' ' + price + '</div>'
			}
			var condition_disp = '<select class="condition_list">';
			var fully_handled = card.fully_handled.toUpperCase();
			// switch (fully_handled) {
			// 	case 'nm':
			// 		fully_handled = 'Near Mint';
			// 		break;
			// 	case 'm':
			// 		fully_handled = 'Mint';
			// 		break;
			// 	case 'ex':
			// 		fully_handled = 'Excellent';
			// 		break;
			// 	case 'vg':
			// 		fully_handled = 'Very Good';
			// 		break;
			// 	case 'g':
			// 		fully_handled = 'Good';
			// 		break;
			// 	case 'used':
			// 		fully_handled = 'Used';
			// 		break;
			// 	case 'p':
			// 		fully_handled = 'Played';
			// 		break;
			// 	case 'd':
			// 		fully_handled = 'Damaged';
			// 		break;
			// 	default:
			// 		break;
			// }

			condition_disp += '<option value="' + card.fully_handled + '">' + fully_handled + '</option>';
			condition_disp += other_card_condition_disp;
			var card_fully_handled = fully_handled;

			condition_disp += '</select>';
			if (cards.length < 2) condition_disp = '<div class="card_condition">' + card_fully_handled + '</div>';

			disp += other_card_disp;
			disp += condition_disp;
			//disp += '<div class="card-info">'+ card.fully_handled  + '</div>';
			disp += '</div>'; // END card-detail-container
			disp += '<div class="btn-add-cart-container" data-condition="' + card.fully_handled + '">';
			//disp += '<ul class="nav nav-tabs" role="tablist"><li class="active"><a href="#">NM</a></li></ul>';

			if (card.stock > 0) {
				if (cart_data[card.id]) {
					disp += '<div class="add-cart-wrapper" data-id="' + card.id + '" data-index="' + gallery_count + '"><button class="btn btn-default add_to_cart" data-id="' + card.id + '" data-target="card-gallery-quantity-content-' + card.id + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="top" data-html="true">' + cart_data[card.id].quantity + ' in Cart</button></div>';
				} else {
					disp += '<div class="add-cart-wrapper" data-id="' + card.id + '" data-index="' + gallery_count + '"><button class="btn btn-primary add_to_cart" data-id="' + card.id + '" data-target="card-gallery-quantity-content-' + card.id + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="top" data-html="true">'+ get_language('Add to Cart', site_language) +'</button></div>';
				}

				disp += '<div id="card-gallery-quantity-content-' + card.id + '" class="hide add-cart-wrapper">';
				disp += '<div class="text-center"> <span class="label-quantity">Select Qty</span> </div>';
				disp += '<div class="cart-quantity-content" data-container="card-quantity-content-' + card.id + '" data-index="' + gallery_count + '" data-id="' + card.id + '" style="width: 150px; min-height: 100px;">';
				for (var count = 0; count <= card.stock; count++) {
					if (card.rarity == 'U' || card.rarity == 'R' || card.rarity == 'M') {
						if (count <= 8)
							disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
					} else {
						if (count <= 24)
							disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
					}

				}
				disp += '</div>';
				disp += '</div>';
			} else {
				disp += '<div class="out-of-stock-container"><span class="out-of-stock">Out of Stock</span></div>';
			}

			disp += '</div>';
			disp += condition_add_cart_disp;
			//disp += condition_add_watchlist_disp;

			disp += '</div>';
			disp += '</div>';
			disp += '</div>';
			disp += '</div>';
			gallery_count++;
		}
		gallery_max_count += 24;
		if (gallery_count < list.length - 1) {
			$('.load_gallery_view').show();
		}

		$('#card-gallery-container .row').append(disp);
		$("#card-gallery-container [data-toggle=popover]").each(function (i, obj) {

			$(this).popover({
				html: true,
				content: function () {
					var target = $(this).data('target');
					return $('#' + target).html();
				}
			});

		});
		gallery_load = false;
		$(function () {
			$("img.lazy").lazyload();
		});
	}

	$(document).on('click', '#card-list-container .after', function () {
		$(this).closest('.card-image').find('a').trigger('click');
	});

	$(document).on('click', '.add_item_to_wishlist', function () {
		var id = $(this).data('id');

		if ($(this).hasClass('btn-success')) {
			$.ajax({
				type: 'POST',
				url: base_url + 'api/add_item_to_wishlist',
				data: {
					token: getCookie('csrf_cookie'),
					id: id,
					language: c_language
				},
				success: function (data) {

				}
			});

			$('.add_item_to_wishlist[data-id="' + id + '"]').html('Remove from Wishlist');
			$('.add_item_to_wishlist[data-id="' + id + '"]').removeClass('btn-success');
			$('.add_item_to_wishlist[data-id="' + id + '"]').addClass('btn-danger');
			return;
		}

		$.ajax({
			type: 'POST',
			url: base_url + 'api/remove_item_to_wishlist',
			data: {
				token: getCookie('csrf_cookie'),
				id: id
			},
			success: function (data) {

			}
		});
		$('.add_item_to_wishlist[data-id="' + id + '"]').html($(this).data('default-text'));
		$('.add_item_to_wishlist[data-id="' + id + '"]').addClass('btn-success');
		$('.add_item_to_wishlist[data-id="' + id + '"]').removeClass('btn-danger');
	});

	$(window).scroll(function () {
		if ($(window).scrollTop() + $(window).height() >= $(document).height() && !gallery_load) {
			if ($('#card-gallery-container').hasClass('active')) {
				gallery_load = true;
				load_galleryview(cards_list);
			}
		}
	});

	$(document).on('click', '.load_gallery_view', function () {
		gallery_load = true;
		load_galleryview(cards_list);
	});

	$(document).on('click', '#datestampModal .btn-apply', function () {
		include_datestamp = true;
		datestamp = $('#datestampModal select option:selected').val();
		$('[name="variation"]').prop('checked', false);
		$('[data-name="variation"]').removeClass('active');
		sort_product();
	});

	$('#datestampModal').on('hidden.bs.modal', function () {
		if (!include_datestamp) {
			$('[name="promo"]').prop('checked', false);
			$('[data-name="promo"]').removeClass('active');
			sort_product();
		}
	});

	function load_datatable(list) {
		var data_list = [];
		list.forEach(function (e) {
			data_list.push(e[0]);
		});

		$('#card-list-container').html(card_table_html);
		$('.table-card-list').DataTable({
			"searching": true,
			"lengthChange": true,
			"oLanguage": {
				sLengthMenu: "Show _MENU_",
			},
			drawCallback: function () {
				$("img.lazy").lazyload();
				setTimeout(function () {
					$("#card-list-container [data-toggle=popover]").each(function (i, obj) {
						$(this).popover({
							html: true,
							content: function () {
								var target = $(this).data('target');
								return $('#' + target).html();
							}
						});
					});
				}, 500)
			},
			data: data_list,
			'aoColumnDefs': [{
					'sType': 'currency',
					'aTargets': [4]
				} // In this case 5th column will be sorted on currency basis.
			],
			"bDestroy": true,
			"bStateSave": false,
			order: [
				[4, "desc"]
			],
			"lengthMenu": [50, 100],
			columns: [{
					data: 'id',
					searchable: false,
					bSortable: false,
					render: function (data, type, row, meta) {
						var img_src = getCardImage(row);
						var card_language = row.language ? row.language.toUpperCase() : '';
						var card_language_tag = row.category == 'single cards' ? getCardLanguage(card_language) : '';
						var card_label = row.category == 'single cards' ? getCardLabel(row.tag, card_language) : '';
						var planeswalker_tag = '';
						var datestamp_tag = '';
						var card_datestamp = '';
						var tag = row.tag == 'custom' ? row.custom_tag : row.tag;
						var is_planeswalker = false;
						var disp = "";
						if (row.promo == 'planeswalker') {
							planeswalker_tag = '<img class="planeswalker_tag img_tag" src="' + base_url + 'assets/images/planeswalker.png">';
							is_planeswalker = true;
						} else if (row.promo == 'datestamp') {
							datestamp_tag = row.datestamp ? '<label class="datestamp_tag img_tag">' + row.datestamp + '</label>' : '';
							card_datestamp = row.datestamp;
						}


						if (row.regular_price) {
							disp = '<div class="card-image image-container" style="display: inline-block">' + card_language_tag + card_label + datestamp_tag + planeswalker_tag + '<a href="javascript:void(0);"><img class="lazy" data-tag="' + tag + '" data-category="'+ row.category +'" data-lang="' + card_language + '" data-planeswalker="' + is_planeswalker + '" data-datestamp="' + card_datestamp + '" data-cardtype="regular" data-original="' + img_src + '"  alt="' + row.name + '"></a></div>';
						} else {
							disp = '<div class="card-image image-container" style="display: inline-block">' + card_language_tag + card_label + datestamp_tag + planeswalker_tag + '<a href="javascript:void(0);"><img class="lazy" data-tag="' + tag + '" data-category="'+ row.category +'" data-lang="' + card_language + '" data-planeswalker="' + is_planeswalker + '" data-datestamp="' + card_datestamp + '" data-cardtype="foil" data-original="' + img_src + '"  alt="' + row.name + '"></a><div class="after"></div></div>';
						}
						return disp;
					}
				},
				{
					data: 'name',
					render: function (data, type, row, meta) {
						var card_name_lang = '';
						var card_type_tw = '';
						var name = data;

						if(!row.regular_price) {
							name = name.replace('[FOIL] ', '');
							name = '[FOIL] ' + name.replace(' - Foil', '');
						}

						if(row.language.toUpperCase() == 'ENG' || row.language.toUpperCase() == 'TW') {
							card_name_lang = row.name_tw ? row.name_tw : '';
							card_type_tw = row.type_tw ? row.type_tw : '';
						} else if(row.language.toUpperCase() == 'JP') {
							card_name_lang = row.name_jp ? row.name_jp : '';
							card_type_tw = row.type_jp ? row.type_jp : '';
						} else if(row.language.toUpperCase() == 'KO') {
							card_name_lang = row.name_ko ? row.name_ko : '';
							card_type_tw = row.type_ko ? row.type_ko : '';
						}
						var lang = row.language;
						if(lang.toUpperCase() == 'ENG') lang = 'EN';
						var card_language = lang ? '[' + lang.toUpperCase() + '] ' : '';
						var disp = "";
						if (row.rarity == 'T') {
							if (row.type.includes('Basic Land')) {
								disp = '<div class="card-name"><a href="' + base_url + 'cards/view/' + row.id + '">' + card_language + name + ' (' + row.collector_number + ') (' + row.set_code + ')' + '</a>';
								// disp += ' (' + row.set_code + ')';
								disp += '</div>';
								disp += card_name_lang;
								disp += '<div class="card-type"><span>' + row.type.replace('?', '—') + '</span></div>';
								disp += card_type_tw;
								return disp;
							} else {
								disp = '<div class="card-name"><a href="' + base_url + 'cards/view/' + row.id + '">' + card_language + name + ' (' + row.set_code + ')' + '</a>';
								// disp += ' (' + row.set_code + ')';
								disp += '</div>';
								disp += card_name_lang;
								disp += '<div class="card-type"><span>' + row.type.replace('?', '—') + '</span></div>';
								disp += card_type_tw;
								return disp;
							}
						} else {
							if (row.type.includes('Basic Land')) {
								disp = '<div class="card-name"><a href="' + base_url + 'cards/view/' + row.id + '">' + card_language + name + ' (' + row.collector_number + ') (' + row.set_code + ')' + '</a>';
								// disp += ' (' + row.set_code + ')';
								disp += '</div>';
								disp += card_name_lang;
								disp += '<div class="card-type"><span>' + row.type.replace('?', '—') + '</span></div>';
								disp += card_type_tw;
								return disp;
							} else {
								disp = '<div class="card-name"><a href="' + base_url + 'cards/view/' + row.id + '">' + card_language + name + ' (' + row.set_code + ')' + '</a>';
								// disp += ' (' + row.set_code + ')';
								disp += '</div>';
								disp += card_name_lang;
								disp += '<div class="card-type"><span>' + row.type.replace('?', '—') + '</span></div>';
								disp += card_type_tw;
								return disp;
							}
						}

					}
				},
				{
					data: 'set_code',
					bSortable: false,
					render: function (data, type, row, meta) {
						if (data.toLowerCase() == "waraa")
							data = "war"

						var rarity = row.rarity;
						if (rarity == "U") {
							rarity = "uncommon";
						} else if (rarity == "C") {
							rarity = "common";
						} else if (rarity == "R") {
							rarity = "rare";
						} else if (rarity == "M") {
							rarity = "mythic";
						}
						var disp = '<i class="ss ss-' + data.toLowerCase() + ' ss-2x ss-' + rarity + '"></i>';
						return disp;
					}
				},
				//   { 
				//   	data: 'manacost',
				//   	searchable: false,
				//   	bSortable: false,
				//   	render: function(data) {
				//   		var disp = '';
				//   		var newStr = data.replace(/[}{]/g, '');

				// for(var count = 0; count < newStr.length; count++) {
				// 	disp += '<i class="mana-'+ newStr[count].toLowerCase() +'"></i>';
				// }

				//   		return disp;
				//   	} 
				//   },
				{
					data: 'fully_handled',
					searchable: false,
					bSortable: false,
					render: function (data, type, row, meta) {
						var disp = '';
						var fully_handled = '';
						if (cards_list[meta.row].length > 1) {
							cards_list[meta.row].forEach(function (e) {
								fully_handled = e.fully_handled.toUpperCase();
								// switch (fully_handled) {
								// 	case 'nm':
								// 		fully_handled = 'Near Mint';
								// 		break;
								// 	case 'm':
								// 		fully_handled = 'Mint';
								// 		break;
								// 	case 'ex':
								// 		fully_handled = 'Excellent';
								// 		break;
								// 	case 'vg':
								// 		fully_handled = 'Very Good';
								// 		break;
								// 	case 'g':
								// 		fully_handled = 'Good';
								// 		break;
								// 	case 'used':
								// 		fully_handled = 'Used';
								// 		break;
								// 	case 'p':
								// 		fully_handled = 'Played';
								// 		break;
								// 	case 'd':
								// 		fully_handled = 'Damaged';
								// 		break;
								// 	default:
								// 		break;
								// }
								disp += '<div style="height: 80px">' + fully_handled + '</div>';
							})
							return disp;
						}

						fully_handled = data.toUpperCase();
						// switch (fully_handled) {
						// 	case 'nm':
						// 		fully_handled = 'Near Mint';
						// 		break;
						// 	case 'm':
						// 		fully_handled = 'Mint';
						// 		break;
						// 	case 'ex':
						// 		fully_handled = 'Excellent';
						// 		break;
						// 	case 'g':
						// 		fully_handled = 'Good';
						// 		break;
						// 	case 'used':
						// 		fully_handled = 'Used';
						// 		break;
						// 	case 'p':
						// 		fully_handled = 'Played';
						// 		break;
						// 	case 'd':
						// 		fully_handled = 'Damaged';
						// 		break;
						// 	default:
						// 		break;
						// }
						return fully_handled;
					}
				},
				{
					data: 'price',
					searchable: false,
					render: function (data, type, row, meta) {
						var disp = '';
						if (cards_list[meta.row].length > 1) {
							cards_list[meta.row].forEach(function (e) {
								if (rate) {
									disp += '<div style="height: 80px"><p><b>' + main_currency_sign + e.price + '</b>' + '<br>(' + parseFloat(e.price * rate).toFixed(2) + ' ' + currency + ')' + '</p></div>';
								} else {
									disp += '<div style="height: 80px"><p><b>' + main_currency_sign + e.price + '</b></p></div>';
								}
								
							});
							return disp;
						}
						if (rate) {
							return '<div style="height: 80px"><b>' + main_currency_sign + ' ' + data + '</b>' + '<br>(' + parseFloat(data * rate).toFixed(2) + ' ' + currency + ')</div>';
						} else {
							return '<div style="height: 80px"><b>' + main_currency_sign + ' ' + data + '</b><div>';
						}
					}
				},
				{
					data: 'id',
					searchable: false,
					bSortable: false,
					render: function (data, type, row, meta) {
						if (cards_list[meta.row].length > 1) {
							var disp = '';
							cards_list[meta.row].forEach(function (e, index) {
								disp += '<div style="height: 80px">';
								if (e.stock > 0) {
									if (cart_data[e.id]) {
										disp += '<div class="add-cart-wrapper" data-id="' + e.id + '" data-index="' + meta.row + '"><button class="btn btn-default add_to_cart" data-id="' + e.id + '" data-target="card-quantity-content-' + e.id + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="left" data-html="true">' + cart_data[e.id].quantity + ' in Cart</button>';
									} else {
										disp += '<div class="add-cart-wrapper" data-id="' + e.id + '" data-index="' + meta.row + '"><button class="btn btn-primary add_to_cart" data-id="' + e.id + '" data-target="card-quantity-content-' + e.id + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="left" data-html="true">'+ get_language('Add to Cart', site_language) +'</button>';
									}

									disp += '</div>';

									disp += '<div id="card-quantity-content-' + e.id + '" class="hide add-cart-wrapper">';
									disp += '<div class="text-center"> <span class="label-quantity">Select Qty</span> </div>';
									disp += '<div class="cart-quantity-content" data-container="card-quantity-content-' + meta.row + '" data-index="' + meta.row + '" data-id="' + e.id + '" style="width: 150px; min-height: 100px;">';
									for (var count = 0; count <= e.stock; count++) {
										if (e.rarity == 'U' || e.rarity == 'R' || e.rarity == 'M') {
											if (count <= 8)
												disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
										} else {
											if (count <= 24)
												disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
										}

									}
									disp += '</div>';
									disp += '</div>';

								} else {
									disp += '<div><span class="out-of-stock">Out of Stock</span></div>';
								}

								if (!watchlist_data[e.id]) {
									disp += '<div><button class="btn add_item_to_wishlist btn-success" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + e.id + '">'+ get_language('Add to Wishlist', site_language) +'</button></div>';
								} else {
									disp += '<div><button class="btn add_item_to_wishlist btn-danger" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + e.id + '">Remove from Wishlist</button></div>';
								}
								disp += '</div>';
							});
							return disp;
						}

						if (row.stock > 0) {
							if (cart_data[row.id]) {
								var disp = '<div class="add-cart-wrapper" data-id="' + row.id + '" data-index="' + meta.row + '"><button class="btn btn-default add_to_cart" data-id="' + row.id + '" data-target="card-quantity-content-' + meta.row + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="left" data-html="true">' + cart_data[row.id].quantity + ' in Cart</button>';
							} else {
								var disp = '<div class="add-cart-wrapper" data-id="' + row.id + '" data-index="' + meta.row + '"><button class="btn btn-primary add_to_cart" data-id="' + row.id + '" data-target="card-quantity-content-' + meta.row + '" data-default-text="'+ get_language('Add to Cart', site_language) +'" data-toggle="popover" data-container="body" data-placement="left" data-html="true">'+ get_language('Add to Cart', site_language) +'</button>';
							}
							disp += '</div>';

							disp += '<div id="card-quantity-content-' + meta.row + '" class="hide add-cart-wrapper">';
							disp += '<div class="text-center"> <span class="label-quantity">Select Qty</span> </div>';
							disp += '<div class="cart-quantity-content" data-container="card-quantity-content-' + meta.row + '" data-index="' + meta.row + '" data-id="' + row.id + '" style="width: 150px; min-height: 100px;">';
							for (var count = 0; count <= row.stock; count++) {
								if (row.rarity == 'U' || row.rarity == 'R' || row.rarity == 'M') {
									if (count <= 8)
										disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
								} else {
									if (count <= 24)
										disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="' + count + '">' + count + '</button>';
								}

							}
							disp += '</div>';
							disp += '</div>';

						} else {
							var disp = '<div><span class="out-of-stock">Out of Stock</span></div>';
						}

						if (!watchlist_data[row.id]) {
							disp += '<div><button class="btn add_item_to_wishlist btn-success" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + row.id + '">'+ get_language('Add to Wishlist', site_language) +'</button></div>';
						} else {
							disp += '<div><button class="btn add_item_to_wishlist btn-danger" data-default-text="'+ get_language('Add to Wishlist', site_language) +'" data-id="' + row.id + '">Remove from Wishlist</button></div>';
						}
						return disp;
					}
				}
			]
		});

		$('.dataTables_length').append('<input type="checkbox" name="show_stock" class="show_stock" style="margin-left: 10px"/> ' + get_language('Show In Stock Items Only', site_language));

		$("#card-list-container [data-toggle=popover]").each(function (i, obj) {
			$(this).popover({
				html: true,
				content: function () {
					var target = $(this).data('target');
					return $('#' + target).html();
				}
			});

		});
	}

	$(document).on("click", ".paginate_button, .cards_table_list th:not(.sorting_disabled)", function () {
		enable_popover();
	});

	$(document).on("click", ".show_stock", function () {
		show_stock = !show_stock;
		sort_product();
	});

	function enable_popover() {
		$('.popover').remove();
		$("#card-list-container [data-toggle=popover]").each(function (i, obj) {

			$(this).popover({
				html: true,
				content: function () {
					var target = $(this).data('target');
					return $('#' + target).html();
				}
			});

		});
	}

	$(document).on('click', '.table-card-list td:not(.active-row), .table-card-list th, .dataTables_paginate, .dataTables_info, .dataTables_filter', function () {
		$('.add-cart-wrapper').removeClass('active');
	});

	$(document).on('click', '.cart-quantity-content button', function () {
		var value = $(this).data('value');
		var index = $(this).closest('.cart-quantity-content').data('index');
		var id = $(this).closest('.cart-quantity-content').data('id');
		var add_cart_button = $('.add_to_cart[data-id="' + id + '"]');
		var default_text = add_cart_button.data('default-text');
		var card_detail = {};

		if (!cards_list[0]) return;
		if (Array.isArray(cards_list[0])) {
			for (var ctr = 0; ctr < cards_list.length; ctr++) {
				index = _.findIndex(cards_list[ctr], function (o) {
					return o.id == id;
				});
				if (index >= 0) {
					card_detail = cards_list[ctr][index];
					break;
				}
			}
		} else {
			index = _.findIndex(cards_list, function (o) {
				return o.id == id;
			});
			card_detail = cards_list[index];
		}
		if (!card_detail) return;
		if (card_detail.id == id) {
			if (value == 0) {
				delete cart_data[card_detail.id];
			} else {
				cart_data[card_detail.id] = {
					'card': card_detail,
					'quantity': value
				};
			}
			var data = cart_data;
			cart_list = [];
			for (i in data) {
				var broken_img_src = base_url + "assets/images/no-image-slide.jpg";
				var img_src = data[i].card.set_code && data[i].card.collector_number && data[i].card.regular_price != "" ? 'https://www.alg.tw/images/regular_cards/' + data[i].card.set_code + '/' + pad(data[i].card.collector_number, 3) + '.jpg' : (data[i].card.set_code && data[i].card.collector_number && data[i].card.foil_price != "" ? 'https://www.alg.tw/images/regular_cards/' + data[i].card.set_code + '/' + pad(data[i].card.collector_number, 3) + '.jpg' : broken_img_src);
				data[i].card.img_src = img_src;
				data[i]['variation'] = parseFloat(data[i].card.price) == parseFloat(data[i].card.regular_price) ? 'Regular' : 'Foil';
				cart_list.push(data[i]);
			}

			$('.add-cart-wrapper').removeClass('active');

			$.ajax({
				type: 'POST',
				url: base_url + 'api/add_item_to_cart',
				data: {
					token: getCookie('csrf_cookie'),
					card_id: card_detail.id,
					quantity: value,
					language: c_language
				},
				success: function (data) {
					$('.loading-container').hide();
					var output = JSON.parse(data);

					if (output.status == 'success') {
						cart_list = output.cart_list;

						if (value == 0) {
							add_cart_button.removeClass('btn-default');
							add_cart_button.addClass('btn-primary');
							add_cart_button.html(default_text);
						} else {
							add_cart_button.addClass('btn-default');
							add_cart_button.removeClass('btn-primary');
							add_cart_button.html(value + ' in Cart');
						}
						bus.$emit('refresh_cart', cart_list);
					} else {
						toastr.error(output.message);
					}
				}
			})
		}
	});

	$('.card-image img').on("error", function () {
		$(this).attr('src', "assets/images/no-image-slide.jpg");
	});

	$(document).on('change', '.filter-section input', function () {
		var type_value = $(this).data('value');
		var data_name = $(this).attr('name');
		$('.mobile-filter-section a[data-name="' + data_name + '"]').removeClass('active');
		$('.mobile-filter-section a[data-name="' + data_name + '"][data-value="' + type_value + '"]').addClass('active');
		if (data_name == 'promo') {
			if ($(this).data('value') == 'planeswalker') {
				$('[name="variation"]').prop('checked', false);
				$('[data-name="variation"]').removeClass('active');
			}
		}
		sort_product();
	});

	$(document).on('click', '.mobile-filter-section a', function () {
		var type_value = $(this).data('value');
		var data_name = $(this).data('name');
		$('.mobile-filter-section a[data-name="' + data_name + '"]').removeClass('active');
		$(this).addClass('active');
		$('.filter-section input[name="' + data_name + '"][data-value="' + type_value + '"]').prop('checked', true);
		$('.filter-section input[name="' + data_name + '"][data-value="' + type_value + '"]').trigger('change');
	});

	function getCardLanguage(language) {
		language += '';
		language = language.toLowerCase();
		var el = '';

		if(language == 'tw') {
			el = '<img src="'+ base_url + 'assets/images/tag/tw_tag.jpeg' +'" class="card_tag_language">';
		} else if(language == 'eng') {
			el = '<img src="'+ base_url + 'assets/images/tag/eng_tag.jpeg' +'" class="card_tag_language">';
		} else if(language == 'jp') {
			el = '<img src="'+ base_url + 'assets/images/tag/jp_tag.jpeg' +'" class="card_tag_language">';
		}

		return el;
	}

	function getCardLabel(tag, language) {
		tag += '';
		tag = tag.toLowerCase();
		language += '';
		language = language.toLowerCase();

		var label = ['best_seller', 'great_value', 'limited_time', 'new_product', 'preorder', 'promotion', 'recommended', 'sale', 'selling_fast', 'special_order'];
		var i = label.indexOf(tag);
		var disp = '';
		var lang = 'en';

		if(site_language == 'traditional_chinese') {
			lang = 'tw';
		}

		if(i >= 0 && lang != '') {
			i++;
			disp = '<img src="' + base_url + 'assets/images/labels/' + 'label-' + lang + '-' + pad(i, 2) + '.png' + '" class="card_label" >';
		}

		return disp;
	}

	function sort_product() {
		$('#card-list-container').html('');
		$('#myDiv').html('');
		$('.loading-container').show();
		$('.load_gallery_view').hide();
		$('#card-gallery-container .row').html('');
		var array_filter = [];
		var filter_data = {};

		$('.filter-section input').each(function () {
			if (this.checked) {
				var filter = $(this).closest('.checkbox-group').data('value');
				array_filter[filter] = array_filter[filter] ? array_filter[filter] : [];
				filter_data[filter] = $(this).data('value');

				if (filter == 'color') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					switch (filter_value) {
						case 'white':
							array_filter[filter].push('W');
							break;
						case 'blue':
							array_filter[filter].push('U');
							break;
						case 'black':
							array_filter[filter].push('B');
							break;
						case 'red':
							array_filter[filter].push('R');
							break;
						case 'green':
							array_filter[filter].push('G');
							break;
						case 'colorless':
							array_filter[filter].push('Colorless');
							break;
						case 'multi':
							array_filter[filter].push('Multi');
							break;
						case 'all':
							array_filter[filter].push('all');
							break;
					}
				} else if (filter == 'type') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					array_filter[filter].push(filter_value);
				} else if (filter == 'rarity') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();

					if (filter_value == 'mythic') {
						array_filter[filter].push('M');
					} else if (filter_value == 'rare') {
						array_filter[filter].push('R');
					} else if (filter_value == 'uncommon') {
						array_filter[filter].push('U');
					} else if (filter_value == 'common') {
						array_filter[filter].push('C');
					} else if (filter_value.toLowerCase() != 'all') {
						array_filter[filter].push('T');
					} else if (filter_value.toLowerCase() == 'all') {
						array_filter[filter].push('all');
					}

				} else if (filter == 'variation') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					array_filter[filter].push(filter_value);
				} else if (filter == 'language') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					array_filter[filter].push(filter_value);
				} else if (filter == 'promo') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					array_filter[filter].push(filter_value);
				}

			}
		});
		array_filter['language'] = array_filter['language'] ? array_filter['language'].indexOf('all') > -1 ? [] : array_filter['language'] : [];
		array_filter['rarity'] = array_filter['rarity'] ? array_filter['rarity'].indexOf('all') > -1 ? [] : array_filter['rarity'] : [];
		array_filter['color'] = array_filter['color'] ? array_filter['color'].indexOf('all') > -1 ? [] : array_filter['color'] : [];
		array_filter['type'] = array_filter['type'] ? array_filter['type'].indexOf('all') > -1 ? [] : array_filter['type'] : [];
		array_filter['promo'] = array_filter['promo'] ? array_filter['promo'].indexOf('all') > -1 ? [] : array_filter['promo'] : [];
		array_filter['variation'] = array_filter['variation'] ? array_filter['variation'].indexOf('all') > -1 ? [] : array_filter['variation'] : ['all'];
		var language = $('input[name="language"]:checked').length > 0 ? $('input[name="language"]:checked').eq(0).data('value') : '';
		c_language = language;

		$.ajax({
			type: 'POST',
			url: base_url + 'api/sort_product',
			data: {
				token: getCookie('csrf_cookie'),
				search_name: search_name,
				cat_code: cat_code,
				filter_language: JSON.stringify(array_filter['language']),
				filter_rarity: JSON.stringify(array_filter['rarity']),
				filter_color: JSON.stringify(array_filter['color']),
				filter_type: JSON.stringify(array_filter['type']),
				filter_variation: JSON.stringify(array_filter['variation']),
				filter_promo: JSON.stringify(array_filter['promo']),
				filter_data: JSON.stringify(filter_data),
				language: language,
				show_stock: show_stock
			},
			success: function (data) {
				cards_list = JSON.parse(data);
				cards_list = _.filter(cards_list, function (el) {
					el.price = parseFloat(el.price);
					el.regular_price = parseFloat(el.regular_price);
					el.foil_price = parseFloat(el.foil_price);
					return el;
				});

				$('#card-list-container').html('');
				$('#myDiv').html('');
				$('#card-gallery-container .row').html('');
				cards_list = _.sortByOrder(cards_list, ['price'], ['desc']);
				var cards_list_group = [];
				var tmp_cards_list = [];
				for(var i = 0; i < cards_list.length; i++){
					if(cards_list[i].price == '0') continue;
					var prod_type = cards_list[i].regular_price != '' ? 'reg' : 'foil';
					var index = cards_list[i].name + '_' + cards_list[i].language + '_' + prod_type + '_' + cards_list[i].set_code;
					if(!tmp_cards_list[index]) {
						tmp_cards_list[index] = [];
					}
					tmp_cards_list[index].push(cards_list[i]);
				}
				for (var prop in tmp_cards_list) {
					// var fully_handled_list = ['nm', 'ex', 'vg', 'g', 'p', 'd'];
					var fully_handled_list = ['nm', 'sp', 'mp', 'hp'];
					var tmp_array = [];

					fully_handled_list.forEach(function (f) {
						var card_condition = _.find(tmp_cards_list[prop], function (e) {
							return e['fully_handled'].toLowerCase() == f
						});
						if (card_condition) {
							tmp_array.push(card_condition);
						}
					})

					if (tmp_array.length > 0) cards_list_group.push(tmp_array);
				}
				gallery_count = 0;
				gallery_max_count = 24;
				if (cards_list_group.length == 0) {
					var no_data_disp = '<center><div><img src="' + base_url + '/assets/images/no_products.png" style="width:50%"></div><div class="text-center" style="font-size:20px; font-weight:bold; margin-bottom: 15px">No cards available</div><a href="#" style="border: 1px solid #00b8f9;background: #05a9e2; color: white; padding: 10px 25px; text-decoration:none;" data-toggle="modal" data-target="#myModal">Try other categories</a></center>';
					$('#myDiv').html(no_data_disp);
					$('#card-list-container').html('');
					$('#card-list-container').css('display', 'none');
					$('#card-gallery-container').css('display', 'none');
					$('.load_gallery_view').hide();
					$('.loading-container').hide();
					return;
				}
				$('.loading-container').hide();

				cards_list = cards_list_group;
				$('#card-list-container').css('display', '');
				$('#card-gallery-container').css('display', '');
				load_datatable(cards_list);
				load_galleryview(cards_list);
				if (show_stock) $('.show_stock').prop('checked', show_stock);
			}
		});
	}

	$(document).on('change', '.condition_list', function () {
		var container = $(this).closest('.gallery_view_container');
		var value = $(this).val();
		var container_price = container.find('.card-text-price[data-condition="' + value + '"]');
		var container_add_card = container.find('.btn-add-cart-container[data-condition="' + value + '"]');
		var container_watchlist = container.find('.card-watchlist-container .add_item_to_wishlist[data-condition="' + value + '"]');
		container.find('.card-watchlist-container .add_item_to_wishlist').removeClass('active');
		if (container_price.length > 0 && container_add_card.length > 0) {
			container.find('.card-text-price').hide();
			container.find('.btn-add-cart-container').hide();
			container_price.show();
			container_add_card.show();
			container_watchlist.addClass('active');
		}
	})
	$(document).on('click', '.card-image a', function (e) {
		e.preventDefault();
		if ($(e.target).hasClass('add_item_to_wishlist')) return;

		var img = $(this).children('img:not(.card_tag_language):not(.card_label)');
		var imgLink = img.attr('src');
		var card_type = img.data('cardtype');
		var planeswalker = img.data('planeswalker');
		var datestamp = img.data('datestamp');
		var language = img.data('lang');
		var card_tag = img.data('tag');
		var category = img.data('category');
		var img_container = $(this).closest('.card-image');

		var card_language_tag = category =='single cards' ? getCardLanguage(language) : '';
		var card_label = category =='single cards' ? getCardLabel(card_tag, language) : '';
		var card_planeswalker_tag = planeswalker ? '<img class="planeswalker_tag img_tag" src="' + base_url + 'assets/images/planeswalker.png">' : '';
		var card_datestamp_tag = datestamp && datestamp != '' ? '<label class="datestamp_tag img_tag">' + datestamp + '</label>' : '';
		if (card_type == "regular") {
			$('.mask').html('<div class="img-box image-container">' + card_language_tag + card_label + card_planeswalker_tag + card_datestamp_tag + '<img src="' + imgLink + '"><a class="close">&times;</a>');
		} else {
			$('.mask').html('<div class="img-box image-container">' + card_language_tag + card_label + card_planeswalker_tag + card_datestamp_tag + '<img src="' + imgLink + '"><div class="after"></div><a class="close">&times;</a>');
		}

		$('.mask').addClass('is-visible fadein').on('animationend', function () {
			$(this).removeClass('fadein is-visible').addClass('is-visible');
		});
		$('.close').on('click', function () {
			$(this).parents('.mask').addClass('fadeout').on('animationend', function () {
				$(this).removeClass('fadeout is-visible')
			});
		});
		$('.is-visible').on('click', function () {
			$(this).addClass('fadeout').on('animationend', function () {
				$(this).removeClass('fadeout is-visible')
			});
		});
	});

	$("a[href='#card-list-container']").on("click", function () {
		if (cards_list.length > 0) {
			$('html, body').animate({
				scrollTop: $("#myDiv").offset().top
			}, 1000);
		}
	})
});
