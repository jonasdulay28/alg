$(document).ready(function(){
	var search_name = '';
	$('.search_form input[type="text"]').keyup(function(e){
		var key = e.which;
		if(key == 13)  // the enter key code
		{
		  $('.search_form button').trigger('click');
		  return false;  
		}
		$('.search_form .auto-complete-container ul').html('');
		search_name = $(this).val();
		$.ajax({
			type: 'POST',
			url: base_url + 'api/get_suggestion_2',
			data: {
				name: search_name
			},
			success: function(data){
				var data2 = JSON.parse(data);
				var disp = '';

				data2.forEach(function(elem){
					if(elem.name_eng_tw != null) {
						disp += '<li><div><span class="none">'+elem.name+'</span>'+ elem.name_eng_tw +'</div></li>';
					} else {
						disp += '<li><div><span class="none">'+elem.name+'</span>'+ elem.name+'</div></li>';
					}
					
				});

				$('.search_form .auto-complete-container ul').html(disp);
			}
		})
	});

	$('.search_form input[type="text"]').focus(function(){
		$('.search_form .auto-complete-container').show();
	});

	$('html').on('click', function(e) {
		if($(e.target).closest('.search_form').length == 0) {
			$('.search_form .auto-complete-container').hide();
		}
	});

	$('.search_form button').click(function(){
		var search_text = search_name;
		//gawin mo na tong ajax para alam kung sa other products or kailangan bang gawin english ung search_text
		window.location.href = base_url + 'cards/?disp=all&name=' + search_text;
	});

	$(document).on('click', '.auto-complete-container ul li', function(){
		var value = $(this).find('span').html();
		$('.search_form input[type="text"]').val(value);
		window.location.href = base_url + 'cards/?disp=all&name=' + value;
		$('.search_form .auto-complete-container ul').html('');
	});

});

function boldString(str, find){
    var re = new RegExp(find, 'g');
    return str.replace(re, '<b>'+find+'</b>');
}

function getUniqueValues(array, key) {
var result = new Set();
array.forEach(function(item) {
    if (item.hasOwnProperty(key)) {
        result.add(item[key]);
    }
});
return result;
}