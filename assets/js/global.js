function getCookie(name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}

$(function () {
	// this bit needs to be loaded on every page where an ajax POST may happen
	$.ajaxSetup({
		data: {
			token: getCookie('csrf_cookie')
		}
	});
	// now you can use plain old POST requests like always
});

/*$(document).ajaxStart(function() { Pace.restart(); });*/

function notify2(header, mes, mes_type) {
	swal(
		header,
		mes,
		mes_type
	);
}

function loading() {
	swal({
		title: 'Please wait...',
		allowOutsideClick: false,
		allowEscapeKey: false
	});
	swal.showLoading();
}

function loading_class(customclass) {
	swal({
		title: 'Please wait...',
		allowOutsideClick: false,
		allowEscapeKey: false,
		customClass: customclass,
	});
	swal.showLoading();
}

function close_loading() {
	swal.close();
}


function confirm_alert(title, text, type) {
	return swal({
		title: title,
		text: text,
		type: type,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	})
}

function show_message(error, status) {
	if (status == 'Error') {
		$(".divError").addClass("Error");
		$(".divError").removeClass("Success");
	} else if (status == 'Success') {
		$(".divError").addClass("Success");
		$(".divError").removeClass("Error");
	}
	$(".divError").fadeIn();
	$(".divError").html(error);
}

function hide_message() {
	$(".divError").fadeOut();
}

function logout() {
	var post_url = "/Authentication/logout";
	$.ajax({
		type: 'POST',
		url: post_url,
		dataType: "json",
		beforeSend: function () {
			loading();
		},
		success: function (res) {
			localStorage.clear();
			window.location = res.url;
		},
		error: function (res) {
			console.log(res);
		}
	});
}

function check_mobile() {
	if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
		/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
		return true;
	}
}

function arrayRemove(arr, value) {

	return arr.filter(function (ele) {
		return ele != value;
	});

}

function convertSymbol(value) {
	var symbol = '';
	var newString = '';
	var start = false;
	value = value + '';

	for (var i = 0; i < value.length; i++) {
		if (start) {
			if (value.charAt(i) == '}') {
				start = false;
				symbol = symbol + '';

				symbol = '<i class="mi mi-' + symbol.toLowerCase() + '"></i>';
				newString += symbol;
				symbol = '';
			} else {
				symbol += value.charAt(i);
			}
		} else if (value.charAt(i) == '{') {
			symbol = '';
			start = true;
		} else {
			newString += value.charAt(i);
		}
	}

	return newString;
}

function getCardImage(card) {
	var broken_img_src = base_url + "assets/images/no-image-slide.jpg";
	var extension = card.regular_price ? '.jpg' : '.jpg';
	var image_base_url = card.regular_price ? 'https://www.alg.tw/images/regular_cards/' : 'https://www.alg.tw/images/regular_cards/';
	var img_src = card.set_code && card.collector_number ? image_base_url + card.set_code + '/' + pad(card.collector_number, 3) + extension : broken_img_src;
	img_src = card.image ? card.image : img_src;

	if (card.rarity == 'T') {
		broken_img_src = base_url + "assets/images/no-image-slide.jpg";
		extension = card.regular_price ? '.jpg' : '.jpg';
		image_base_url = card.regular_price ? 'https://www.alg.tw/images/Tokens/' : 'https://www.alg.tw/images/Tokens/';
		img_src = card.set_code && card.collector_number ? image_base_url + card.set_code + '/' + pad(card.collector_number, 3) + extension : broken_img_src;
		img_src = card.image ? card.image : img_src;
		return img_src;
	}
	return img_src;
}

function truncate_string(input, length) {
	if (input.length > length)
		return input.substring(0, length) + '...';
	else
		return input;
}

$(document).on("change", ".change_language", function (e) {
	e.preventDefault();
	var language = this.value;
	var post_url = base_url + 'homepage/change_language';
	$.ajax({
		type: 'POST',
		url: post_url,
		data: {
			'language': language
		},
		dataType: "json",
		success: function (res) {
			location.reload();
		},
		error: function (res) {}
	});
});

$(document).on("change", ".change_currency", function (e) {
	e.preventDefault();
	var currency = this.value ? this.value : '';
	var post_url = base_url + 'currency_api/get_currency';
	$.ajax({
		type: 'POST',
		url: post_url,
		data: {
			'currency': currency
		},
		dataType: "json",
		success: function (res) {
			location.reload();
		},
		error: function (res) {}
	});
});

function get_language(key,language) {
  var value = key + '';

  if(site_language == 'traditional_chinese') {
  	return typeof tw_text[key] !== 'undefined' ? tw_text[key] : value;
  }

  return value;
}