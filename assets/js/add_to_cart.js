$(document).ready(function(){
	toastr.options = {
	positionClass: "toast-top-center"
	};
		$("[data-toggle=popover]").each(function(i, obj) {

			$(this).popover({
			  html: true,
			  content: function() {
			    var target = $(this).data('target');
			    return $('#' + target).html();
			  }
			});

		});

		$('html').on('click', function(e) {
		  if (typeof $(e.target).data('original-title') == 'undefined' &&
		     !$(e.target).parents().is('.popover.in')) {
		    $('[data-original-title]').popover('hide');
		  } else {
		  	$('[data-original-title]:not([data-id="'+ $(e.target).data('id') +'"])').popover('hide');
		  }
		});

		$('body').on('hidden.bs.popover', function (e) {
		  $(e.target).data("bs.popover").inState = { click: false, hover: false, focus: false }
		});

		$(document).on('afterChange', '.slick-initialized', function(){
		    $('.slick-initialized [data-original-title]').popover('hide');
		});

		function check_add_to_cart(id, value){
			var add_cart_button = $('.add_to_cart[data-id="'+ id +'"]');
			var default_text = add_cart_button.data('default-text');
			if(value == 0){
				add_cart_button.removeClass('btn-default');
				add_cart_button.addClass('btn-primary');
				add_cart_button.html(default_text);
			} else {
				add_cart_button.addClass('btn-default');
				add_cart_button.removeClass('btn-primary');
				add_cart_button.html(value + ' in Cart');
			}
		}

		$(document).on('click', '.cart-quantity-content button', function(){
			$('[data-original-title]').popover('hide');
			var value = $(this).data('value');
			var add_cart_container = $(this).closest('.cart-quantity-content');
			var id = add_cart_container.data('id');
			var index = -1;
			var add_cart_button = $('[data-target="' + add_cart_container.data('container') + '"]');
			var card_detail = {};

			if(!cards_list[0]) return;
			if(Array.isArray(cards_list[0])){
				for(var ctr = 0; ctr < cards_list.length; ctr++) {
					index = _.findIndex(cards_list[ctr], function(o) { return o.id == id; });
					if(index >= 0){
						card_detail = cards_list[ctr][index];
						break;
					}
				}
			} else {
				index = _.findIndex(cards_list, function(o) { return o.id == id; });
				card_detail = cards_list[index];
			}

			if(!card_detail) return;

			if(value == 0){
				delete cart_data[card_detail.id];
			} else {
				cart_data[card_detail.id] = {
					'card': card_detail,
					'quantity': value
				};
			}

			var data = cart_data;
			cart_list = [];
			for (i in data) {
	  		var broken_img_src = base_url + "assets/images/no-image-slide.jpg";
	  		var img_src = data[i].card.set_code && data[i].card.collector_number && data[i].card.regular_price != "" ? 'https://www.alg.tw/images/regular_cards/' + data[i].card.set_code + '/' + pad(data[i].card.collector_number, 3) + '.jpg' : (data[i].card.set_code && data[i].card.collector_number && data[i].card.foil_price != "" ? 'https://www.alg.tw/images/foil_cards/' + data[i].card.set_code + '/' + pad(data[i].card.collector_number, 3) + '.JPG' : broken_img_src);
			data[i].card.img_src = img_src;
			data[i]['variation'] = data[i].card.regular_price ? 'Regular' : 'Foil';
			  cart_list.push(data[i]);
			}
			$('.add-cart-wrapper').removeClass('active');

			$.ajax({
				type: 'POST',
				url: base_url + 'api/add_item_to_cart',
				data: {
					token: getCookie('csrf_cookie'),
					card_id: cards_list[index].id,
					quantity: value,
					language: c_language
				},
				success: function(data) {
					var output = JSON.parse(data);
					
					if(output.status == 'success'){
						cart_list = output.cart_list;
						check_add_to_cart(id, value);
						bus.$emit('refresh_cart', cart_list);
					} else {
						toastr.error(output.message);
					}

					$('.loading_screen').hide();
				}
			})
		
		});

});