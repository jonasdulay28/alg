//Vue
import Vue from 'vue'
//router
import VueRouter from 'vue-router'
//components
import Admin_Dashboard from './Admin_Dashboard.vue'
import Suggestions from 'v-suggestions'
//import 'v-suggestions/dist/v-suggestions.css' // you can import the stylesheets also (optional)

window.Vue = Vue;
//axios
import VueAxios from 'vue-axios'
import axios from 'axios'
import Dashboard from './admin/Dashboard.vue'

//Users
import View_Users from './admin/Users/View.vue'
import Add_Users from './admin/Users/Add.vue'
import Edit_Users from './admin/Users/Edit.vue'

//Orders
import View_Orders from './admin/Orders/View.vue'
import List_Orders from './admin/Orders/List.vue'

//Audit trails
import View_Audit from './admin/Audit/View.vue'

//Coupons
import View_Coupons from './admin/Coupons/View.vue'
import Add_Coupons from './admin/Coupons/Add.vue'
import Edit_Coupons from './admin/Coupons/Edit.vue'

//Categories
import View_Categories from './admin/Categories/View.vue'
import Add_Categories from './admin/Categories/Add.vue'
import Edit_Categories from './admin/Categories/Edit.vue'

//Loyalty Program
import View_Loyalty from './admin/Loyalty/View.vue'
import Edit_Loyalty from './admin/Loyalty/Edit.vue'

//Producs
import View_Products from './admin/Products/View.vue'
import Edit_Products from './admin/Products/Edit.vue'
import Add_Products from './admin/Products/Add.vue'

//Feedbacks
import View_Feedbacks from './admin/Feedbacks/View.vue'
import Edit_Feedbacks from './admin/Feedbacks/Edit.vue'

//Widgets
import Edit_Newest_Expansion from './admin/Widgets/Edit_Newest_Expansion.vue'
import Edit_Banner from './admin/Widgets/Edit_Banner.vue'
//Reports
import View_Reports from './admin/Reports/View.vue'

//Purrcoins
import View_Purrcoins from './admin/Purrcoins/View.vue'

//Pages
import List_Pages from './admin/Pages/List.vue'
import Add_Pages from './admin/Pages/Add.vue'
import Edit_Pages from './admin/Pages/Edit.vue'

import Navbar from './admin/Header.vue'
import Footer from './admin/Footer.vue'
import Sidebar from './admin/Sidebar.vue'
import VueCkeditor from 'vue-ckeditor2';
import DatatableFactory from 'vuejs-datatable';

Vue.use(VueCkeditor);

window.axios = axios
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.use(Suggestions);
Vue.use(DatatableFactory);
Vue.component('vue-ckeditor',VueCkeditor);
Vue.component('admin_header', Navbar);
Vue.component('admin_footer', Footer);
Vue.component('admin_sidebar', Sidebar);
/*export const resumeBus = new Vue();*/

const routes = [
	{
		name: '/',
		path: '/',
		component: Dashboard
	},
	{
		name: '/view_users',
		path: '/view_users',
		component: View_Users
	},
	{
		name: '/add_users',
		path: '/add_users',
		component: Add_Users
	},
	{
		name: '/edit_user',
		path: '/edit_user/:id',
		component: Edit_Users
	},
	{
		name: '/view_purrcoins',
		path: '/view_purrcoins/:id',
		component: View_Purrcoins
	},
	{
		name: '/view_orders',
		path: '/view_orders',
		component: List_Orders
	},
	{
		name: '/view_orders/:id',
		path: '/view_orders/:id',
		component: View_Orders
	},
	{
		name: '/view_categories',
		path: '/view_categories',
		component: View_Categories
	},
	{
		name: '/add_category',
		path: '/add_category',
		component: Add_Categories
	},
	{
		name: '/edit_category',
		path: '/edit_category/:id',
		component: Edit_Categories
	},
	{
		name: '/edit_newest_expansion',
		path: '/edit_newest_expansion',
		component: Edit_Newest_Expansion
	},
	{
		name: '/edit_banner',
		path: '/edit_banner',
		component: Edit_Banner
	},
	{
		name: '/view_coupons',
		path: '/view_coupons',
		component: View_Coupons
	},
	{
		name: '/add_coupons',
		path: '/add_coupons',
		component: Add_Coupons
	},
	{
		name: '/edit_coupon',
		path: '/edit_coupon/:id',
		component: Edit_Coupons
	},
	{
		name: '/view_loyalty',
		path: '/view_loyalty',
		component: View_Loyalty
	},
	{
		name: '/add_products',
		path: '/add_products',
		component: Add_Products
	},
	{
		name: '/edit_loyalty',
		path: '/edit_loyalty/:id',
		component: Edit_Loyalty
	},
	{
		name: '/view_feedbacks',
		path: '/view_feedbacks',
		component: View_Feedbacks
	},
	{
		name: '/edit_product',
		path: '/edit_product/:id',
		component: Edit_Products
	},
	{
		name: '/view_products',
		path: '/view_products',
		component: View_Products
	},
	{
		name: '/view_reports',
		path: '/view_reports',
		component: View_Reports
	},
	{
		name: '/view_audit',
		path: '/view_audit',
		component: View_Audit
	},
	{
		name: '/list_pages',
		path: '/list_pages',
		component: List_Pages
	},
	{
		name: '/add_pages',
		path: '/add_pages',
		component: Add_Pages
	},
	{
		name: '/edit_page',
		path: '/edit_page/:id',
		component: Edit_Pages
	}
	/*,
	{
		name: '/add_work_history',
		path: '/add_work_history',
		component: Add_Work_History
	},
	{
		name: '/view_work_history',
		path: '/view_work_history',
		component: View_Work_History
	},
	{
		name: '/edit_work_history',
		path: '/edit_work_history/:id',
		component: Edit_Work_History,
	},
	{
		name: '/add_prof_summary',
		path: '/add_prof_summary',
		component: Add_Prof_Summary
	},
	{
		name: '/view_prof_summary',
		path: '/view_prof_summary',
		component: View_Prof_Summary
	},
	{
		name: '/edit_prof_summary',
		path: '/edit_prof_summary/:id',
		component: Edit_Prof_Summary,
	}*/
];
const router = new VueRouter({
	routes,
	linkExactActiveClass: "active" // active class for *exact* links.,
	//,mode: 'history'
});


new Vue({
    el: '#app',
    router,
    render: app => app(Admin_Dashboard)
});