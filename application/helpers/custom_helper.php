<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// ------------------------------------------------------------------------


if ( ! function_exists('encrypt')) {

    function encrypt($string) {
        $output = FALSE;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
        $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';

        // hash
        $key = hash('sha256', $secret_key);
        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }
}

if ( ! function_exists('admin_folder')) {
    function admin_folder() {
        return base_url('admin/');
    }
}

if ( ! function_exists('decrypt')) {

    function decrypt($string) {
        $output = FALSE;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
        $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';
        // hash
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }
}

function get_language($key,$language="english") {
  $value = $key;
  $ci =& get_instance();
  $ci->lang->load('global',$language);
  $key = strtolower(str_replace(" ", "_", $key));

  if($ci->lang->line($key))
  return $ci->lang->line($key);

  return $value;
}

function in_arrayi($needle, $haystack) {
    return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

if ( ! function_exists('clean_data')) {
  function clean_data($string) {
      return trim(strip_tags(stripslashes($string)));
  }
}

if ( ! function_exists('generateRandomString')) {
  function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
}


if ( ! function_exists('is_logged')) {
  function is_logged() {
      $ci =& get_instance();
      $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : '';
      $cookie_pass = !empty($_COOKIE['ses_pass']) ? $_COOKIE['ses_pass'] : '';
      $ci->load->model('User_model');
      if($ci->session->ses_key != "") {
        $email = decrypt($ci->session->ses_key);
        $filter = ["email"=>$email];
        $status = $ci->User_model->check_exist('users',$filter);
        if($status) 
         return 1;
        else 
          return 0;
      }else {
        if($cookie_key != "" && $cookie_pass != "") {
          $email = decrypt($cookie_key);
          $password = decrypt($cookie_pass);
          $filter = ["email"=>$email];
          $row = $ci->User_model->fetch_tag_row('password','users',$filter);
          if(empty($row)) {
            return 0; 
          }else {
            $user_password = $row->password;
            if(password_verify($password,$user_password)) {
              return 1; 
            } else {
              return 0;
            }
          }
        } else {
          return 0;
        }
      }

  }
}

if ( ! function_exists('is_logged_admin')) {
  function is_logged_admin() {
      $ci =& get_instance();
      $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : '';
      $cookie_pass = !empty($_COOKIE['ses_pass']) ? $_COOKIE['ses_pass'] : '';
      $ci->load->model('User_model');
      if($ci->session->ses_key != "") {
        $email = decrypt($ci->session->ses_key);
        $filter = ["email"=>$email,"role"=>"admin"];
        $status = $ci->User_model->check_exist('users',$filter);
        if($status) 
         return 1;
        else 
          return 0;
      }else {
        if($cookie_key != "" && $cookie_pass != "") {
          $email = decrypt($cookie_key);
          $password = decrypt($cookie_pass);
          $filter = ["email"=>$email,"role"=>"admin"];
          $row = $ci->User_model->fetch_tag_row('password','users',$filter);
          if(empty($row)) {
            return 0; 
          }else {
            $user_password = $row->password;
            if(password_verify($password,$user_password)) {
              return 1; 
            } else {
              return 0;
            }
          }
        } else {
          return 0;
        }
      }

  }
}


if ( ! function_exists('get_email')) {
  function get_email() { 
    $ci =& get_instance();
    $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : $ci->session->ses_key;
    $email = decrypt($cookie_key);
    return $email;
  }
}

if ( ! function_exists('get_username')) {
  function get_username() { 
    $ci =& get_instance();
    $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : $ci->session->ses_key;
    $username = strstr(decrypt($cookie_key), '@', true);;
    return $username;
  }
}



if ( ! function_exists('hash_password')) {
  function hash_password($password) {
    $options = array(
        'cost' => 11,
    );
    return password_hash($password, PASSWORD_BCRYPT, $options);
  }

}

if ( ! function_exists('check_privilege')) {
  function check_privilege($page_privilege,$privilege) {
    $privilege_array = explode(',',$privilege);
    $exists = in_array($page_privilege,$privilege_array);
    return $exists;
  }

}

if ( ! function_exists('sendMail')) {
  function sendMail($heading='',$content='',$from='',$receiver='')//lagay nalang parameter ilalagay natin sa helper para global
  {
    require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "ssl://smtp.googlemail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "jonasdulay28@gmail.com";  // user email address
        $mail->Password   = "";               // password in GMail
        $mail->SetFrom($receiver, 'Mail');  //Who is sending 
        $mail->isHTML(true);
        $mail->Subject    = "PABILE - ".$heading;
        $mail->Body      = '
            <html>
            <head>
                <title>'.$heading.'</title>
            </head>
            <body>
            <h3>'.$heading.'</h3>
            <br>
            '.$content.'
            <br>
            <p>With Regards</p>
            <p>'.$from.'</p>
            </body>
            </html>
        ';
        $mail->AddAddress($receiver, $receiver);
        if(!$mail->Send()) {
            return 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return 'success';
        }
  }
}

if ( ! function_exists('sendMail_file')) {
  function sendMail_file($heading='',$content='',$from='',$receiver='',$path)//lagay nalang parameter ilalagay natin sa helper para global
  {
    require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer();
    $mail->addAttachment($path,$name = '', $encoding = 'base64', $type = 'application/octet-stream');
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "ssl://smtp.googlemail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "jonasdulay28@gmail.com";  // user email address
        $mail->Password   = "Watheshet288";            // password in GMail
        $mail->SetFrom($receiver, 'Mail');  //Who is sending 
        $mail->isHTML(true);
        $mail->Subject    = $heading." - PACUCOA";
        $mail->Body      = '
            <html>
            <head>
                <title>Title</title>
            </head>
            <body>
            <h3>'.$heading.'</h3>
            <br>
            '.$content.'
            <br>
            <p>With Regards</p>
            <p>'.$from.'</p>
            </body>
            </html>
        ';
        $mail->AddAddress($receiver, $receiver);
        if(!$mail->Send()) {
            return false;
        } else {
            return true;
        }
  }
}

if (!function_exists('audit')){
    function audit($action,$description,$type,$additional_info){

        $ci =& get_instance();
         $email = get_email();
        $ci->load->library('user_agent');
        $agent = "Unidentified User Agent";
        if ($ci->agent->is_browser())
        {
          $agent = $ci->agent->browser().' '.$ci->agent->version();
        }
        elseif ($ci->agent->is_robot())
        {
          $agent = $this->agent->robot();
        }
        elseif ($ci->agent->is_mobile())
        {
          $agent = $ci->agent->mobile();
        }

        $platform =  $ci->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)

        $ci->load->model('Audit_model');
        $mode = flag('mode','live');

        if($mode = "test"){
          $data = array("action"=>$action,"description"=>$description,"type"=>$type,"additional_info"=>$additional_info,
            "email"=>$email,"user_agent"=>$agent,"platform"=>$platform,"ip_address"=>$ci->input->ip_address(),"mode"=>"test");   
        }

        $data = array("action"=>$action,"description"=>$description,"type"=>$type,"additional_info"=>$additional_info,
            "email"=>$email,"user_agent"=>$agent,"platform"=>$platform,"ip_address"=>$ci->input->ip_address(),"mode"=>"live");  
        $ci->Audit_model->insert('tbl_audit',$data);
      
      return true;
    }
}

if (!function_exists('audit_purrcoins')){
    function audit_purrcoins($action,$description,$type,$additional_info,$email){

        $ci =& get_instance();
        $ci->load->library('user_agent');
        $agent = "Unidentified User Agent";
        if ($ci->agent->is_browser())
        {
          $agent = $ci->agent->browser().' '.$ci->agent->version();
        }
        elseif ($ci->agent->is_robot())
        {
          $agent = $this->agent->robot();
        }
        elseif ($ci->agent->is_mobile())
        {
          $agent = $ci->agent->mobile();
        }

        $platform =  $ci->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)

        $ci->load->model('Audit_model');
        $mode = flag('mode','live');
        
        if($mode = "test"){
          $data = array("action"=>$action,"description"=>$description,"type"=>$type,"additional_info"=>$additional_info,
            "email"=>$email,"user_agent"=>$agent,"platform"=>$platform,"ip_address"=>$ci->input->ip_address(),"mode"=>"test");   
        }
        
        $data = array("action"=>$action,"description"=>$description,"type"=>$type,"additional_info"=>$additional_info,
            "email"=>$email,"user_agent"=>$agent,"platform"=>$platform,"ip_address"=>$ci->input->ip_address(),"mode"=>"live");  
        $ci->Audit_model->insert('tbl_audit',$data);
      
      return true;
    }
}

if ( ! function_exists('sendMailCIMailer')) {
    function sendMailCIMailer($email_content)
    {
        $ci =& get_instance();
        $ci->load->library('email');
        $_config = array(
            'protocol' => 'ssl',
            'smtp_host' => 'smtp.office365.com',
            'smtp_user' => 'hello@alg.tw',
            'smtp_pass' => 'algtw123!',
            'smtp_port' => 587,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'smtp_crypto' => "ssl",
            'wordwrap' => TRUE
        );
        $ci->email->initialize($_config);
        $ci->email->from($email_content['from'], $email_content['from_name']);
        $ci->email->to($email_content['to']);
        $ci->email->subject($email_content['subject']);
        $ci->email->cc('jonasdulay28@gmail.com');
        $ci->email->bcc('jonasdulay28@gmail.com');
        $ci->email->message($email_content['message']);
        if($ci->email->send()){
            return true;
        }else{
             var_dump($ci->email->print_debugger()); 
             die;
            return false;               
        }
    }
}

if ( ! function_exists('sendMailCIMailer2')) {
    function sendMailCIMailer2($email_content)
    {
        $ci =& get_instance();
        $ci->load->library('email');
        $_config = array(
            'protocol' => 'tls',
            'smtp_host' => 'outlook.office365.com',
            'smtp_user' => 'hello@alg.tw',
            'smtp_pass' => 'algtw123!',
            'smtp_port' => 993,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'smtp_crypto' => "tls",
            'wordwrap' => TRUE
        );
        $ci->email->initialize($_config);
        $ci->email->from($email_content['from'], $email_content['from_name']);
        $ci->email->to($email_content['to']);
        $ci->email->subject($email_content['subject']);
        if(isset($email_content['reply_to'])){
          $ci->email->reply_to($email_content['reply_to']);
        }
        $ci->email->cc('jonasdulay28@gmail.com');
        $ci->email->bcc('jonasdulay28@gmail.com');
        $ci->email->message($email_content['message']);
        if($ci->email->send()){
            return true;
        }else{
             var_dump($ci->email->print_debugger()); 
             die;
            return false;               
        }
    }
}


if (!function_exists('check_status')){
  function check_status($status = 0) {
    if($status == '0') {
      return 'Pending';
    } else if($status == '1') {
      return 'Processing';
    } else if($status == '2') {
      return 'On Hold';
    } else if($status == '3') {
      return 'Completed';
    } else if ($status == '4') {
      return 'Cancelled';
    } else if ($status == '5') {
      return 'Refunded';
    }
    
    return 'Failed';
  }
}

if (!function_exists('flag')){
     function flag($flag, $defaultValue){
            $flag_value = !empty($_GET[$flag]) ? $_GET[$flag] : (!empty($_COOKIE[$flag]) ? $_COOKIE[$flag] : $defaultValue);
            setcookie($flag, $flag_value, time() + (86400 * 30));
        return $flag_value;
     }
}

if ( ! function_exists('styles_bundle')) {
    function styles_bundle($style) {
        return base_url('assets/css/'.$style);
    }
}

if ( ! function_exists('images_bundle')) {
    function images_bundle($image) {
        return base_url('assets/images/'.$image);
    }
}

if ( ! function_exists('scripts_bundle')) {
    function scripts_bundle($script) {
        return base_url('assets/js/'.$script);
    }
}

if ( ! function_exists('material_scripts')) {
    function material_scripts() {
        return base_url('material/assets/js/');
    }
}
if ( ! function_exists('material_styles')) {
    function material_styles() {
        return base_url('material/assets/css/');
    }
}

if ( ! function_exists('third_party_bundle')) {
    function third_party_bundle() {
        return base_url('assets/third_party/');
    }
}

if ( ! function_exists('pad')) {
    function pad($num, $size) {
      $s = $num."";
      while (strlen($s) < $size) $s = "0" . $s;
      return $s;
    }
}

if ( ! function_exists('getCardImage')) {
    function getCardImage($card, $data_type = 'array') {
      if($data_type == 'array'){
        if($card['rarity'] == 'T') {
          $broken_img_src = base_url() . "assets/images/no-image-slide.jpg";
          $img_src = $card['set_code'] && $card['collector_number'] && $card['regular_price'] != ""  ? 'https://www.alg.tw/images/Tokens/' . $card['set_code'] . '/' .  str_pad($card['collector_number'], 3, "0", STR_PAD_LEFT) . '.jpg' : ($card['set_code'] && $card['collector_number'] && $card['foil_price'] != "" ? 'https://www.alg.tw/images/Tokens/' . $card['set_code'] . '/' .  str_pad($card['collector_number'], 3, "0", STR_PAD_LEFT) . '.jpg' : $broken_img_src);
          $img_src = $card['image'] ? $card['image'] : $img_src;
          return $img_src;
        }

        $broken_img_src = base_url() . "assets/images/no-image-slide.jpg";
        $img_src = $card['set_code'] && $card['collector_number'] && $card['regular_price'] != ""  ? 'https://www.alg.tw/images/regular_cards/' . $card['set_code'] . '/' .  str_pad($card['collector_number'], 3, "0", STR_PAD_LEFT) . '.jpg' : ($card['set_code'] && $card['collector_number'] && $card['foil_price'] != "" ? 'https://www.alg.tw/images/regular_cards/' . $card['set_code'] . '/' .  str_pad($card['collector_number'], 3, "0", STR_PAD_LEFT) . '.jpg' : $broken_img_src);
        $img_src = $card['image'] ? $card['image'] : $img_src;
        return $img_src;
      }

      if($card->rarity == 'T') {
        $broken_img_src = base_url() . "assets/images/no-image-slide.jpg";
        $img_src = $card->set_code && $card->collector_number && $card->regular_price != '' ? 'https://www.alg.tw/images/Tokens/' . $card->set_code . '/' . pad($card->collector_number, 3) . '.jpg' 
        : ($card->set_code && $card->collector_number && $card->foil_price != '' ? 'https://www.alg.tw/images/Tokens/' . $card->set_code . '/' . pad($card->collector_number, 3) . '.jpg' :$broken_img_src);
        $img_src = isset($card->image) ? $card->image ? $card->image : $img_src : $img_src;
        return $img_src;
      }

      $broken_img_src = base_url() . "assets/images/no-image-slide.jpg";
      $img_src = $card->set_code && $card->collector_number && $card->regular_price != '' ? 'https://www.alg.tw/images/regular_cards/' . $card->set_code . '/' . pad($card->collector_number, 3) . '.jpg' 
      : ($card->set_code && $card->collector_number && $card->foil_price != '' ? 'https://www.alg.tw/images/regular_cards/' . $card->set_code . '/' . pad($card->collector_number, 3) . '.jpg' :$broken_img_src);
      $img_src = isset($card->image) ? $card->image ? $card->image : $img_src : $img_src;
      return $img_src;
    }
}

if ( ! function_exists('getCardName')) {
    function getCardName($card, $data_type = 'array') {
      $card_name = '';
      if($data_type == 'array'){
        if(!$card['regular_price']) {
          $card['name'] = str_replace("[FOIL] ","",$card['name']);
          $card['name'] = '[FOIL] ' . str_replace(" - Foil","",$card['name']);
        }

        if($card['language'] == 'eng' || $card['language'] == 'en' || $card['language'] == 'tw') {
          if($card['language'] == 'eng') $card['language'] = 'en';
          if($card['rarity'] == 'T'){
            $card_type = explode(' ? ', $card['type']);
            if(in_array('Basic Land', $card_type)){
              $card_name = $card['name'] . ' (' . $card['collector_number'] . ') ('. $card['set_code'] .')';
            } else {
              $card_name = $card['name'];
            }
          } else{
            $card_type = explode(' ? ', $card['type']);
            if(in_array('Basic Land', $card_type)){
              $card_name = $card['name'] . ' (' . $card['collector_number'] . ') ('. $card['set_code'] .')';
            } else {
              $card_name = $card['name'];
            }
          }
          $name_tw = isset($card['name_tw']) ? $card['name_tw'] ? $card['name_tw'] : '' : '';
          $card_language = isset($card['language']) ? $card['language'] ? '[' . strtoupper($card['language']) . '] ' : '' : '';
          return '<p>' . '<span>' . $card_language . '</span>' . $card_name .' '. $name_tw. '</p>' ;
        } else if($card['language'] == 'jp') {
          if($card['rarity'] == 'T'){
            $card_type = explode(' ? ', $card['type']);
            if(in_array('Basic Land', $card_type)){
              $card_name = $card['name'] . ' (' . $card['collector_number'] . ') ('. $card['set_code'] .')';
            } else {
              $card_name = $card['name'];
            }
          } else{
            $card_type = explode(' ? ', $card['type']);
            if(in_array('Basic Land', $card_type)){
              $card_name = $card['name'] . ' (' . $card['collector_number'] . ') ('. $card['set_code'] .')';
            } else {
              $card_name = $card['name'];
            }
          }
          $name_jp = isset($card['name_jp']) ? $card['name_jp'] ? $card['name_jp'] : '' : '';
          $card_language = isset($card['language']) ? $card['language'] ? '[' . strtoupper($card['language']) . '] ' : '' : '';
          return '<p>' . '<span>' . $card_language . '</span>' . $card_name .' '. $name_jp. '</p>' ;
        } else if($card['language'] == 'ko') {
          if($card['rarity'] == 'T'){
            $card_type = explode(' ? ', $card['type']);
            if(in_array('Basic Land', $card_type)){
              $card_name = $card['name'] . ' (' . $card['collector_number'] . ') ('. $card['set_code'] .')';
            } else {
              $card_name = $card['name'];
            }
          } else{
            $card_type = explode(' ? ', $card['type']);
            if(in_array('Basic Land', $card_type)){
              $card_name = $card['name'] . ' (' . $card['collector_number'] . ') ('. $card['set_code'] .')';
            } else {
              $card_name = $card['name'];
            }
          }
          $name_ko = isset($card['name_ko']) ? $card['name_ko'] ? $card['name_ko'] : '' : '';
          $card_language = isset($card['language']) ? $card['language'] ? '[' . strtoupper($card['language']) . '] ' : '' : '';
          return '<p>' . '<span>' . $card_language . '</span>' . $card_name .' '. $name_ko. '</p>' ;
        }
        return '';
      }

      if(!$card->regular_price) {
        $card->name = str_replace("[FOIL] ","",$card->name);
        $card->name = '[FOIL] ' . str_replace(" - Foil","",$card->name);
      }

      if($card->language == 'eng' || $card->language == 'en' || $card->language == 'tw') {
        if($card->language == 'eng') $card->language = 'en';
        if($card->rarity == 'T'){
          $card_type = explode(' ? ', $card->type);
          if(in_array('Basic Land', $card_type)){
            $card_name = $card->name . ' (' . $card->collector_number . ') ('. $card->set_code .')';
          } else {
            $card_name = $card->name;
          }
        } else{
          $card_type = explode(' ? ', $card->type);
          if(in_array('Basic Land', $card_type)){
            $card_name = $card->name . ' (' . $card->collector_number . ') ('. $card->set_code .')';
          } else {
            $card_name = $card->name;
          }
        }
        $name_tw = isset($card->name_tw) ? $card->name_tw ? $card->name_tw : '' : '';
        $card_language = isset($card->language) ? $card->language ? '[' . strtoupper($card->language) . '] ' : '' : '';
        return '<p>' . '<span>' . $card_language . '</span>' . $card_name .'</p><p>' . $name_tw. '</p>';
      } else if($card->language == 'jp') {
        if($card->rarity == 'T'){
          $card_type = explode(' ? ', $card->type);
          if(in_array('Basic Land', $card_type)){
            $card_name = $card->name . ' (' . $card->collector_number . ') ('. $card->set_code .')';
          } else {
            $card_name = $card->name;
          }
        } else{
          $card_type = explode(' ? ', $card->type);
          if(in_array('Basic Land', $card_type)){
            $card_name = $card->name . ' (' . $card->collector_number . ') ('. $card->set_code .')';
          } else {
            $card_name = $card->name;
          }
        }
        $name_jp = isset($card->name_jp) ? $card->name_jp ? $card->name_jp : '' : '';
        $card_language = isset($card->language) ? $card->language ? '[' . strtoupper($card->language) . '] ' : '' : '';
        return '<p>' . '<span>' . $card_language . '</span>' . $card_name .'</p><p>' . $name_jp. '</p>';
      } else if($card->language == 'ko') {
        if($card->rarity == 'T'){
          $card_type = explode(' ? ', $card->type);
          if(in_array('Basic Land', $card_type)){
            $card_name = $card->name . ' (' . $card->collector_number . ') ('. $card->set_code .')';
          } else {
            $card_name = $card->name;
          }
        } else{
          $card_type = explode(' ? ', $card->type);
          if(in_array('Basic Land', $card_type)){
            $card_name = $card->name . ' (' . $card->collector_number . ') ('. $card->set_code .')';
          } else {
            $card_name = $card->name;
          }
        }
        $name_ko = isset($card->name_ko) ? $card->name_ko ? $card->name_ko : '' : '';
        $card_language = isset($card->language) ? $card->language ? '[' . strtoupper($card->language) . '] ' : '' : '';
        return '<p>' . '<span>' . $card_language . '</span>' . $card_name .'</p><p>' . $name_ko. '</p>';
      }
      return '';
    }
}

if ( ! function_exists('getRarity')) {
    function getRarity($rarity = '') {
      if($rarity == '') return;

      $rarity = strtoupper(substr($rarity, 0, 1));
      switch ($rarity) {
        case 'U':
          return 'Uncommon';
          break;
        case 'C':
          return 'Common';
          break;
        case 'R':
          return 'Rare';
          break;
        case 'M':
          return 'Mythic';
          break;
        case 'T':
          return 'Others';
          break;
        default:
          return '';
      }
    }
}

if (!function_exists('get_badge')){
     function get_badge($badge){
      $badge = strtolower($badge);
      return images_bundle('badges/'.$badge.'.png');
     }
}


if ( ! function_exists('post')) {
    function post($name) {
        $controller = & get_instance();
        return $controller->input->post($name);
    }
}

if ( ! function_exists('get')) {
    function get($name) {
        $controller = & get_instance();
        return $controller->input->get($name);
    }
}

if ( ! function_exists('truncate_string')) {
    function truncate_string($str, $length) {
      if(strlen($str) > $length) {
        return substr($str, 0, $length) . '..';
      }

      return $str;
    }
}

if ( ! function_exists('cardTag')) {
    function cardTag($card, $data_type = 'array') {
      $tag = '';
      if($data_type == 'array') {
        if($card['tag']) {
          $tag = $card['tag'] == 'custom' ? $card['custom_tag'] : $card['tag'];
          $tag = '<label class="label card_tag card_img_tag" style="color: '. $card['tag_color'] .'; background: '. $card['tag_bg_color'] .'">' . $tag . '</label>';
        }
      } else {
        if($card->tag) {
          $tag = $card->tag == 'custom' ? $card->custom_tag : $card->tag;
          $tag = '<label class="label card_tag card_img_tag" style="color: '. $card->tag_color .'; background: '. $card->tag_bg_color .'">' . $tag . '</label>';
        }
      }
      return $tag;
    }
}
        

class ManaReplacer
{
  var $text;

  var $pairs = array(
    'wp'  => 'wp',
    'up'  => 'up',
    'bp'  => 'bp',
    'rp'  => 'rp',
    'gp'  => 'gp',

    '2w' => '2w',
    '2u' => '2u',
    '2b' => '2b',
    '2r' => '2r',
    '2g' => '2g',

    'wu' => 'wu',
    'ub' => 'ub',
    'br' => 'br',
    'rg' => 'rg',
    'gw' => 'gw',

    'uw' => 'wu',
    'bu' => 'ub',
    'rb' => 'br',
    'gr' => 'rg',
    'wg' => 'gw',

    'wb' => 'wb',
    'bg' => 'bg',
    'gu' => 'gu',
    'ur' => 'ur',
    'rw' => 'rw',

    'bw' => 'wb',
    'gb' => 'bg',
    'ug' => 'gu',
    'ru' => 'ur',
    'wr' => 'rw',

    '20'  => '20',
    '19'  => '19',
    '18'  => '18',
    '17'  => '17',
    '16'  => '16',
    '15'  => '15',
    '14'  => '14',
    '13'  => '13',
    '12'  => '12',
    '11'  => '11',
    '10'  => '10',
    '9'   => '9',
    '8'   => '8',
    '7'   => '7',
    '6'   => '6',
    '5'   => '5',
    '4'   => '4',
    '3'   => '3',
    '2'   => '2',
    '1'   => '1',
    '0'   => '0',
    'x'   => 'x',
    'y'   => 'y',

    'w'   => 'w',
    'u'   => 'u',
    'b'   => 'b',
    'r'   => 'r',
    'g'   => 'g',

    'q'   => 'q',
    't'   => 't',
    's'   => 's',
  );

  function __construct($text)
  {
    $this->text = $text;
    foreach($this->pairs as $key => $value) {
      $this->pairs[$key] = '<i class="mana-'.$value.'"></i>';
    }
  }

  function to_symbols($input) {
    return strtr(strtolower($input[1]), $this->pairs);
  }

  function replace() {
    return preg_replace_callback(
      '/\{([^\}]+)\}/',
      array($this, 'to_symbols'),
      $this->text
    );
  }
}


function mana_replace($text = '')
{
  $replacer = new ManaReplacer($text);
  return $replacer->replace();
}

if (!function_exists('getCookie')) {
  function getCookie($name = '')
  {
    return isset($_GET[$name]) ? $_GET[$name] : (isset($_COOKIE[$name]) ? $_COOKIE[$name] : '');
  }
}

/*if ( ! function_exists('get_level_name')) {
  function get_level_name($level) {
    $this->load->model('Crud_model');
    $filter = array('level_id'=>$level);
    $row = $this->Crud_model->fetch_tag_row('level_name','tbllevel',$filter);
    return $row->level_name;
  }

}*/


if(!function_exists('updateCartList')) {
  function updateCartList($cart_list = array()){
    $ci =& get_instance();    
    $ci->load->model("Global_model");
    if(get_email()){
      $email = get_email();
      $tmp_cart = $ci->Global_model->fetch('tmp_cart', array('email' => $email));
      if(empty($tmp_cart)) {
        $ci->Global_model->insert('tmp_cart', array('email' => $email, 'cart_list'=> json_encode($cart_list)));
        $tmp_cart = array();
      } else {
        $ci->Global_model->update('tmp_cart', array('cart_list'=> json_encode($cart_list)), array('email' => $email));
      }
    } else {
      if(!$ci->session->userdata('cart_session_key')) {
        $ctr = 30;
        $session_key = generateRandomString($ctr);
        while($ci->Global_model->fetch('tmp_cart', array('session_key' => $session_key))) {
          $ctr++;
          $session_key = generateRandomString($ctr);
        }

        $ci->session->set_userdata(array('cart_session_key' => $session_key));
        $ci->Global_model->insert('tmp_cart', array('session_key' => $session_key, 'cart_list'=> json_encode($cart_list)));
        $tmp_cart = array();
      } else{
        $session_key = $ci->session->userdata('cart_session_key');
        
        $ci->Global_model->update('tmp_cart', array('cart_list'=> json_encode($cart_list)), array('session_key' => $session_key));
      }
    } 
  }
}

if(!function_exists('getUpdatedCartList')) {
  function getUpdatedCartList(){
    $ci =& get_instance();
    $ci->load->model("Global_model");
    if(get_email()){
      $email = get_email();
      $tmp_cart = $ci->Global_model->fetch('tmp_cart', array('email' => $email));
      if(empty($tmp_cart)) {
        $ci->Global_model->insert('tmp_cart', array('email' => $email, 'cart_list'=> '[]'));
        $tmp_cart = array();
      } else {
        $tmp_cart = json_decode($tmp_cart[0]->cart_list, TRUE);
      }
    } else {
      if(!$ci->session->userdata('cart_session_key')) {
        $ctr = 30;
        $session_key = generateRandomString($ctr);
        while($ci->Global_model->fetch('tmp_cart', array('session_key' => $session_key))) {
          $ctr++;
          $session_key = generateRandomString($ctr);
        }

        $ci->session->set_userdata(array('cart_session_key' => $session_key));
        $ci->Global_model->insert('tmp_cart', array('session_key' => $session_key, 'cart_list'=> '[]'));
        $tmp_cart = array();
      } else{
        $session_key = $ci->session->userdata('cart_session_key');
        $tmp_cart = $ci->Global_model->fetch('tmp_cart', array('session_key' => $session_key));

        if(empty($tmp_cart)){
          $ci->Global_model->insert('tmp_cart', array('session_key' => $session_key, 'cart_list'=> '[]'));
          $tmp_cart = array();
        } else{
          $tmp_cart = json_decode($tmp_cart[0]->cart_list, TRUE);
        }
      }
    }

    if(!empty($tmp_cart)){
      foreach($tmp_cart as $i => $cart){

        $display_name = 'a.name as name, a.ability as ability, a.fully_handled as fully_handled, a.type as type, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw, a.category as category';
        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';
        $language = isset($cart['language']) ? $cart['language'] : '';
        // switch ($language) {
        //   case 'tw':
        //     $display_name = 'b.name_tw as name, b.ability_tw as ability, b.type_tw as type, b.flavor_tw as flavor';
        //     break;
        //   default:
        //     break;
        // }

        $prod_tag = 'a.id as id, a.name as img_src, a.set_code as set_code, a.fully_handled as fully_handled, a.collector_number as collector_number, a.rarity as rarity, a.color as color, a.set as `set`, a.set as set_name, a.manacost as manacost, a.additional_img as additional_img, a.category as category, a.artist as artist, a.regular_price as regular_price, a.foil_price as foil_price, image as image, a.stock as stock, a.base_price as base_price, a.datestamp, a.promo, a.language as language, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color, ' . $display_name;
        $ci->load->model("Product_model");
        $card = $ci->Product_model->get_sort_tag($prod_tag, 'WHERE a.id = ' . $i);
        if(!empty($card)) {
          $card = $card[0];
          $img_src = getCardImage($card, 'json');
          $card->img_src = $card->image ? $card->image : $img_src;
          $variation = $card->regular_price ? 'Regular' : 'Foil';
          $card->price = $variation == 'Regular' ? $card->regular_price : $card->foil_price;

          $tmp_cart[$i]['card'] = $card;
          $tmp_cart[$i]['variation'] = $variation;
        }
      }
    }
    return $tmp_cart;
  }
}

if(!function_exists('getDiscountedPrice')) {
  function getDiscountedPrice($fully_handled, $price){
    $fully_handled = strtoupper($fully_handled);
    $discount = 0;

    if($fully_handled == 'SP') {
      $discount = 0.8;
    } else if($fully_handled == 'MP') {
      $discount = 0.7;
    } else if($fully_handled == 'HP') {
      $discount = 0.6;
    } else {
      $discount = 1;
    }

    $price = floatval($price) * $discount;
    $price = round($price, 2);

    if(!is_numeric( $price ) && floor( $price ) != $price) {
      $price = round($price);
    }

    return $price;
  }

}

if(!function_exists('getCardLanguageTag')) {
  function getCardLanguageTag($language){
    $language = strtolower($language);
    $disp = '';

    if($language == 'tw') {
      $disp = '<img src="' . base_url() . 'assets/images/tag/tw_tag.jpeg' . '" class="card_tag_language">';
    } else if($language == 'eng') {
      $disp = '<img src="' . base_url() . 'assets/images/tag/eng_tag.jpeg' . '" class="card_tag_language">';
    } else if($language == 'jp') {
      $disp = '<img src="' . base_url() . 'assets/images/tag/jp_tag.jpeg' . '" class="card_tag_language">';
    }

    return $disp;
  }

}

if(!function_exists('getCardLabel')) {
  function getCardLabel($tag, $language){
    $ci =& get_instance();
    $tag = strtolower($tag);
    $language = strtolower($language);
    $lang = 'en';
    $disp = '';

    $label = array('', 'best_seller', 'great_value', 'limited_time', 'new_product', 'preorder', 'promotion', 'recommended', 'sale', 'selling_fast', 'special_order');
    $i = array_search($tag, $label);

    if($ci->session->current_language == 'traditional_chinese') $lang = 'tw';

    if($i && $lang != '') {
      $disp = '<img src="' . base_url() . 'assets/images/labels/' . 'label-' . $lang . '-' . pad($i, 2) . '.png' . '" class="card_label" >';
    }

    return $disp;
  }

}

?>