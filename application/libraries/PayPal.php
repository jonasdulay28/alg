<?php
require_once 'vendor/autoload.php';

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payee;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;

use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

use PayPal\Api\Agreement;
use PayPal\Api\ShippingAddress;

use PayPal\Api\Sale;

use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;

use PayPal\Api\AgreementStateDescriptor;

class PayPal{
    public $apiContext;
    public $PaymentCancelURL;
    public $PaymentReturnURL;

    public $AgreementCancelURL;
    public $AgreementReturnURL;

    public $response = [
        'success' => TRUE,
        'status' => [
			'message' => 'All is well :)'
		],
		'debug' => [],
		'data' => [],
		'approvalUrl' => ''
	];

	public function __construct() {
		$baseUrl = base_url();

		$this->PaymentCancelURL = $baseUrl.'Functions/executePayment/failed/';
		$this->PaymentReturnURL = $baseUrl.'Functions/executePayment/success/';

		$this->AgreementCancelURL = $baseUrl.'Functions/executeAgreement/failed/';
		$this->AgreementReturnURL = $baseUrl.'Functions/executeAgreement/success/';

	}

	public function createCharge($data=null, $mode='Live'){
		// ### Payer
		// A resource representing a Payer that funds a payment
		// For paypal account payments, set payment method
		// to 'paypal'.
		$this->connect($mode);

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		// ### Itemized information
		// (Optional) Lets you specify item wise
		// information
		$item1 = new Item();
		$item1->setName($data['Plan'])
		    ->setCurrency($data['Currency'])
		    ->setQuantity(1)
		    ->setSku($data['PlanID']) // Similar to `item_number` in Classic API
		    ->setPrice($data['Price']);

		$itemList = new ItemList();
		$itemList->setItems(array($item1));

		// ### Amount
		// Lets you specify a payment amount.
		// You can also specify additional details
		// such as shipping, tax.
		$amount = new Amount();
		$amount->setCurrency($data['Currency'])
		    ->setTotal($data['Price']);

			// ### Transaction
		// A transaction defines the contract of a
		// payment - what is the payment for and who
		// is fulfilling it.
		$transaction = new Transaction();
		$transaction->setAmount($amount)
		    ->setItemList($itemList)
		    ->setDescription($data['Plan'])
		    ->setInvoiceNumber(uniqid());

		// ### Redirect urls
		// Set the urls that the buyer must be redirected to after
		// payment approval/ cancellation.
		//$baseUrl = getBaseUrl();
		$baseUrl = base_url();
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($this->PaymentReturnURL."?PlanID=" . $data['PlanID'])
		    ->setCancelUrl($this->PaymentCancelURL);

		// ### Payment
		// A Payment Resource; create one using
		// the above types and intent set to 'sale'
		$payment = new Payment();
		$payment->setIntent("sale")
		    ->setPayer($payer)
		    ->setRedirectUrls($redirectUrls)
		    ->setTransactions(array($transaction));

		try {
			$payment->create($this->apiContext);
			$approvalUrl = $payment->getApprovalLink();
			$this->response['approvalUrl'] = $approvalUrl;
		} catch (Exception $e) {
            $this->handleError($e);
		}

		return $this->response;
	}

	public function refundSale($param=NULL, $mode='Live'){

		$this->connect($mode);

        if(is_null($param)) return;
        try {
        $payment = Payment::get($param['paymentId'], $this->apiContext);

				$transactions = $payment->getTransactions();
				$resources = $transactions[0]->getRelatedResources();
				$sale = $resources[0]->getSale();
				$saleID = $sale->getId();
				
        $amt = new Amount();
        $amt->setCurrency(strtoupper($param['currency']))
            ->setTotal($param['total']);
        $refundRequest = new RefundRequest();
        $refundRequest->setAmount($amt);
        $sale = new Sale();
        $sale->setId($saleID);
            $refundedSale = $sale->refundSale($refundRequest, $this->apiContext);
        } catch (Exception $ex) {
            print_r($ex);
            return 'Error';
        }
        return $refundedSale;
	}

	public function ExecutePayment($param=NULL, $mode='Live'){
		$this->connect($mode);

    if(is_null($param)) return;
    
    $payment = Payment::get($param['paymentId'], $this->apiContext);

    $execution = new PaymentExecution();
    $execution->setPayerId($param['PayerID']);

    $transaction = new Transaction();
    $amount = new Amount();

    $amount->setCurrency(strtoupper($param['planCurrency']));
    $amount->setTotal($param['planAmount']);
    
    $transaction->setAmount($amount);
    $execution->addTransaction($transaction);
    
    try{
      $result = $payment->execute($execution, $this->apiContext);
      
      try{
        $payment = Payment::get($param['paymentId'], $this->apiContext);

				$transactions = $payment->getTransactions();
				$resources = $transactions[0]->getRelatedResources();
				$sale = $resources[0]->getSale();
				$saleID = $sale->getId();
				$FirstName = $payment->getPayer()->getPayerInfo()->getFirstName();	
				$LastName = $payment->getPayer()->getPayerInfo()->getLastName();	
				$TotalAmount = $payment->transactions[0]->amount->total;

				$result_data = array(
					'ChargeID' => $saleID,
					'PayerID' => $_GET['PayerID'],
					'FirstName' => $FirstName,
					'LastName' => $LastName,
					'TotalAmount' => $TotalAmount
				);

				$this->response['data'] = $result_data;
      }catch(Exception $e){
    		echo  'PayPal Error: '.$e->getMessage();
    		exit;
      }
    }catch(Exception $e){
    	echo  'PayPal Error: '.$e->getMessage();
    	exit;
    }
		return $this->response;
  }

	private function handleError($e, $msg = 'err')
  {
		$_error = $e->getMessage();
		$this->response['success'] = FALSE;
		$this->response['status'] = [
			'message' => ($_error) ? $_error : $msg
		];
		$this->response['debug']['code'] = $e->getCode();
		$this->response['debug']['desc'] = $e->getMessage();
	}

	private function connect($mode = 'live')
  {
		if (strtolower($mode) == 'test') {
			$clientId = paypal_clientId_test;
			$clientSecret = paypal_clientSecret_test;
			$PayPalMode = 'sandbox';
		} else {
			$clientId = paypal_clientId_live;
			$clientSecret = paypal_clientSecret_live;
			$PayPalMode = 'live';
		}

    $this->apiContext = new ApiContext(
        new OAuthTokenCredential(
             $clientId
            ,$clientSecret
        )
    );
    $this->apiContext->setConfig(
        array(
            'mode' => $PayPalMode
        )
    );        
	}


}





?>