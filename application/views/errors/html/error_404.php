<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet"> 
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}

</style>
<style type="text/css">
	.container-img img {
		margin: auto;
		width: 30%;
	}
	.text-container {
		margin: auto; 
		width: 80%;
		font-family: 'Poppins';
		line-height: 35px;
		font-size: 30px;
		font-weight: bold;
		text-align: left;
	}
	@media (max-width:1100px) {
		.container-img img {
			width: 50%;
		}
	}
	@media (max-width:700px) {
		.container-img img {
			width: 65%;
		}
		.text-container {
			width: auto;
			font-size: 24px;
		}
	}
</style>
</head>
<body>
	<div style="text-align: center">
		<div class="container-img">
			<img src="<?php echo base_url() ?>assets/images/logo8.jpg">
		</div>
		<div  class="text-container">
			<div style="text-align: center;">
				<h1>404. Page not found.</h1>
			</div>
			<p>Oops! Nong Maew, our magical in-house cat, can't seem to find what you were looking for. No worries, though! How about going back to the main page just to get you back on track? Purr!</p>
			<div style="text-align: center;">
				<button class="btn-home" onclick="window.location.href=window.location.origin" style="border: 1px solid #00b8f9;background: #05a9e2; color: white; padding: 10px 25px; margin: auto; cursor: pointer;">Go Back</button>
			</div>
		</div>
	</div>
</body>
</html>