<nav class="navbar top_header">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

      <a class="navbar-brand welcome_title" href=""><?php echo get_language('header_welcome', $this->session->current_language); ?></a>
      <a class="navbar-brand mobile_welcome_title" href=""><?php echo get_language('header_welcome2', $this->session->current_language); ?></a>
      <div class="mobile_change_language_container">
        <span class="lang_currency_mobile"><a href="javascript:void(0);" data-toggle="modal" data-target="#change_currency_modal"><i class="glyphicon glyphicon-globe"> </i>
            <?php echo get_language('lang_currency', $this->session->current_language); ?></a></span>
      </div>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#change_currency_modal"><i class="glyphicon glyphicon-globe"> </i> <?php echo get_language('lang_currency', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('our_store') ?>"><?php echo get_language('About ALG', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('contact') ?>"><?php echo get_language('Contact Us', $this->session->current_language); ?></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- Static navbar -->
<nav class="navbar navbar-default mobile_navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url() ?>" style="min-height: 50px;height: auto"><img src="<?php echo images_bundle('logo9.png') ?>" alt="ALG logo" title="ALG logo" class="img-responsive" style="height: 40px;"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li>
          <form class="navbar-form  search_form" role="search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="<?php echo get_language('Search for...', $this->session->current_language) ?>">
              <div class="auto-complete-container">
                <ul></ul>
              </div>
              <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div><!-- /input-group -->
          </form>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo get_language('Account',$this->session->current_language);?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url('my_account') ?>"><?php echo get_language('My Account', $this->session->current_language); ?></a></li>
            <li><a href="<?php echo base_url('cart') ?>"><?php echo get_language('Shopping Cart', $this->session->current_language); ?></a></li>
            <li><a href="<?php echo base_url('watchlist') ?>"><?php echo get_language('Wishlist', $this->session->current_language); ?></a></li>
          </ul>
        </li>
        <li role="separator" class="divider"></li>
        <li><a href="#" data-toggle="modal" data-target="#myModal"><?php echo get_language('All Singles', $this->session->current_language); ?> <i class="fa fa-star" style="color:yellow;"></i></a></li>
        <!-- <li><a href="<?php echo base_url('sealed') ?>">Sealed</a></li>
        <li><a href="#">Card Lots</a></li>
        <li><a href="#">Accessories</a></li>
        <li><a href="#">Buylist</a></li>
        <li><a href="#">Articles</a></li> -->
        <li><a href="<?php echo base_url('allsets') ?>"><?php echo get_language('All sets', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('accessories') ?>"><?php echo get_language('Accessories', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('accessories?type=sealed products') ?>"><?php echo get_language('Sealed Products', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('loyalty_program') ?>"><?php echo get_language('Loyalty Program', $this->session->current_language); ?></a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo get_language('Info', $this->session->current_language); ?>
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url('our_store') ?>"><?php echo get_language('About ALG', $this->session->current_language); ?></a></li>
            <!-- 
            <li><a href="<?php echo base_url('faq') ?>">F.A.Q</a></li> -->
            <li><a href="<?php echo base_url('visit_us') ?>"><?php echo get_language('Find our Store', $this->session->current_language); ?></a></li>
            <li><a href="<?php echo base_url('shipping') ?>"><?php echo get_language('Shipping', $this->session->current_language); ?></a></li>
            <li><a href="<?php echo base_url('contact') ?>"><?php echo get_language('Contact Us', $this->session->current_language); ?></a></li>
          </ul>
        </li>
        <?php if (isset($data['pages'])) : ?>
          <?php if (!empty($data['pages'])) : ?>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo get_language('Blog', $this->session->current_language); ?>
                <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <?php foreach ($data['pages'] as $page) : ?>
                  <?php $slug = 'pages/' . $page->slug; ?>
                  <li><a href="<?php echo base_url($slug) ?>"><?php echo $page->name ?></a></li>
                <?php endforeach; ?>
              </ul>
            </li>
          <?php endif; ?>
        <?php endif; ?>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
  <!--/.container-fluid -->
</nav>

<nav class="navbar main_header">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo images_bundle('logo9.png') ?>" alt="ALG logo" title="ALG logo" class="img-responsive"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="main_header_cta">

      <ul class="nav navbar-nav main_header_sub_cta pull-right">
        <li>
          <form class="navbar-form  search_form" role="search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="<?php echo get_language('Search for...', $this->session->current_language) ?>">
              <div class="auto-complete-container">
                <ul></ul>
              </div>
              <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div><!-- /input-group -->
          </form>
        </li>
        <li class="cta"><a href="<?php echo base_url('watchlist') ?>"><?php echo get_language('Wishlist', $this->session->current_language); ?> <i class="fa fa-heart"></i></a></li>
        <!-- <li class="cta"><a href="#">My Account <i class="fa fa-user"></i></a></li> -->
        <li class="cta"><a href="<?php echo base_url('my_account') ?>"><?php echo get_language('My Account', $this->session->current_language); ?> <i class="fa fa-user"></i></a></li>
        <li class="cta"><a href="<?php echo base_url('cart') ?>"><?php echo get_language('Shopping Cart', $this->session->current_language); ?> <i class="fa fa-shopping-cart"></i></a></li>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<nav class="navbar bottom_header">
  <div class="container">
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse categories_container" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#" data-toggle="modal" data-target="#myModal" class="clickable"><?php echo get_language('All Singles', $this->session->current_language); ?> <i class="fa fa-star" style="color:yellow;"></i></a></li>
        <!-- <li><a href="#">Sealed</a></li>
        <li><a href="#">Card Lots</a></li>
        <li><a href="#">Accessories</a></li>
        <li><a href="#">Buylist</a></li>
        <li><a href="#">Articles</a></li> -->
        <li><a href="<?php echo base_url('allsets') ?>"><?php echo get_language('All Sets', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('accessories') ?>"><?php echo get_language('Accessories', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('accessories?type=sealed products') ?>"><?php echo get_language('Sealed Products', $this->session->current_language); ?></a></li>
        <li><a href="<?php echo base_url('loyalty_program') ?>"><?php echo get_language('Loyalty Program', $this->session->current_language); ?></a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo get_language('Info', $this->session->current_language); ?>
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url('our_store') ?>"><?php echo get_language('About ALG', $this->session->current_language); ?></a></li>
            <!-- 
            <li><a href="<?php echo base_url('faq') ?>">F.A.Q</a></li> -->
            <li><a href="<?php echo base_url('visit_us') ?>"><?php echo get_language('Find our Store', $this->session->current_language); ?></a></li>
            <li><a href="<?php echo base_url('shipping') ?>"><?php echo get_language('Shipping', $this->session->current_language); ?></a></li>
            <li><a href="<?php echo base_url('contact') ?>"><?php echo get_language('Contact Us', $this->session->current_language); ?></a></li>
          </ul>
        </li>
        <?php if (isset($data['pages'])) : ?>
          <?php if (!empty($data['pages'])) : ?>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo get_language('Blog', $this->session->current_language); ?>
                <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <?php foreach ($data['pages'] as $page) : ?>
                  <?php $slug = 'pages/' . $page->slug; ?>
                  <li><a href="<?php echo base_url($slug) ?>"><?php echo $page->name ?></a></li>
                <?php endforeach; ?>
              </ul>
            </li>
          <?php endif; ?>
        <?php endif; ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo get_language('All Singles', $this->session->current_language); ?></h4>
      </div>
      <div class="modal-body">
        <div class="panel with-nav-tabs panel-default">
          <div class="panel-heading">
            <ul class="nav nav-tabs">
              <?php
              $x = 0;
              foreach ($data["categories"] as $key) { ?>
                <li class="<?php echo ($x == 0) ? 'active' : ''; ?>"><a href="#tab<?php echo $x + 1 ?>" class="<?php echo strtolower(str_replace(' ', '_', $key->main_category)) ?>_tab" data-toggle="tab"><?php echo $key->main_category ?></a></li>
              <?php
                $x++;
              }
              ?>
            </ul>
          </div>
          <div class="panel-body">
            <div class="tab-content">
              <?php
              $x = 0;
              foreach ($data["categories"] as $key) { ?>
                <div class="tab-pane fade in <?php echo ($x == 0) ? 'active' : ''; ?>" id="tab<?php echo $x + 1 ?>">
                  <div class="container-fluid">
                    <div class="row">
                      <?php
                      $sub_categories = json_decode($key->sub_category);
                      $categories_name = $this->session->current_language == "english" ? json_decode($key->sub_category) : json_decode($key->sub_category_chi);
                      $sub_categories_chi = json_decode($key->sub_category);
                      $sub_cat_code = json_decode($key->sub_category_code);
                      $sub_category_main_code = json_decode($key->sub_category_main_code);
                      $y = 0;
                      foreach ($categories_name as $item) {
                        $cat = isset($sub_cat_code[$y]) ? strtolower($sub_cat_code[$y]) : "";
                        $main_cat = isset($sub_category_main_code[$y]) ? strtolower($sub_category_main_code[$y]) : "";
                      ?>
                        <?php if ($cat != "") { ?>
                          <div class="col-md-4">
                            <p class="menu_sub_category"><a href="<?php echo base_url() ?>cards/?cat=<?php echo $main_cat; ?>">
                            <i class="ss ss-<?php echo (isset($sub_cat_code[$y]) ? strtolower($sub_cat_code[$y]) : ""); ?>"></i> <?php echo $item ? $item : $sub_categories[$y]; ?></a></p>
                          </div>
                        <?php } ?>
                      <?php $y++;
                      } ?>
                    </div>
                  </div>
                </div>
              <?php
                $x++;
              }
              ?>
            </div>
          </div>
        </div>
        <!-- <a href="<?php echo base_url('loyalty_program') ?>">
          <div class="header_banner">
            <div class="effect-sparkle bg-effect fill no-click"></div>
            <div class="overlay"></div>
          </div>
        </a> -->
      </div>
    </div>

  </div>
</div>

<div id="change_currency_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo get_language('Edit Language Currency', $this->session->current_language); ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <b><?php echo get_language('Interface Language', $this->session->current_language); ?></b>
          </div>
          <div class="col-md-8">
            <select class="form-control change_language" value="<?php echo $this->session->current_language ? $this->session->current_language : 'English'; ?>">
              <option value="english" <?php echo $this->session->current_language == 'english' ? 'selected' : ''; ?>>English</option>
              <option value="traditional_chinese" <?php echo $this->session->current_language == 'traditional_chinese' ? 'selected' : ''; ?>>Traditional Chinese</option>
            </select>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <b><?php echo get_language('Currency Reference', $this->session->current_language); ?></b>
            <img src="<?php echo images_bundle('currency.jpg') ?>" class="img-responsive hidden-sm hidden-xs">
          </div>
          <div class="col-md-8">
            <select class="form-control change_currency" value="<?php echo $this->session->current_language ?>">
              <option value="">New Taiwan Dollar</option>
              <?php
              $rates = json_decode($currencies[0]->rates);
              $currency_name = json_decode($currencies[0]->currency_name);
              $x = 0;
              foreach ($rates as $key => $value) {
                echo "<option value='" . $key . "'>" . $currency_name[$x] . "</option>";
                $x++;
              }
              ?>
            </select>
            <br>
            <p>* <?php echo get_language('currency_text', $this->session->current_language); ?></p>
            <p>* <?php echo get_language('currency_text2', $this->session->current_language); ?></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_language('Close', $this->session->current_language); ?></button>
      </div>
    </div>

  </div>
</div>