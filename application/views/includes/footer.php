<!-- Site footer -->
<footer class="site-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-5">
        <h6><?php echo get_language('About',$this->session->current_language);?></h6>
        <p class="text-justify"><?php echo get_language('footer_text',$this->session->current_language);?></p>
      </div>

      <div class="col-xs-6 col-md-offset-1 col-md-3">
        <h6><?php echo get_language('Hot Categories',$this->session->current_language);?></h6>
        <ul class="footer-links">
          <li><a href="<?php echo base_url()?>cards/?cat=m20"><?php echo get_language('Core Set 2020',$this->session->current_language) ?></a></li>
          <li><a href="<?php echo base_url()?>cards/?cat=mh1"><?php echo get_language('Modern Horizons',$this->session->current_language) ?></a></li>
          <li><a href="<?php echo base_url()?>cards/?cat=war"><?php echo get_language('War of the Spark',$this->session->current_language) ?></a></li>
          <li><a href="<?php echo base_url()?>cards/?cat=rna"><?php echo get_language('Ravnica Allegiance',$this->session->current_language) ?></a></li>
          <li><a href="<?php echo base_url()?>cards/?cat=uma"><?php echo get_language('Ultimate Masters',$this->session->current_language) ?></a></li>
          <li><a href="<?php echo base_url()?>cards/?cat=grn"><?php echo get_language('Guilds of Ravnica',$this->session->current_language) ?></a></li>
        </ul>
      </div>

      <div class="col-xs-6 col-md-3">
        <h6><?php echo get_language('Quick Links',$this->session->current_language);?></h6>
        <ul class="footer-links">
          <li><a href="<?php echo base_url('our_store')?>"><?php echo get_language('Our Store',$this->session->current_language);?></a></li>
          <li><a href="<?php echo base_url('privacy_policy')?>"><?php echo get_language('Privacy Policy',$this->session->current_language);?></a></li>
          <li><a href="<?php echo base_url('tos')?>"><?php echo get_language('Terms of Service',$this->session->current_language);?></a></li>
          <li><a href="<?php echo base_url('contact')?>"><?php echo get_language('Contact Us',$this->session->current_language);?></a></li>
        </ul>
      </div>
    </div>
    <hr>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-xs-12">
        <p class="copyright-text">Copyright &copy; <?php echo date('Y')?> All Rights Reserved by 
     <a href="<?php echo base_url()?>">ALG.tw</a>.
        </p>
      </div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <ul class="social-icons">
          <li><a class="facebook" href="https://www.facebook.com/ALGTW//"><i class="fab fa-facebook-f"></i></a></li>
          <li><a class="instagram" href="https://www.instagram.com/ALGTW/"><i class="fab fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
</footer>