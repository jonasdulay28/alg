<div class="loyalty_program">
	<div class="container">
		<div class="row">

			<table class="table table-striped text-center loyalty_table">
				<thead>
					<tr>
						<td>
							<div>Level Number</div>
						</td>
						<td colspan="2">
							<div>Name of Level</div>
						</td>
						<td>
							<div>Lifetime Spend</div>
							<div>(NT)</div>
						</td>
						<td>
							<div>Store Credit Rebate</div>
							<div>(Single Cards)</div>
						</td>
						<td>
							<div>Store Credit Rebate</div>
							<div>(All Other Products)</div>
						</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach($data['loyalty'] as $loyalty): ?>
					<tr>
						<td><?php echo $loyalty['level_number'] ?></td>
						<td>
							<img class="img-badge" src="<?php echo get_badge($loyalty['badge']) ?>" alt="ALG badge">
						</td>
						<td>
							<?php echo $this->session->current_language == 'traditional_chinese' && $loyalty['level_name_tw'] ? $loyalty['level_name_tw'] : $loyalty['level_name'] ?>
						</td>
						<td><?php echo $loyalty['lifetime_spend'] ?></td>
						<td><?php echo $loyalty['single_cards_rebate'] ?></td>
						<td><?php echo $loyalty['other_products_rebate'] ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<br>
	</div>
</div>