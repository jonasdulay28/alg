<div class="container shipping">
	<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >
	<?php echo get_language('Shipping Options',$this->session->current_language);?>
	</span><b></b></h3></div>
	<p class="text-center"><b><?php echo get_language('Singles & Accessories',$this->session->current_language);?></b></p>
	<p class="text-center"><b><?php echo get_language('(Taiwan & Outlying Islands)',$this->session->current_language);?></b></p>
	<div class=" shipping_details">
		<div class="row">
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 1</b></p>
				<p class="text-center"><?php echo get_language('Regular Local Mail',$this->session->current_language);?></p>
				<p class="text-center">35 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 1000 NT!',$this->session->current_language);?></p>
			</div>
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 2</b></p>
				<p class="text-center"><?php echo get_language('7-11 / Family Mart Pickup',$this->session->current_language);?></p>
				<p class="text-center">70 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 1500 NT!',$this->session->current_language);?></p>
			</div>
		</div>	
	</div>
	<hr>
	<p class="text-center"><b><?php echo get_language('Boosters & Accessories',$this->session->current_language);?></b></p>
	<p class="text-center"><b><?php echo get_language('(Taiwan & Outlying Islands)',$this->session->current_language);?></b></p>
	<div class=" shipping_details">
		<div class="row">
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 1</b></p>
				<p class="text-center"><?php echo get_language('Regular Domestic Mail',$this->session->current_language);?></p>
				<p class="text-center">70 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 1500 NT!',$this->session->current_language);?></p>
			</div>
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 2</b></p>
				<p class="text-center"><?php echo get_language('7-11 / Family Mart Pickup',$this->session->current_language);?></p>
				<p class="text-center">70 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 1500 NT!',$this->session->current_language);?></p>
			</div>
		</div>	
	</div>
	<hr>
	<p class="text-center"><b><?php echo get_language('Single Cards Only',$this->session->current_language);?></b></p>
	<p class="text-center"><b><?php echo get_language('(International Shipping)',$this->session->current_language);?></b></p>
	<div class=" shipping_details">
		<div class="row">
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 1</b></p>
				<p class="text-center">Registered International Mail</p>
				<p class="text-center">90 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 3000 NT!',$this->session->current_language);?></p>
			</div>
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 2</b></p>
				<p class="text-center">International Express Mail Service</p>
				<p class="text-center">600 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 12000 NT!',$this->session->current_language);?></p>
			</div>
			<div class="col-md-12">
				<p class="text-center"><b><?php echo get_language('Option',$this->session->current_language);?> 3</b></p>
				<p class="text-center">FedEx Priority Shipping</p>
				<p class="text-center">2100 NT</p>
				<p class="text-center"><?php echo get_language('Free shipping on orders over 45000 NT!',$this->session->current_language);?></p>
			</div>
		</div>	
	</div>
</div>