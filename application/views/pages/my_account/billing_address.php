<div class="billing_address">
	<?php echo form_open("#","method='POST' id='billing_address_form'") ?>
		<div class="row">
			<div class="col-md-12">
				<p><b>Billing address</b></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label>First name <span class="required">*</span></label>
				<input type="text" name="first_name" class="form-control first_name" required>
			</div>
			<div class="col-md-6">
				<label>Last name <span class="required">*</span></label>
				<input type="text" name="last_name" class="form-control last_name" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label>Country <span>*</span></label>
				<select class="form-control country" name="country" required>
					<option value="">Please select your country</option>
					<?php foreach($countries as $key) { ?>
						<option value="<?php echo $key->id ?>"><?php echo $key->name;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-6">
				<label>State <span>*</span></label>
				<select class="form-control state" name="state" disabled="" required>
					<option value="">Please select your state</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<label>Street address <span class="required">*</span></label>
				<input type="text" name="street_address" class="form-control street_address" required>
			</div>
			<div class="col-md-4">
				<label>Postal Code/Zip code <span>*</span></label>
				<input type="text" name="postal_zip" class="form-control postal_zip" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Town / City  <span>*</span></label>
				<input type="text" name="town_city" class="form-control town_city" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Phone <span>*</span></label>
				<input type="text" name="phone" class="form-control phone" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<button type="submit" class="btn btn-primary cta_view btn_billing_address">Save Billing Address</button>
			</div>
		</div>
	<?php echo form_close() ?>
</div>

<script type="text/javascript">
	$("#billing_address_form").on("submit",function(e){
		e.preventDefault();
		var post_url = "<?php echo base_url('my_account/update_billing_address')?>";
		$.ajax({
        type : 'POST',
        url : post_url,
        data: $(this).serialize(),
        dataType:"json",
        beforeSend:function(){
        	loading();
        },
        success : function(res){
        	close_loading();
        	notify2("Updated successfully","Default billing address updated","success");
        },
        error : function(res) {
          close_loading();
        	notify2("Error","something went wrong. Please try again","error");
          console.log(res);
        }
    });
	})

	$(".billing_address .country").on("change",function(){
		$(".billing_address .state").val("");
		set_state('.billing_address ');
	})

	function set_state(page) {
      $(page + ".state option").remove();
			$.ajax({
        type: "GET",
        url: base_url+"api/get_states",
        data:{'q':$(page + ".country option:selected").attr('value')},
        dataType: "json",
        success: function(data) {
        	var states = data.states;
        	var options = $(page + ".state");
        	options.find('option')
			    .remove()
			    .end()
			    //don't forget error handling!
			    $.each(states, function(index,item) {
			    	options.append($("<option />").val(item.id).attr('data-value', item.name).text(item.name));
			    });
			    $(page + ".state").prop("disabled",false);
        },
        error: function(err) {
            console.log(err);
        }
    	});
		}

	function set_user_billing_address(){
		$.ajax({
      type: "GET",
      url: base_url+"api/get_user_billing_address",
      data:{},
      dataType: "json",
      success: function(data) {
      	if(data.billing_address.length > 0) {
      		var tmp_billing_address = data.billing_address;
        	var billing_address = tmp_billing_address[0];
        	for(var i in billing_address){
        		if(i != "id" && i != "state"){
        			$(".billing_address ."+i).val(billing_address[i]);
        		}
					}

					set_state('.billing_address ');
					setTimeout(function(){
						$(".billing_address .state").val(billing_address['state']);
					},300)
					
      	}else {
      		set_country('.billing_address ');
    			set_state('.billing_address ');
      	}
      },
      error: function(err) {
          console.log(err);
      }
  	});
	}
</script>