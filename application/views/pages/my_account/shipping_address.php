<div class="shipping_address">
	<?php echo form_open("#","method='POST' id='shipping_address_form'") ?>
		<div class="row">
			<div class="col-md-12">
				<p><b>Shipping address</b></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label>First name <span class="required">*</span></label>
				<input type="text" name="first_name" class="form-control first_name" required>
			</div>
			<div class="col-md-6">
				<label>Last name <span class="required">*</span></label>
				<input type="text" name="last_name" class="form-control last_name" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label>Country <span>*</span></label>
				<select class="form-control country" name="country" required>
					<option value="">Please select your country</option>
					<?php foreach($countries as $key) { ?>
						<option value="<?php echo $key->id ?>"><?php echo $key->name;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-6">
				<label>State <span>*</span></label>
				<select class="form-control state" name="state" disabled="" required>
					<option value="">Please select your state</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<label>Street address <span class="required">*</span></label>
				<input type="text" name="street_address" class="form-control street_address" required>
			</div>
			<div class="col-md-4">
				<label>Postal Code/Zip code <span>*</span></label>
				<input type="text" name="postal_zip" class="form-control postal_zip" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Town / City  <span>*</span></label>
				<input type="text" name="town_city" class="form-control town_city" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Phone <span>*</span></label>
				<input type="text" name="phone" class="form-control phone" required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<button type="submit" class="btn btn-primary cta_view btn_shipping_address">Save Shipping Address</button>
			</div>
		</div>
	<?php echo form_close() ?>
</div>

<script type="text/javascript">
	$("#shipping_address_form").on("submit",function(e){
		e.preventDefault();
		var post_url = "<?php echo base_url('my_account/update_shipping_address')?>";
		$.ajax({
        type : 'POST',
        url : post_url,
        data: $(this).serialize(),
        dataType:"json",
        beforeSend:function(){
        	loading();
        },
        success : function(res){
        	close_loading();
        	notify2("Updated successfully","Default shipping address updated","success");
        },
        error : function(res) {
          close_loading();
        	notify2("Error","something went wrong. Please try again","error");
          console.log(res);
        }
    });
	})

	$(".shipping_address .country").on("change",function(){
		$(".shipping_address .state").val("");
		set_state('.shipping_address ');
	})

	function set_user_shipping_address(){
		$.ajax({
      type: "GET",
      url: base_url+"api/get_user_shipping_address",
      data:{},
      dataType: "json",
      success: function(data) {
      	if(data.shipping_address.length > 0) {
      		var tmp_shipping_address = data.shipping_address;
        	var shipping_address = tmp_shipping_address[0];
        	for(var i in shipping_address){
        		if(i != "id" && i != "state"){
        			$(".shipping_address ."+i).val(shipping_address[i]);
        		}
					}

					set_state('.shipping_address ');
					setTimeout(function(){
						$(".shipping_address .state").val(shipping_address['state']);
					},300)
					
      	}else {
      		set_country('.shipping_address ');
    			set_state('.shipping_address ');
      	}
      },
      error: function(err) {
          console.log(err);
      }
  	});
	}
</script>