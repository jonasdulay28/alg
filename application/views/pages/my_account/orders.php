<div class="orders">
	<div class="text_container">
    <div>
      <img src="<?php echo isset($badge) ? get_badge($badge) : get_badge('badge1')?>" class="img-responsive sabai_badge">
      <div class="label-exp-progress">
      	<label><?php echo $required_exp ?> Exp Required to level up</label>
      </div>
      <div class="progress exp-progress">
		   <div class="progress-bar progress-bar-striped" role="progressbar" style="width: <?php echo $current_exp ?>%;" aria-valuenow="<?php echo $current_exp ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $current_exp ?>%</div>
		  </div>
    </div>

    <h3>My Orders</h3>
    <p>You have <span class="label label-info" style="font-size: 20px;background:#340E5B;">
    	<?php echo $user_credit[0] ? $user_credit[0]->credits : '0' ?> <img src="<?php echo images_bundle('Purrcoins.png')?>" style="height: 20px;margin-left: 5px;"></span> </p>
  </div> 
  	<?php if($orders){ ?>
	<table id="tbl_orders" class="display table-responsive">
	  <thead>
	      <tr>
	          <th>ORDER</th>
	          <th>DATE</th>
	          <th>PAYMENT STATUS</th>
	          <th>STATUS</th>
	          <th>TOTAL</th>
	          <th>ACTIONS</th>
	      </tr>
	  </thead>
	  <tbody>
	  	<?php foreach($orders as $order){ ?>
	      <tr>
	          <td><a href="<?php echo base_url() . 'order/?order=' . $order->id ?>">#<?php echo $order->invoice_number ?></a></td>
	          <td><?php echo $order->created_at ?></td>
	          <td><?php echo $order->payment_mode ?></td>
	          <td><?php echo check_status($order->admin_status) ?></td>
	          <td><?php echo main_currency_sign ?> <?php echo $order->price ?></td>
	          <td><button class="btn btn-primary cta_view" onclick="redirect_order(<?php echo $order->id ?>)">VIEW</button></td>
	      </tr>
  		<?php } ?>
	  </tbody>
	</table>
	<?php } else { ?>
		<table id="tbl_orders" class="display table-responsive">
	  <thead>
	      <tr>
	          <th>ORDER</th>
	          <th>DATE</th>
	          <th>PAYMENT STATUS</th>
	          <th>STATUS</th>
	          <th>TOTAL</th>
	          <th>ACTIONS</th>
	      </tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
	<?php } ?>
</div>

<script type="text/javascript">
	function redirect_order(id) {
		window.location.href = "<?php echo base_url() . 'order/?order=' ?>" + id;
	}
</script>