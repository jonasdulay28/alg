<div class="account_details">
	<?php echo form_open("#","method='POST' id='account_details_form'") ?>
		<div class="row">
			<div class="col-md-12">
				<p><b>Account Details</b></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label>First name</label>
				<input type="text" name="first_name" class="form-control first_name">
			</div>
			<div class="col-md-6">
				<label>Last name</label>
				<input type="text" name="last_name" class="form-control last_name">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Email address <span>*</span></label>
				<input type="email" name="email" class="form-control" value="<?php echo get_email()?>" disabled required>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p><b>Password Change</b></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Current password (leave blank to leave unchanged)</label>
				<input type="password" name="password" class="form-control">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>New password (leave blank to leave unchanged)</label>
				<input type="password" name="new_password" class="form-control">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Confirm new password</label>
				<input type="password" name="cf_password" class="form-control">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<button type="submit" class="btn btn-primary cta_view" >Save Changes</button>
			</div>
		</div>
	<?php echo form_close() ?>
</div>

<script type="text/javascript">
	$("#account_details_form").on("submit",function(e){
		e.preventDefault();
		var cf_password = $("input[name='cf_password']").val();
		var new_password = $("input[name='new_password']").val();
		if(cf_password == new_password) {
			var post_url = "<?php echo base_url('my_account/update_account_info')?>";
			$.ajax({
	        type : 'POST',
	        url : post_url,
	        data: $(this).serialize(),
	        dataType:"json",
	        beforeSend:function(){
	        	loading();
	        },
	        success : function(res){
	        	close_loading();
	        	if(res.message == "success") {
	        		notify2("Updated successfully","Account information updated","success");
	        		$("input[name='cf_password']").val('');
	        		$("input[name='new_password']").val('');
	        		$("input[name='password']").val('');
	        	} else {
	        		notify2("Error","Incorrect password","error");
	        	}
	        },
	        error : function(res) {
	          close_loading();
	        	notify2("Error","something went wrong. Please try again","error");
	          console.log(res);
	        }
	    });
		} else {
			notify2("Error","Confirm password does not match","error");
		}
		
	})
	function set_user_information(){
		$.ajax({
      type: "GET",
      url: base_url+"api/get_user_information",
      data:{},
      dataType: "json",
      success: function(data) {
      	if(data.user_information.length > 0) {
      		var tmp_user_information = data.user_information;
        	var user_information = tmp_user_information[0];
        	console.log(user_information);
        	for(var i in user_information){
        		if(i == "first_name" || i == "last_name"){
        			$(".account_details ."+i).val(user_information[i]);
        		}
					}	
      	}
      },
      error: function(err) {
          console.log(err);
      }
  	});
	}
</script>