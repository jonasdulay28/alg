<div class="container tos">
	<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >
	TERMS OF USE

	</span><b></b></h3></div>
	<p class="text-center"><b>PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING THIS WEBSITE.</b></p>
	<div class=" tos_details">
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Access Upon Agreement</h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Access Upon Agreement</b><br>
					Your access to and use of ALG.tw is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use our services. By accessing or using ALG.tw, you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.
				</p>
			</div>
		</div>	
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Termination of Account</h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Termination of Account</b><br>
					We may terminate or suspend access to user account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Orders </span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Orders </b><br>
ALG.tw reserves the right to refuse any order placed on the Site. ALG.tw may, in its sole and absolute discretion, limit or cancel quantities of any order for any reason, especially to orders found to be hostile. In the event ALG.tw changes or cancels your order, we will attempt to notify you by sending you an e-mail provided at the time your order was made.
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Links to Other Websites</h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Links to Other Websites</b><br>
Our Service may contain links to third-party websites or services that are not owned or controlled by ALG.tw. ALG.tw has no control over and assumes no responsibility for, the content, privacy policies, or practices of any third party websites or services. You further acknowledge and agree that ALG.tw shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such websites or services. We strongly advise you to read the terms and conditions and privacy policies of any third-party websites or services that you visit.
				</p>
			</div>
		</div>	
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">
Governing Law</span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Unsubscribe From Newsletters</b><br>
					You can easily stop receiving our newsletters at any time. Simply click on the unsubscribe link at the bottom of any newsletter you’ve already received.
				</p>
			</div>
		</div>		
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Contact Us</span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Contact Us </b><br>
If you have any questions about these Terms, please contact us. We’ll be happy to answer any queries you might have! Have a great day ahead!
				</p>
			</div>
		</div>	
	</div>
</div>