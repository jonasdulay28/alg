<div class="container cart_page" style="min-height: 360px;">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><?php echo get_language('Home',$this->session->current_language)?></a></li>
	    <li class="breadcrumb-item active" aria-current="page"><?php echo get_language('Shopping Cart',$this->session->current_language)?></li>
	    <li class="breadcrumb-item"><a href="<?php echo base_url('checkout') ?>" class="checkout-cart"><?php echo get_language('Checkout',$this->session->current_language)?></a></li>
	  </ol>
	</nav>

	<div id="outOfStockModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-md">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4><?php echo get_language('Insufficient stocks for the products',$this->session->current_language)?>:</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-md-12">
					<table class="table">
						<thead>
							<th style="width: 200px"><?php echo get_language('Product',$this->session->current_language)?></th>
							<th><?php echo get_language('Set Name',$this->session->current_language)?></th>
							<th><?php echo get_language('Quantity',$this->session->current_language)?></th>
							<th><?php echo get_language('Available Stock',$this->session->current_language)?></th>
						</thead>
						<tbody>
						</tbody>
					</table>
	      		</div>
	      		<div class="col-md-12">
	      			<a href="javascript:void(0)" data-dismiss="modal"><?php echo get_language('Go back to cart summary',$this->session->current_language)?></a>
	      		</div>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row">
		<div class="col-md-7">
			<div class="cart-container">
			<?php $subtotal = 0; ?>
			<?php if($cart_list): ?>
				<table class="table cart_table">
					<thead>
						<th style="width: 150px;"><?php echo get_language('Product',$this->session->current_language)?></th>
						<th></th>
						<th><?php echo get_language('Price',$this->session->current_language)?></th>
						<th><?php echo get_language('Quantity',$this->session->current_language)?></th>
						<th><?php echo get_language('Total',$this->session->current_language)?></th>
					</thead>
					<tbody>
						<?php foreach($cart_list as $key => $card){ ?>
						<tr data-id="<?php echo $card['card']->id ?>">
							<?php $card_language_tag = $card['card']->category == 'single cards' ? getCardLanguageTag($card['card']->language) : '' ?>
							<?php $card_tag = $card['card']->category == 'single cards' ? getCardLabel($card['card']->tag, $card['card']->language) : '' ?>
							<?php $card_datestamp_tag = $card['card']->datestamp && $card['card']->promo == 'datestamp' ? '<label class="datestamp_tag">'.$card['card']->datestamp.'</label>' : ''; ?>
							<?php $card_planeswalker_tag = $card['card']->promo == 'planeswalker' ? '<img src="'.base_url().'assets/images/planeswalker.png" class="planeswalker_tag">' : ''; ?>
							<td><span class="remove_item" data-id="<?php echo $card['card']->id ?>">-</span> 
								<div class="image-container">
									<?php echo $card_language_tag . $card_tag . $card_datestamp_tag . $card_planeswalker_tag ?>
									<img src="<?php echo $card['card']->img_src ?>">
									<?php if($card['card']->foil_price): ?>
									<div class="after"></div>
									<?php endif; ?>
								</div> 
							</td>
							<td>
								<?php $card_language = isset($card['language']) ? '?lang=' . $card['language'] : ''; ?>
								<a href="<?php echo base_url("cards/view/") . $card['card']->id . $card_language ?>"><?php echo getCardName($card['card'], 'json'); ?></a>
								<div><?php echo str_replace("?","-",$card['card']->type); ?></div>
							</td>
							<td><p><?php echo main_currency_sign ?> <?php echo $card['variation'] == 'Foil' ? $card['card']->foil_price : $card['card']->regular_price ?></p></td>
							<td><button class="btn btn-default add_to_cart" data-id="<?php echo $card['card']->id ?>" data-target="card-quantity-content-<?php echo $card['card']->id ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true"><?php echo $card['quantity'] ?> in cart</button></td>
							<td><p><span class="card_total_amount"><?php echo main_currency_sign ?> <?php echo (int)$card['quantity'] * ($card['variation'] == 'Foil' ? (double)$card['card']->foil_price : (double)$card['card']->regular_price) ?></span></p></td>
						</tr>
						<div id="card-quantity-content-<?php echo $card['card']->id ?>" class="hide add-cart-wrapper">
							<div class="cart-quantity-content-container" data-container="card-quantity-content-<?php echo $card['card']->id ?>" data-index="<?php echo $key ?>" data-id="<?php echo $card['card']->id ?>" style="width: 150px; max-height: 180px; overflow: auto;">
								<div class="text-center">
									<span class="label-quantity">Select Qty</span> 
								</div>
								<?php 
									for($ctr = 0; $ctr <= $card['card']->stock; $ctr++){ 
										
								?>
									<button style="width: 23%; margin: 5px 3%; height: 35px;" data-value="<?php echo $ctr ?>"><?php echo $ctr ?></button>
								<?php } ?>
							</div>
						</div>
						<?php $subtotal += (int)$card['quantity'] * ($card['variation'] == 'Foil' ? (double)$card['card']->foil_price : (double)$card['card']->regular_price) ?>
						<?php } ?>
					</tbody>
				</table>
				<div class="order_summary_cta_container">
					<a href="<?php echo base_url('allsets')?>"><button class="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<?php echo get_language('Continue Shopping',$this->session->current_language)?></button></a>
					<button class="btn add_to_watchlist"><i class="fa fa-heart"></i>&nbsp;&nbsp;<?php echo get_language('Add all to Watchlist',$this->session->current_language)?></button>
				</div>
			<?php else: ?>
				<p class="text-center"> <a href="<?php echo base_url()?>"><?php echo get_language('Your cart is currently empty',$this->session->current_language)?></a></p>
			<?php endif; ?>
			</div>
		</div>

		<?php if($cart_list): ?>
		<div class="col-md-5 order_details" style="padding:10px 20px">

			<p class="order_details_title"><?php echo get_language('Order Summary',$this->session->current_language)?></p>
			<hr>
			<div>
				<div class="row">
					<div class="col-md-6">
						<div>
				      <!-- <input type="checkbox" id="use_purrcoins" name="question"> &nbsp;  --><span class="order_details_label">purrcoins: </span>
				      <span>( <?php echo isset($credits) ? $credits : 0 ?> <img src="<?php echo images_bundle('Purrcoins.png')?>" class="purrcoins_badge" alt="badge"> )</span>
				      <span style="color:red;font-size: 12px;">"*Whole numbers only."</span>
				    </div>
				    <div id="purrcoins">
				    	<form class="coupon_form" role="search">
							  <div class="input-group">
						      <input type="number" class="form-control" min="0" step="1" oninput="validity.valid||(value='');"  name="purrcoins" <?php echo isset($credits) ? $credits != 0 ? '' : 'readonly' : 'readonly' ?> placeholder="Enter amount" value="<?php echo $this->session->userdata('purrcoins') ? $this->session->userdata('purrcoins') : '' ?>">
						      <!-- <span class="input-group-btn">
						        <button class="btn btn-default" type="button"><i class="fa fa-check"></i></button>
						      </span> -->
						    </div><!-- /input-group -->
							</form>
				    </div>
						<div class="coupon_container">
							<p class="order_details_label"><?php echo get_language('Apply coupon',$this->session->current_language)?></p>
							<form class="coupon_form" role="search">
							  <div class="input-group">
						      <input type="text" class="form-control" name="coupon_code" placeholder="Enter code"  value="<?php echo $this->session->userdata('coupons') ? $this->session->userdata('coupons') : '' ?>">
						     <!--  <span class="input-group-btn">
						        <button class="btn btn-default" type="button"><i class="fa fa-check"></i></button>
						      </span> -->
						    </div><!-- /input-group -->
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<img src="<?php echo isset($badge) ? get_badge($badge) : get_badge('badge1')?>" class="order_badge" alt="ALG badge">
					</div>
				</div>
				<br>
				<div class="order_details_text">
					<p class="marginbottom0"><?php echo get_language('By continuing the purchase you will earn', $this->session->current_language) ?> ( <span class="rebate_points"><?php echo number_format(floatval($rebate),2) ?></span> <img src="<?php echo images_bundle('Purrcoins.png')?>" class="purrcoins_badge" alt="badge"> ).</p>
					<small><?php echo get_language('Purchase more to level up!', $this->session->current_language) ?> <a href="<?php echo base_url('loyalty_program')?>"><?php echo get_language('Learn more about our Loyalty Program', $this->session->current_language) ?></a></small>
					<h4><?php echo get_language('Subtotal',$this->session->current_language)?>: <span class="order_subtotal"><?php echo main_currency_sign ?> <?php echo $subtotal ?></span></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-primary tmp_btn_checkout"><?php echo get_language('Proceed to Checkout',$this->session->current_language)?> &nbsp;&nbsp; <i class="fa fa-arrow-right"></i></button><!-- 
					<button type="submit" class="btn btn-primary cta_view btn_checkout">Checkout</button> -->
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>

</div>

<script type="text/javascript">
	var deduction = parseFloat('<?php echo $deduction; ?>');
	var credits = '<?php echo isset($credits) ? $credits : 0 ?>';
	var single_card_rebate = parseFloat('<?php echo $single_card_rebate ?>');
	var other_card_rebate = parseFloat('<?php echo $other_card_rebate ?>');
	var rebate_points = parseFloat('<?php echo $rebate ?>').toFixed(2);

	$(document).on("click",".add_to_watchlist",function(e) {
		e.preventDefault();
		var post_url = "<?php echo base_url('cart/add_all_to_watchlist')?>"
			$.ajax({
	      type : 'POST',
	      url : post_url,
	      data: {"cart_data": cart_data},
	      dataType:"json",
	      beforeSend:function(){
	      },
	      success : function(res){
						notify2("Added to Watchlist","Watchlist updated successfully","success");
	      },
	      error : function(res) {
	           console.log(res);
	      }
  	});

	})
</script>