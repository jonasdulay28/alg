<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-139871344-1');
		</script>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title>ALG</title>
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
    <!-- Bootstrap library -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- Stylesheet file -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
</head>
<body>
    <br>
<div class="container panel panel-default">
	<div class="panel-body">
     <div class="row">
         <div class="col-md-10 col-md-offset-1">
            <br>
            <a href="<?php echo base_url('secure_admin')?>">Go back to Dashboard</a>
            <h2>Import CSV</h2>
            <p>Instruction below:</p>
            <!-- <?php if(!empty($success_msg)){ ?>
            <div class="col-xs-12">
                <div class="alert alert-success"><?php echo $success_msg; ?></div>
            </div>
            <?php } ?>
            <?php if(!empty($error_msg)){ ?>
            <div class="col-xs-12">
                <div class="alert alert-danger"><?php echo $error_msg; ?></div>
            </div>
            <?php } ?> -->
            
            <!-- <div class="row">
                <div class="col-md-12" id="importFrm" >
                    <?php echo form_open_multipart("products/import","method='POST'") ?>
                        <input type="file" name="file" />
                        <br>
                        <br>
                        <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
                    <?php echo form_close() ?>
                </div>
                
            </div> -->
            <ol>
                <li>Click and install <a href="<?php echo base_url('assets/heidisql.exe')?>" download>heidiSQL</a></li>
                <li>Download <a href="<?php echo base_url('assets/sample-csv.csv')?>" download>Sample CSV Template</a> and follow the format<Br>
                	Note: not all columns are required.
                </li>
                <li>Click the link below to watch the tutorial for the detailed instruction:<br>
                	<a href="https://www.dropbox.com/s/o4935w8rg9bpzz9/How%20to%20Import%20CSV%20File-ALG.mp4?dl=0" target="_blank">Import Cards Tutorial</a></li>
            </ol>
           <!--  <video width="100%" height="440" controls>
						  <source src="<?php echo base_url('assets/tutorial.mp4')?>" type="video/mp4">
						Your browser does not support the video tag.
						</video> -->
            </div>   
        </div>   
    </div>
</div>

<script>
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}
</script>
</body>
</html>