<div class="homepage">
	<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
		<!-- Overlay -->
		<div class="overlay"></div>

		<!-- Indicators -->
		<ol class="carousel-indicators">
		  <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
		  <li data-target="#bs-carousel" data-slide-to="1"></li>
		  <li data-target="#bs-carousel" data-slide-to="2"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
		  <div class="item slides active">
		    <div class="slide-1"></div>
		    <div class="effect-sparkle bg-effect fill no-click"></div>
		  </div><!-- 
		  <div class="item slides">
		    <div class="slide-2"></div>
		    <div class="effect-sparkle bg-effect fill no-click"></div>
		  </div>
		  <div class="item slides">
		    <div class="slide-3"></div>
		    <div class="effect-sparkle bg-effect fill no-click"></div>
		  </div> -->
		</div> 
	</div>
	<div class="container">
		<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >Newest Expansions</span><b></b></h3></div>
		<div class="row">
			<div class="col-lg-4 mt-3">
				<figure class="standard_container">
					<div class="bg_6">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=mh1">
							<button class="btn btn-default cta">Modern Horizons</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="standard_container">
					<div class="bg_1">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=war">
							<button class="btn btn-default cta">War of the Spark</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="standard_container">
					<div class="bg_2">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=rna">
							<button class="btn btn-default cta">Ravnica Allegiance</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 mt-3">
				<figure class="standard_container">
					<div class="bg_3">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=UMA">
							<button class="btn btn-default cta">Ultimate Masters</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="standard_container">
					<div class="bg_4">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=grn">
							<button class="btn btn-default cta">Guilds of Ravnica</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="standard_container">
					<div class="bg_5">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=m19">
							<button class="btn btn-default cta">Core Set 2019</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
		</div>
		<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >HOT CATEGORIES</span><b></b></h3></div>
		<div class="row">
			<div class="col-lg-4 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_1">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta standard">Standard</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_2">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta modern">Modern</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_3">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta legacy" >Legacy</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_4">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta commander">Commander</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_5">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta old_school" >Old School</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_6">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url()?>cards/?cat=waraa">
							<button class="btn btn-default cta bargain_bin">War of the Spark - Jap AA</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
		</div>
		<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >Top Products</span><b></b></h3></div>
		<section class="top_products">
			<?php $cart_data = getUpdatedCartList() ?>
			<?php if(!empty($top_card)) : foreach($top_card as $cards): ?>
			<?php 
				$img_src = getCardImage($cards, 'json');
			?>
			<div>
				<div class="card_container" >
					<img src="<?php echo $img_src ?>" class="img-responsive">
					<div class="card-details">
						<small class="set"><?php echo $cards->set ?></small>
						<?php
						$card_name = getCardName($cards, 'json');
						?>
						<p class="card_name"><a href="<?php echo base_url() . 'cards/view/' . $cards->id ?>"><?php echo $card_name ?></a></p>
						<p class="price"><?php echo main_currency_sign ?> <?php echo $cards->regular_price ? $cards->regular_price : $cards->foil_price ?></p>
						<?php if($cards->stock == 0): ?>
							<p style="color: red">Out of Stock</p>
						<?php else: ?>
							<?php if(isset($cart_data[$cards->id])): ?>
							<button class="btn btn-default add_to_cart" data-id="<?php echo $cards->id ?>" data-target="card-quantity-content-<?php echo $cards->id ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true"><?php echo $cart_data[$cards->id]['quantity'] ?> in Cart</button>
							<?php else: ?>
							<button class="btn btn-primary add_to_cart" data-id="<?php echo $cards->id ?>" data-target="card-quantity-content-<?php echo $cards->id ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true">Add to cart</button>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
				<div id="card-quantity-content-<?php echo $cards->id ?>" class="hide add-cart-wrapper">
					<div class="cart-quantity-content" data-container="card-quantity-content-<?php echo $cards->id ?>" data-id="<?php echo $cards->id ?>" style="width: 150px; min-height: 100px; overflow: auto;">
						<div class="text-center">
							<span class="label-quantity">Select Qty</span> 
						</div>
						<?php 
							for($ctr = 0; $ctr <= $cards->stock; $ctr++){ 
								if(($cards->rarity=='U' || $cards->rarity=='R' || $cards->rarity=='M') && $ctr > 8){
									break;
								} else if($ctr > 24) {
									break;
								}
						?>
							<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="<?php echo $ctr ?>"><?php echo $ctr ?></button>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php endforeach; else: echo "No products found."; endif;?>
		</section>

		<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >SABAI WITH US!</span><b></b></h3></div>
		<section class="sabai_with_us">
				<div><img src="http://scontent-lax3-1.cdninstagram.com/vp/a167f2c11eca045cadf416eed1a33867/5D3E0CF1/t51.2885-15/e35/s320x320/46393207_272965980035951_3727217902289199267_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" alt="Instagram Image" class="img-responsive"></div>
				<div><img src="http://scontent-lax3-1.cdninstagram.com/vp/d7dd747e1a787b40bc1bf8f58ea8ff3c/5D740826/t51.2885-15/e35/s320x320/46378400_2232834510083786_5304984210123664983_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" alt="Instagram Image" class="img-responsive"></div>
				<div><img src="http://scontent-lax3-1.cdninstagram.com/vp/815fc789ef99d0aee623e8bff911b209/5D7383F6/t51.2885-15/e35/s320x320/46748459_129955427909335_3768038457310224303_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
				<div><img src="http://scontent-lax3-1.cdninstagram.com/vp/d5aad4b3987e5c97b99a5347349211bb/5D779251/t51.2885-15/e35/s320x320/47139854_233695547547921_7514370449731279305_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
				<div><img src="http://scontent-lax3-1.cdninstagram.com/vp/0748618709c37c74c78ddab1b72cc3d2/5D39AF51/t51.2885-15/e35/s320x320/47692996_2261355017468565_9117993546293776886_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
				<div><img src="http://scontent-lax3-1.cdninstagram.com/vp/5f0d79ad04a0cc9613fcff81c0ae4e60/5D412C8C/t51.2885-15/e35/s320x320/46888901_275289609836800_4436095451195463735_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>

		    <div><img src="http://scontent-lax3-1.cdninstagram.com/vp/3e33e3edfedd9a18cce07268d9016263/5D41AA22/t51.2885-15/e35/s320x320/47424047_310290526248622_8194534973114336214_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
		    <div><img src="http://scontent-lax3-1.cdninstagram.com/vp/8ed15482a7ccaac159ab7a221d515d8e/5D58993A/t51.2885-15/e35/s320x320/47128211_663287624069483_2043749880870076702_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
		    <div><img src="http://scontent-lax3-1.cdninstagram.com/vp/f611e36d97c9431a422c567fa59db3c1/5D385233/t51.2885-15/e35/s320x320/47251926_1735625516543861_2305929391701821224_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
		    <div><img src="http://scontent-lax3-1.cdninstagram.com/vp/97299a6ce21178250616c1c68630430e/5D572A19/t51.2885-15/e35/s320x320/45496007_1967756140008661_5620960373532973274_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
		    <div><img src="http://scontent-lax3-1.cdninstagram.com/vp/43cf2069bbebd199f0afce74fede6561/5D5D8459/t51.2885-15/e35/c0.0.1079.1079a/s320x320/46979216_617301495368707_1556424945226925719_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
		    <div><img src="http://scontent-lax3-1.cdninstagram.com/vp/5704b71d9faf32988c028d57d613425e/5D70B5FE/t51.2885-15/e35/c0.0.1079.1079a/s320x320/46391037_565379473924587_7100358119678011679_n.jpg?_nc_ht=scontent-lax3-1.cdninstagram.com" class="img-responsive"></div>
		</section>
	</div>

</div>
