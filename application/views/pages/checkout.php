<div class="container checkout_page" style="min-height: 360px;">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><?php echo get_language('Home','english')?></a></li>
	    <li class="breadcrumb-item"><a href="<?php echo base_url() . 'cart' ?>"><?php echo get_language('Shopping Cart','english')?></a></li>
	    <li class="breadcrumb-item active" aria-current="page"><?php echo get_language('Checkout','english')?></li>
	  </ol>
	</nav>
	<!-- Trigger the modal with a button -->

<!-- Modal -->

<div id="outOfStockModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4><?php echo get_language('Insufficient stocks for the products','english')?>:</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-12">
				<table class="table">
					<thead>
						<th style="width: 200px"><?php echo get_language('Product','english')?></th>
						<th><?php echo get_language('Set Name','english')?></th>
						<th><?php echo get_language('Quantity','english')?>y</th>
						<th><?php echo get_language('Available Stock','english')?></th>
					</thead>
					<tbody>
					</tbody>
				</table>
      		</div>
      		<div class="col-md-12">
      			<a href="<?php echo base_url('cart') ?>"><?php echo get_language('Go back to cart summary','english')?></a>
      		</div>
      	</div>
      </div>
    </div>
  </div>
</div>

<div id="confirmationModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo get_language('Confirm Order','english')?></h4>
      </div>
      <div class="modal-body">
      	<div class="row confirmation_order">
      		<div class="col-md-6 billing_address">
      			<label><?php echo get_language('Billing Info','english')?></label>
      			<div class="form-group">
      				<label><?php echo get_language('First Name','english')?>:</label>
      				<span class="c_first_name"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Last Name','english')?>:</label>
      				<span class="c_last_name"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Country','english')?>:</label>
      				<span class="c_country"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('State','english')?>:</label>
      				<span class="c_state"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Street address','english')?>:</label>
      				<span class="c_street_address"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Town','english')?> / <?php echo get_language('City','english')?>:</label>
      				<span class="c_town_city"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Postal Code','english')?>/<?php echo get_language('Zip code','english')?>:</label>
      				<span class="c_postal_zip"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Phone','english')?>:</label>
      				<span class="c_phone"></span>
      			</div>
      		</div>
      		<div class="col-md-6 shipping_address">
      			<label><?php echo get_language('Shipping Info','english')?></label>
      			<div class="form-group">
      				<label><?php echo get_language('First Name','english')?>:</label>
      				<span class="c_first_name"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Last Name','english')?>:</label>
      				<span class="c_last_name"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Country','english')?>:</label>
      				<span class="c_country"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('State','english')?>:</label>
      				<span class="c_state"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Street address','english')?>:</label>
      				<span class="c_street_address"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Town','english')?> / <?php echo get_language('City','english')?>:</label>
      				<span class="c_town_city"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Postal Code','english')?>/<?php echo get_language('Zip code','english')?>:</label>
      				<span class="c_postal_zip"></span>
      			</div>
      			<div class="form-group">
      				<label><?php echo get_language('Phone','english')?>:</label>
      				<span class="c_phone"></span>
      			</div>
      		</div>
      	</div>
        <div class="row">
					<div class="col-md-12">
							<table class="table cart_table">
								<thead>
									<th><?php echo get_language('Product','english')?></th>
									<th><?php echo get_language('Set Name','english')?></th>
									<th><?php echo get_language('Price','english')?></th>
									<th><?php echo get_language('Quantity','english')?></th>
									<th><?php echo get_language('Total','english')?></th>
								</thead>
								<tbody>
								<?php foreach($cart_list as $key => $card){ ?>
									<tr>
										<td> 
											<?php echo getCardName($card['card'], 'json'); ?>
										</td>
										<td><?php echo $card['card']->set_name ?></td>
										<td><p><?php echo main_currency_sign ?> <?php echo isset($card['card']->price) ? $card['card']->price : $card['card']->regular_price ?></p></td>
										<td><?php echo $card['quantity'] ?></td>
										<td><p><?php echo main_currency_sign ?> <?php echo (isset($card['card']->price) ? $card['card']->price : $card['card']->regular_price) * $card['quantity'] ?></p></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
					</div>
					<div class="col-md-5">
						<h4><?php echo get_language('Payment Details','english')?></h4>
						<table class="table table-bordered">
							<tbody>
								<tr class="tr-shipping-fee">
									<td><?php echo get_language('Shipping Fee','english')?>:</td>
									<td><span class="shipping_fee_amount"><?php echo main_currency_sign ?> 0</span> </td>
								</tr>
								<tr>
									<td><?php echo get_language('Sub Total','english')?>:</td>
									<td><span class="sub_total_amount"><?php echo main_currency_sign ?> 0</span> </td>
								</tr>
								<tr>
									<td><?php echo get_language('Total Amount','english')?>:</td>
									<td><span class="total_amount"><?php echo main_currency_sign ?> 0</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_language('Cancel','english')?></button>
      	<button type="button" class="btn btn-primary hidden_checkout_btn" data-dismiss="modal"><?php echo get_language('Proceed','english')?></button>
      </div>
    </div>

  </div>
</div>
	<div class="row">
		<div class="col-md-7" style="padding-top:10px;">
			<div class="row">
				<div class="col-md-12">
					<p class="checkout_title"><?php echo get_language('Shipping Address','english')?></p> 
					<hr>
				</div>
			</div>
			
			<div class="row shipping_address" style="margin-top: 10px;">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<label><?php echo get_language('First name','english')?> <span class="required">*</span></label>
							<input type="text" name="first_name" class="form-control first_name" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->first_name : '' ?>" required>
						</div>
						<div class="col-md-6">
							<label><?php echo get_language('Last name','english')?> <span class="required">*</span></label>
							<input type="text" name="last_name" class="form-control last_name" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->last_name : '' ?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label><?php echo get_language('Country','english')?> <span class="required">*</span></label>
							<select class="form-control country" name="country" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->country : '' ?>" required>
								<option value="">Please select your country</option>
								<?php foreach($countries as $key) { ?>
									<option value="<?php echo $key->name ?>" data-id="<?php echo $key->id ?>"
										<?php 
										if($shipping_info):
											echo $shipping_info[0]->country == $key->name ? 'selected' : '';
										endif; 
										?>
										 data-value="<?php echo $key->name;?>"><?php echo $key->name;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6">
							<label><?php echo get_language('State','english')?> <span class="required">*</span></label>
							<select class="form-control state" name="state" <?php echo is_logged() > 0 && $shipping_info ? '' : 'disabled' ?> value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->state : '' ?>" required>
								<option value="">Please select your state</option>
								<?php if($shipping_state): ?>
								<?php foreach($shipping_state as $key) { ?>
									<option value="<?php echo $key->name ?>"  data-id="<?php echo $key->id ?>"
									 <?php 
									if($shipping_info):
										echo $shipping_info[0]->state == $key->name ? 'selected' : '';
									endif; 
									?>
									 data-value="<?php echo $key->name;?>"><?php echo $key->name;?></option>
								<?php } ?>
								<?php endif; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label><?php echo get_language('Street address','english')?> <span class="required">*</span></label>
							<input type="text" name="street_address" class="form-control street_address" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->street_address : '' ?>" required>
						</div>
						<div class="col-md-4">
							<label><?php echo get_language('Postal Code','english')?> / <?php echo get_language('Zip code','english')?> <span class="required">*</span></label>
							<input type="text" name="postal_zip" class="form-control postal_zip" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->postal_zip : '' ?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label><?php echo get_language('Town','english')?> / <?php echo get_language('City','english')?>  <span class="required">*</span></label>
							<input type="text" name="town_city" class="form-control town_city" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->town_city : '' ?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label><?php echo get_language('Phone','english')?> <span class="required">*</span></label>
							<input type="text" name="phone" class="form-control phone" value="<?php echo is_logged() > 0 && $shipping_info ? $shipping_info[0]->phone : '' ?>" required>
						</div>
					</div>
				</div>
			</div>
			<div style="margin: 10px 0px;">
				<p><input type="checkbox" checked name="" class="shipping_checkbox"><?php echo get_language('Same as Billing Address','english')?></p>
			</div>
			<div class="row billing_address" style="margin-top: 10px; display: none">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<p><b><?php echo get_language('Billing Address','english')?></b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label><?php echo get_language('First name','english')?> <span class="required">*</span></label>
							<input type="text" name="first_name" class="form-control first_name" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->first_name : '' ?>" required>
						</div>
						<div class="col-md-6">
							<label><?php echo get_language('Last name','english')?> <span class="required">*</span></label>
							<input type="text" name="last_name" class="form-control last_name" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->last_name : '' ?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label><?php echo get_language('Country','english')?> <span class="required">*</span></label>
							<select class="form-control country" name="country" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->country : '219' ?>" required>
								<option value="">Please select your country</option>
								<?php foreach($countries as $key) { ?>
									<option value="<?php echo $key->name ?>"  data-id="<?php echo $key->id ?>"
									<?php 
									/*if($billing_info[0]):
										echo $billing_info[0]->country == $key->id ? 'selected' : '';
									endif; */
									?>
									><?php echo $key->name;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6">
							<label><?php echo get_language('State','english')?> <span class="required">*</span></label>
							<select class="form-control state" name="state" <?php echo is_logged() > 0 && $billing_info ? '' : 'disabled' ?> value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->state : '' ?>" required>
								<?php if($billing_state): ?>
								<?php foreach($billing_state as $key) { ?>
									<option value="<?php echo $key->name ?>"  data-id="<?php echo $key->id ?>"
									<?php 
									if($billing_info[0]):
										echo $billing_info[0]->state == $key->name ? 'selected' : '';
									endif; 
									?>
									><?php echo $key->name;?></option>
								<?php } ?>
								<?php endif; ?>
								<option value="">Please select your state</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label><?php echo get_language('Street address','english')?> <span class="required">*</span></label>
							<input type="text" name="street_address" class="form-control street_address" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->street_address : '' ?>" required>
						</div>
						<div class="col-md-4">
							<label><?php echo get_language('Postal Code','english')?>/<?php echo get_language('Zip code','english')?> <span class="required">*</span></label>
							<input type="text" name="postal_zip" class="form-control postal_zip" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->postal_zip : '' ?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label><?php echo get_language('Town','english')?> / <?php echo get_language('City','english')?>  <span class="required">*</span></label>
							<input type="text" name="town_city" class="form-control town_city" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->town_city : '' ?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label><?php echo get_language('Phone','english')?> <span class="required">*</span></label>
							<input type="text" name="phone" class="form-control phone" value="<?php echo is_logged() > 0 && $billing_info ? $billing_info[0]->phone : '' ?>" required>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5" style="padding: 10px 20px;">
			<div class="row">
				<div class="col-md-12">
					<p class="checkout_title"><?php echo get_language('ORDER SUMMARY','english')?></p>
					<hr>
				</div>
			</div>
			<div>
				<div class="row">
					<div class="col-md-12">
						<p class="checkout_label">Purrcoins Used: <span class="purrcoin_used">N/A</span> <a href="<?php echo base_url('cart')?>">
							<small>(<?php echo get_language('Apply','english')?> purrcoins)</small></a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="checkout_label">Coupon used: <span class="coupon_used">N/A</span> <a href="<?php echo base_url('cart')?>"><small>(<?php echo get_language('Apply','english')?> coupons)</small></a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 checkout_type">
						<p class="checkout_title type_local"><?php echo get_language('Local Shipping','english')?>: <small></small></p>
						<p class="checkout_title type_int"><?php echo get_language('International Shipping','english')?>: <small></small></p>
						<p class="checkout_title type_free"><?php echo get_language('Shipping Fee','english')?>: <span style="color:#E29922;font-weight: bold;"><?php echo get_language('Free','english')?></span></p>
						<hr>
					</div>
				</div>
				<div class="row" style="display: none;">
					<div class="col-md-12 checkout_subtype">
						<div class="radio">
						  <label><input type="radio" name="optradio" value="Alpha Fast" checked>Aplha Fast <a href="<?php echo base_url('shipping')?>" target="_blank"><i class="fa fa-info-circle">	</i></a></label>
						</div>
						<div class="radio">
						  <label><input type="radio" name="optradio" value="Express Mail Service">Express Mail Service <a href="<?php echo base_url('shipping')?>" target="_blank"><i class="fa fa-info-circle">	</i></a></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="checkout_label"><?php echo get_language('Total Amount','english')?>: <span class="total_amount"><?php echo main_currency_sign ?> 0</span></p>
						<a href="<?php echo base_url('allsets')?>"><small><?php echo get_language('continue_shopping2','english')?></small></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="checkout_method"><?php echo get_language('Payment Method','english')?></p>
					<div class="local_checkout">
						<div>
							<input type="radio" name="payment_method" value="Regular Domestic Mail" checked="" class="payment_method" data-fee="<?php echo $shipping_fee[0] ?>" data-free="<?php echo $free_shipping_fee[0] ?>"> <b>Regular Domestic Mail</b>
						</div>
						<div>
							<input type="radio" name="payment_method" value="7-11 / Family Mart Pickup" class="payment_method" data-fee="<?php echo $shipping_fee[1] ?>" data-free="<?php echo $free_shipping_fee[1] ?>"> <b>7-11 / Family Mart Pickup</b>
						</div>
					</div>
					<div class="int_checkout" style="display: none">
						<div>
							<input type="radio" name="payment_method_int" value="Registered International Mail" checked="" class="payment_method" data-fee="<?php echo $int_shipping_fee[0] ?>" data-free="<?php echo $free_int_shipping_fee[0] ?>"> <b>Registered International Mail</b>
						</div>
						<div>
							<input type="radio" name="payment_method_int" value="International Express Mail Service" class="payment_method" data-fee="<?php echo $int_shipping_fee[1] ?>" data-free="<?php echo $free_int_shipping_fee[1] ?>"> <b>International Express Mail Service</b>
						</div>
						<div>
							<input type="radio" name="payment_method_int" value="FedEx Priority Shipping" class="payment_method" data-fee="<?php echo $int_shipping_fee[2] ?>" data-free="<?php echo $free_int_shipping_fee[2] ?>"> <b>FedEx Priority Shipping</b>
						</div>
					</div>
					<!-- <div>
						<input type="radio" name="payment_method" value="Paypal" checked="" class="payment_method"> <b>Paypal</b>
						<p class="payment_subtext paypal "><?php echo get_language('paypal_text','english')?></p>
					</div>
					<div>
						<input type="radio" name="payment_method" value="Bank Transfer" class="payment_method"> <b><?php echo get_language('Bank Transfer','english')?> (Kasikorn Bank)</b>
						<p class="payment_subtext bank_transfer none"><?php echo get_language('bank_transfer_text','english')?></p>
					</div>
					<div>
						<input type="radio" name="payment_method" value="Pickup" class="payment_method"> <b><?php echo get_language('InStore Pick Up','english')?></b>
						<p class="payment_subtext pickup none"></p>
					</div> -->
					<!-- <div>
						<input type="radio" name="payment_method" value="Paypal" checked="" class="payment_method"> <b>Paypal</b>
						<p class="payment_subtext paypal "><?php echo get_language('paypal_text','english')?></p>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="checkout_title"><?php echo get_language('Satisfaction Guaranteed','english')?></p>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="satisfaction_guarantee_text"><?php echo get_language('satisfaction_text','english')?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary btn_checkout">Proceed &nbsp;&nbsp; <i class="fa fa-arrow-right"></i></button>
					<p class="privacy_policy_text">Your personal data will be used to process your order, support your experience throughout this website, 
						and for other purposes described in our <a href="<?php echo base_url('privacy_policy')?>">privacy policy</a>.</p>
					<img src="<?php echo images_bundle('credit-cards-icon.png')?>" class="image-responsive center-block credit_card" alt="credit card">
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(".payment_method").on("change",function(e){
		var payment_method = $(this).val();
		$(".payment_subtext").hide();
		if(payment_method == "Paypal") {
			$(".paypal").show();
		} else if(payment_method == "Bank Transfer") {
			$(".bank_transfer").show();
		} else {
			$(".pickup").show();
		}

	});

	var single_only = '<?php echo $single_only ? "1" : ""?>';
</script>