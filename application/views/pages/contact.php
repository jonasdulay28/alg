<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.4001261469566!2d121.54047531535169!3d25.05442398396276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abe0ccb82889%3A0xe710b26caab79ea4!2s%E8%99%9F5%E6%A8%93%2C%20No.%20368%2C%20Changchun%20Rd%2C%20Zhongshan%20District%2C%20Taipei%20City%2C%20Taiwan%2010491!5e0!3m2!1sen!2sph!4v1576698423302!5m2!1sen!2sph" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1" style="border:1px solid #e1e1e1;min-height: 150px;margin-top:30px;margin-bottom:30px;border-radius: 5px;padding:20px;">
			<center><h3><?php echo get_language('Contact Us2',$this->session->current_language);?></h3>
				<p>ALG<br>
中山區長春路368號5樓<br>
Taipei, Taiwan 10491</p></center>
			<hr>
			<?php echo form_open("Contact_Us/send_message","method='post' id='send_inquiry'") ?>
			<div class="row">
				<div class="col-md-6">
					<input type="text" name="name" class="form-control" placeholder="<?php echo get_language('Full Name',$this->session->current_language);?>" required>
				</div>
				<div class="col-md-6">
					<input type="email" name="email" class="form-control" placeholder="<?php echo get_language('Email Address',$this->session->current_language);?>" required>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<textarea class="form-control" name="message" placeholder="<?php echo get_language('Enter your concerns here.',$this->session->current_language);?>" style="height: 100px;resize: none;" required></textarea>
				</div>
			</div>
			<br>
			<center><button class="btn btn-primary"><?php echo get_language('Send Message',$this->session->current_language);?></button></center>
			<?php echo form_close();?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#send_inquiry").on("submit",function(e){
		e.preventDefault();
		var baseurl = "<?php echo base_url();?>";
		var formData = $("#send_inquiry").serialize();
		loading();
		$.ajax( {
				type: 'POST',
				url: baseurl + 'contact/send_message',
				data: formData,
				dataType:"json",
				success: function (data) {
					var output = data;
					if (output.status == "success") {
						close_loading();
						swal('Message sent','We\'ll get back to you soon','success')
						$('#send_inquiry').trigger("reset");
						$('#btnSubmit').removeAttr('disabled');
					} else {
						close_loading();
						show_message(output.error_msg,"Error");
					}
				},
				error: function (data) {
					console.log(data);
					close_loading();
				}
			});	
	});
</script>