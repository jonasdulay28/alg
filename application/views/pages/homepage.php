<div class="homepage">
	<div class="container">
		<br>
		<div id="myCarousel" class="carousel  slide" data-ride="carousel" data-interval="4000" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
				$x = 1;
				if (count($banners) > 0) {
					foreach ($banners as $key) {
				?>
						<li data-target="#bs-myCarousel" data-slide-to="<?php echo $x - 1; ?>" class="<?php echo $x == 1 ? "active" : ""; ?>"></li>
					<?php $x++;
					}
				} else {
					?>
					<li data-target="#bs-myCarousel" data-slide-to="0" class="active"></li>
				<?php
				} ?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<?php
				$x = 1;
				if (count($banners) > 0) {
					foreach ($banners as $key) {
				?>
						<div class="item slides <?php echo $x == 1 ? "active" : ""; ?>">
							<img src="https://via.placeholder.com/1500x300.png" alt="" style="width:100%;">
							<div class="carousel-caption">
							</div>
						</div>
					<?php $x++;
					}
				} else {
					?>
					<div class="item slides active">
						<img src="https://via.placeholder.com/1500x300.png" alt="" style="width:100%;">
						<div class="carousel-caption">
						</div>
					</div>
				<?php
				} ?>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<?php if (!empty($newest_expansion)) { ?>
			<div class="section_container">
				<h3 class="section-title section-title-center"><b></b><span class="section-title-main"><?php echo get_language('Newest Expansions', $this->session->current_language); ?></span><b></b></h3>
			</div>
			<div class="row">
				<?php
				$rarity = ['mythic', 'rare', 'uncommon', 'common'];
				foreach ($newest_expansion as $key) {
				?>
					<div class="col-lg-4 mt-3">
						<figure class="standard_container">
							<a href="<?php echo base_url('cards') . '?cat=' . $key->set_code ?>">
								<img src="<?php echo $key->image ?>" class="img-responsive center-block">
								<div class="row keyrune_row">
									<div class="col-xs-12">
										<center>
											<?php
											for ($x = 0; $x < 4; $x++) {
											?>
												<p class="keyrune_style"><a href="<?php echo base_url('cards/?cat=' . $key->set_code . '&rarity=' . $rarity[$x] . '') ?>">
														<i class="ss ss-<?php echo strtolower($key->set_code); ?> ss-grad ss-<?php echo strtolower($rarity[$x]) ?>"></i></a></p>
											<?php
											}
											?>
										</center>
									</div>
								</div>
							</a>
						</figure>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="section_container">
			<h3 class="section-title section-title-center"><b></b><span class="section-title-main"><?php echo get_language('Top Products', $this->session->current_language); ?></span><b></b></h3>
		</div>
		<section class="top_products">
			<?php $cart_data = getUpdatedCartList() ?>
			<?php if (!empty($top_card)) : foreach ($top_card as $cards) : ?>
					<?php
					$img_src = getCardImage($cards, 'json');
					$card_name = getCardName($cards, 'json');

					$card_language_tag = $cards->category == 'single cards' ? getCardLanguageTag($cards->language) : '';
					$card_label = $cards->category == 'single cards' ? getCardLabel($cards->tag, $cards->language) : '';
					$card_datestamp_tag = $cards->datestamp && $cards->promo == 'datestamp' ? '<label class="datestamp_tag">' . $cards->datestamp . '</label>' : '';
					$card_planeswalker_tag = $cards->promo == 'planeswalker' ? '<img src="' . base_url() . 'assets/images/planeswalker.png" class="planeswalker_tag">' : '';
					?>
					<div>
						<div class="card_container">
							<div class="image-container">
								<?php echo $card_language_tag . $card_label . $card_datestamp_tag . $card_planeswalker_tag ?>
								<a href="<?php echo base_url() . 'cards/view/' . $cards->id ?>"><img src="<?php echo $img_src ?>" class="img-responsive" alt="<?php echo $cards->name ?>" style="width: 100%"></a>
								<?php if ($cards->card_type == 'foil') : ?>
									<div class="after"></div>
								<?php endif; ?>
							</div>
							<div class="card-details">
								<small class="set"><?php echo $cards->set ?></small>

								<span class="card_name"><a href="<?php echo base_url() . 'cards/view/' . $cards->id ?>"><?php echo $card_name ?></a></span>
								<div class="card-info-container">
									<p class="price"><?php echo main_currency_sign ?> <?php echo $cards->regular_price ? $cards->regular_price : $cards->foil_price ?></p>
									<?php if($cards->stock == 0): ?>
										<p style="color: red">Out of Stock</p>
									<?php else: ?>
										<?php if(isset($cart_data[$cards->id])): ?>
										<button class="btn btn-default add_to_cart" data-id="<?php echo $cards->id ?>" data-target="card-quantity-content-<?php echo $cards->id ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true"><?php echo $cart_data[$cards->id]['quantity'] ?> in Cart</button>
										<?php else: ?>
										<button class="btn btn-primary add_to_cart" data-id="<?php echo $cards->id ?>" data-target="card-quantity-content-<?php echo $cards->id ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true">Add to cart</button>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div id="card-quantity-content-<?php echo $cards->id ?>" class="hide add-cart-wrapper">
							<div class="cart-quantity-content" data-container="card-quantity-content-<?php echo $cards->id ?>" data-id="<?php echo $cards->id ?>" style="width: 150px; min-height: 100px; overflow: auto;">
								<div class="text-center">
									<span class="label-quantity">Select Qty</span>
								</div>
								<?php
								for ($ctr = 0; $ctr <= $cards->stock; $ctr++) {
									if (($cards->rarity == 'U' || $cards->rarity == 'R' || $cards->rarity == 'M') && $ctr > 8) {
										break;
									} else if ($ctr > 24) {
										break;
									}
								?>
									<button style="width: 27%; margin: 5px 2%; height: 35px;" data-value="<?php echo $ctr ?>"><?php echo $ctr ?></button>
								<?php } ?>
							</div>
						</div>
					</div>
			<?php endforeach;
			else : echo "No products found.";
			endif; ?>
		</section>
		<div class="section_container">
			<h3 class="section-title section-title-center"><b></b><span class="section-title-main"><?php echo get_language('HOT CATEGORIES', $this->session->current_language); ?></span><b></b></h3>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_1">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="#tab1" data-toggle="tab"><button class="btn btn-default cta standard">Standard</button></a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 col-md-6 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_2">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="#tab2" data-toggle="tab"><button class="btn btn-default cta modern">Modern</button></a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 col-md-6 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_3">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta legacy">Legacy</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 col-md-6 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_4">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta commander">Commander</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 col-md-6 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_5">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<button class="btn btn-default cta old_school">Old School</button>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
			<div class="col-lg-4 col-md-6 mt-3">
				<figure class="hot_categories_container">
					<div class="bg_6">
						<div class="effect-sparkle bg-effect fill no-click"></div>
						<a href="<?php echo base_url() ?>cards/?cat=waraa">
							<button class="btn btn-default cta bargain_bin">War of the Spark - Jap AA</button>
						</a>
						<div class="overlay"></div>
					</div>
				</figure>
			</div>
		</div>
	</div>
</div>