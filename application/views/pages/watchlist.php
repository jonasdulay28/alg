<div class="container cart_page" style="min-height: 360px;">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Watchlist</li>
	  </ol>
	</nav>

	<div id="outOfStockModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-md">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-md-12">
					<h4>This product is currently out of stock. Do you wish to continue without this items?</h4>
	      		</div>
	      		<div class="col-md-12">
					<table class="table">
						<thead>
							<th style="width: 300px">Product</th>
							<th>Set Name</th>
						</thead>
						<tbody>
						</tbody>
					</table>
	      		</div>
	      		<div class="col-md-12">
	      			<button class="btn btn-primary btn-confirm-watchlist">Confirm</button>
	      			<button class="btn btn-default">Cancel</button>
	      		</div>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="watchlist-container">
			<?php if(!empty($watchlist)): ?>
				<table class="table table-hover table-watchlist">
					<thead>
						<th></th>
						<th>Image</th>
						<th>Product</th>
						<th>Edition</th>
						<th>Cost</th>
						<th>Condition</th>
						<th>Price</th>
					</thead>
					<tbody>
						<?php foreach($watchlist as $card): ?>
						<tr data-id="<?php echo $card['id'] ?>">
							<td><a href='javascript:void(0)' class="remove_card" data-id="<?php echo $card['id'] ?>">X</a></td>
							<td>
								<?php
				        		$img_src = getCardImage($card);
				        		$card_language_tag = $card['category'] == 'single cards' ? getCardLanguageTag($card['language']) : '';
				        		$card_label = $card['category'] == 'single cards' ? getCardLabel($card['tag'], $card['language']) : '';
				        		if($card['regular_price']):
				        		$disp = '<div class="card-image image-container asd" style="display: inline-block">'
				        		. $card_language_tag
				        		. $card_label
				        		.'<img src="'. $img_src .'" alt="'. $card['name'] .'"></div>';
				        		else:
				        		$disp = '<div class="card-image image-container" style="display: inline-block">'
				        		. $card_language_tag
				        		. $card_label
				        		.'<img src="'. $img_src .'" alt="'. $card['name'] .'"><div class="after"></div></div>';
					        	endif;
				        		echo $disp;
								?>
							</td>
							<td>
								<div class="card-name"><a href="<?php echo base_url() . 'cards/view/' . $card['id'] ?>"><?php echo $card['name'] ?></a> (<?php echo $card['set_code'] ?>)</div><div class="card-type"><span><?php echo $card['type'] ?></span></div>
							</td>
							<td><i class="ss ss-<?php echo strtolower($card['set_code']) ?> ss-2x ss-mythic"></i></td>
							<td>
							<?php
							$newStr = str_split($card['manacost']);

							for($count = 0; $count < sizeof($newStr); $count++) {
								if($newStr[$count] != '{' && $newStr[$count] != '}') {
								?><i class="mana-<?php echo strtolower($newStr[$count]) ?>"></i>
							<?php
								}
							}							 
							?>
							</td>
							<?php 
				        		$fully_handled = isset($card['fully_handled']) ? $card['fully_handled'] : '';
				        		$fully_handled = strtoupper($fully_handled);
				        		// switch($fully_handled){
				        		// 	case 'nm':
				        		// 		$fully_handled = 'Near mint';
				        		// 		break;
				        		// 	case 'm':
				        		// 		$fully_handled = 'Mint';
				        		// 		break;
				        		// 	case 'ex':
				        		// 		$fully_handled = 'Excellent';
				        		// 		break;
				        		// 	case 'g':
				        		// 		$fully_handled = 'Good';
				        		// 		break;
				        		// 	case 'used':
				        		// 		$fully_handled = 'Used';
				        		// 		break;
				        		// 	default:
				        		// 		break;
				        		// }
							?>
							<td><?php echo $fully_handled ?></td>
							<td><?php echo main_currency_sign ?> <?php echo $card['regular_price'] ? $card['regular_price'] : $card['foil_price'] ?> </td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			<?php else: ?>
				<center><p>Your watchlist is currently empty. <a href="<?php echo base_url()?>">Click here to continue shopping.</a></p> </center>
			<?php endif; ?>
			</div>
		</div>
		<?php if(!empty($watchlist)): ?>
		<div class="col-md-12">
			<button class="btn btn-primary btn-watchlist-to-cart">Move watchlist to cart</button>
		</div>
		<?php endif; ?>
	</div>

</div>