<style type="text/css">
	.forgot_password_modal {
		padding:20px !important;
	}
	.authentication_header{
		text-align: center;
	}
	.authentication_container{
		min-height: 300px;
		border: 1px solid #e1e1e1;
		margin: 50px auto; 
	}
</style>

<div class="container authentication_container">	
	<div class="row">
		<div class="col-md-12">
			<div class="authentication_header">
				<h2>Forgot Password</h2>  
				<p>Enter your new password.</p>
			</div>
		</div>
	</div>
	<div class="row">	
		<div class="col-sm-6 col-md-6 col-lg-6 col-md-offset-3 ">
			<center>
			<?php echo form_open("#",array('autocomplete'=>'off','id'=>'forgot_password_form')); ?>
			<div class="row">
				<div class="col-md-12 mt-3">
					<input type="password" name="password" class="form-control" placeholder="New Password" required >
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 mt-3">
					<input type="password" name="confirm_password" class="form-control" id="password" placeholder="Confirm Password" required>
				</div>
			</div>
			<div id="result"></div>
			<br>
			<button type="submit" class="btn btn-success reg_btn mt-2" >Reset Password</button>
			<?php echo form_close(); ?>
			</center>
			<br>
		</div>	
	</div>
</div>	
<script type="text/javascript">
	$("#forgot_password_form").on("submit",function(e){
		e.preventDefault();
		var post_url = "<?php echo base_url('Authentication/reset_password')?>";

		$.ajax({
	      type : 'POST',
	      url : post_url,
	      data: $('#forgot_password_form').serialize(),
	      dataType:"json",
	      beforeSend:function(){
	      	loading_class('forgot_password_modal');
	      },
	      success : function(res){
	        close_loading();
	        if(res.message=="success") {
	        	notify2("Success!","Your password was successfully changed.","success");
	        	setTimeout(() => {
	              window.location.href= res.url;
	          }, 2000);
	        }
	        else{
	        	$("#result").html(res.message);
	        }
	      },
	      error : function(res) {
	      }
	  });
	})
</script>