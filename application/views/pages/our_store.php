<div class="container our_store">
		<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >OUR STORE AND OUR UNWAVERING SERVICE GUARANTEE
</span><b></b></h3></div>
		<section>
		  <div class="container gal-container">
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" >
		          <img src="<?php echo images_bundle('01/1.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<p>AT ALG, YOU COME FIRST.</p>
		          	<center>
		          		<small>Nong Maew might be the cat of the kingdom, but deep in our hearts, we know you’re the king. You chose us, and we’ll do our best to exceed your expectations each and every time.</small>
		          	</center>
		          </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" >
		          <img src="<?php echo images_bundle('01/2.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<p>Our Address:</p>
		          	<center>
		          		<small>ALG<br>
The Trendy Office Building<br>
2101-D (21st Floor)<br><br>

10 Soi Sukhumvit 13 <br>
Khlong Tan, Khet Watthana Krung Thep Maha Nakhon 10110 Thailand</small>
		          	</center>
		          </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" >
		          <img src="<?php echo images_bundle('01/3.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<p>12 NOON TO <br> 9PM DAILY</p>
		          	<center>
		          		<small>We’re always here to welcome you, all year round! Expect ALG to be open on time each and every day!</small>
		          	</center>
		          </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#">
		          <img src="<?php echo images_bundle('01/4.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<p>DAILY TOURNAMENTS. PAYOUTS GUARANTEED.</p>
		          	<center>
		          		<small>Rain or shine, we understand you want a place to game. That’s why when ALG holds daily tournaments, guaranteed to fire and honoring the prize payouts no matter what</small>
		          	</center>
		          </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#">
		          <img src="<?php echo images_bundle('01/5.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<p>WELL-STOCKED FOR YOUR CONVENIENCE</p>
		          	<center>
		          		<small>Whether you’re looking for singles, sealed products, or accessories, we strive to be the most well-stocked store throughout Thailand.</small>
		          	</center>
		          </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" >
		          <img src="<?php echo images_bundle('01/6.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<p>BUYLIST AVAILABLE.</p>
		          	<center>
		          		<small>Have some cards you don’t need? Come check out our attractive buylist and transform your collection into instant cash.</small>
		          	</center>
		          </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#">
		          <img src="<?php echo images_bundle('01/7.jpeg')?>">
		        </a>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		          <div class="center_text">
		          	<<p>THE MOST REWARDING EXPERIENCE</p>
		          	<center>
		          		<small>Whenever you shop at ALG, you earn points and rebates towards your next purchase. The more you support us, the more rewards we can give back to our most loyal royals.</small>
		          </div>
		      </div>
		    </div>
				<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#">
		          <img src="<?php echo images_bundle('01/8.jpeg')?>">
		        </a>
		      </div>
		    </div>
		  </div>
		</section>
</div>