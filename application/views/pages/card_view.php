<div class="container card_view">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><?php echo get_language('Home',$this->session->current_language) ?></a></li>
			<?php if ($card['set']) : ?>
				<?php if($card['category'] == 'single cards'): ?>
					<li class="breadcrumb-item"><a href="<?php echo base_url() . 'cards/?cat=' . $card['set_code'] ?>"><?php echo get_language($card['set'], $this->session->current_language) ?></a></li>
				<?php else: ?>
					<li class="breadcrumb-item"><a href="<?php echo base_url() . 'accessories/?type=' . strtolower($card['type']) ?>"><?php echo get_language($card['set'], $this->session->current_language) ?></a></li>
				<?php endif; ?>
			<?php endif; ?>
			<?php if($this->session->current_language == 'traditional_chinese'): ?>
				<li class="breadcrumb-item active" aria-current="page"><?php echo $card['name_tw'] . $this->session->current_language ?></li>
			<?php else: ?>
				<li class="breadcrumb-item active" aria-current="page"><?php echo $card['name'] ?></li>
			<?php endif; ?>
		</ol>
	</nav>
	<div class="row">
		<div class="col-md-9 card_view_container">
			<div class="col-sm-3 card-image-container">
				<?php
				$additional_img = array();
				if ($card['additional_img']) $additional_img = explode(",", $card['additional_img']);
				?>
				<div class="image-container <?php echo !empty($additional_img) ? 'addtl-img' : 'single-img' ?>">
					<div class="display-card-image">
						<?php
						if($card['category'] == 'single cards'){
							echo getCardLanguageTag($card['language']);
							echo getCardLabel($card['tag'], $card['language']);
						}

						if ($card['promo'] == 'datestamp') {
							echo $card['datestamp'] ? '<label class="datestamp_tag card_img_tag">' . $card['datestamp'] . '</label>' : '';
						} else if ($card['promo'] == 'planeswalker') {
							echo '<img src="' . base_url() . 'assets/images/planeswalker.png" class="planeswalker_tag card_img_tag">';
						}
						?>
						<img class="active" data-id="1" src="<?php echo $card['img_src'] ?>" style="width: 100%;" alt="<?php echo $card['name'] ?>">

						<?php
						for ($ctr = 0; $ctr < sizeof($additional_img) && $ctr < 3; $ctr++) {
						?>
							<img src="<?php echo $additional_img[$ctr] ?>" data-id="<?php echo $ctr + 2 ?>" style="width: 100%;" alt="<?php echo $card['name'] ?>">
						<?php } ?>
					</div>
					<?php if ($card['foil_price']) : ?>
						<div class="after"></div>
					<?php endif; ?>
					<div class="row additional-card-image">

						<?php if ($card['additional_img']) :
							$additional_img = explode(",", $card['additional_img']);
							if (sizeof($additional_img) > 0) :
						?>
								<div style="width: 23%; display: inline-block;">
									<img src="<?php echo $card['img_src'] ?>" data-id="1" style="width: 100%;" alt="<?php echo $card['name'] ?>">
								</div>
								<?php for ($ctr = 0; $ctr < sizeof($additional_img) && $ctr < 3; $ctr++) { ?>
									<div style="width: 23%; display: inline-block;">
										<img src="<?php echo $additional_img[$ctr] ?>" data-id="<?php echo $ctr + 2 ?>" style="width: 100%;" alt="<?php echo $card['name'] ?>">
									</div>
								<?php } ?>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
				<center>
					<div>
						<?php if (sizeof($other_card) > 0) : ?>
							<select class="condition_list form-control">
								<?php
								$fully_handled = strtoupper($card['fully_handled']);
								// switch ($fully_handled) {
								// 	case 'nm':
								// 		$fully_handled = 'Near Mint';
								// 		break;
								// 	case 'm':
								// 		$fully_handled = 'Mint';
								// 		break;
								// 	case 'ex':
								// 		$fully_handled = 'Excellent';
								// 		break;
								// 	case 'vg':
								// 		$fully_handled = 'Very Good';
								// 		break;
								// 	case 'g':
								// 		$fully_handled = 'Good';
								// 		break;
								// 	case 'used':
								// 		$fully_handled = 'Used';
								// 		break;
								// 	case 'p':
								// 		$fully_handled = 'Played';
								// 		break;
								// 	case 'd':
								// 		$fully_handled = 'Damaged';
								// 		break;
								// 	default:
								// 		break;
								// }
								?>
								<option value="<?php echo $card['fully_handled'] ?>" data-lang="<?php echo strtoupper($card['language']) ?>"><?php echo $fully_handled ?></option>

								<?php foreach ($other_card as $value) : ?>
									<?php
									$fully_handled = strtoupper($value['fully_handled']);
									// switch ($fully_handled) {
									// 	case 'nm':
									// 		$fully_handled = 'Near Mint';
									// 		break;
									// 	case 'm':
									// 		$fully_handled = 'Mint';
									// 		break;
									// 	case 'ex':
									// 		$fully_handled = 'Excellent';
									// 		break;
									// 	case 'vg':
									// 		$fully_handled = 'Very Good';
									// 		break;
									// 	case 'g':
									// 		$fully_handled = 'Good';
									// 		break;
									// 	case 'used':
									// 		$fully_handled = 'Used';
									// 		break;
									// 	case 'p':
									// 		$fully_handled = 'Played';
									// 		break;
									// 	case 'd':
									// 		$fully_handled = 'Damaged';
									// 		break;
									// 	default:
									// 		break;
									// }
									?>
									<option value="<?php echo $value['fully_handled'] ?>" data-lang="<?php echo strtoupper($value['language']) ?>"><?php echo $fully_handled ?></option>
								<?php endforeach; ?>
							</select>
						<?php endif; ?>
					</div>
				</center>
				<Br>

				<?php if (!empty($watchlist)) : ?>
					<button class="btn add_item_to_wishlist <?php echo isset($watchlist[$card['id']]) ? 'btn-danger' : 'btn-success' ?>" data-default-text="Add to Watchlist" data-id="<?php echo $card['id'] ?>" style="display: block;margin:0 auto; white-space: nowrap;width: 145px; overflow: hidden; text-overflow: ellipsis;"><?php echo isset($watchlist[$card['id']]) ? 'Remove from Watchlist' : 'Add to Watchlist' ?></button>
				<?php else : ?>
					<button class="btn add_item_to_wishlist btn-success" style="display: block;margin:0 auto; white-space: nowrap;width: 145px; overflow: hidden; text-overflow: ellipsis;" data-default-text="Add to Watchlist" data-id="<?php echo $card['id'] ?>" style="display: block;margin:0 auto;">Add to Watchlist</button>
				<?php endif; ?>
				<div class="quantity buttons_added hidden-md hidden-lg">
					<div class="add_to_cart_container" data-condition="<?php echo $card['fully_handled'] ?>">
						<?php $cart_data = getUpdatedCartList() ?>
						<?php if (isset($cart_data[$card['id']])) : ?>
							<button class="btn btn-default add-to-cart-quantity add_to_cart" data-id="<?php echo $card['id'] ?>" data-target="card-quantity-content-<?php echo $card['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo $cart_data[$card['id']]['quantity'] ?> in Cart</button>
						<?php else : ?>
							<button class="btn btn-primary add-to-cart-quantity add_to_cart" data-id="<?php echo $card['id'] ?>" data-target="card-quantity-content-<?php echo $card['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo get_language('Add to Cart', $this->session->current_language) ?></button>
						<?php endif; ?>
						<a href="<?php echo base_url('Checkout') ?>"><button class="btn btn-default "><?php echo get_language('Checkout', $this->session->current_language) ?> &nbsp;<i class="fa fa-arrow-right"></i></button></a>
					</div>

					<?php if (sizeof($other_card) > 0) : ?>
						<?php foreach ($other_card as $value) : ?>
							<div class="add_to_cart_container" data-condition="<?php echo $value['fully_handled'] ?>" style="display: none">
								<?php if (isset($cart_data[$value['id']])) : ?>
									<button class="btn btn-default add-to-cart-quantity add_to_cart" data-id="<?php echo $value['id'] ?>" data-target="card-quantity-content-<?php echo $value['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo $cart_data[$value['id']]['quantity'] ?> in Cart</button>
								<?php else : ?>
									<button class="btn btn-primary add-to-cart-quantity add_to_cart" data-id="<?php echo $value['id'] ?>" data-target="card-quantity-content-<?php echo $value['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo get_language('Add to Cart', $this->session->current_language) ?></button>
								<?php endif; ?>
								<a href="<?php echo base_url('Checkout') ?>"><button class="btn btn-default "><?php echo get_language('Checkout', $this->session->current_language) ?> &nbsp;<i class="fa fa-arrow-right"></i></button></a>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-sm-6 card-information">
				<?php
				$card_name = getCardName($card);
				$card_type = '';
				if(strtolower($card['language']) == 'tw' || strtolower($card['language']) == 'eng') {
					$card_type = isset($card['type_tw']) ? $card['type_tw'] ? '(' . $card['type_tw'] . ')' : '' : '';
				} else if(strtolower($card['language']) == 'jp') {
					$card_type = isset($card['type_jp']) ? $card['type_jp'] ? '(' . $card['type_jp'] . ')' : '' : '';
				} else if(strtolower($card['language']) == 'ko') {
					$card_type = isset($card['type_ko']) ? $card['type_ko'] ? '(' . $card['type_ko'] . ')' : '' : '';
				}
				?>
				<h3 class="card-title"><?php echo $card_name ?></h3>
				<span><?php echo '<p>' . $card['type'] . ' ' . $card_type . '</p>' ?></span>
				<div>
					<?php if (sizeof($other_card) == 0) : ?>
						<?php
						$fully_handled = strtoupper($card['fully_handled']);
						// switch ($fully_handled) {
						// 	case 'nm':
						// 		$fully_handled = 'Near Mint';
						// 		break;
						// 	case 'm':
						// 		$fully_handled = 'Mint';
						// 		break;
						// 	case 'ex':
						// 		$fully_handled = 'Excellent';
						// 		break;
						// 	case 'vg':
						// 		$fully_handled = 'Very Good';
						// 		break;
						// 	case 'g':
						// 		$fully_handled = 'Good';
						// 		break;
						// 	case 'used':
						// 		$fully_handled = 'Used';
						// 		break;
						// 	case 'p':
						// 		$fully_handled = 'Played';
						// 		break;
						// 	case 'd':
						// 		$fully_handled = 'Damaged';
						// 	default:
						// 		break;
						// }
						?>
						<p><?php echo $fully_handled ?></p>
					<?php endif; ?>
				</div>
				<div class="price-container" data-condition="<?php echo $card['fully_handled'] ?>">
					<?php
					$card_price = $card['regular_price'] ? $card['regular_price'] : $card['foil_price'];
					$rates = $this->session->support_currency_rate ? $this->session->support_currency_rate : "";
					$currency = $this->session->support_currency ? $this->session->support_currency : "";
					if ($rates && $card_price) { ?>
						<p><span class="card-price-amount"><?php echo $card_price ?>
							</span><span class="card-price-symbol">‎<?php echo main_currency_sign ?> <?php echo ' (' . round($card_price * $rates, 2) . ' ' . $currency . ')' ?></span></p>
					<?php } elseif ($card_price) { ?>
						<p><span class="card-price-amount"><?php echo main_currency_sign ?> <?php echo $card_price ?>
							</span></p>
					<?php } ?>
				</div>
				<?php if (sizeof($other_card) > 0) : ?>
					<?php foreach ($other_card as $value) : ?>
						<div class="price-container" data-condition="<?php echo $value['fully_handled'] ?>" style="display:none">
							<?php
							$card_price_2 = $value['regular_price'] ? $value['regular_price'] : $value['foil_price'];
							$rates_2 = $this->session->support_currency_rate ? $this->session->support_currency_rate : "";
							$currency_2 = $this->session->support_currency ? $this->session->support_currency : "";
							if ($rates_2) { ?>
								<p><span class="card-price-amount"><?php echo $card_price_2 ?>
									</span><span class="card-price-symbol">‎<?php echo main_currency_sign ?> <?php echo ' (' . round($card_price_2 * $rates_2, 2) . ' ' . $currency_2 . ')' ?></span></p>
							<?php } else { ?>
								<p><span class="card-price-amount"><?php echo main_currency_sign ?> <?php echo $card_price_2 ?>
									</span></p>
							<?php } ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
				<div class="card-ability">
					<p><?php echo mana_replace($card['ability']) ?></p>
					<?php 
						if(strtolower($card['language']) == 'tw' || strtolower($card['language']) == 'eng'){
							echo isset($card['ability_tw']) ? $card['ability_tw'] ? '<p>' . mana_replace($card['ability_tw']) . '</p>' : '' : '';
						} else if(strtolower($card['language']) == 'jp') {
							echo isset($card['ability_jp']) ? $card['ability_jp'] ? '<p>' . mana_replace($card['ability_jp']) . '</p>' : '' : '';
						} else if(strtolower($card['language']) == 'ko') {
							echo isset($card['ability_ko']) ? $card['ability_ko'] ? '<p>' . mana_replace($card['ability_ko']) . '</p>' : '' : '';
						}
					?>
				</div>

				<div class="quantity buttons_added hidden-xs hidden-sm">
					<?php $cart_data = getUpdatedCartList() ?>
					<div class="add_to_cart_container" data-condition="<?php echo $card['fully_handled'] ?>">
						<?php if ($card['stock']) { ?>
							<?php if (isset($cart_data[$card['id']])) : ?>
								<button class="btn btn-default add-to-cart-quantity add_to_cart" data-id="<?php echo $card['id'] ?>" data-target="card-quantity-content-<?php echo $card['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo $cart_data[$card['id']]['quantity'] ?> in Cart</button>
							<?php else : ?>
								<button class="btn btn-primary add-to-cart-quantity add_to_cart" data-id="<?php echo $card['id'] ?>" data-target="card-quantity-content-<?php echo $card['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo get_language('Add to Cart', $this->session->current_language) ?></button>
							<?php endif; ?>
						<?php } else { ?>
							<button class="btn btn-danger" disabled=""><?php echo get_language('Out of stock', $this->session->current_language) ?></button>
						<?php } ?>
						<a href="<?php echo base_url('Checkout') ?>"><button class="btn btn-default "><?php echo get_language('Checkout', $this->session->current_language) ?> &nbsp;<i class="fa fa-arrow-right"></i></button></a>
					</div>

					<?php if (sizeof($other_card) > 0) : ?>
						<?php foreach ($other_card as $value) : ?>
							<div class="add_to_cart_container" data-condition="<?php echo $value['fully_handled'] ?>" style="display: none">
								<?php if ($value['stock']) { ?>
									<?php if (isset($cart_data[$value['id']])) : ?>
										<button class="btn btn-default add-to-cart-quantity add_to_cart" data-id="<?php echo $value['id'] ?>" data-target="card-quantity-content-<?php echo $value['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo $cart_data[$value['id']]['quantity'] ?> in Cart</button>
									<?php else : ?>
										<button class="btn btn-primary add-to-cart-quantity add_to_cart" data-id="<?php echo $value['id'] ?>" data-target="card-quantity-content-<?php echo $value['id'] ?>" data-toggle="popover" data-container="body" data-default-text="Add to Cart" data-placement="top" data-html="true"><?php echo get_language('Add to Cart', $this->session->current_language) ?></button>
									<?php endif; ?>
								<?php } else { ?>
									<button class="btn btn-danger" disabled=""><?php echo get_language('Out of stock', $this->session->current_language) ?></button>
								<?php } ?>
								<a href="<?php echo base_url('Checkout') ?>"><button class="btn btn-default "><?php echo get_language('Checkout', $this->session->current_language) ?> &nbsp;<i class="fa fa-arrow-right"></i></button></a>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>

				<div class="card-meta">
					<?php if ($card['rarity'] != "") { ?>
						<span class="posted_in">Rarity: <?php echo getRarity($card['rarity']) ?></span>
					<?php }
					if ($card['set'] != "") { ?>
						<?php if($card['category'] == 'single cards'): ?>
							<span class="tagged_as">Set: <a href="<?php echo base_url() . 'cards/?cat=' . $card['set_code'] ?>"><?php echo $card['set'] ?></a></span>
						<?php else: ?>
							<span class="tagged_as">Set: <a href="<?php echo base_url() . 'accessories/?type=' . strtolower($card['type']) ?>"><?php echo $card['set'] ?></a></span>
						<?php endif; ?>

						<span style="display:none;">Mana cost: <span class="mana_cost"><?php echo $card['manacost'] ?></span></span>
					<?php }
					if ($card['manacost'] != "") { ?>
						<span>Mana Cost: <span class="mana_cost2"><?php echo mana_replace($card['manacost']) ?></span></span>
					<?php }
					if ($card['artist'] != "") { ?>
						<span class="posted_in">Artist: <?php echo $card['artist'] ?></span>
					<?php } ?>
				</div>

			</div>

			<?php if (sizeof($other_card) > 0) : ?>
				<?php foreach ($other_card as $value) : ?>
					<div id="card-quantity-content-<?php echo $value['id'] ?>" class="hide add-cart-wrapper">
						<div class="text-center">
							<span class="label-quantity">Select Qty</span>
						</div>
						<div class="cart-quantity-content" data-container="card-quantity-content" data-id="<?php echo $value['id'] ?>" style="width: 150px; min-height: 100px;">
							<?php
							for ($ctr = 0; $ctr <= $value['stock']; $ctr++) {
								if (($value['rarity'] == 'U' || $value['rarity'] == 'R' || $value['rarity'] == 'M') && $ctr > 8) {
									break;
								} else if ($ctr > 24) {
									break;
								}
							?>
								<button style="width: 25%; margin: 5px 3%; height: 35px;" data-value="<?php echo $ctr ?>"><?php echo $ctr ?></button>

							<?php } ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>

			<div id="card-quantity-content-<?php echo $card['id'] ?>" class="hide add-cart-wrapper">
				<div class="text-center">
					<span class="label-quantity">Select Qty</span>
				</div>
				<div class="cart-quantity-content" data-container="card-quantity-content" data-id="<?php echo $card['id'] ?>" style="width: 150px; min-height: 100px;">
					<?php
					for ($ctr = 0; $ctr <= $card['stock']; $ctr++) {
						if (($card['rarity'] == 'U' || $card['rarity'] == 'R' || $card['rarity'] == 'M') && $ctr > 8) {
							break;
						} else if ($ctr > 24) {
							break;
						}
					?>
						<button style="width: 25%; margin: 5px 3%; height: 35px;" data-value="<?php echo $ctr ?>"><?php echo $ctr ?></button>

					<?php } ?>
				</div>
			</div>

			<?php if (!empty($related_card)) : ?>
				<div class="col-sm-12 card-footer">
					<h3>RELATED PRODUCTS</h3>
					<section class="related-card-section">
						<?php foreach ($related_card as $cards) : ?>
							<?php
							$img_src = getCardImage($cards);
							$card_name = getCardName($cards);
							?>
							<div>
								<div class="card_container">
									<a href="<?php echo base_url('cards/view/' . $cards['id']) ?>">
										<div class="image-container" style="display: inline-block;">
											<?php

											if($cards['category'] == 'single cards'){
												echo getCardLanguageTag($cards['language']);
												echo getCardLabel($cards['tag'], $cards['language']);
											}
											
											if ($cards['promo'] == 'datestamp') {
												echo $cards['datestamp'] ? '<label class="datestamp_tag">' . $cards['datestamp'] . '</label>' : '';
											} else if ($cards['promo'] == 'planeswalker') {
												echo '<img src="' . base_url() . 'assets/images/planeswalker.png" class="planeswalker_tag">';
											}
											?>
											<img src="<?php echo $img_src ?>" class="img-responsive" alt="<?php echo $cards['name'] ?>">
											<?php if ($cards['foil_price']) : ?>
												<div class="after"></div>
											<?php endif; ?>
										</div>
									</a>
									<div class="card-details">
										<a href="<?php echo base_url('cards/view/' . $cards['id']) ?>">
											<small class="set"><?php echo $cards['set'] ?></small>
											<span class="card_name"><?php echo $card_name ?></span>
											<p class="price"><?php echo main_currency_sign ?> <?php echo $cards['regular_price'] ? $cards['regular_price'] : $cards['foil_price'] ?></p>
										</a>
										<?php if ($cards['stock'] > 0) { ?>
											<?php if (isset($cart_data[$cards['id']])) : ?>
												<button class="btn btn-default add_to_cart" data-id="<?php echo $cards['id'] ?>" data-target="card-quantity-content-<?php echo $cards['id'] ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true"><?php echo $cart_data[$cards['id']]['quantity'] ?> in cart</button>
											<?php else : ?>
												<button class="btn btn-primary add_to_cart" data-id="<?php echo $cards['id'] ?>" data-target="card-quantity-content-<?php echo $cards['id'] ?>" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true"><?php echo get_language('Add to Cart', $this->session->current_language) ?></button>
											<?php endif; ?>
										<?php } else { ?>
											<div style="color: #bf4473; font-weight: bold"><?php echo get_language('Out of stock', $this->session->current_language) ?></div>
										<?php } ?>
									</div>
								</div>
								<?php if ($cards['stock'] > 0) { ?>
									<div id="card-quantity-content-<?php echo $cards['id'] ?>" class="hide add-cart-wrapper">
										<div class="text-center">
											<span class="label-quantity">Select Qty</span>
										</div>
										<div class="cart-quantity-content" data-container="card-quantity-content-<?php echo $cards['id'] ?>" data-id="<?php echo $cards['id'] ?>" style="width: 150px; min-height: 100px;">
											<?php
											for ($ctr = 0; $ctr <= $cards['stock']; $ctr++) {
												if (($cards['rarity'] == 'U' || $cards['rarity'] == 'R' || $cards['rarity'] == 'M') && $ctr > 8) {
													break;
												} else if ($ctr > 24) {
													break;
												}
											?>
												<button style="width: 25%; margin: 5px 3%; height: 35px;" data-value="<?php echo $ctr ?>"><?php echo $ctr ?></button>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php endforeach; ?>
					</section>
				</div>
			<?php endif; ?>
		</div>
		<div class="col-md-3 product-sidebar">
			<div id="card-tree-view"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	c_language = '<?php echo $language ?>';
</script>