<div class="container">
	<h4>Order Summary</h4>
	<table class="table">
		<thead>
			<th>Product</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Total</th>
		</thead>
		<tbody>
			<?php foreach($order_list as $order): ?>
			<tr>
				<td><?php echo $order->card->name ?></td>
				<td><?php echo main_currency_sign ?> <?php echo $order->card->regular_price ?></td>
				<td><?php echo $order->quantity ?></td>
				<td><?php echo main_currency_sign ?> <?php echo $order->quantity * $order->card->regular_price ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
