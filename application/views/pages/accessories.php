<div class="mask"></div>
<div class="container accessories_list">
	<nav aria-label="breadcrumb" class="breadcrumb_list" style="min-width: 265px;margin-bottom: 0px !important;float: left;">
	  <ol class="breadcrumb" style="margin-bottom: 0px !important; background:white; ">
	    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><?php echo get_language('Home',$this->session->current_language) ?></a></li>
	    <li class="breadcrumb-item active" aria-current="page"><?php echo $set_name ?></li><!-- 
	    <li class="breadcrumb-item active" aria-current="page"><?php echo isset($_GET['cat']) ? strtoupper($_GET['cat']) : $_GET['name']?></li> -->
	  </ol>
	</nav>
	<ul class="nav nav-tabs" role="tablist">
		<li class="pull-right"><a data-toggle="tab" href="#card-list-container"><?php echo get_language('List View',$this->session->current_language) ?></a></li>
		<li class="pull-right active"><a data-toggle="tab" href="#card-gallery-container"><?php echo get_language('Gallery View',$this->session->current_language) ?></a></li>
	</ul>
	<div class="row">
		<?php if($search_name): ?><!-- 
		<div class="col-md-12">Search Result for <?php echo $_GET['name'] ?></div> -->
		<?php endif; ?>
		<div class="col-md-3 filter-section">
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Accessories',$this->session->current_language) ?></label>
				<div class="checkbox-group filter-accessories" data-value="accessories">
					<?php $ctr = 0; ?>
					<?php foreach($filter_accessories as $accessories): ?>
					<div class="checkbox-container">
					<?php if(get('type')== 'sealed products') { ?>
						<input type="radio" name="accessories" data-value="<?php echo strtolower($accessories); ?>" checked> <?php echo get_language($accessories,$this->session->current_language) ?>
					<?php } else { ?>
						<input type="radio" name="accessories" data-value="<?php echo strtolower($accessories); ?>" <?php echo $ctr == 0 && !$search_name ? 'checked' : '' ?>> <?php echo get_language($accessories,$this->session->current_language) ?>
					<?php } ?>
					</div>
					<?php $ctr++; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header">Brand</label>
				<div class="checkbox-group filter-brand" data-value="brand">
					<?php $ctr = 0; ?>
					<?php foreach($filter_brand as $brand): ?>
					<div class="checkbox-container">
					<input type="radio" name="brand" data-value="<?php echo strtolower($brand); ?>" <?php echo $ctr == 0 && !$search_name ? 'checked' : '' ?>> <?php echo get_language($brand,$this->session->current_language) ?>
					</div>
					<?php $ctr++; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header">Franchise</label>
				<div class="checkbox-group filter-franchise" data-value="franchise">
					<?php $ctr = 0; ?>
					<?php foreach($filter_franchise as $franchise): ?>
					<div class="checkbox-container">
					<input type="radio" name="franchise" data-value="<?php echo strtolower($franchise); ?>" <?php echo $ctr == 0 && !$search_name ? 'checked' : '' ?>> <?php echo get_language($franchise,$this->session->current_language) ?>
					</div>
					<?php $ctr++; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header">Shop By Price</label>
				<div class="checkbox-group filter-by-price" data-value="by_price">
					<?php $ctr = 0; ?>
					<?php foreach($filter_by_price as $by_price): ?>
					<div class="checkbox-container">
					<input type="radio" name="by_price" data-value="<?php echo strtolower($by_price); ?>" <?php echo $ctr == 0 && !$search_name ? 'checked' : '' ?>> <?php echo str_replace("<thb>", "&#x0E3F;", $by_price) ?>
					</div>
					<?php $ctr++; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="col-md-3 mobile-filter-section text-center">
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-accessories" data-value="accessories">
					<?php $ctr = 0; ?>
					<?php foreach($filter_accessories as $accessories): ?>
					<a href="javascript:void(0)" class="<?php echo $ctr == 0 && !$search_name ? 'active' : '' ?>" data-name="accessories" data-value="<?php echo strtolower($accessories); ?>"><?php echo get_language($accessories,$this->session->current_language) ?></a> 
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_accessories) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-brand" data-value="brand">
					<?php $ctr = 0; ?>
					<?php foreach($filter_brand as $brand): ?>
					<a href="javascript:void(0)" class="<?php echo $ctr == 0 && !$search_name ? 'active' : '' ?>" data-name="brand" data-value="<?php echo strtolower($brand); ?>"><?php echo get_language($brand,$this->session->current_language) ?></a> 
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_brand) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-franchise" data-value="franchise">
					<?php $ctr = 0; ?>
					<?php foreach($filter_franchise as $franchise): ?>
					<a href="javascript:void(0)" class="<?php echo $ctr == 0 && !$search_name ? 'active' : '' ?>" data-name="franchise" data-value="<?php echo strtolower($franchise); ?>"><?php echo get_language($franchise,$this->session->current_language) ?></a> 
					<?php $ctr++; ?>
					<?php echo $ctr < sizeof($filter_franchise) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-by-price" data-value="by_price">
					<?php $ctr = 0; ?>
					<?php foreach($filter_by_price as $by_price): ?>
					<a href="javascript:void(0)" class="<?php echo $ctr == 0 && !$search_name ? 'active' : '' ?>" data-name="by_price" data-value="<?php echo strtolower($by_price); ?>"><?php echo str_replace("<thb>", "&#x0E3F;", $by_price) ?></a> 
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_by_price) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="loading-container">
				<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
			</div>
			<div class="tab-content">
				<div id="card-list-container" class="tab-pane"></div>
				<div id="myDiv"></div>
				<div id="card-gallery-container" class="tab-pane active">
					<div class="row row-eq-height"></div><div class="text-center"><a class="load_gallery_view" href="javascript:void(0)" style="display: none"><?php echo get_language('Load More',$this->session->current_language) ?></a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var type = '<?php echo get('type')?>';
	if(type != "") {
		$(".filter-section input").val("sealed products");
		$(".filter-section input").prop("checked", true);
	}
	var search_name = '<?php echo $search_name ?>';
	var session_sort = JSON.parse('<?php echo json_encode($session_sort) ?>');
</script>