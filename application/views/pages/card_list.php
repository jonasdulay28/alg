<div class="mask"></div>
<div class="container card_list">
	<nav aria-label="breadcrumb" class="breadcrumb_list" style="min-width: 265px;margin-bottom: 0px !important;float: left;">
	  <ol class="breadcrumb" style="margin-bottom: 0px !important; background:white; ">
	    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><?php echo get_language('Home',$this->session->current_language) ?></a></li>
	    <li class="breadcrumb-item active" aria-current="page"><?php echo get_language($set_name,$this->session->current_language) ?></li><!-- 
	    <li class="breadcrumb-item active" aria-current="page"><?php echo isset($_GET['cat']) ? strtoupper($_GET['cat']) : $_GET['name']?></li> -->
	  </ol>
	</nav>
	<ul class="nav nav-tabs" role="tablist">
		<li class="pull-right"><a data-toggle="tab" href="#card-list-container"><?php echo get_language('List View',$this->session->current_language) ?></a></li>
		<li class="pull-right active"><a data-toggle="tab" href="#card-gallery-container"><?php echo get_language('Gallery View',$this->session->current_language) ?></a></li>
	</ul>
	<div class="row">
		<?php if($search_name): ?><!-- 
		<div class="col-md-12">Search Result for <?php echo $_GET['name'] ?></div> -->
		<?php endif; ?>
		<?php 
			$filter_condition = !empty($filter_condition) ? $filter_condition : array();
		?>
		<div class="col-md-3 filter-section">
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Variations', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-variation" data-value="variation">
					<?php foreach($filter_variations as $variations): ?>
					<div class="checkbox-container">
					<input type="radio" name="variation" data-value="<?php echo strtolower($variations); ?>"> <?php echo get_language($variations,$this->session->current_language) ?>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Promo', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-promo" data-value="promo">
					<?php foreach($filter_promo as $promo): ?>
					<div class="checkbox-container">
						<?php if(strtolower($promo) == 'prerelease'): ?>
							<input type="radio" name="promo" data-value="datestamp"> <?php echo get_language($promo,$this->session->current_language) ?> 
						<?php else: ?>
							<input type="radio" name="promo" data-value="<?php echo strtolower($promo) ?>"> <?php echo get_language($promo,$this->session->current_language) ?> 
						<?php endif;?>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Language', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-language" data-value="language">
					<?php foreach($language_filter as $lang): ?>
					<div class="checkbox-container">
					<input type="radio" name="language" data-value="<?php echo strtolower($lang->language_code); ?>" 
					<?php echo $this->session->userdata('language') == strtolower($lang->language_code) && !$search_name ? 'checked' : '' ?>
					> <?php echo get_language($lang->language,$this->session->current_language) ?> 
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Rarity', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-rarity" data-value="rarity">
					<?php foreach($filter_rarity as $rarity): ?>
					<div class="checkbox-container">
					<input type="radio" name="rarity" 
					data-value="<?php echo strtolower($rarity); ?>" <?php echo get('rarity') ? (strtolower($rarity) == strtolower(get('rarity')) ? 'checked' : '') : '' ?>> <?php echo get_language($rarity,$this->session->current_language) ?>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Colors', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-color" data-value="color">
					<?php foreach($filter_colors as $colors): ?>
					<div class="checkbox-container">
					<input type="radio" name="color" data-value="<?php echo strtolower($colors); ?>"> <?php echo get_language($colors,$this->session->current_language) ?>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Type', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-type" data-value="type">
					<?php foreach($filter_type as $type): ?>
					<div class="checkbox-container">
					<input type="radio" name="type" data-value="<?php echo strtolower($type); ?>"> <?php echo get_language($type,$this->session->current_language) ?> 
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<label class="filter-header"><?php echo get_language('Condition', $this->session->current_language) ?></label>
				<div class="checkbox-group filter-condition" data-value="condition">
					<input type="radio" name="condition" data-value="all"> <?php echo get_language("All",$this->session->current_language) ?> 
					<?php foreach($filter_condition as $condition): ?>
					<div class="checkbox-container">
					<input type="radio" name="condition" data-value="<?php echo $condition['fully_handled'] ?>"> <?php echo get_language($condition['fully_handled'],$this->session->current_language) ?> 
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="col-md-3 mobile-filter-section text-center">
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-language" data-value="language">
					<?php $ctr = 0; ?>
					<?php foreach($language_filter as $lang): ?>
					<a href="javascript:void(0)" data-name="language" data-value="<?php echo strtolower($lang->language_code); ?>"><?php echo get_language($lang->language,$this->session->current_language) ?></a> 
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($language_filter) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-variation" data-value="variation">
					<?php $ctr = 0; ?>
					<?php foreach($filter_variations as $variations): ?>
					<a href="javascript:void(0)" data-name="variation" data-value="<?php echo strtolower($variations); ?>"><?php echo get_language($variations,$this->session->current_language) ?></a> 
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_variations) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-promo" data-value="promo">
					<?php $ctr = 0; ?>
					<?php foreach($filter_promo as $promo): ?>
						<?php if(strtolower($promo) == 'prerelease'): ?>
							<a href="javascript:void(0)" data-name="promo" data-value="datestamp"><?php echo get_language($promo,$this->session->current_language) ?></a>
						<?php else: ?>
							<a href="javascript:void(0)" data-name="promo" data-value="<?php echo strtolower($promo); ?>"><?php echo get_language($promo,$this->session->current_language) ?></a>
						<?php endif;?>
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_promo) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-rarity" data-value="rarity">
					<?php $ctr = 0; ?>
					<?php foreach($filter_rarity as $rarity): ?>
					<a href="javascript:void(0)" data-name="rarity" data-value="<?php echo strtolower($rarity); ?>"><?php echo get_language($rarity,$this->session->current_language) ?></a>
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_rarity) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-color" data-value="color">
					<?php $ctr = 0; ?>
					<?php foreach($filter_colors as $color): ?>
					<a href="javascript:void(0)" data-name="color" data-value="<?php echo strtolower($color); ?>"><?php echo get_language($color,$this->session->current_language) ?></a>
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_colors) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-type" data-value="type">
					<?php $ctr = 0; ?>
					<?php foreach($filter_type as $type): ?>
					<a href="javascript:void(0)" data-name="type" data-value="<?php echo strtolower($type); ?>"><?php echo get_language($type,$this->session->current_language) ?></a>
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_type) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="form-group">
				<div class="mobile-group-filter mobile-filter-condition" data-value="condition">
					<a href="javascript:void(0)" data-name="condition" data-value="all">All</a>
					<?php echo sizeof($filter_condition) > 0 ? ' | ' : '' ?>
					<?php $ctr = 0; ?>
					<?php foreach($filter_condition as $condition): ?>
					<a href="javascript:void(0)" data-name="condition" data-value="<?php echo $condition['fully_handled']; ?>"><?php echo get_language($condition['fully_handled'],$this->session->current_language) ?></a>
					<?php $ctr++; ?> 
					<?php echo $ctr < sizeof($filter_condition) ? ' | ' : '' ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="loading-container">
				<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
			</div>
			<div class="tab-content">
				<div id="card-list-container" class="tab-pane"></div>
				<div id="myDiv"></div>
				<div id="card-gallery-container" class="tab-pane active">
					<div class="row row-eq-height"></div><div class="text-center"><a class="load_gallery_view" href="javascript:void(0)" style="display: none"><?php echo get_language('Load More',$this->session->current_language) ?></a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Datestamp Modal -->
<!-- <div id="datestampModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
 -->
    <!-- Modal content-->
<!--     <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
        <p>Prerelease.</p>
        <select class="form-control">
        	<option value="">All</option>
        	<?php if(!empty($datestamp)): ?>
        		<?php foreach ($datestamp as $key => $value) : ?>
        			<option value="<?php echo $value['datestamp'] ?>"><?php echo $value['datestamp'] ?></option>
        		<?php endforeach; ?>
        	<?php endif; ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-apply" data-dismiss="modal">Apply</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> -->

<script type="text/javascript">
	c_language = '<?php echo $this->session->userdata("language") ?>';
	var search_name = '<?php echo $search_name ?>';
	var cat_code = '<?php echo $cat_code ?>';
	var display_type = '<?php echo get('disp') ?>';
	var session_sort = JSON.parse('<?php echo json_encode($session_sort) ?>');
	var watchlist_data = JSON.parse('<?php echo json_encode($watchlist_data) ?>');
</script>