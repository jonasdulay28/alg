<div class="container authentication">
	<!-- Sing in  Form -->
  <section class="sign-in">
      <div class="container">
          <div class="signin-content">
              <div class="signin-image">
                  <figure><img src="<?php echo images_bundle('logo8.png')?>" alt="sing up image" style="height: 160px;"></figure>
                  <center class="hidden-xs hidden-sm">
                  	<button    class="form-submit signup-image-link" style="background: #e29922;color:white;margin-top: -3px;"><?php echo get_language('Create a New Account', $this->session->current_language)?></button>
                  	<button    class="form-submit signin-image-link" style="background: #e29922;color:white;margin-top: -30px;"><?php echo get_language('Login here', $this->session->current_language)?></button>
                  </center>
              </div>
              <div class="signin-form">
                  <?php echo form_open("#","method='POST' id='login-form' class='login-form'") ?>
                  <input type="hidden" value="<?php echo (get('redirect') != '' ? get('redirect') : '')?>" name="redirect_url">
                      <div class="form-group">
                      	<label><?php echo get_language('Email Address', $this->session->current_language)?>:</label>
                          <input type="text" name="email" class="form-control" id="your_name" placeholder="<?php echo get_language('Email Address', $this->session->current_language)?>" required/>
                      </div>
                      <div class="form-group">
                          <label><?php echo get_language('Password', $this->session->current_language)?>: </label>
                          <input type="password" class="form-control" name="password" id="login_password" placeholder="<?php echo get_language('Password', $this->session->current_language)?>" required/>
                      </div>
                      <div id="result"></div>
                      <div class="form-group remember_me_container">
                          <input type="checkbox" name="remember_me" value="remember" id="remember-me" class="agree-term" checked="" />
                          <label for="remember-me" class="label-agree-term" ><span><span></span></span><?php echo get_language('Remember me', $this->session->current_language)?></label><br>
                          <small><a href="javascript:void(0)" class="forgot_password" id="forgot-password-modal"><?php echo get_language('Forgot your password', $this->session->current_language)?>?</a></small>
                      </div>
                      <div class="form-group form-button">
                      	<button  id="signin" class="form-submit login" ><?php echo get_language('Login', $this->session->current_language)?></button>
                      	 <div class="hidden-md hidden-lg">
                      	 	<br>	
	                      	<button  class="form-submit signup-image-link" style="background: #e29922;color:white;margin-top: 10px;padding: 15px 17px;margin-top: 0px;"><?php echo get_language('Create Account', $this->session->current_language)?></button>
	                      </div>
                      </div>
                     	
                 <?php echo form_close() ?>
                  <!-- <div class="social-login">
                      <span class="social-label">Or login with</span>
                      <ul class="socials">
                          <li><a href="#"><i class="display-flex-center fab fa-facebook"></i></a></li>
                          <li><a href="#"><i class="display-flex-center fab fa-google"></i></a></li>
                      </ul>
                  </div> -->
              </div>
              <div class="signup-form">
                  <h2 class="form-title"><?php echo get_language('Sign up', $this->session->current_language)?></h2>
                  <?php echo form_open("#","method='POST' id='register-form' class='register-form'") ?>
                      <input type="hidden" value="<?php echo (get('redirect') != '' ? get('redirect') : '')?>" name="redirect_url">
                      <div class="form-group">
                      	<label><?php echo get_language('Email Address', $this->session->current_language)?>:</label>
                          <input type="email" name="email" class="form-control" id="" placeholder="<?php echo get_language('Email Address', $this->session->current_language)?>"/>
                      </div>
                      <div class="form-group">
                          <label><?php echo get_language('Password', $this->session->current_language)?>: </label>
                          <input type="password" class="form-control password" name="password" id="" placeholder="<?php echo get_language('Password', $this->session->current_language)?>"/>
                      </div>
                      <div class="form-group">
                          <label><?php echo get_language('Confirm Password', $this->session->current_language)?>: </label>
                          <input type="password" class="form-control cf_password" name="cf_password" id="" placeholder="<?php echo get_language('Confirm Password', $this->session->current_language)?>"/>
                      </div>
                      <div id="result2"></div>
                      <small class="terms">Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our <a href="https://www.alg.tw/privacy-2/" class="woocommerce-privacy-policy-link" target="_blank">privacy policy</a>.</small>
                      <div class="form-group form-button">
                          <input type="submit" name="signin" id="signin" class="form-submit  register" value="<?php echo get_language('Register', $this->session->current_language) ?>"/>
                          <div class="hidden-md hidden-lg">
                      	 	<br>	
	                      	<button    class="form-submit signin-image-link" style="background: #e29922;color:white;margin-top: 0px;"><?php echo get_language('Login here', $this->session->current_language)?></button>
	                      </div>
                      </div>
                  <?php echo form_close() ?>
              </div>
          </div>
      </div>
  </section>
</div>

<!-- Modal -->
<div id="forgot-password-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo get_language('Forgot Password', $this->session->current_language)?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label><?php echo get_language('Email Address', $this->session->current_language)?>: </label>
            <input type="text" class="form-control" name="email_address" id="" placeholder="<?php echo get_language('Email Address', $this->session->current_language)?>"/><br>
            <center><button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_language('Submit', $this->session->current_language)?></button> </center>
        </div>
      </div>
    </div>

  </div>
</div>