<div class="container all_sets_container">
	<div class="section_container">
		<h3 class="section-title section-title-center"><b></b><span class="section-title-main">All Sets A-Z</span><b></b></h3>
	</div>
	<Br>
	<div class="row all_sets_container">
		<div class="col-md-6">
			<?php
			$tmp_char = "#";

			$sets = $main_category;

			$set_code = $main_set_code;
			$combined_set = array_combine($sets, $set_code);

			sort($sets);

			$second_row = ["J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
			$not_included = [
				"15th anniversary",
				"Archenemy schemes",
				'Archenemy: Nicol Bolas "Schemes"',
				'Archenemy "Schemes"',
				"Celebration cards",
				"Defeat a god",
				"Duels of the planeswalkers",
				"Foreign black border",
				"Hachette UK",
				"Hero's Path",
				"Heroes of the Realm",
				"Ixalan treasure chest",
				"Legacy championship",
				"Legendary cube",
				"Magic premiere shop",
				"Renaissance",
				"Summer of magic",
				"Two headed giant tournament",
				"Vintage championship",
				"Vintage masters",
				"Welcome deck 2016",
				"Welcome deck 2017",
				"World's",
				"You make the cube"
			];
			?>

			<?php for ($x = 0; $x < count($sets); $x++) { ?>
				<?php if ($sets[$x]) { ?>
					<?php if (in_array($sets[$x][0], $second_row) == 0) { ?>
						<?php if ($tmp_char != $sets[$x][0]) {
							if ($x > 0 && is_numeric($sets[$x][0]) == false) {
						?>
								<div class="set_header">
									<?php echo is_numeric($sets[$x][0]) ? "#" : $sets[$x][0] ?>
								</div>
							<?php } elseif ($x == 0) { ?>
								<div class="set_header">
									<?php echo is_numeric($sets[$x][0]) ? "#" : $sets[$x][0] ?>
								</div>
						<?php }
						} ?>
						<p><a href="<?php echo base_url('cards') . '?cat=' . $combined_set[$sets[$x]] ?>">
								<?php echo isset($sets_translations[$sets[$x]]) ? $sets_translations[$sets[$x]] . ' ' . $sets[$x] : $sets[$x] ; ?></a>
						</p>
						<?php
						if ($tmp_char != $sets[$x][0]) $tmp_char = $sets[$x][0]; ?>
			<?php }
				}
			} ?>
		</div>
		<div class="col-md-6">
			<?php
			$tmp_char = "#";
			$second_row = ["J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
			for ($x = 0; $x < count($sets); $x++) { ?>
				<?php if ($sets[$x]) { ?>
					<?php if (in_array($sets[$x][0], $second_row)) { ?>
						<?php if ($tmp_char != $sets[$x][0]) { ?>
							<div class="set_header">
								<?php echo is_numeric($sets[$x][0]) ? "#" : $sets[$x][0] ?>
							</div>
						<?php } ?>
						<p><a href="<?php echo base_url('cards') . '?cat=' . $combined_set[$sets[$x]] ?>"><?php echo isset($sets_translations[$sets[$x]]) ? $sets_translations[$sets[$x]] . ' ' . $sets[$x] : $sets[$x] ; ?></a></p>
						<?php
						if ($tmp_char != $sets[$x][0]) $tmp_char = $sets[$x][0]; ?>
			<?php }
				}
			} ?>
		</div>
	</div>
</div>