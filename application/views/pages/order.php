<div class="container cart_page" style="min-height: 360px;">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
	    <li class="breadcrumb-item"><a href="<?php echo base_url('my_account') ?>">My Account</a></li>
	    <li class="breadcrumb-item active" aria-current="page">View Order</li>
	  </ol>
	</nav>
	<div class="row">
		<div class="col-md-12">
			<?php if($order->payment_mode == 'Bank Transfer'): ?>
			<h3 class="text-center">Bank Account Details: </h3>
			<p class="text-center" style="">
				<span style="text-align: justify;"><b >
					Bank Name: Kasikorn Bank<br>
					Account Name: Hobby Fusion<br>
					Account Number: 055-1-48339-8
				</b></span>
			</p>
			<center>
				<p class="text-center" style="width: 70%;">Thank you for placing your order. The items in your order have been reserved. Kindly make payment via bank transfer within the next 72 hours otherwise your order may be canceled. Once payment is received, your order will be shipped out within 1 - 2 business days! Thank you for your continued support!
			</p>
			</center>
			<?php endif; ?>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-7">
			<?php if($order->cart_list): ?>
				<table class="table cart_table">
					<thead>
						<th>Product</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total</th>
					</thead>
					<tbody>
						<?php $cart_list = json_decode($order->cart_list) ?>
						<?php foreach($cart_list as $key => $card){ ?>
						<tr>
							<td> 
								<a href="<?php echo base_url("cards/view/") . $card->card->id ?>"><?php echo getCardName($card->card, 'json'); ?></a>
							</td>
							<td><p><?php echo main_currency_sign ?> <?php echo $card->variation == 'Foil' ? $card->card->foil_price : $card->card->regular_price ?></p></td>
							<td><?php echo $card->quantity ?></td>
							<td><p><?php echo main_currency_sign ?> <?php echo (int)$card->quantity * ($card->variation == 'Foil' ? (double)$card->card->foil_price : (double)$card->card->regular_price) ?></p></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
		<div class="col-md-5">
			<h4>Payment Details</h4>
			<?php $payment = json_decode($order->payment_details) ?>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td>Invoice Number:</td>
						<td><?php echo $order->invoice_number ?></td>
					</tr>
					<tr>
						<td>Payment Status</td>
						<td><?php echo $order->status == 1 ? 'Paid' : 'Unpaid' ?></td>
					</tr>
					<tr>
						<td>Shipping Fee:</td>
						<td><?php echo main_currency_sign ?> <?php echo $payment->shipping_fee ?></td>
					</tr>
					<tr>
						<td>Sub Total:</td>
						<td><?php echo main_currency_sign ?> <?php echo $payment->sub_total ?></td>
					</tr>
					<tr>
						<td>Total Amount:</td>
						<td><?php echo main_currency_sign ?> <?php echo $payment->total_amount ?></td>
					</tr>
					<tr>
						<td>Points Earned:</td>
						<td><?php echo $order->points_earned ?></td>
					</tr>
					<tr>
						<td>Status:</td>
						<td><?php echo check_status($order->admin_status) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>