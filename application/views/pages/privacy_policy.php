<div class="container privacy_policy">
	<div class="section_container"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >
	PRIVACY POLICY
	</span><b></b></h3></div>
	<p class="text-center"><b>PLEASE READ OUR PRIVACY POLICY CAREFULLY BEFORE USING THIS WEBSITE.</b></p>
	<div class=" privacy_policy_details">
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">What Information We Collect & How We Use It</h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>What Information We Collect and How We Use It </b><br>
					When you visit our website, ALG.tw collects your IP address, which we use to improve our products and services as well as to help us administer our website and diagnose technical problems. Your IP address is also used to gather broad demographic information that does not personally identify you.
					<br><br>
					To receive our website’s newsletters and/or periodic email notifications with special offers, you only need to leave your email address with us. However, to order from us, you must set up a ALG.tw account by providing us with your name, email address and a chosen password. These constitute your “Account Information”. Account Information also includes any information that you may provide to us in response to customer surveys. When you place an order with us, we will also collect your shipping address and telephone number, as well as credit card number, billing address, and card expiration date, if applicable. This information constitutes your “Order Information”. Order Information is necessary for us both to process your order and notify you of your order status. Order Information also includes any information that you provide us in connection with an order that you later cancel or that is not completed for any reason.
					<br><br>
					We may use your Account Information and Order Information to occasionally notify you about important changes to the website, new ALG.tw features, special ALG.tw offers that you may find useful, or offers from companies with whom we have business relationships.
					<br><br>
					We may also contact you if you have entered one of our lucky draw contests or responded to one of our surveys. Choosing not to receive email newsletters or email notifications from us when you enter a lucky draw contest will not affect your chances of winning. If you would prefer not to receive email newsletters or email notifications from us (other than email notifications relating to order processing), simply visit the “My Account” section.
					<br><br>
					In addition, to protect against the loss, misuse, and alteration of the information under our control we have implemented security measures at this website.
				</p>
			</div>
		</div>	
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Making Changes To Your Information</h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Making Changes To Your Information</b><br>
					You may change your personal information on file with ALG.tw at any time. To do this, simply visit the “My Account” section to edit your personal information.
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Cookies</span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Cookies</b><br>
The ALG.tw website employs “cookies,” which are small pieces of information that are stored by your browser on your computer’s hard drive. Our cookies enable us both to provide a Shopping Cart and to store your shipping and billing information between visits. If you have set your browser to reject cookies, you can still use our site, although you may need to re-enter information that would normally be obtained from the cookie.
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Children’s Online Privacy</h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Children’s Online Privacy</b><br>
At ALG.tw, we believe that the privacy of children is important. We do not monitor the age of our user audience except in connection with surveys and contests, and except for age data optionally provided by users who wish to customize their account preferences, However, visitors and users under the age of thirteen (13) should not submit personal identifying information to MTGMintCard.com without the consent of their parent(s) or guardian(s). If you are a parent and want to limit your children’s access to material you believe may be harmful to them, you may want to look into commercially available hardware, software and filtering devices.
				</p>
			</div>
		</div>	
		<hr>
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Unsubscribe From Newsletters</span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Unsubscribe From Newsletters</b><br>
					You can easily stop receiving our newsletters at any time. Simply click on the unsubscribe link at the bottom of any newsletter you’ve already received.
				</p>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Changes to Privacy Policy</span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Changes to Privacy Policy</b><br>
Please check back frequently, as this Privacy Policy may change from time to time. The date of the most recent update to the Privacy Policy will be indicated at the top of this page. In the event of a material change to the Privacy Policy, we will add “Updated”, “New”, or a similar term near the link to the Privacy Policy on the home page of MTGMintCard.com and elsewhere throughout the site.
				</p>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-3">
				<h3 class="privacy_policy_header">Contact Us</span></h3>
			</div>
			<div class="col-md-offset-1 col-md-8">
				<p>
					<b>Contact Us </b><br>
If you have any questions about these Terms, please contact us. We’ll be happy to answer any queries you might have! Have a great day ahead!
				</p>
			</div>
		</div>	
	</div>
</div>