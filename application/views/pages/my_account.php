<div class="my_account">
	<div class="subheader_container" style="background: ">
		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<h3>MY ACCOUNT</h3>
				<p>Dashboard</p>
			</div>
		</div>
		</div>
	</div>
	<div class="container my_account_contents">
		<div class="tabbable tabs-left">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#dashboard" data-toggle="tab">Dashboard</a></li>
        <li ><a href="#orders" data-toggle="tab">Orders</a></li>
        <li><a href="#billing_address" data-toggle="tab">Billing Address</a></li>
        <li><a href="#shipping_address" data-toggle="tab">Shipping Address</a></li>
        <!-- <li><a href="#my_wallet" data-toggle="tab">My Wallet</a></li> -->
        <li><a href="#account_details" data-toggle="tab">Account Details</a></li>
        <!-- <li><a href="#my_points" data-toggle="tab">My Points</a></li> -->
        <!-- <li><a href="#wishlist" data-toggle="tab">Wishlist</a></li> -->
        <li><a href="<?php echo base_url('my_account/logout')?>" >Logout</a></li>
      </ul>
      <div class="tab-content">
       <div class="tab-pane dashboard active" id="dashboard">
       	<p>Hello <b><?php echo get_username()?></b> (not <b><?php echo get_username()?></b>? <a href="<?php echo base_url('my_account/logout')?>">Log out</a>)</p>
       	<br>
       	<p>From your account dashboard you can view your <a href="javascript:void(0);" class="sub_cta_btn orders_button" data-tab="#orders">recent orders</a>, <a href="javascript:void(0);" class="sub_cta_btn bill_address_button" data-tab="#shipping_address">manage your shipping</a> and <a href="javascript:void(0);" class="sub_cta_btn shipping_address_button" data-tab="#billing_address">billing addresses</a>, and <a href="javascript:void(0);" class="sub_cta_btn account_details_button" data-tab="#account_details">edit your password and account details</a>.</p>
       	<div class="container-fluid hidden-sm hidden-xs">
       		<div class="row">
	       		<div class="col-xs-3 col-sm-3 col-md-3">
	       			<button class="btn btn-default sub_cta_btn orders_button" data-tab="#orders">Orders</button>
	       		</div>
	       		<div class="col-xs-3 col-sm-3 col-md-3">
	       			<button class="btn btn-default sub_cta_btn bill_address_button" data-tab="#billing_address">Billing Address</button>
	       		</div>
	       		<div class="col-xs-3 col-sm-3 col-md-3">
	       			<button class="btn btn-default sub_cta_btn shipping_address_button" data-tab="#shipping_address">Shipping Address</button>
	       		</div>
	       		<div class="col-xs-3 col-sm-3 col-md-3">
	       			<button class="btn btn-default sub_cta_btn account_details_button" data-tab="#account_details">Account Details</button>
	       		</div>
	       		<!-- <div class="col-xs-3 col-sm-3 col-md-3">
	       			<button class="btn btn-default sub_cta_btn" data-tab="#my_points">My Points</button>
	       		</div> -->
	       		<!-- <div class="col-xs-3 col-sm-3 col-md-3">
	       			<button class="btn btn-default sub_cta_btn" data-tab="#wishlist">Wishlist</button>
	       		</div> -->
	       	</div>
       	</div>
       </div>
       <div class="tab-pane orders" id="orders">
       	<div class="container">
       		  <div class="row">
       		  	<div class="col-xs-12 col-sm-8 col-md-8">
       		  		<?php $this->load->view('pages/my_account/orders', $data); ?>
       		  	</div>
       		  </div>
       	</div>

       </div>
       <div class="tab-pane addresses" id="billing_address">
       	<center><p>The following <b>Billing Address</b> will be used on the checkout page by default.</p></center>
       	<div class="container">
       		<div class="row">
       			<div class="col-xs-12 col-sm-8 col-md-8">
       				<?php $this->load->view('pages/my_account/billing_address',$data); ?>
       			</div>
       		</div>
       	</div>
       </div>

       <div class="tab-pane addresses" id="shipping_address">
       	<center><p>The following <b>Shipping Address</b> will be used on the checkout page by default.</p></center>
       	<div class="container">
       		<div class="row">
       			<div class="col-xs-12 col-sm-8 col-md-8">
       				<?php $this->load->view('pages/my_account/shipping_address'); ?>
       			</div>
       		</div>
       	</div>
       </div>

       <div class="tab-pane addresses" id="my_wallet">
       	<div class="container">
       		<div class="row">
       			<div class="col-xs-12 col-sm-8 col-md-8">
       				<?php //$this->load->view('pages/my_account/my_wallet'); ?>
       			</div>
       		</div>
       	</div>
       </div>

       <div class="tab-pane addresses" id="account_details">
       	<center><p>The following addresses will be used on the checkout page by default.</p></center>
       	<div class="container">
       		<div class="row">
       			<div class="col-xs-12 col-sm-8 col-md-8">
       				<?php $this->load->view('pages/my_account/account_details'); ?>
       			</div>
       		</div>
       	</div>
       </div>

       <div class="tab-pane addresses" id="my_points">
       	<div class="container">
       		<div class="row">
       			<div class="col-xs-12 col-sm-8 col-md-8">
       				<?php $this->load->view('pages/my_account/my_points'); ?>
       			</div>
       		</div>
       	</div>
       </div>

       <div class="tab-pane addresses" id="wishlist">
       	<center><p>The following addresses will be used on the checkout page by default.</p></center>
       	<div class="container">
       		<div class="row">
       			<div class="col-xs-12 col-sm-8 col-md-8">
       				<?php $this->load->view('pages/my_account/wishlist'); ?>
       			</div>
       		</div>
       	</div>
       </div>
      </div>
    </div>
	</div>
</div>