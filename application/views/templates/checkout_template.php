<!DOCTYPE html>
<html>
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-139871344-1');
  </script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="name" content="ALG.tw" />
  <meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />	
  <meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
  <meta name="author" content="ALG">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta property="og:url"           content="<?php echo base_url()?>" />
  <meta property="og:type"          content="ALG.tw" />
  <meta property="og:title"         content="ALG.tw - Taiwan's Premier Hobby Store!" />
  <meta property="og:description"   content="Selling Cards for Magic the Gathering" />	
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png')?>">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet"> 
	
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('carousel.css')?>">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/datatables/css/datatables.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/keyrunes/css/keyrune.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('checkout.css') ?>">
  <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css')?>">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <style type="text/css">
    .checkout_page .btn_checkout {
      background: #340E5B;
      width: 100%;
      padding:15px 20px;
       text-transform: uppercase;
    }

    .hidden_checkout_btn {
      background: #340E5B;
    }

    .checkout_page .btn_checkout:hover {
      background: #340E5B;
    }

    .checkout_page .credit_card {
      height: 40px;
      margin-top: 15px;
    }

    .checkout_page .checkout_title {
      font-weight: 600;
      font-size: 16px;
      color:#333;
      text-transform: uppercase;
    }

    .checkout_page hr {
      margin-top: 10px;
      margin-bottom: 0px;
    }

    .checkout_page .satisfaction_guarantee_text {
      font-size: 12px;
    }
    .checkout_page .row {
      margin-bottom: 10px;
    }
    .checkout_page .checkout_label{
      text-transform: uppercase;
      letter-spacing: 1px;
      color:#333;
      font-weight: 500;
      margin-bottom: 0px;
    }
    .checkout_page label {
      text-transform: uppercase;
      letter-spacing: 1px;
      color:#333;
      font-weight: 500;
      margin-bottom: 5px;
    }
    span.required {
      color:red;
    }
  </style>
</head>
<body>

	<?php if(is_logged() == 1)
				$this->load->view('includes/logged_header',$data);
		 	else
				$this->load->view('includes/header',$data);
	?>
	<div id="app" style="margin-top: 20px; margin-bottom: 20px;">
		<?php $this->load->view($content) ?>
	</div>
	<?php $this->load->view('includes/footer'); ?>

</body>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/global.js?random=<?php echo uniqid(); ?>"></script>
<script type="text/javascript">
  var base_url = '<?php echo base_url() ?>';  
  var sub_total = '<?php echo $sub_total ?>';
  var shipping_fee = '<?php echo shipping_fee ?>';
  var int_shipping_fee = '<?php echo int_shipping_fee ?>';
  var free_shipping_fee = '<?php echo free_shipping_fee ?>';
  var free_int_shipping_fee = '<?php echo free_int_shipping_fee ?>';

  var total_amount = '<?php echo $sub_total ?>';
  var discount = 0;
  var purrcoins = parseFloat('<?php echo $this->session->userdata("purrcoins") ? $this->session->userdata("purrcoins") : 0 ?>');
  var coupon_discount = parseFloat('0.' + '<?php echo $this->session->userdata("coupons") ? $this->session->userdata("coupons")->discount_type == 'percentage' ? pad($this->session->userdata("coupons")->discount , 2) : 0 : 0 ?>');
  var coupon_value = parseFloat('<?php echo $this->session->userdata("coupons") ? $this->session->userdata("coupons")->discount_type != 'percentage' ? $this->session->userdata("coupons")->discount : 0 : 0 ?>');

  var single_card_rebate = parseFloat('<?php echo floatval(str_replace("%", "", $loyalty[0]->single_cards_rebate)) / 100 ?>');
  var other_card_rebate = parseFloat('<?php echo floatval(str_replace("%", "", $loyalty[0]->other_products_rebate)) / 100 ?>');
</script>
<script src="<?php echo scripts_bundle('sweetalert2.min.js')?>"></script>
<script type="text/javascript" src="<?php echo scripts_bundle('checkout.js') ?>?random=<?php echo uniqid(); ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript" src="<?php echo scripts_bundle('search_product.js')?>?random=<?php echo uniqid(); ?>"></script>
</html>