<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-139871344-1');
	</script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="name" content="ALG.tw" />
	<meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
	<meta name="author" content="ALG">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta property="og:url" content="<?php echo base_url() ?>" />
	<meta property="og:type" content="ALG.tw" />
	<meta property="og:title" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta property="og:description" content="Selling Cards for Magic the Gathering" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('slick-theme.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	<link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('carousel.css') ?>">
	<link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/src/dist/css/cart.css">
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/global.js?random=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript">
		var tw_text = {};
	</script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/language.js?random=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('search_product.js') ?>"></script>
	<style type="text/css">
		.card_name a:hover, .card_name a:active {
			outline: 0;
		}
		.card_name p {
			margin-bottom: 0px;
		}
		.top_products .slick-track {
			display: flex;
		    align-items: stretch;
		    justify-content: center;
		}
		.top_products .slick-track .slick-slide {
			height: auto !important;
		}
		.top_products .card-details {
			margin-bottom: 60px;
		}
		.card-info-container {
			bottom: 10px;
			position: absolute;
		}

		@media (max-width: 766px) and (min-width: 500px) {
			.datestamp_tag {
				font-size: 15px;
				bottom: 43%;
			    right: 24px;
			}
			.planeswalker_tag {
			    bottom: 45%;
			    right: 30px;
			}
		}

		@media (max-width: 600px) and (min-width: 500px) {
			.datestamp_tag {
			    font-size: 13px;
			    right: 22px;
			}
			.planeswalker_tag {
			    right: 25px;
			}
		}
	</style>
	<script type="text/javascript">
		var currency = "<?php echo $this->session->support_currency ? $this->session->support_currency : ""; ?>";
		var rate = "<?php echo $this->session->support_currency_rate ? $this->session->support_currency_rate : ""; ?>";
		var c_language = '';
		var site_language = '<?php echo $this->session->current_language  ? $this->session->current_language  : "traditional_chinese"; ?>';

		function sabai_with_us() {
			$(".sabai_with_us").not('.slick-initialized').slick({
				lazyLoad: 'ondemand',
				autoplay: true,
				dots: false,
				slidesToShow: 4,
				slidesToScroll: 4,
				responsive: [{
					breakpoint: 500,
					settings: {
						arrows: false,
						infinite: false,
						slidesToShow: 3,
						slidesToScroll: 3
					}
				}]
			});
		}

		function top_products() {
			setTimeout(function(){
			    $('.card-info-container').css({'width': $('.slick-slide .card_container').width() + 'px'});
			}, 100);
			$(".top_products").not('.slick-initialized').slick({
				lazyLoad: 'ondemand',
				autoplay: true,
				dots: false,
				slidesToShow: 6,
				slidesToScroll: 6,
				responsive: [{
					breakpoint: 992,
					settings: {
						arrows: false,
						infinite: false,
						slidesToShow: 4,
						slidesToScroll: 4
					}
				},{
					breakpoint: 767,
					settings: {
						arrows: false,
						infinite: false,
						slidesToShow: 2,
						slidesToScroll: 2
					}
				}]
			});
		}
		/*				var token = '8798059400.fbb5020.91a53ebe81a24428a78bb1273b762fe8', // learn how to obtain it below
						    userid = 8798059400, // User ID - get it in source HTML of your Instagram profile or look at the next example :)
						    num_photos = 20; // how much photos do you want to get
						 
						$.ajax({
							url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent', // or /users/self/media/recent for Sandbox
							dataType: 'jsonp',
							type: 'GET',
							data: {access_token: token, count: num_photos},
							success: function(data){
								for( x in data.data ){
									$('.sabai_with_us').append('<div><img src="'+data.data[x].images.low_resolution.url+'" class="img-responsive"></div>'); // data.data[x].images.low_resolution.url - URL of image, 306х306
									// data.data[x].images.thumbnail.url - URL of image 150х150
									// data.data[x].images.standard_resolution.url - URL of image 612х612
									// data.data[x].link - Instagram post URL 
								}
								sabai_with_us();
							},
							error: function(data){
								console.log(data); // send the error notifications to console
							}
						});*/
	</script>
	<style>
		.homepage .carousel {
			height:  auto !important;
		}
	</style>
</head>

<body>
	<?php
	if (is_logged() == 1)
		$this->load->view('includes/logged_header', $data);
	else
		$this->load->view('includes/header', $data);
	?>
	<!-- <div class="homepage">	
			<div class="container">	
					<section class="sabai_with_us">
					</section>
			</div>
		</div>	 -->
	<div id="app" style="margin-bottom: 20px;">
		<?php $this->load->view('pages/homepage', $banners); ?>
		<sabai-cart items=[]></sabai-cart>
	</div>
	<script type="text/javascript">
		setTimeout(function() {
			top_products();
		}, 1000)
	</script>

	<?php
	$this->load->view('includes/footer');
	?>
	<!-- Modal -->
	<div id="loyalty_modal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<img src="<?php echo images_bundle('badges/pearl.png') ?>" class="img-responsive center-block thank_you_badge ">
					<center>
						<p class="level_name">Pearl</p>
						<small class="level_number">Level 1</small>
						<br><br>
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:1%">
								0%
							</div>
						</div>
						<p class="level_left pull-left">Pearl</p>
						<p class="level_right pull-right"><b>Next Level:</b> Sapphire</p>
						<!-- <div class="block-container">
						  <div id="block" class="div-show block-main">
						    <div class="block">
						      <img src="<?php echo images_bundle('badges/pearl.png') ?>" class="img-responsive center-block thank_you_badge">
						    </div>
						  </div>
						  <div id="overlay"></div>
							</div>
							<button type="button" class="btn" onclick="onBtnClick();" id="btn">Snap</button> -->
						<br>
						<br>
						<div class="cta_container">
							<a href="<?php echo base_url('Loyalty_program') ?>"><button class="btn btn-default">Learn more about our Loyalty Program</button></a>
							<br><br>
							<a href="#" class="close_loyalty_modal" data-dismiss="modal">
								<p>Continue shopping</p>
							</a>
						</div>
					</center>
				</div>
			</div>

		</div>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


	<script type="text/javascript">
		var base_url = '<?php echo base_url() ?>';
		var cart_count = 0;

		var cart_data = JSON.parse('<?php echo addslashes(json_encode(getUpdatedCartList())) ?>');
		var cards_list = JSON.parse('<?php echo isset($cards_list) ? addslashes(json_encode($cards_list)) : json_encode([]) ?>');

		function pad(num, size) {
			var s = num + "";
			while (s.length < size) s = "0" + s;
			return s;
		}

		var cart_list = [];
		for (i in cart_data) {
			var img_src = getCardImage(cart_data[i].card);
			cart_data[i].card.img_src = img_src;
			cart_list.push(cart_data[i]);
		}
		var bus = '';
	</script>


	<script type="text/javascript">
		$(document).ready(function() {

			// <-- CartComponent !-->
			var $cart_trigger = $('#cd-cart-trigger');
			var $lateral_cart = $('#cd-cart');
			var $shadow_layer = $('#cd-shadow-layer');

			function toggle_panel_visibility() {
				if ($lateral_cart.hasClass('speed-in')) {
					$lateral_cart.removeClass('speed-in');
					$shadow_layer.removeClass('is-visible');
				} else {
					$lateral_cart.addClass('speed-in');
					$shadow_layer.addClass('is-visible');
				}
			}

			$(document).on('click', '.cd-cart-close', function() {
				$lateral_cart.removeClass('speed-in');
				$shadow_layer.removeClass('is-visible');
			});

			$(document).on('click', '#cd-cart-trigger', function() {
				toggle_panel_visibility();
			});

			$(document).on('click', '#cd-shadow-layer', function() {
				toggle_panel_visibility();
			});
			// <!-- CartComponent !-->

			//Now it will not throw error, even if called multiple times.
			$(window).on('resize', sabai_with_us);
			$(window).on('resize', top_products);
		})
		<?php if ($authentication_status == "registered") { ?>
			$('#loyalty_modal').modal('toggle');
			setTimeout(function() {
				$(".thank_you_badge").addClass("wobble-hor-bottom");
			}, 1500)
		<?php } ?>
		const button = $(".testbtn");

		$(".testbtn").on('click', function(e) {
			$("#myModal").modal('show');
		});
	</script>

	<script type="text/javascript">
		$(document).on("click", ".standard", function(e) {
			$("#myModal").modal("show");
			$('a[href="#tab1"').click()
		})
		$(document).on("click", ".modern", function(e) {
			$("#myModal").modal("show");
			$('a[href="#tab2"').click()
		})
		$(document).on("click", ".legacy", function(e) {
			$("#myModal").modal("show");
			$('a[href="#tab3"').click()
		})
		$(document).on("click", ".commander", function(e) {
			$("#myModal").modal("show");
			$('a[href="#tab5"').click()
		})
		$(document).on("click", ".old_school", function(e) {
			$("#myModal").modal("show");
			$('a[href="#tab4"').click()
		})
	</script>

	<script src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
	
	<script type="text/javascript" src="<?php echo scripts_bundle('add_to_cart.js') ?>?random=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>
</body>

</html>