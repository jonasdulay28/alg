<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-139871344-1');
		</script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="name" content="ALG.tw" />
  <meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />	
  <meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
  <meta name="author" content="ALG">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta property="og:url"           content="<?php echo base_url()?>" />
  <meta property="og:type"          content="ALG.tw" />
  <meta property="og:title"         content="ALG.tw - Taiwan's Premier Hobby Store!" />
  <meta property="og:description"   content="Selling Cards for Magic the Gathering" />	
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png')?>">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css')?>">
	<link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('carousel.css')?>">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/datatables/css/datatables.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/keyrunes/css/keyrune.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/mtg-font/css/magic-font.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/src/dist/css/cart.css">
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>
		 <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
	<?php if(isset($styles)): ?>
		<?php foreach($styles as $style): ?>
			<?php echo $style ?>
		<?php endforeach; ?>
  <?php endif; ?>
  <style type="text/css">
	.add-to-cart-quantity {
		color:white !important;
	}
	.accessories-container img {
		width: 60% !important;
		position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
	}
	<?php 
	$rate = $this->session->support_currency_rate ? $this->session->support_currency_rate : "";
	if($rate) {
	?>
	#card-gallery-container .card-price {
	    height: 180px;
	}
	<?php 
	} ?>
</style>
<script type="text/javascript">
	var c_language = '';
	var site_language = '<?php echo $this->session->current_language  ? $this->session->current_language  : "traditional_chinese"; ?>';
</script>
</head>
<body>
	<?php if(is_logged() == 1)
				$this->load->view('includes/logged_header',$data);
		 	else
				$this->load->view('includes/header',$data);
	?>


	<div id="app" style="margin-top: 20px; margin-bottom: 20px;">
		<?php $this->load->view($content) ?>
		<?php if($content != 'pages/cart'): ?>
		<sabai-cart items=[]></sabai-cart>
		<?php endif ?>
	</div>


	<?php $this->load->view('includes/footer'); ?>

	<div class="loading_screen">
		<img src="<?php echo base_url() ?>assets/images/index.cutie-fox-spinner.gif">
	</div>

</body>
<script type="text/javascript">
	var tw_text = {};
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/language.js?random=<?php echo uniqid(); ?>"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/global.js?random=<?php echo uniqid(); ?>"></script>
<?php 
	$cart_data = '[]';
	if(getUpdatedCartList() != NULL) {
		$cart_data = addslashes(json_encode(getUpdatedCartList()));
	}
?>
<script type="text/javascript">
	var currency = "<?php echo $this->session->support_currency ? $this->session->support_currency : ""; ?>";
	var rate = "<?php echo $this->session->support_currency_rate ? $this->session->support_currency_rate : ""; ?>";
	var base_url = '<?php echo base_url() ?>';
	var cart_count = 0;

	var cart_data = JSON.parse('<?php echo $cart_data;?>');
	var cards_list = JSON.parse('<?php echo isset($cards_list) ? addslashes(json_encode($cards_list)) : json_encode([]) ?>');

	function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
	}

	var cart_list = [];
	for (i in cart_data) {
		var img_src = getCardImage(cart_data[i].card);
		cart_data[i].card.img_src = cart_data[i].card.img_src ? cart_data[i].card.img_src : img_src;
	  cart_list.push(cart_data[i]);
	}
	var bus = '';
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>
<script src="<?php echo scripts_bundle('sweetalert2.min.js')?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/libraries/datatables/js/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo scripts_bundle('add_to_cart.js') ?>?random=<?php echo uniqid(); ?>"></script>
		<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.19/sorting/currency.js"></script>

<?php if($content == 'pages/card_view'): ?>
<script type="text/javascript">
	var card_category = JSON.parse('<?php echo addslashes(json_encode($card_category)) ?>');
</script>
<?php endif; ?>

<?php if(isset($scripts)): ?>
		<?php foreach($scripts as $script): ?>
			<?php echo $script ?>
		<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function(){

		// <-- CartComponent !-->
		var $cart_trigger = $('#cd-cart-trigger');
		var $lateral_cart = $('#cd-cart');
		var $shadow_layer = $('#cd-shadow-layer');

		function toggle_panel_visibility() {
			if( $lateral_cart.hasClass('speed-in') ) {
				$lateral_cart.removeClass('speed-in');
				$shadow_layer.removeClass('is-visible');
			} else {
				$lateral_cart.addClass('speed-in');
				$shadow_layer.addClass('is-visible');
			}
		}

		$(document).on('click', '.cd-cart-close', function(){
				$lateral_cart.removeClass('speed-in');
				$shadow_layer.removeClass('is-visible');
		});

		$(document).on('click', '#cd-cart-trigger', function(){
			toggle_panel_visibility();
		});

		$(document).on('click', '#cd-shadow-layer', function(){
			toggle_panel_visibility();
		});
		// <-- CartComponent !-->

		 // Get the form fields and hidden div
  var checkbox = $("#use_purrcoins");
  var hidden = $("#purrcoins");
  
  // Setup an event listener for when the state of the 
  // checkbox changes.
  checkbox.on("change",function() {
    if (checkbox.is(':checked')) {
      hidden.css({'display':'block'});
    } else {
      hidden.css({'display':'none'});
    }
	})

	$(document).on('click',".paginate_button", function() {
	  $('html, body').animate({
	    scrollTop: $(".dataTables_wrapper").offset().top
	  }, 'slow');
	});
});
</script>
<script type="text/javascript" src="<?php echo scripts_bundle('search_product.js')?>?random=<?php echo uniqid(); ?>"></script>
</html>