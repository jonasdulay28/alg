<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-139871344-1');
	</script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="name" content="ALG.tw" />
	<meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
	<meta name="author" content="ALG">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta property="og:url" content="<?php echo base_url() ?>" />
	<meta property="og:type" content="ALG.tw" />
	<meta property="og:title" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta property="og:description" content="Selling Cards for Magic the Gathering" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('slick-theme.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('carousel.css') ?>">
	<link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/src/dist/css/cart.css">
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<style type="text/css">
		.rainbow {
			position: absolute;
			left: 0;
			top: 0;
			z-index: 1
		}

		.foil:hover {
			-webkit-animation: 1s linear infinite foilHue;
			animation: 1s linear infinite foilHue;
			-moz-animation: 1s linear infinite foilHue;
			-ms-animation: foilHue 1s linear infinite
		}

		@keyframes foilHue {
			100% {
				-webkit-filter: hue-rotate(360deg)
			}
		}
	</style>
</head>

<body>
	<?php
	if (is_logged() == 1)
		$this->load->view('includes/logged_header', $data);
	else
		$this->load->view('includes/header', $data);
	?>
	<div id="app" style="margin-bottom: 20px;">
		<?php $this->load->view('pages/homepage'); ?>
		<sabai-cart items=[]></sabai-cart>
	</div>
	<?php
	$this->load->view('includes/footer');
	?>
	<!-- Modal -->
	<div id="loyalty_modal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<img src="<?php echo images_bundle('badges/pearl.png') ?>" class="img-responsive center-block thank_you_badge ">
					<center>
						<p class="level_name">Pearl</p>
						<small class="level_number">Level 1</small>
						<br><br>
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:1%">
								0%
							</div>
						</div>
						<p class="level_left pull-left">Pearl</p>
						<p class="level_right pull-right"><b>Next Level:</b> Sapphire</p>
						<!-- <div class="block-container">
						  <div id="block" class="div-show block-main">
						    <div class="block">
						      <img src="<?php echo images_bundle('badges/pearl.png') ?>" class="img-responsive center-block thank_you_badge">
						    </div>
						  </div>
						  <div id="overlay"></div>
							</div>
							<button type="button" class="btn" onclick="onBtnClick();" id="btn">Snap</button> -->
						<br>
						<br>
						<div class="cta_container">
							<a href="<?php echo base_url('Loyalty_program') ?>"><button class="btn btn-default">Learn more about our Loyalty Program</button></a>
							<br><br>
							<a href="#" class="close_loyalty_modal" data-dismiss="modal">
								<p>Continue shopping</p>
							</a>
						</div>
					</center>
				</div>
			</div>

		</div>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('search_product.js') ?>"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('add_to_cart.js') ?>?random=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>
</body>

</html>