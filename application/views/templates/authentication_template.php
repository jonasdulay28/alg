<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-139871344-1');
	</script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="name" content="ALG.tw" />
	<meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
	<meta name="author" content="ALG">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta property="og:url" content="<?php echo base_url() ?>" />
	<meta property="og:type" content="ALG.tw" />
	<meta property="og:title" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta property="og:description" content="Selling Cards for Magic the Gathering" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo styles_bundle('ladda.min.css') ?>">
	<link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('authentication.css') ?>">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId: '2172564809529982',
				cookie: true,
				xfbml: true,
				version: 'v3.2'
			});

			FB.AppEvents.logPageView();

		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s);
			js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
</head>

<body>
	<?php
	$this->load->view('includes/header', $data);
	$this->load->view($content);
	$this->load->view('includes/footer');
	?>
	<div class="loading_screen">
		<img src="<?php echo base_url() ?>assets/images/index.cutie-fox-spinner.gif">
		<div id="loading_percentage"> 0%</div>
	</div>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
	<script src="<?php echo scripts_bundle('spin.min.js') ?>"></script>
	<script src="<?php echo scripts_bundle('ladda.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>?random=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript">
		function progress() {
			let percent = document.getElementById('loading_percentage');
			let count = 5;
			let progress = 25;
			let id = setInterval(frame, 50);

			function frame() {
				if (count == 100) {
					clearInterval(id);
					return;
				}
				count += 5;
				percent.innerHTML = count + "%";
			}
		}

		var base_url = '<?php echo base_url() ?>';
		var redirect = '<?php get("redirect") ?>';


		$(document).on("click", ".signup-image-link", function(e) {
			e.preventDefault();
			$(".signin-form").hide();
			$(".signup-image-link").hide();
			$(".signup-form").show();
			$(".signin-image-link").show();
		})

		$(document).on("click", ".signin-image-link", function(e) {
			e.preventDefault();
			$(".signin-form").show();
			$(".signup-image-link").show();
			$(".signup-form").hide();
			$(".signin-image-link").hide();
		})

		if (redirect == "checkout") {
			$(".signup-image-link").click()
		}

		$(".forgot_password").on("click", function(e) {
			swal({
				title: 'Forgot Password',
				input: 'email',
				inputPlaceholder: 'Email address',
				inputAttributes: {
					required: true,
				},
				customClass: 'forgot_password_modal',
				showCancelButton: true,
				confirmButtonText: 'Send',
				showLoaderOnConfirm: true,
				allowOutsideClick: function() {
					!swal.isLoading()
				}
			}).then(function(result) {
				if (result != '') {
					var post_url = "<?php echo base_url('Authentication/send_forgot_password') ?>";
					var email = result;
					$.ajax({
						type: 'POST',
						url: post_url,
						data: {
							'email': email
						},
						dataType: "json",
						beforeSend: function() {
							loading_class('forgot_password_modal');
						},
						success: function(res) {
							close_loading();
							if (res.message == "success") {
								notify2("Please check your email", "We have already sent the password key.", "success");
							} else {
								notify2("Forgot password not allowed", res.message, "error");
							}
						},
						error: function(res) {
							console.log(res);
						}
					});
				}
			})
		})

		$(document).on("submit", '#login-form', function(e) {
			e.preventDefault();
			var post_url = '<?php echo base_url('authentication/login'); ?>';
			var l = Ladda.create(document.querySelector('.login'));
			$.ajax({
				type: 'POST',
				url: post_url,
				data: $('#login-form').serialize(),
				dataType: "json",
				beforeSend: function() {
					l.start();
				},
				success: function(res) {
					l.stop();
					if (res.message == "success") {
						$('.loading_screen').show();
						progress();
						if (redirect == 'checkout') {
							window.location.href = base_url + 'cart';
							return;
						}
						window.location.href = res.url;
					} else {
						$('#login_password').val('');
						$("#result").html(res.message);
					}
				},
				error: function(res) {
					console.log(res);
				}
			});
		});

		$(document).on("submit", '#register-form', function(e) {
			e.preventDefault();
			var post_url = '<?php echo base_url('authentication/register'); ?>';
			var l = Ladda.create(document.querySelector('.register'));
			$.ajax({
				type: 'POST',
				url: post_url,
				data: $('#register-form').serialize(),
				dataType: "json",
				beforeSend: function() {
					l.start();
				},
				success: function(res) {
					l.stop();
					if (res.message == "success") {
						$('.loading_screen').show();
						progress();
						if (redirect == 'checkout') {
							window.location.href = base_url + 'cart';
							return;
						}
						window.location.href = res.url;
					} else {
						$('.password').val('');
						$('.cf_password').val('');
						$("#result2").html(res.message);
					}
				},
				error: function(res) {
					console.log(res);
				}
			});
		});
	</script>
	<script type="text/javascript" src="<?php echo scripts_bundle('search_product.js') ?>"></script>
</body>

</html>