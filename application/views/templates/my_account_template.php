<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-139871344-1');
	</script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="name" content="ALG.tw" />
	<meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
	<meta name="author" content="ALG">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta property="og:url" content="<?php echo base_url() ?>" />
	<meta property="og:type" content="ALG.tw" />
	<meta property="og:title" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta property="og:description" content="Selling Cards for Magic the Gathering" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/datatables/css/datatables.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('my_account.css') ?>">
	<link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<script type="text/javascript">
	var c_language = '';
</script>
<style>
	.btn-primary {
		background: #340E5B !important;
	}
</style>
<body>
	<?php
	if (is_logged() == 1)
		$this->load->view('includes/logged_header', $data);
	else
		$this->load->view('includes/header', $data);
	$this->load->view('pages/my_account');
	$this->load->view('includes/footer');
	?>
	<script type="text/javascript">
		var base_url = "<?php echo base_url() ?>"
	</script>
	<script src="<?php echo scripts_bundle('spin.min.js') ?>"></script>
	<script src="<?php echo scripts_bundle('ladda.min.js') ?>"></script>
	<script src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/libraries/datatables/js/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('search_product.js') ?>?random=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			set_user_information();
			$(".orders_button").on("click", function(e) {
				$("a[href='#orders']").click()
			});
			$(".bill_address_button").on("click", function(e) {
				$("a[href='#billing_address']").click()
			});
			$(".account_details_button").on("click", function(e) {
				$("a[href='#account_details']").click()

			});
			$(".shipping_address_button").on("click", function(e) {
				$("a[href='#shipping_address']").click()
			});
			$('#tbl_orders').DataTable({
				"searching": false,
				"order": [
					[1, "desc"]
				],
				"columnDefs": [{
					"targets": 0,
					"orderable": false
				}, {
					"targets": 4,
					"orderable": false
				}, {
					"targets": 5,
					"orderable": false
				}]
			});
		});
	</script>
</body>

</html>