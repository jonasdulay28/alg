<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-139871344-1');
	</script>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ALG</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo admin_folder() ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo admin_folder() ?>bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo admin_folder() ?>bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo admin_folder() ?>dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo admin_folder() ?>plugins/iCheck/square/blue.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('admin_global.css') ?>">
</head>

<body class="hold-transition login-page">
	<?php $this->load->view('pages/admin/admin_authentication'); ?>
	<!-- jQuery 3 -->
	<script src="<?php echo admin_folder() ?>bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo admin_folder() ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo admin_folder() ?>plugins/iCheck/icheck.min.js"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});

			$('#login-form').submit(function(e) {
				e.preventDefault();
				var post_url = '<?php echo base_url('secure_admin/login'); ?>';
				//var l = Ladda.create( document.querySelector( '.form-submit' ) );
				$.ajax({
					type: 'POST',
					url: post_url,
					data: $('#login-form').serialize(),
					dataType: "json",
					beforeSend: function() {
						//l.start();
					},
					success: function(res) {
						//	l.stop();
						if (res.message == "success") {
							window.location.href = res.url;
						} else {
							$('#login_password').val('');
							$("#result").html(res.message);
						}
					},
					error: function(res) {
						console.log(res);
					}
				});
			});
		});
	</script>
</body>

</html>