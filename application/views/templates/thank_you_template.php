<!DOCTYPE html>
<html style="height: 100%;">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-139871344-1');
	</script>
	<title>ALG.tw - Taiwan's Premier Hobby Store!</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="name" content="ALG.tw" />
	<meta name="description" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta name="keywords" content="Magic Cards, Magic the Gathering, magic the gathering cardlistm magic the gathering singles, Magic Booster Box, " />
	<meta name="author" content="ALG">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta property="og:url" content="<?php echo base_url() ?>" />
	<meta property="og:type" content="ALG.tw" />
	<meta property="og:title" content="ALG.tw - Taiwan's Premier Hobby Store!" />
	<meta property="og:description" content="Selling Cards for Magic the Gathering" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
</head>

<body style="height: 100%;">
	<a href="<?php echo base_url() ?>">
		<div style="background: url('<?php echo images_bundle('thank_you.png') ?>');background-size: 100% 100%;height: 100%;"></div>
	</a>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$('#loyalty_modal').modal('toggle');
		setTimeout(function() {
			$(".thank_you_badge").addClass("wobble-hor-bottom");
		}, 1500)

		function classHas(base, has) {
			const arr = base.split(" ")
			for (let i = 0; i < arr.length; i++) {
				if (arr[i] === has)
					return true;
			}
		}

		function classReplace(base, replace, next) {
			return base.replace(replace, next)
		}

		function onBtnClick() {
			const block = document.getElementById("block");
			const btn = document.getElementById("btn");
			const overlay = document.getElementById("overlay");

			if (classHas(block.className, "div-show")) {
				block.className = classReplace(block.className, "div-show", "div-hidden")
				btn.innerHTML = "Show"
				const content = block.innerHTML;
				overlay.innerHTML = "<div class=\"div-overlay div-overlay-left\">" + content + "</div>" + "<div class=\"div-overlay div-overlay-right\">" + content + "</div>";
			} else {
				block.className = classReplace(block.className, "div-hidden", "div-show")
				btn.innerHTML = "Snap"
				overlay.innerHTML = null;
			}
		}
	</script>
</body>

</html>