<?php
class Filter_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_where($data) {
		$this->db->where($data);
		return $this->db->get('filters')->result_array();
	}

}