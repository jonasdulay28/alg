<?php
class Cart_Order_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function insert_data($data) {
		$result = $this->db->insert('cart_order',$data);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$result = $this->db->update('cart_order', $data);
		if ($result) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function get($id = null) {
		if($id == null) $query = $this->db->get('cart_order');
		else $query = $this->db->get_where( 'cart_order', array('id' => $id) );

		return $query->result_array();
	}

}