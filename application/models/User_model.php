<?php
class User_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_all_users() {
		$query = $this->db->select('a.*,b.level_name as level_name,c.credits,x.price as total_price')->from('users a')
		->join('loyalty_program b','b.id = a.user_loyalty','left')
		->join('user_credit c','c.email = a.email','left')
		->join('(select email, sum(price) as price from cart_order where admin_status = 3 and mode = "live" group by email) x','x.email = a.email','left')
		->get();
		return $query->result();
	}

	public function get_user_info($filter) {
		$query = $this->db->select("a.*,b.credits,SUM(c.price) as total_price_spent")->from("users a")
		->join("user_credit b","b.email = a.email","left")
		->join('cart_order c','c.email = a.email','left')
		->where($filter)
		->where('c.admin_status',3)
		->get();
		return $query->result();
	}
}