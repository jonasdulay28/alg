<?php
class Menu_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAll() {
		return $this->db->get('menu')->result_array();
	}
}