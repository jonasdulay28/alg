<?php
class Product_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAll() {
		$this->db->where('name !=', '');
		return $this->db->get('products')->result_array();
	}

	public function get_where($data) {
		return $this->db->get_where('products', $data)->result_array();
	}

	public function check_other_sort($tag = '*', $sort) {
		$query = "SELECT ". $tag ." FROM products " . $sort . " LIMIT 1";
		return $this->db->query($query)->result_array();
	}

	public function get_other_sort($tag = '*', $sort) {
		$query = "SELECT ". $tag ." FROM products " . $sort;
		return $this->db->query($query)->result_array();
	}

	public function get_sort($tag = '*', $sort) {
		$query = "SELECT ". $tag ." FROM products as a LEFT JOIN cards as b ON REPLACE(a.`name`, ' - Foil', '') = b.`name` " . $sort;
		return $this->db->query($query)->result_array();
	}

	public function get_sort_tag($tag = '*', $sort) {
		$query = "SELECT ". $tag ." FROM products as a LEFT JOIN cards as b ON REPLACE(a.`name`, ' - Foil', '') = b.`name` " . $sort;
		return $this->db->query($query)->result();
	}

	public function get_search_product($tag = '*', $name, $where = '') {
		$query = "SELECT ". $tag ." FROM products WHERE name LIKE '%" . $name . "%'";
		if($where != '') $query .= ' AND ' . $where;
		return $this->db->query($query)->result_array();
	}

	public function get_suggestion($name) {
		$query = 'SELECT DISTINCT name, category FROM products WHERE name LIKE "%' . $name . '%" AND (regular_price != "" OR foil_price != "") AND stock > 0 LIMIT 10';
		return $this->db->query($query)->result_array();
	}

	public function get_available_products($name) {
		$query = "SELECT DISTINCT(REPLACE(a.name,' - Foil','')) as name, CONCAT(b.name,' - ', b.name_tw) as name_eng_tw FROM products a LEFT JOIN cards b 
					ON REPLACE(a.name,' - Foil','') = b.name WHERE (a.name LIKE '%" . $name . "%') AND (a.regular_price != '' OR a.foil_price != '') AND a.stock > 0 LIMIT 10";
		$cnt_rows =$this->db->query($query)->num_rows();
		if($cnt_rows == 0) {
			$query = "SELECT  DISTINCT(REPLACE(a.name,' - Foil','')) as name, CONCAT(b.name,' - ', b.name_tw) as name_eng_tw FROM products a LEFT JOIN cards b 
					ON REPLACE(a.name,' - Foil','') = b.name WHERE (b.name_tw LIKE '%".$name."%') AND (a.regular_price != '' OR a.foil_price != '') AND a.stock > 0 LIMIT 10";
		} 

		return $this->db->query($query)->result_array();
		
	}

	public function run_product() {
		$query = "SELECT * FROM products INNER JOIN products_prices ON products.name = products_prices.name WHERE products_prices.is_active = 0 AND products.regular_price = '' AND products_prices.regular_price != '' LIMIT 5";
		return $this->db->query($query)->result();
	}

	public function fetch_products($set_code,$rarity,$variation,$product_name,$category,$language){
		if($rarity!="")
			$this->db->where('a.rarity',$rarity);
		if($variation != "All" && $variation != ""){
			$this->db->where("card_type",$variation);
		}
		if($category != "") {
			$this->db->where("category",$category);
		}
		if($language != "") {
			$this->db->where("language",$language);
		}
		$display_name = 'a.name as name,a.base_price,a.card_type, a.language, a.image, a.collector_number, a.ability as ability, a.type as type, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw, a.category as category';
        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';
		$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number, a.image
				, a.set_code as set_code, a.rarity as rarity, a.fully_handled as fully_handled, a.regular_price as price
				, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag as tag, a.custom_tag as custom_tag';
		$product_tag .= ', a.category as category, a.priority as priority, a.set as `set`, a.color as color, a.rating as rating, a.power as power, a.toughness as toughness, a.manacost as manacost, a.converted_manacost as converted_manacost, a.ruling as ruling, a.artist as artist, a.tag_color as tag_color, a.tag_bg_color as tag_bg_color, a.tag_color as row_tag_color, a.tag_bg_color as row_bg_color';

		//$query = "SELECT ". $product_tag ." FROM products as a LEFT JOIN cards as b ON REPLACE(a.`name`, ' - Foil', '') = b.`name` " . $sort;
		$query =  $this->db->select($product_tag)->from("products a")
		->join('cards b',"REPLACE(a.`name`, ' - Foil', '') = b.`name`", 'left')
		->where('a.set_code',$set_code)
		->like('a.name',$product_name)
		->order_by('a.name','ASC')
		->order_by('FIELD(fully_handled,"NM","EX","VG","G","P","D")')
		->group_by('id')
		->get();
		return $query->result();
	}

	public function get_condition($set_code) {
		$query = "SELECT DISTINCT fully_handled FROM products WHERE set_code='". $set_code ."'";
		return $this->db->query($query)->result_array();
	}

	public function get_all_condition() {
		$query = "SELECT DISTINCT fully_handled FROM products";
		return $this->db->query($query)->result_array();
	}

	public function get_other_product($card){
		$query = 'SELECT * FROM products WHERE id!="'.$card['id'].'" AND fully_handled!="'.$card['fully_handled'].'" AND name="'.$card['name'].'" AND set_code="'.$card['set_code'].'" AND (regular_price != "" OR foil_price != "") AND stock > 0  GROUP BY fully_handled ORDER BY FIELD(fully_handled,"NM","EX","VG","G","P","D")';
		return $this->db->query($query)->result_array();
	} 
}