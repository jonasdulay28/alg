<?php
class Allsets_model extends MY_Model{

	public function __construct(){
		parent::__construct();

	}

	public function get_sets() {
		$query = $this->db->query("select distinct products.set,set_code from products where products.set != '' order by products.set ");
		return $query->result();
	}

}