<?php
class Dashboard_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_total_sales() {
		$query = $this->db->select('SUM(price) as total_price')->from('cart_order')->where('status',1)->get()->row();
		return $query->total_price;
	}
}