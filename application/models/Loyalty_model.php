<?php
class Loyalty_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_loyalty_by_level() {
		$query = "SELECT * FROM loyalty_program ORDER by badge";

		return $this->db->query($query)->result_array();
	}
}