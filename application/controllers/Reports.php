<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Reports_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}

	public function get_orders() {
		$data = [];
		if($_POST){
			$from = clean_data(post('from'));
			$to = clean_data(post('to'));
			if($from != "") {
				$from .= " 00:00:00";
			} else {
				$from = "2018-10-12 00:00:00";
			}

			if($to != "") {
				$to .= " 23:59:59";
			} else {
				$to = date("Y-m-d" , time() + 86400)." 23:59:59";
			}
			$this->session->set_userdata('from',$from);
			$this->session->set_userdata('to',$to);
		}else {
			$t=time();
			$curr_date = date("Y-m-d",$t);
			$from = (isset($this->session->from) ? $this->session->from :  $curr_date." 00:00:00");
			$to = (isset($this->session->to) ? $this->session->to :  date("Y-m-d" , time() + 86400)." 00:00:00");
			$data['from'] = $from;
			$data['to'] = $to;
			$this->session->unset_userdata('to');
			$this->session->unset_userdata('from');
			$data['orders'] = $this->Reports_model->get_orders($from,$to);
		}
		echo json_encode($data);
	}

}