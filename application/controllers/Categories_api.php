<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class categories_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Categories_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}
	
	public function get_all_categories() {
		$data['menu'] = $this->Categories_model->fetch('menu');
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$category_data = json_decode(post('category_data'));
		//die(print_r($category_data));
		$this->Categories_model->insert('menu',$category_data);
		$row = $this->Categories_model->fetch_tag_row('id','menu','','1','','id desc');
		$response['id'] = $row->id;
		echo json_encode($response);
	}

	public function get_menu(){
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
		$data["menu"] = $this->Categories_model->fetch_data('menu',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$sub_category = post('sub_category');
		$sub_category_chi = post('sub_category_chi');
		$sub_category_code = post('sub_category_code');
		$sub_category_main_code = post('sub_category_main_code');
		$id = clean_data(post('id'));
		$data = ["sub_category" => $sub_category, "sub_category_chi" => $sub_category_chi, "sub_category_code" => $sub_category_code, "sub_category_main_code" => $sub_category_main_code];
		$filter = ["id"=>$id]; 
		$this->Categories_model->update('menu',$data,$filter);
		echo json_encode($response);
	}

	public function delete()
	{
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Categories_model->delete('coupons',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$id = clean_data(post('id'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status"=>$status];
		$filter = ["id"=>$id]; 
		$this->Categories_model->update('coupons',$data,$filter);
		echo json_encode($response);
	}
}