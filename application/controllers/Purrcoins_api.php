<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class purrcoins_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Audit_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}

	public function get_all_purrcoins() {
		$email = get('email');
		$filter = ["email"=>$email];
		$data['audits'] = $this->Audit_model->fetch_tag('*','tbl_audit',$filter);
		echo json_encode($data);
	}

}