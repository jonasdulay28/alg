<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class currency_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
	}

	public function index() {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://fixer-fixer-currency-v1.p.rapidapi.com/latest?base=TWD&symbols=USD%2CGBP%2CJPY%2CEUR%2CCNY%2CHKD%2CSGD%2CNT%2CTHB",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"x-rapidapi-host: fixer-fixer-currency-v1.p.rapidapi.com",
				"x-rapidapi-key: ee450c2ae8msh85211cf5adcb369p13de3djsnbef05bcc250b"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			print_r($response);
			$res = json_decode($response);
			$data['rates'] = json_encode($res->rates);
			$this->Global_model->update('currencies',$data);
		}
	}

	private function currency_name() {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://fixer-fixer-currency-v1.p.rapidapi.com/symbols",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"x-rapidapi-host: fixer-fixer-currency-v1.p.rapidapi.com",
				"x-rapidapi-key: ee450c2ae8msh85211cf5adcb369p13de3djsnbef05bcc250b"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}

	public function get_currency() {
		$currency = clean_data(post('currency'));
		$filter = ["id"=>1];
		$row = $this->Global_model->fetch_tag_row("*","currencies",$filter);
		$rates = json_decode($row->rates);
		$this->session->support_currency = $currency;
		$this->session->support_currency_rate = $rates->$currency;

		echo json_encode(['message'=>"success"]);
	}

}