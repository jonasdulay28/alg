    <?php
		defined('BASEPATH') or exit('No direct script access allowed');

		class functions extends CI_Controller
		{

			public function __construct()
			{
				parent::__construct();
				$this->load->model('Cart_Order_model');
				$this->load->model("Global_model");
			}


			public function order_product_bt()
			{
				$data = $this->input->post();
				if (!$data) show_404();

				$user = $this->Global_model->fetch('users', array('email' => get_email()));

				$user_credit = $this->Global_model->fetch_data('user_credit', array('email' => get_email()));

				$purrcoins = $this->session->userdata('purrcoins') ? $this->session->userdata('purrcoins') : 0;
				$credits = 0;
				if ($user_credit) {
					$credits = $user_credit[0]->credits;
					$credits -= $this->session->userdata('purrcoins') ? floatval($this->session->userdata('purrcoins')) : 0;
					if ($credits >= 0) $this->Global_model->update('user_credit', array('credits' => $credits), array('id' => $user_credit[0]->id));
				}

				if ($credits < 0) {
					$this->session->unset_userdata('purrcoins');
					echo json_encode(array('status' => 'failed', 'error_msg' => 'Invalid Purrcoins'));
					return;
				}
				$coupon_code = '';

				if ($this->session->userdata('coupons')) {
					$coupons = $this->Global_model->fetch('coupons', array('id' => $this->session->userdata('coupons')->id, 'status' => 1, 'num_limit >' => 0));

					if (!$coupons) {
						$this->session->unset_userdata('coupons');
						echo json_encode(array('status' => 'failed', 'error_msg' => 'Invalid Coupon'));
						return;
					} else {
						$coupon_code = $coupons[0]->coupon_code;
						$limit = $coupons[0]->num_limit;
						$limit -= 1;

						$this->Global_model->update('coupons', array('num_limit' => $limit), array('id' => $this->session->userdata('coupons')->id));
						$this->session->unset_userdata('coupons');
					}
				}
				$expiration = Date('Y-m-d H:i', strtotime("+2 days"));
				$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';

				$insert_data = array(
					'email' => get_email(),
					'billing_info' => $data['billing_info'],
					'shipping_info' => $data['shipping_info'],
					'shipping_mode' => $data['shipping_mode'],
					'cart_list' => $data['cart_list'],
					'payment_details' => $data['payment_details'],
					'price' => $data['amount'],
					'total_base_price' => $data['total_base_price'],
					'coupon_discount' => $data['coupon_discount'],
					'coupon_code' => $coupon_code,
					'coupon_value' => $data['coupon_value'],
					'points_used' => $purrcoins,
					'points_earned' => number_format(floatval($data['points_earned']), 2),
					'status' => 0,
					'payment_mode' => $data['payment_mode'],
					'expired_at' => $expiration,
					'mode' => $mode
				);
				$shipping_info = json_decode($data['shipping_info']);
				$billing_info = json_decode($data['billing_info']);

				$cart_id = $this->Cart_Order_model->insert_data($insert_data);
				if ($cart_id == FALSE) {
					echo json_encode(array('status' => 'failed', 'error_msg' => 'Fail'));
					return;
				}
				$invoice_number = rand(1000, 9999) . '-' . pad($cart_id, 5);
				$this->Global_model->update('cart_order', array('invoice_number' => $invoice_number), array('id' => $cart_id));

				$this->session->unset_userdata('purrcoins');
				$this->session->unset_userdata('cart_list');
				updateCartList(array());

				$approval_link = base_url() . 'order/?order=' . $cart_id;
				$returned_value = array('status' => 'success', 'error_msg' => '', 'approval_link' => $approval_link);

				$payment_details = json_decode($data['payment_details']);
				$order_list = json_decode($data['cart_list']);

				/*<span style="font-weight: bold;">Billing Info:</span> '.$data['billing_info'].'
    										<br>
    										<span style="font-weight: bold;">Shipping Info:</span> '.$data['shipping_info'].'
    										<br>*/
				$fullname = $billing_info->firstname . ' ' . $billing_info->lastname;
				$email_message = '
    						<html>
    						<head>
    						<style>
    						#orders {
    						  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    						  border-collapse: collapse;
    						  width: 100%;
    						}
    
    						#orders td, #orders th {
    						  border: 1px solid #ddd;
    						  padding: 8px;
    						}
    
    						#orders tr:nth-child(even){background-color: #f2f2f2;}
    
    						#orders tr:hover {background-color: #ddd;}
    
    						#orders th {
    						  padding-top: 12px;
    						  padding-bottom: 12px;
    						  text-align: left;
    						  background-color: #05A9E2;
    						  color: white;
    						}
    						</style>
    						</head>
    						<body>
    							<p><span style="font-weight: bold;">Thank you for your order!</span></p>
    							<table style="width: 700px; font-family: arial;">
    								<tr style="height: 80px;">
    									<td style="text-align: left;">';

				if ($fullname) {
					$email_message .= '<span style="font-weight: bold;">Name:</span> ' . $fullname . ' <br>';
				}

				$email_message .= '
    										<span style="font-weight: bold;">Email:</span> ' . get_email() . '
    										<br>';

				if ($data['payment_mode'] == "Bank Transfer") {
					$email_message .= '<span style="font-weight: bold;">Shipping Mode:</span> ' . $data['shipping_mode'] . ' <br>';
				}

				$email_message .= 	'
    										<span style="font-weight: bold;">SubTotal:</span> ' . $payment_details->sub_total . '฿
    										<br>';

				if ($data['payment_mode'] == "Bank Transfer") {
					$email_message .= '<span style="font-weight: bold;">Shipping Fee:</span> ' . $payment_details->shipping_fee . '฿';
				}

				$email_message .=  '
    										<br>
    										<span style="font-weight: bold;">Total amount:</span> ' . $payment_details->total_amount . '฿
    									</td>
    								</tr>
    							</table>';

				if ($data['payment_mode'] == "Bank Transfer") {
					$email_message .= '
							</table>
							<p><h3>Shipping Details </h3></p>

							<table id="shipping_details">
								<tr>
									<td><b>Country:</b> ' . $shipping_info->country . '</td>
								</tr>
								<tr>
									<td><b>State:</b> ' . $shipping_info->state . '</td>
								</tr>
								<tr>
									<td><b>Street Address:</b> ' . $shipping_info->street . '</td>
								</tr>
								<tr>
									<td><b>Postal Code:</b> ' . $shipping_info->zip . '</td>
								</tr>
								<tr>
									<td><b>Town/City:</b> ' . $shipping_info->city . '</td>
								</tr>
								<tr>
									<td><b>Phone:</b> ' . $shipping_info->phone . '</td>
								</tr>
							</table>';
				}
				usort($order_list, function ($a, $b) {
					return strcmp($a->card->set_name, $b->card->set_name);
				});
				$email_message .= '
    							<p><h3>Order List </h3></p>
    							<table id="orders">
    							  <tr>
    							    <th>Product</th>
                                    <th>Category</th>
    							    <th>Price</th>
    							  </tr>
    							';
				foreach ($order_list as $order) {
					$card_name = getCardName($order->card, 'json');
					$email_message .= '<tr>';
					$email_message .= '<td>' . $order->quantity . 'x ' . $card_name . '</td>';
					$email_message .= '<td>' . $order->card->set . '</td>';
					$email_message .= '<td>' . $order->card->price . '฿</td>';
					$email_message .= '</tr>';

					$card = $this->Global_model->fetch('products', array('id' => $order->card->id));

					if ($card) {
						$quantity = intval($card[0]->stock) - intval($order->quantity);
						$quantity = $quantity < 0 ? 0 : $quantity;
						$this->Global_model->update('products', array('stock' => $quantity), array('id' => $card[0]->id));
					}
				}
				$email_message .= '
    							</table>
    						</body>
    						</html>
    		';
    
    		$email_content = [
          'from' => 'hello@alg.tw',
          'from_name' => "ALG Orders",
          'to' => get_email(),  
          'subject' => 'Thank you for order',
          'message' => $email_message
        ];
        
            audit("payment","payment: ".get_email(),$data['payment_mode'],"success");
            if($purrcoins > 0 ){
                    audit("Deducted purrcoins","order pick up","Deducted purrcoins",$purrcoins);
            }
    
        
        
    		$isSent = sendMailCIMailer($email_content);	
    
    		echo json_encode($returned_value);
    	}
    
    	public function order_product_pp() {
    		$data = $this->input->post();
    		if(!$data) show_404();
    
    		$user = $this->Global_model->fetch('users', array('email' => get_email()));
    
    		$user_credit = $this->Global_model->fetch_data('user_credit', array('email'=>get_email()));
    		$purrcoins = $this->session->userdata('purrcoins') ? $this->session->userdata('purrcoins') : 0;
    
    		$credits = 0;
    		if($user_credit) {
    			$credits = $user_credit[0]->credits;
    			$credits -= $this->session->userdata('purrcoins') ? floatval($this->session->userdata('purrcoins')) : 0;
    		} 
    
    		if($credits < 0) {
    			$this->session->unset_userdata('purrcoins');
    			echo json_encode(array('status'=> 'failed', 'error_msg' => 'Invalid Purrcoins'));
    			return;
    		}
    		$coupon_code = '';
    
    		if($this->session->userdata('coupons')){
    			$coupons = $this->Global_model->fetch('coupons', array('id'=>$this->session->userdata('coupons')->id, 'status' => 1, 'num_limit >'=> 0));
    
    			if(!$coupons) {
    				$this->session->unset_userdata('coupons');
    				echo json_encode(array('status'=> 'failed', 'error_msg' => 'Invalid Coupon'));
    				return;
    			} else {
    				$coupon_code = $coupons[0]->coupon_code;
    				$limit = $coupons[0]->num_limit;
    				$limit -= 1;
    
    				$this->Global_model->update('coupons', array('num_limit'=>$limit), array('id'=> $this->session->userdata('coupons')->id));
    				$this->session->unset_userdata('coupons');
    			}
    		}
            $expiration = Date('Y-m-d H:i', strtotime("+2 days"));
    		$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
    
    		$insert_data = array(
    			'email' => get_email(),
    			'billing_info' => $data['billing_info'],
    			'shipping_info' => $data['shipping_info'],
    			'shipping_mode' => $data['shipping_mode'],
    			'cart_list' => $data['cart_list'],
    			'payment_details' => $data['payment_details'],
    			'price' => $data['amount'],
    			'total_base_price' => $data['total_base_price'],
    			'coupon_discount' => $data['coupon_discount'],
    			'coupon_code' => $coupon_code,
    			'coupon_value' => $data['coupon_value'],
    			'points_used' => $purrcoins,
    			'points_earned' => number_format(floatval($data['points_earned']), 2),
    			'status' => 0,
                'expired_at' => $expiration,
    			'payment_mode' => paypal,
    			'mode' => $mode
    		);
            
            $cart_id = $this->Cart_Order_model->insert_data($insert_data);
            if($cart_id == FALSE) {
                redirect(base_url());
                return;
            }
            $invoice_number = rand(1000,9999) . '-' . pad($cart_id, 5);
            $insert_data['invoice_number'] = $invoice_number;
            $this->Global_model->update('cart_order', array('invoice_number'=> $invoice_number), array('id'=> $cart_id));
    		$this->session->set_userdata(array('cart_id' => $cart_id));
            $this->session->set_userdata(array('insert_order' => $insert_data));
            $order_list = json_decode($data['cart_list']);

            foreach($order_list as $order){
                $card = $this->Global_model->fetch('products', array('id' => $order->card->id));

                if($card) {
                    $quantity = intval($card[0]->stock) - intval($order->quantity);
                    $quantity = $quantity < 0 ? 0 : $quantity;
                    $this->Global_model->update('products', array('stock' => $quantity), array('id' => $card[0]->id));
                }
            }
    
    		$this->load->library('PayPal');
    
    		$required_data = array(
    			'Currency' => 'TWD',
    			'Plan' => 'One Time',
    			'PlanID' => '1',
    			'Price' => $data['amount']
    		);
    		$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
    		$PayPal = $this->paypal;
    		$CreateCharge = $PayPal->createCharge($required_data, $mode);
    
    		if ($CreateCharge['status']){
    			$returned_value = array('status'=>'success', 'error_msg'=>'', 'approval_link'=>$CreateCharge['approvalUrl']);
    			echo json_encode($returned_value);
    			return;
    		}
    	}
    
    	public function refundPayment() {
    		if(!$this->input->post()) show_404();
    		$id = post('id');
    		$email = post('email');
    
    		$retrieved_user = $this->Global_model->fetch('users', array('email' => $email));
    		if(!$retrieved_user) show_404();
    
    		$order = $this->Cart_Order_model->get($id);
    		$cart_order = json_decode($order[0]['cart_list']);
    		foreach($cart_order as $cart) {
    			$card = $this->Global_model->fetch('products', array('id'=>$cart->card->id));
    			if($card) {
    				$quantity = intval($card[0]->stock) + intval($cart->quantity);
    				$this->Global_model->update('products', array('stock'=>$quantity), array('id'=>$card[0]->id));
    			}
    		}
    		$user_credit = $this->Global_model->fetch('user_credit', array('email' => $email));
    
    		if($user_credit){
    			$credits = floatval($user_credit[0]->credits) + floatval($order[0]['points_used']);
    			$this->Global_model->update('user_credit', array('credits' => $credits), array('email' => $email));
    		}
    
    		if($order[0]['payment_mode'] != paypal || $order[0]['payment_id'] == '') return;
    
    		$required_data = array(
    			'currency' => 'THB',
    			'total' => $order[0]['price'],
    			'paymentId' => $order[0]['payment_id']
    		);
    		audit("refund","refund: ".$email,"order","");
    		$this->load->library('PayPal');
    		$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
    		$PayPal = $this->paypal;
    		$PayPal->refundSale($required_data, $mode);
    	}

        public function check_paypal_order(){
            $cur_date = date('Y-m-d');
            
            $order = $this->Global_model->fetch('cart_order', array('expired_at >=' => $cur_date, 'status' => 0, 'admin_status' => 0, 'payment_mode' => paypal, 'id'=> '567'));
            if($order) {
                foreach($order as $cart) {
                    $cart_list = json_decode($cart->cart_list);
    
                    foreach($cart_list as $card) {
                        $cards = $this->Global_model->fetch('products', array('id' => $card->card->id));
                        if($cards) {
                            $quantity = intval($cards[0]->stock) + intval($card->quantity);
                            $this->Global_model->update('products', array('stock'=>$quantity), array('id'=>$cards[0]->id));
                        }
                    }
    
                    $this->Global_model->update('cart_order', array('admin_status' => 4), array('id' => $cart->id));
                }
            }
        }
    	
    	public function check_order(){
    		$cur_date = date('Y-m-d');
    		
    		$order = $this->Global_model->fetch('cart_order', array('expired_at <' => $cur_date, 'admin_status' => 0));
    		if($order) {
    			foreach($order as $cart) {
    				$cart_list = json_decode($cart->cart_list);
    
    				foreach($cart_list as $card) {
    					$cards = $this->Global_model->fetch('products', array('id' => $card->card->id));
    					if($cards) {
    						$quantity = intval($cards[0]->stock) + intval($card->quantity);
    						$this->Global_model->update('products', array('stock'=>$quantity), array('id'=>$cards[0]->id));
    					}
    				}
    
    				$this->Global_model->update('cart_order', array('admin_status' => 4), array('id' => $cart->id));
    			}
    		}
    	}
    	
    	public function executePayment($param=NULL){
    		if(is_null($param)) {
    			redirect(base_url());
    			exit();
    		}
    
    		$data = $this->session->userdata('insert_order');
    		$billing_info = json_decode($data['billing_info']);
    		$required_data = array(
    			'paymentId' => $_GET['paymentId'],
    			'PayerID' => $_GET['PayerID'],
    			'PlanID' => $_GET['PlanID'],
    			'planCurrency' => 'THB',
    			'planAmount' => $data['price']
    		);		
    		$purrcoins = $this->session->userdata('purrcoins') ? $this->session->userdata('purrcoins') : 0;
            $cart_id = $this->session->userdata('cart_id') ? $this->session->userdata('cart_id') : 0; 		
    
    		$retrieved_user = $this->Global_model->fetch('users', array('email' => get_email()));
    		if(!$retrieved_user) redirect(base_url());
    		$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
    		$update_data = array(
    			'paypal_customer' => $_GET['PayerID'],
    			'user_mode' => $mode
    		);
    
    		$this->load->library('PayPal');
    		$PayPal = $this->paypal;
    		
    		switch(strtoupper($param)){
    			case 'SUCCESS':
    				$request = $PayPal->ExecutePayment($required_data, $mode);
    
    				if ($request['success']){
                        $this->session->unset_userdata('cart_list');
                        $this->session->unset_userdata('cart_id');
                        updateCartList(array());
                        $this->session->unset_userdata('insert_order');
                        $this->session->unset_userdata('purrcoins');    
                        $this->Global_model->update('users', $update_data, array( 'id' => $retrieved_user[0]->id ));
                        audit("logs","executePayment: ".get_email(),json_encode($update_data),"success");


				$email_content = [
					'from' => 'hello@alg.tw',
					'from_name' => "ALG Orders",
					'to' => get_email(),
					'subject' => 'Thank you for order',
					'message' => $email_message
				];
				if ($data['payment_mode'] == "Bank Transfer") {
					audit("payment", "payment: " . get_email(), "order bank transfer", "success");
					if ($purrcoins > 0) {
						audit("Deducted purrcoins", "order bank transfer", "Deducted purrcoins", $purrcoins);
					}
				} else {
					audit("payment", "payment: " . get_email(), "order pick up", "success");
					if ($purrcoins > 0) {
						audit("Deducted purrcoins", "order pick up", "Deducted purrcoins", $purrcoins);
					}
				}



				$isSent = sendMailCIMailer($email_content);

				echo json_encode($returned_value);
			}

			public function order_product_pp()
			{
				$data = $this->input->post();
				if (!$data) show_404();

				$user = $this->Global_model->fetch('users', array('email' => get_email()));

				$user_credit = $this->Global_model->fetch_data('user_credit', array('email' => get_email()));
				$purrcoins = $this->session->userdata('purrcoins') ? $this->session->userdata('purrcoins') : 0;

				$credits = 0;
				if ($user_credit) {
					$credits = $user_credit[0]->credits;
					$credits -= $this->session->userdata('purrcoins') ? floatval($this->session->userdata('purrcoins')) : 0;
				}

				if ($credits < 0) {
					$this->session->unset_userdata('purrcoins');
					echo json_encode(array('status' => 'failed', 'error_msg' => 'Invalid Purrcoins'));
					return;
				}
				$coupon_code = '';

				if ($this->session->userdata('coupons')) {
					$coupons = $this->Global_model->fetch('coupons', array('id' => $this->session->userdata('coupons')->id, 'status' => 1, 'num_limit >' => 0));

					if (!$coupons) {
						$this->session->unset_userdata('coupons');
						echo json_encode(array('status' => 'failed', 'error_msg' => 'Invalid Coupon'));
						return;
					} else {
						$coupon_code = $coupons[0]->coupon_code;
						$limit = $coupons[0]->num_limit;
						$limit -= 1;

						$this->Global_model->update('coupons', array('num_limit' => $limit), array('id' => $this->session->userdata('coupons')->id));
						$this->session->unset_userdata('coupons');
					}
				}
				$expiration = Date('Y-m-d H:i', strtotime("+2 days"));
				$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';

				$insert_data = array(
					'email' => get_email(),
					'billing_info' => $data['billing_info'],
					'shipping_info' => $data['shipping_info'],
					'shipping_mode' => $data['shipping_mode'],
					'cart_list' => $data['cart_list'],
					'payment_details' => $data['payment_details'],
					'price' => $data['amount'],
					'total_base_price' => $data['total_base_price'],
					'coupon_discount' => $data['coupon_discount'],
					'coupon_code' => $coupon_code,
					'coupon_value' => $data['coupon_value'],
					'points_used' => $purrcoins,
					'points_earned' => number_format(floatval($data['points_earned']), 2),
					'status' => 0,
					'expired_at' => $expiration,
					'payment_mode' => paypal,
					'mode' => $mode
				);

				$cart_id = $this->Cart_Order_model->insert_data($insert_data);
				if ($cart_id == FALSE) {
					redirect(base_url());
					return;
				}
				$invoice_number = rand(1000, 9999) . '-' . pad($cart_id, 5);
				$insert_data['invoice_number'] = $invoice_number;
				$this->Global_model->update('cart_order', array('invoice_number' => $invoice_number), array('id' => $cart_id));
				$this->session->set_userdata(array('cart_id' => $cart_id));
				$this->session->set_userdata(array('insert_order' => $insert_data));
				$order_list = json_decode($data['cart_list']);

				foreach ($order_list as $order) {
					$card = $this->Global_model->fetch('products', array('id' => $order->card->id));

					if ($card) {
						$quantity = intval($card[0]->stock) - intval($order->quantity);
						$quantity = $quantity < 0 ? 0 : $quantity;
						$this->Global_model->update('products', array('stock' => $quantity), array('id' => $card[0]->id));
					}
				}

				$this->load->library('PayPal');

				$required_data = array(
					'Currency' => 'TWD',
					'Plan' => 'One Time',
					'PlanID' => '1',
					'Price' => $data['amount']
				);
				$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
				$PayPal = $this->paypal;
				$CreateCharge = $PayPal->createCharge($required_data, $mode);

				if ($CreateCharge['status']) {
					$returned_value = array('status' => 'success', 'error_msg' => '', 'approval_link' => $CreateCharge['approvalUrl']);
					echo json_encode($returned_value);
					return;
				}
			}

			public function refundPayment()
			{
				if (!$this->input->post()) show_404();
				$id = post('id');
				$email = post('email');

				$retrieved_user = $this->Global_model->fetch('users', array('email' => $email));
				if (!$retrieved_user) show_404();

				$order = $this->Cart_Order_model->get($id);
				$cart_order = json_decode($order[0]['cart_list']);
				foreach ($cart_order as $cart) {
					$card = $this->Global_model->fetch('products', array('id' => $cart->card->id));
					if ($card) {
						$quantity = intval($card[0]->stock) + intval($cart->quantity);
						$this->Global_model->update('products', array('stock' => $quantity), array('id' => $card[0]->id));
					}
				}
				$user_credit = $this->Global_model->fetch('user_credit', array('email' => $email));

				if ($user_credit) {
					$credits = floatval($user_credit[0]->credits) + floatval($order[0]['points_used']);
					$this->Global_model->update('user_credit', array('credits' => $credits), array('email' => $email));
				}

				if ($order[0]['payment_mode'] != paypal || $order[0]['payment_id'] == '') return;

				$required_data = array(
					'currency' => 'THB',
					'total' => $order[0]['price'],
					'paymentId' => $order[0]['payment_id']
				);
				audit("refund", "refund: " . $email, "order", "");
				$this->load->library('PayPal');
				$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
				$PayPal = $this->paypal;
				$PayPal->refundSale($required_data, $mode);
			}

			public function check_paypal_order()
			{
				$cur_date = date('Y-m-d');

				$order = $this->Global_model->fetch('cart_order', array('expired_at >=' => $cur_date, 'status' => 0, 'admin_status' => 0, 'payment_mode' => paypal, 'id' => '567'));
				if ($order) {
					foreach ($order as $cart) {
						$cart_list = json_decode($cart->cart_list);

						foreach ($cart_list as $card) {
							$cards = $this->Global_model->fetch('products', array('id' => $card->card->id));
							if ($cards) {
								$quantity = intval($cards[0]->stock) + intval($card->quantity);
								$this->Global_model->update('products', array('stock' => $quantity), array('id' => $cards[0]->id));
							}
						}

						$this->Global_model->update('cart_order', array('admin_status' => 4), array('id' => $cart->id));
					}
				}
			}

			public function check_order()
			{
				$cur_date = date('Y-m-d');

				$order = $this->Global_model->fetch('cart_order', array('expired_at <' => $cur_date, 'admin_status' => 0));
				if ($order) {
					foreach ($order as $cart) {
						$cart_list = json_decode($cart->cart_list);

						foreach ($cart_list as $card) {
							$cards = $this->Global_model->fetch('products', array('id' => $card->card->id));
							if ($cards) {
								$quantity = intval($cards[0]->stock) + intval($card->quantity);
								$this->Global_model->update('products', array('stock' => $quantity), array('id' => $cards[0]->id));
							}
						}

						$this->Global_model->update('cart_order', array('admin_status' => 4), array('id' => $cart->id));
					}
				}
			}

			public function executePayment($param = NULL)
			{
				if (is_null($param)) {
					redirect(base_url());
					exit();
				}

				$data = $this->session->userdata('insert_order');
				$billing_info = json_decode($data['billing_info']);
				$required_data = array(
					'paymentId' => $_GET['paymentId'],
					'PayerID' => $_GET['PayerID'],
					'PlanID' => $_GET['PlanID'],
					'planCurrency' => 'THB',
					'planAmount' => $data['price']
				);
				$purrcoins = $this->session->userdata('purrcoins') ? $this->session->userdata('purrcoins') : 0;
				$cart_id = $this->session->userdata('cart_id') ? $this->session->userdata('cart_id') : 0;

				$retrieved_user = $this->Global_model->fetch('users', array('email' => get_email()));
				if (!$retrieved_user) redirect(base_url());
				$mode = getCookie('payment') == 'paypal_test' ? 'Test' : 'Live';
				$update_data = array(
					'paypal_customer' => $_GET['PayerID'],
					'user_mode' => $mode
				);

				$this->load->library('PayPal');
				$PayPal = $this->paypal;

				switch (strtoupper($param)) {
					case 'SUCCESS':
						$request = $PayPal->ExecutePayment($required_data, $mode);

						if ($request['success']) {
							$this->session->unset_userdata('cart_list');
							$this->session->unset_userdata('cart_id');
							updateCartList(array());
							$this->session->unset_userdata('insert_order');
							$this->session->unset_userdata('purrcoins');
							$this->Global_model->update('users', $update_data, array('id' => $retrieved_user[0]->id));
							audit("logs", "executePayment: " . get_email(), json_encode($update_data), "success");


							$user_credit = $this->Global_model->fetch_data('user_credit', array('email' => get_email()));
							$user = $this->Global_model->fetch('users', array('email' => get_email()));

							$credits = 0;
							if ($user_credit) {
								$credits = $user_credit[0]->credits;
								$credits -= $purrcoins;
								$credits = $credits < 0 ? 0 : $credits;
								$this->Global_model->update('user_credit', array('credits' => $credits), array('id' => $user_credit[0]->id));
							}

							// $insert_data = $data;
							// $cart_id = $this->Cart_Order_model->insert_data($insert_data);
							// if($cart_id == FALSE) {
							// 	redirect(base_url());
							// 	return;
							// }
							// $invoice_number = rand(1000,9999) . '-' . pad($cart_id, 5);
							// $this->Global_model->update('cart_order', array('invoice_number'=> $invoice_number), array('id'=> $cart_id));
							$invoice_number = $data['invoice_number'];

							$update_data = array(
								'status' => 1,
								'payment_id' => $_GET['paymentId']
							);
							audit("payment", "payment: " . get_email(), "order paypal", "success");
							if ($purrcoins > 0) {
								audit("Deducted purrcoins", "order paypal", "Deducted purrcoins", $purrcoins);
							}
							$this->Cart_Order_model->update_data($cart_id, $update_data);

							$email = get_email();
							$isSent = false;
							$message = clean_data(post('message'));

							$payment_details = json_decode($data['payment_details']);
							$order_list = json_decode($data['cart_list']);
							$shipping_info = json_decode($data['shipping_info']);
							/*	<span style="font-weight: bold;">Billing Info:</span> '.$data['billing_info'].'
    													<br>
    													<span style="font-weight: bold;">Shipping Info:</span> '.$data['shipping_info'].'
    													<br>*/
							$fullname = $billing_info->firstname . ' ' . $billing_info->lastname;
							$email_message = '
    									<html>
    									<head>
    									<style>
    									#orders {
    									  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    									  border-collapse: collapse;
    									  width: 100%;
    									}
    
    									#orders td, #orders th {
    									  border: 1px solid #ddd;
    									  padding: 8px;
    									}
    
    									#orders tr:nth-child(even){background-color: #f2f2f2;}
    
    									#orders tr:hover {background-color: #ddd;}
    
    									#orders th {
    									  padding-top: 12px;
    									  padding-bottom: 12px;
    									  text-align: left;
    									  background-color: #05A9E2;
    									  color: white;
    									}
    									</style>
    									</head>
    									<body>
    										<p><span style="font-weight: bold;">Thank you for your purchase!</span></p>
    										<p>This email confirms your recent payment. </p>
    										<table style="width: 700px; font-family: arial;">
    											<tr style="height: 80px;">
    												<td style="text-align: left;">';

							if ($fullname) {
								$email_message .= '<span style="font-weight: bold;">Name:</span> ' . $fullname . ' <br>';
							}

							$email_message .= '
														<span style="font-weight: bold;">Email: </span> ' . get_email() . '
														<br>
    													<span style="font-weight: bold;">Invoice Number:</span> #' . $invoice_number . '
    													<br>
    													<span style="font-weight: bold;">SubTotal:</span> ' . $payment_details->sub_total . '฿
    													<br>
    													<span style="font-weight: bold;">Shipping Fee:</span> ' . $payment_details->shipping_fee . '฿
    													<br>
    													<span style="font-weight: bold;">Total amount:</span> ' . $payment_details->total_amount . '฿
    												</td>
    											</tr>
    										</table>';

							$email_message .= '
							</table>
							<p><h3>Shipping Details </h3></p>

							<table id="shipping_details">
								<tr>
									<td><b>Country:</b> ' . $shipping_info->country . '</td>
								</tr>
								<tr>
									<td><b>State:</b> ' . $shipping_info->state . '</td>
								</tr>
								<tr>
									<td><b>Street Address:</b> ' . $shipping_info->street . '</td>
								</tr>
								<tr>
									<td><b>Postal Code:</b> ' . $shipping_info->zip . '</td>
								</tr>
								<tr>
									<td><b>Town/City:</b> ' . $shipping_info->city . '</td>
								</tr>
								<tr>
									<td><b>Phone:</b> ' . $shipping_info->phone . '</td>
								</tr>
							</table>';

							$email_message .= '
    										<p><h3>Order List </h3></p>
    										<table id="orders">
    										  <tr>
    										    <th>Product</th>
                                                <th>Category</th>
    										    <th>Price</th>
    										  </tr>
											 ';
							usort($order_list, function ($a, $b) {
								return strcmp($a->card->set_name, $b->card->set_name);
							});
							foreach ($order_list as $order) {
								$card_name = getCardName($order->card, 'json');
								$email_message .= '<tr>';
								$email_message .= '<td>' . $order->quantity . 'x ' . $card_name . '</td>';
								$email_message .= '<td>' . $order->card->set . '</td>';
								$email_message .= '<td>' . $order->card->price . '฿</td>';
								$email_message .= '</tr>';

								// $card = $this->Global_model->fetch('products', array('id' => $order->card->id));

								// if($card) {
								// 	$quantity = intval($card[0]->stock) - intval($order->quantity);
								// 	$quantity = $quantity < 0 ? 0 : $quantity;
								// 	$this->Global_model->update('products', array('stock' => $quantity), array('id' => $card[0]->id));
								// }
							}
							$email_message .= '
    										</table>
    									</body>
    									</html>
    					';

							$email_content = [
								'from' => 'hello@alg.tw',
								'from_name' => "ALG Orders",
								'to' => get_email(),
								'subject' => 'Thank you for purchasing',
								'message' => $email_message
							];

							$isSent = sendMailCIMailer($email_content);
						} else {
							audit("payment", "payment: " . get_email(), "order paypal", "failed");
							redirect(base_url());
							exit();
						}
						break;
					case 'FAILED':
						$order_list = json_decode($data['cart_list']);
						foreach ($order_list as $order) {
							$card = $this->Global_model->fetch('products', array('id' => $order->card->id));

							if ($card) {
								$quantity = intval($card[0]->stock) + intval($order->quantity);
								$quantity = $quantity < 0 ? 0 : $quantity;
								$this->Global_model->update('products', array('stock' => $quantity), array('id' => $card[0]->id));
							}
						}
						$this->Global_model->update('cart_order', array('admin_status' => 4), array('id' => $cart_id));

						audit("payment", "payment: " . get_email(), "order paypal", "failed");
						redirect(base_url());
						exit();
				}

				redirect(base_url() . "Thank_you/?id=" . $cart_id);
				exit;
			}
		}
