<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loyalty_program_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Loyalty_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}

	public function get_all_loyalty_program() {
		$data['loyalty_program'] = $this->Loyalty_model->fetch_tag('*','loyalty_program');
		echo json_encode($data);
	}

	public function get_loyalty_program() {
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
		$data["loyalty_program"] = $this->User_model->fetch_data('loyalty_program',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$loyalty_data = json_decode(post('loyalty_data'));
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Loyalty_model->update('loyalty_program',$loyalty_data,$filter);
		echo json_encode($response);
	}
}