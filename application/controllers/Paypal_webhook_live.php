<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Webhooks_paypal_live extends MY_Controller{
		private $mode;

		public function __construct(){
			parent::__construct();
			$this->mode = 'Live';
			$this->load->model('Global_model');
			$this->load->library('PayPal');
		}

		public function index(){
			$input = @file_get_contents('php://input');

			if($input == '') return;
			$event = json_decode($input);
			if(!isset($event->event_type)) return;
			switch ($event->event_type) {
				case 'PAYMENT.SALE.COMPLETED':
					if(!isset($event->resource)) break;
					$this->saleCompleted($event->resource);
					break;
				case 'PAYMENT.SALE.REFUNDED':
					if(!isset($event->resource)) break;
					$this->saleRefund($event->resource);
					break;
			}
		}

		public function saleCompleted($param=NULL){

			if(is_null($param)) return;
			$returnObj = isset($param->parent_payment) ? $this->paypal->getPayment($param->parent_payment, $this->mode) : NULL;
			$returnObj = isset($param->billing_agreement_id) ? $this->paypal->getBillingAgreement($param->billing_agreement_id, $this->mode) : $returnObj;

			if(is_null($returnObj)) return;
			$cx = $this->user_model->get_data(array('paypal_customer'=>$returnObj->payer->payer_info->payer_id));

			if(empty($cx)) return;
			$isfound = $this->Global_model->fetch('payments', array('charge_id', $param->id);

			if($isfound) return;
			//----------------
			$charge_data = array(
				'created' => getDateTimeByZone(array('date'=>$param->create_time,'zone'=>'PST')),
				'charge_id' => $param->id,
				'amount' => $param->amount->total,
				'currency' => isset($param->amount->currency) ? strtolower($param->amount->currency):'THB',
				'customer' => $returnObj->payer->payer_info->payer_id,
				'refund' => 'Active',
				'processor' => 'PayPal',
				'mode' => $this->mode
			);
			$this->Global_model->insert('payments', $charge_data);			
			//----------------
		}

		public function saleRefund($param=NULL){

			if(is_null($param)) return;
			$payment = $this->Global_model->fetch('payments', array('charge_id'=>$param->sale_id));
			
			if(empty($payment)) return;
			//-----------------

			$charge_data = array(
				'refund' => 'Refunded'
			);

			
			$this->Global_model->update('payments', $charge_data, array('charge_id' => $param->sale_id));
			//-----------------
		}

	}
