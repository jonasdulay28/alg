<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
		if(is_logged() < 1) 
			redirect(base_url());
	}

	public function index()
	{
		$id = clean_data(get('order'));
		$order = $this->Global_model->fetch_tag('cart_list, invoice_number, payment_details, points_earned, status, admin_status, payment_mode','cart_order', array('email' => get_email(), 'id' => $id));
		if(empty($order)) show_404();
		
		$data['order'] = $order[0];
		$data['content'] = 'pages/order';
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');

		$this->load->view('templates/main_template', $data);
	}
}