<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class users_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}
	
	public function get_users() {
		$data['users'] = $this->User_model->get_all_users('users');
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$user_data = json_decode(post('user_data'));
		$user_data->password = hash_password($user_data->password);
		
		$data = array("email"=>$user_data->email,"credits"=>0);
		$this->User_model->insert("user_credit",$data);
		$this->User_model->insert('users',$user_data);
		echo json_encode($response);
	}

	public function get_user(){
		$email = clean_data(rawurldecode(get('q')));
		$filter = ["a.email"=>$email];
		$data["user"] = $this->User_model->get_user_info($filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$user_data = json_decode(post('user_data'));
		$current_loyalty = clean_data(post('current_loyalty'));
		$current_credits= clean_data(post('current_credits'));
		$credits = clean_data(post('credits'));
		$new_loyalty = $user_data->user_loyalty;
		$email = clean_data(post('email'));
		if(isset($user_data->password)) {
			$user_data->password = hash_password($user_data->password);
		}

		if($current_loyalty != $new_loyalty) {
			$filter = ["id"=>$new_loyalty];
			$row = $this->User_model->fetch_tag_row("lifetime_spend","loyalty_program",$filter);
			$lifetime_spend = $row->lifetime_spend;
			$user_data->user_exp = $lifetime_spend;
		}

		
		
		$filter = ["email"=>$email]; 
		$this->User_model->update('users',$user_data,$filter);
   // audit("logs","grant_user_points: ".$data[0]['email'],json_encode($update_data),"success");
		if($current_credits != $credits) {
			$data = ["credits"=>$credits];
			$this->User_model->update('user_credit',$data,$filter);
			$admin_email = get_email();
			audit_purrcoins("Set purrcoins",$admin_email." changed ".$email."'s purrcoins","set purrcoins",$credits,$email);
		}
		
		echo json_encode($response);
	}

	public function delete()
	{
		$email = clean_data(post('email'));
		$filter = ["email"=>$email]; 
		$this->User_model->delete('users',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$email = clean_data(post('email'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status"=>$status];
		$filter = ["email"=>$email]; 
		$this->User_model->update('users',$data,$filter);
		audit("logs","changeStatus: ".$email,json_encode($data),"success");
		echo json_encode($response);
	}
}