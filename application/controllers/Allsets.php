<?php
defined('BASEPATH') or exit('No direct script access allowed');

class allsets extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Global_model");
		$this->load->model("Allsets_model");
	}

	public function index()
	{
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status' => 'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		//$data['data']['sets'] = $this->Allsets_model->get_sets();
		$tmp_sets_translations = $this->Global_model->fetch('sets_translations');
		$data['data']['sets_translations'] = [];
		$data['main_category'] = [];
		$data['main_set_code'] = [];

		foreach ($tmp_sets_translations as $key) {
			$data['data']['sets_translations'][$key->set_name] = $key->set_name_chi;
		}

		foreach ($data['data']['categories'] as $key) {
			//print_r(json_decode($key->sub_category));
			$data['main_category'] = array_merge($data['main_category'], json_decode($key->sub_category));
			$data['main_set_code'] = array_merge($data['main_set_code'], json_decode($key->sub_category_main_code));
		}
		$this->load->view('templates/allsets_template', $data);
	}

	public function get_set_chi()
	{
		$tmp_sets_translations = $this->Global_model->fetch('sets_translations');
		$sets_translations = [];
		foreach ($tmp_sets_translations as $key) {
			$sets_translations[$key->set_name] = $key->set_name_chi;
		}

		$row = $this->Global_model->fetch_tag_row('sub_category','menu',['main_category'=>'Special']);
		$sub_category = json_decode($row->sub_category);
		$sets = [];
		foreach($sub_category as $item) {
			$sets[] = isset($sets_translations[$item]) ? '"'.$sets_translations[$item].'"' : '"'.$item.'"';
		}
		echo "[".implode(",",$sets)."]";
		//$this->Global_model->update('menu',['sub_category_chi'=>json_encode([$sets])],['main_category'=>'Modern']);


	}
}
