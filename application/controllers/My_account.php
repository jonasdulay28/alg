<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class my_account extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
		if(is_logged() < 1) 
			redirect(base_url());
	}

	public function index()
	{
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$data['data']['countries'] = $this->Global_model->fetch_tag('id,name','countries');
		$data['data']['user_credit'] = $this->Global_model->fetch('user_credit', array('email'=>get_email()));

		$user = $this->Global_model->fetch_data('users', array('email' => get_email()));
		if($user){
			$loyalty = $this->Global_model->fetch_data('loyalty_program', array('id' => $user[0]->user_loyalty), 1);
			$user_credits = $this->Global_model->fetch_data('user_credit', array('email'=> get_email()));

			$next_loyalty = $this->Global_model->fetch_data('loyalty_program', array('level_number =' => 'Level '. ($user[0]->user_loyalty + 1)), 1);
			$current_loyalty = $this->Global_model->fetch_data('loyalty_program', array('level_number =' => 'Level '. ($user[0]->user_loyalty)), 1);

			$current_exp = (int)$user[0]->user_exp;
			$data['current_exp'] = $next_loyalty ? (int)((($current_exp - $current_loyalty[0]->lifetime_spend) / ($next_loyalty[0]->lifetime_spend - $current_loyalty[0]->lifetime_spend)) * 100) : 100;
			$data['current_exp'] = $data['current_exp'] < 0 ? 0 : $data['current_exp'];
			$data['required_exp'] = $next_loyalty ? (int)$next_loyalty[0]->lifetime_spend - $current_exp : 0;
		
			if($user_credits) $data['credits'] = $user_credits[0]->credits;
			if($loyalty) $data['badge'] = $loyalty[0]->badge;
		}

		$data['data']['cities'] = $this->Global_model->fetch_tag('id,name','cities');
		$data['data']['orders'] = $this->Global_model->fetch_tag('id, invoice_number, created_at, status, admin_status,payment_mode, price','cart_order', array('email' => get_email()));
		$this->load->view('templates/my_account_template',$data);
	}

	public function update_billing_address() {
		$res = ["message" => "error"];
		if($_POST) {
			$email = get_email();
			if($email != "") {
				$billing_address_data = [];
				foreach($_POST as $key => $value){
					if($key == "submit_"){
					
					}else{
							$billing_address_data[$key] = strtoupper(clean_data($value));
					}
				}
				$filter = ["email" => $email]; 
				$result = $this->Global_model->check_exist('billing_address',$filter);
				if($result) {
					$this->Global_model->update('billing_address', $billing_address_data, $filter);	
				} else {
					$billing_address_data['email'] = $email; 
					$this->Global_model->insert('billing_address', $billing_address_data);
				}
			}
		}
		echo json_encode($res);
	}

	public function update_shipping_address() {
		$res = ["message" => "error"];
		if($_POST) {
			$email = get_email();
			if($email != "") {
				$shipping_address_data = [];
				foreach($_POST as $key => $value){
						if($key == "submit_"){
					
					}else{
							$shipping_address_data[$key] = clean_data($value);
					}
				}
				$filter = ["email" => $email]; 
				$result = $this->Global_model->check_exist('shipping_address',$filter);
				if($result) {
					$this->Global_model->update('shipping_address', $shipping_address_data, $filter);	
				} else {
					$shipping_address_data['email'] = $email; 
					$this->Global_model->insert('shipping_address', $shipping_address_data);
				}
				$res["message"] = "success";
			}
		}
		echo json_encode($res);
	}

	public function update_account_info(){
		$res = ["message" => "error"];
			$email = get_email();
			$password = clean_data(post('password'));
			$new_password = clean_data(post('new_password'));
			$cf_password = clean_data(post('cf_password'));
			$firstname = clean_data(post('first_name'));
			$lastname = clean_data(post('last_name'));
			if($email != "" && $password != "") {
				$filter = ["email" => $email]; 
				$row = $this->Global_model->fetch_tag_row('password','users',$filter);
				$current_password = $row->password; 

				$user_data = [];
				if(password_verify($password,$current_password)) {
					if($new_password != "" && $cf_password != "" && ($new_password == $cf_password)){
						$user_data['password'] = hash_password($new_password);
						$res["message"] = "success";
					} else {
						$res["message"] = "success";
					}
				} else {
					$res["message"] = "error2";
				}
				
				foreach($_POST as $key => $value){
					if($key == "password" || $key == "new_password" || $key == "cf_password" ||  $key == "submit_" || $key == "email"){
					
					}else{
							$user_data[$key] = clean_data($value);
					}
				}
				$this->Global_model->update('users', $user_data, $filter);	
        audit("logs","update_account_info: ".$email,json_encode($user_data),"success");

			} else {
				$filter = ["email" => $email]; 
				$user_data = ["first_name"=>$firstname,"last_name" => $lastname];
				$res["message"] = "success";
				$this->Global_model->update('users', $user_data, $filter);
        audit("logs","update_account_info: ".$email,json_encode($user_data),"success");
			}
		echo json_encode($res);
	}

	public function logout(){
		session_destroy();
		if (isset($_COOKIE['ses_pass'])) {
	    unset($_COOKIE['ses_pass']);
	    setcookie('ses_pass', '', time() - 3600, '/'); // empty value and old timestamp
		}
		if (isset($_COOKIE['ses_key'])) {
	    unset($_COOKIE['ses_key']);
	    setcookie('ses_key', '', time() - 3600, '/'); // empty value and old timestamp
		}
		redirect(base_url());
	}
}

