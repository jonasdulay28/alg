<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class products extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        // Load member model
        $this->load->model('Csv_product_model');
        
        // Load form validation library
        $this->load->library('form_validation');
        
        // Load file helper
        $this->load->helper('file');
        if(is_logged_admin()){
				}else {
					redirect(base_url());
				}
    }
    
    public function index(){
        $data = array();
        
        // Get messages from the session
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        
        // Get rows
      	//$data['members'] = $this->Csv_product_model->getRows();
        
        // Load the list page view
        $this->load->view('pages/import', $data);
    }
    
    public function import(){
        $data = array();
        $prodData = array();
        
        // If import request is submitted
        if($this->input->post('importSubmit')){
            // Form field validation rules
            $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
            if($this->form_validation->run() == true){
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
                
                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');
                    
                    // Parse data from CSV file
                    
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
                    
                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row){ $rowCount++;
                            // Prepare data for DB insertion
                            $prodData = array(
                                'name' => $row['name'],
                                'set' => $row['set'],
                                'set_code' => $row['set_code'],
                                'base_price' => $row['base_price'],
                                'regular_price' => $row['regular_price'],
                                'foil_price' => $row['foil_price'],
                                'stock' => $row['stock'],
                                'image' => $row['image'],
                                'type' => $row['type'],
                                'rarity' => $row['rarity'],
                                'color' => $row['color'],
                                'filtered_color' => $row['filtered_color'],
                                'collector_number' => $row['collector_number'],
                                'card_type' => $row['card_type'],
                                'priority' => $row['priority'],
                                'category' => $row['category'],
                                'serial_number' => $row['serial_number'],
                                'power' => $row['power'],
                                'toughness' => $row['toughness'],
                                'loyalty' => $row['loyalty'],
                                'manacost' => $row['manacost'],
                                'converted_manacost' => $row['converted_manacost'],
                                'artist' => $row['artist'],
                                'flavor' => $row['flavor'],
                                'generated_mana' => $row['generated_mana'],
                                'rating' => $row['rating'],
                                'ruling' => $row['ruling'],
                                'variation' => $row['variation'],
                                'ability' => $row['ability'],
                                'watermark' => $row['watermark'],
                                'is_original' => $row['is_original'],
                                'back_id' => $row['back_id'],
                                'number_int' => $row['number_int'],
                                'color_identity' => $row['color_identity'],
                                'fully_handled' => $row['fully_handled'],
                                'is_special' => $row['is_special'],
                                'custom_sort' => $row['custom_sort'],
                                
                            );
                            // Check whether email already exists in the database
                            $con = array(
                                'where' => array(
                                    'name' => $row['name'],
                                    'set_code' => $row['set_code']
                                ),
                                'returnType' => 'count'
                            );
                            $prevCount = $this->Csv_product_model->getRows($con);
                            if($prevCount > 0){
                                // Update member data
                                $condition = array('name' => $row['name'], 'set_code'=> $row['set_code']);
                                $update = $this->Csv_product_model->update($prodData, $condition);
                                
                                if($update){
                                    $updateCount++;
                                }
                            }else{
                                // Insert member data
                                $insert = $this->Csv_product_model->insert($prodData);
                                
                                if($insert){
                                    $insertCount++;
                                }
                            }
                        }
                        
                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'Products imported successfully. Total Rows ('.$rowCount.') | Inserted ('.$insertCount.') | Updated ('.$updateCount.') | Not Inserted ('.$notAddCount.')';
                        $this->session->set_userdata('success_msg', $successMsg);
                    }
                }else{
                    $this->session->set_userdata('error_msg', 'Error on file upload, please try again.');
                }
            }else{
                $this->session->set_userdata('error_msg', 'Invalid file, please select only CSV file.');
            }
        }
       redirect('products');
    }
    
    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }
}