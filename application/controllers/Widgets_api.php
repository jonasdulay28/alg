<?php
defined('BASEPATH') or exit('No direct script access allowed');

class widgets_api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Widgets_model');
		if (is_logged_admin() == 0) {
			redirect(base_url());
		}
	}

	public function getNewestExpansion()
	{
		$data['newest_expansion'] = $this->Widgets_model->fetch('newest_expansion', "", "6", "", "id desc");
		echo json_encode($data);
	}

	public function get_banners()
	{
		$data['banners'] = $this->Widgets_model->fetch('banners', "", "", "", "order asc");
		echo json_encode($data);
	}

	public function updateBanner()
	{
		$response['message'] = "success";
		$image_list = post('image_list');
		$data;
		$x = 0;
		foreach ($image_list as $banner) {
			foreach ($banner as $key => $value) {
				$data[$x][$key] = $value;
				if ($key == 'updated_at') {
					$x++;
				}
			}
		}
		$this->Widgets_model->delete('banners', ["id LIKE" => "%%"]);
		$x = 0;
		foreach ($data as $key) {
			$this->Widgets_model->insert('banners', $data[$x]);
			$x++;
		}

		echo json_encode($response);
	}

	public function deleteBanner()
	{
		$response['message'] = "success";
		$id = clean_data(post('id'));
		$file_name = clean_data(post('file_name'));
		$filter = ['id' => $id];
		unlink('uploads/' . $file_name);
		$this->Widgets_model->delete('banners', $filter);
		echo json_encode($response);
	}

	public function add()
	{
		$response = ["message" => "success"];
		$banner_data = [];
		if (!empty($_FILES['file']['name'])) {
			// where are you storing it
			$path = './uploads';
			// what are you naming it
			$new_name = 'prod-' . time();
			// start upload
			$input = 'file';
			$result = $this->upload_image($path, $new_name, $input);
			if ($result == "success") {
				$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$banner_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
				$banner_data['file_name'] = $new_name . "." . $ext;
				$row = $this->Widgets_model->fetch_tag_row('order', 'banners', '', 1, '', 'order desc');
				$order = $row->order + 1;
				$banner_data['order'] = $order;
			} else {
				$response["message"] = "failed";
			}
		} else {
			$response["message"] = "failed";
		}

		$this->Widgets_model->insert('banners', $banner_data);
		echo json_encode($response);
	}



	public function add_newest_expansion()
	{
		$response = ["message" => "success"];
		$banner_data = [];
		if (!empty($_FILES['file']['name'])) {
			// where are you storing it
			$path = './uploads';
			// what are you naming it
			$new_name = 'prod-' . time();
			// start upload
			$input = 'file';
			$result = $this->upload_image($path, $new_name, $input);
			if ($result == "success") {
				$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$banner_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
				$banner_data['file_name'] = $new_name . "." . $ext;
				$banner_data['set'] = clean_data(post('set_name'));
				$banner_data['set_code'] = clean_data(post('set_code'));
				$this->Widgets_model->insert('newest_expansion', $banner_data);
			} else {
				$response["message"] = $result;
			}
		} else {
			$response["message"] = "error";
		}


		echo json_encode($response);
	}

	public function update_newest_expansion()
	{
		$response = ["message" => "success"];
		$banner_data = [];
		if (!empty($_FILES['file']['name'])) {
			// where are you storing it
			$path = './uploads';
			// what are you naming it
			$new_name = 'prod-' . time();
			// start upload
			$input = 'file';
			$result = $this->upload_image($path, $new_name, $input);
			if ($result == "success") {
				$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$banner_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
				$banner_data['file_name'] = $new_name . "." . $ext;
				$banner_data['set'] = clean_data(post('set_name'));
				$banner_data['set_code'] = clean_data(post('set_code'));
				$id = clean_data(post('id'));
				$tmp_file_name = clean_data(post('tmp_file_name'));
				//unlink('uploads/' . $tmp_file_name);
				$filter = ['id' => $id];
				$this->Widgets_model->update('newest_expansion', $banner_data, $filter);
			} else {
				$response["message"] = $result;
			}
		} else {
			$banner_data['set'] = clean_data(post('set_name'));
			$banner_data['set_code'] = clean_data(post('set_code'));
			$id = clean_data(post('id'));
			$filter = ['id' => $id];
			$response["message"] = "success";
			$this->Widgets_model->update('newest_expansion', $banner_data, $filter);
		}


		echo json_encode($response);
	}

	public function get_menu()
	{
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id" => $id];
		$data["menu"] = $this->Widgets_model->fetch_data('menu', $filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message" => "success"];
		$sub_category = post('sub_category');
		$sub_category_code = post('sub_category_code');
		$sub_category_main_code = post('sub_category_main_code');
		$id = clean_data(post('id'));
		$data = ["sub_category" => $sub_category, "sub_category_code" => $sub_category_code, "sub_category_main_code" => $sub_category_main_code];
		$filter = ["id" => $id];
		$this->Widgets_model->update('menu', $data, $filter);
		echo json_encode($response);
	}

	public function deleteNewestExpansion()
	{
		$response['message'] = "success";
		$id = clean_data(post('id'));
		$file_name = clean_data(post('file_name'));
		$filter = ['id' => $id];
		unlink('uploads/' . $file_name);
		$this->Widgets_model->delete('newest_expansion', $filter);
		echo json_encode($response);
	}

	public function changeStatus()
	{
		$id = clean_data(post('id'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status" => $status];
		$filter = ["id" => $id];
		$this->Widgets_model->update('coupons', $data, $filter);
		echo json_encode($response);
	}

	function upload_image($path, $new_name, $input)
	{
		// define parameters
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';
		$config['max_size'] = '0';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['file_name'] = $new_name;
		$this->load->library('upload');
		$this->upload->initialize($config);
		// upload the image
		if ($this->upload->do_upload($input)) {
			return "success";
		} else {
			return $this->upload->file_type;
		}
	}
}
