<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class secure_admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("User_model");
	}

	public function index(){
		if(is_logged_admin()){
			$this->load->view('templates/admin_template');
		}else {
			redirect(base_url('secure_admin/authentication'));
		}
	}

	public function authentication() {
		if(is_logged_admin()){
			redirect(base_url('secure_admin'));
		}else {
			$this->load->view('templates/admin_login_template');
		}
		
	}

	public function login() {
		$response = array("message"=>"Invalid email or password.");
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|trim');
    if ($this->form_validation->run() == TRUE) {
			$email = strtolower(clean_data(post('email')));
			$password=  clean_data(post('password'));
			$filter = array("email"=>$email,"status"=>1);
			$user_details = $this->User_model->fetch_tag_row("password,role","users",$filter);
			if(empty($user_details)) {
				$response["message"] = "Invalid email or password.";
			} else {
				$user_password = $user_details->password;
				$user_role = $user_details->role;
				if($user_role == "admin") {
					if(password_verify($password,$user_password)) {
						$response["message"] = 'success';
						$response["url"] = base_url('secure_admin');
						$ses_key = encrypt($email);
						$ses_pass = encrypt($password);
						$sessdata = array('ses_key'  => $ses_key); 
						$remember_me = clean_data(post('remember_me'));
						if($remember_me == "remember"){
							setcookie( 'ses_key', $ses_key, time() +2592000,"/");
							setcookie( 'ses_pass', $ses_pass, time() +2592000,"/");
						}
						$this->session->set_userdata($sessdata);
						audit("login","admin","authentication","");
					} 
				} else {
					audit("login","admin","authentication","attempt");
					$response["message"] = "Invalid email or password.";
				}
			}
    } else {
    	$response["message"] = "Invalid email or password.";
    }
		echo json_encode($response);
	}

	public function logout(){
		audit("logout","admin","authentication","");
		session_destroy();
		if (isset($_COOKIE['ses_pass'])) {
	    unset($_COOKIE['ses_pass']);
	    setcookie('ses_pass', '', time() - 3600, '/'); // empty value and old timestamp
		}
		if (isset($_COOKIE['ses_key'])) {
	    unset($_COOKIE['ses_key']);
	    setcookie('ses_key', '', time() - 3600, '/'); // empty value and old timestamp
		}
		$data["url"] = base_url('secure_admin');
		echo json_encode($data);

	}
}