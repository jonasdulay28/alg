<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class checkout extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
	}

	public function index() {
		$this->session->unset_userdata('insert_order');
		$data['content'] = 'pages/checkout';
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$data['countries'] = $this->Global_model->fetch_tag('id,name','countries');
		$data['cities'] = $this->Global_model->fetch_tag('id,name','cities');
		$single_only = true;

		$cart_data = getUpdatedCartList();
		$data['cart_list'] = $cart_data;
		$sub_total = 0;
		foreach($cart_data as $cart) {
			$sub_total += (isset($cart['card']->price) ? $cart['card']->price : $cart['card']->regular_price) * $cart['quantity'];
			if($cart['card']->category != 'single cards') $single_only = false;
		}

	  	if($single_only) {
		  	$data['shipping_fee'] = array(35, 70);
	  		$data['free_shipping_fee'] = array(1000, 1500);
	  	} else {
		  	$data['shipping_fee'] = array(70, 70);
	  		$data['free_shipping_fee'] = array(1500, 1500);
	  	}
	  	$data['int_shipping_fee'] = array(90, 600, 2100);
	  	$data['free_int_shipping_fee'] = array(3000, 12000, 45000);

		$data['single_only'] = $single_only;
		$data['sub_total'] = $sub_total;

		if(is_logged() > 0) {
			if(!$cart_data) {
				redirect(base_url('cart'));
			}

			$data['user'] = $this->Global_model->fetch('users', array('email' => get_email()));
			$data['loyalty'] = $this->Global_model->fetch('loyalty_program', array('id' => $data['user'][0]->user_loyalty));
			$data['billing_info'] = $this->Global_model->fetch('billing_address', array('email' => get_email()));
			$data['shipping_info'] = $this->Global_model->fetch('shipping_address', array('email' => get_email()));
			if($data['billing_info'])
				$data['billing_state'] = $this->Global_model->fetch('states', array('country_id' => $data['billing_info'][0]->country));
			
			if($data['shipping_info'])
				$data['shipping_state'] = $this->Global_model->fetch('states', array('country_id' => $data['shipping_info'][0]->country));
		} else {
			redirect(base_url('authentication?redirect=checkout'));
		}

		$this->load->view('templates/checkout_template', $data);
	}

}