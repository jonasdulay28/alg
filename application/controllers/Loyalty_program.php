<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loyalty_program extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
		$this->load->model("Loyalty_model");
	}

	public function index($id = null) {
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$data['data']['loyalty'] = $this->Loyalty_model->get_loyalty_by_level();
		$this->load->view('templates/loyalty_program_template',$data);
	}

}