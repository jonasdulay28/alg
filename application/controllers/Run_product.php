<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Run_product extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
		$this->load->model('Product_model');
	}

	public function index() {
		$this->load->view('run');
	}

	public function run() {
		$products = $this->Product_model->run_product();
		if($products){
			for($count = 0; $count < sizeof($products); $count++) {
				$price = $this->Global_model->fetch('products_prices', array('name' => $products[$count]->name), '1');
				if($price) {
					$this->Global_model->update('products',array('regular_price'=>$price[0]->regular_price), array('id'=>$products[$count]->id));
					$this->Global_model->update('products_prices', array('is_active'=> 1), array('id'=>$price[0]->id));
				}
			}
		}
	}

}