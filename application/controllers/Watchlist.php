<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Watchlist extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
		$this->load->model("Product_model");
	}

	public function index() {
		if(is_logged() == 0) {
			$watchlist_data = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist'): [];
		} else {
			$watchlist_data = $this->Global_model->fetch('watchlist', array('email'=>get_email()));
			$watchlist_data = $watchlist_data ? json_decode($watchlist_data[0]->watchlist, TRUE) : [];
		}

		$watchlist = [];
		if(!empty($watchlist_data)) {
			foreach($watchlist_data as $watch) {
        		$display_name = 'a.name as name, a.ability as ability, a.type as type, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw, a.language as language, a.category as category';
		        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
		        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';

				$prod_tag = 'a.id as id, a.fully_handled as fully_handled, a.set_code as set_code, a.collector_number as collector_number, a.rarity as rarity, a.color as color, a.set as `set`, a.manacost as manacost, a.additional_img as additional_img, a.artist as artist, a.regular_price as regular_price, a.fully_handled as fully_handled, a.foil_price as foil_price, image as image, a.stock as stock, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color, ' . $display_name;
				$card = $this->Product_model->get_sort($prod_tag, 'WHERE a.id = ' . $watch['id']);
				if(!empty($card)) $watchlist[$watch['id']] = $card[0];
				else unset($watchlist[$watch['id']]);
			}
		}
		$data['watchlist'] = $watchlist;
		$data['content'] = 'pages/watchlist';
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');

		$data['styles'] = array(
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/css/watchlist.css">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/libraries/manareplacer/mana-symbols.css">'
		);
		$data['scripts'] = array(
			'<script type="text/javascript" src="'. base_url() .'assets/js/watchlist.js"></script>'
		);

		$this->load->view('templates/main_template', $data);
	}

}