<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class thank_you extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
	}

	public function index() {
		$id = clean_data(get('id'));
		if(is_logged() < 1) redirect(base_url('authentication'));

		//if(!$cart_order[0]) show_404();

		$this->load->view('templates/thank_you_template');
	}

	public function reference() {
		$reference_id = clean_data(get('reference_id'));
		if(is_logged() < 1) redirect(base_url('authentication'));

		//if(!$cart_order[0]) show_404();
		$data['reference_id'] = $reference_id;
		$data['content'] = 'pages/thank_you_reference';
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');

		$this->load->view('templates/main_template', $data);
	}

}