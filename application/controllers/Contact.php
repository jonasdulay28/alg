<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class contact extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
	}

	public function index()
	{
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$this->load->view('templates/contact_template',$data);
	}

	public function send_message() {
		$isSent = false;
		$name = clean_data(post('name'));
		$email = clean_data(post('email'));
		$message = clean_data(post('message'));
 
		$contactusemail = '
						<html>
						<body>
							<table style="width: 700px; font-family: arial">
								<tr style="height: 80px;">
									<td style="text-align: left;">
										<span style="font-weight: bold;">Name:</span> '.$name.'
										<br>
										<span style="font-weight: bold;">Email:</span> '.$email.'
										<br>
										<span style="font-weight: bold;">Message:</span> '.nl2br($message).'
									</td>
								</tr>
							</table>
						</body>
						</html>
		';

        $email_content = [
            'from' => 'hello@alg.tw',
            'from_name' => $name."- [".$email."]",
            'to' => 'hello@alg.tw',  
            'subject' => 'Contact Us - '.$email,
            'message' => $contactusemail,
            'reply_to' => $email
        ];

		$isSent = sendMailCIMailer2($email_content);		

		if ($isSent){
			$returned_value = [
				'status' => 'success',
				'error_msg' => 'Message Sent'
			];

		}else{
			$returned_value = [
				'status' => 'failed',
				'error_msg' => 'Something went wrong. Please try again later.'
			];
		}
		echo json_encode($returned_value);
	}
	public function test_inc_email() {
		$email_content = [
            'from' => 'hello@alg.tw',
            'from_name' => "test",
            'to' => 'hello@alg.tw',  
            'subject' => 'Contact Us - ',
            'message' => "test"
        ];

		$isSent = sendMailCIMailer2($email_content);	
		echo $isSent;
	}


	public function test_email() {
		$email_content = [
            'from' => 'hello@alg.tw',
            'from_name' => "test",
            'to' => 'jonasdulay28@gmail.com',  
            'subject' => 'Contact Us - ',
            'message' => "test sdfsdf"
        ];

		$isSent = sendMailCIMailer($email_content);	
		echo $isSent;
	}

	public function test(){
		$data= "";
		$this->load->view('pages/test');
	}
}
