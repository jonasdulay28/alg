<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cards extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("User_model");
		$this->load->model("Product_model");
		$this->load->model("Menu_model");
		$this->load->model("Filter_model");
		$this->load->model("Global_model");
	}

/*	public function test() {
		$re = '/(.*)(?:\(?\d*\/\d*\)?)/m';
		$str = 'Soldier Token 21/12';

		preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);



		// Print the entire match result
		var_dump($matches);
			echo preg_replace('/\d/', '', $matches[0][1] ); 
	}*/
	public function index() {
		if(is_logged() == 0) {
			$watchlist_data = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist'): [];
		} else {
			$watchlist_data = $this->Global_model->fetch('watchlist', array('email'=>get_email()));
			$watchlist_data = $watchlist_data ? json_decode($watchlist_data[0]->watchlist, TRUE) : [];
		}
		$data['watchlist_data'] = $watchlist_data;
		$data['search_name'] = trim(addslashes(urldecode(get('name'))));

		if($data['search_name']){
			$query = 'SELECT id FROM products WHERE `name` LIKE "%'.$data['search_name'].'%" AND category != "other products" LIMIT 1';
			$product_category = $this->Product_model->get_query($query);
			if(!$product_category) {
				//quest mo naman ng chinese
				$query = 'SELECT id FROM cards WHERE `name_tw` LIKE "%'.$data['search_name'].'%"  LIMIT 1';
				$product_category = $this->Product_model->get_query($query);
			} 

			if(!$product_category) {
				header("Location: ". base_url('accessories/?name='.urlencode($data['search_name'])));
				return;
			}
		}

		$data['cat_code'] = get('cat') ? get('cat') : '' ;
		$filter = ["set_code"=>clean_data(get('cat'))];
		$get_set_name = $this->Global_model->fetch_tag_row("products.set as set_name","products",$filter,1);
		$data['set_name'] = get('cat');
		$data['language_filter'] = $this->Global_model->fetch('language');

		$session_sort = $this->session->userdata('session_sort');
		if($session_sort){
			if($session_sort['cat'] != $data['cat_code'] || $session_sort['search_name'] != $data['search_name']) {
				$session_sort = [];
				$this->session->unset_userdata('session_sort');
			}
		}

		if($session_sort && $session_sort['sort']) {
			if(get('rarity')) $session_sort['sort']->rarity = get('rarity');
		} else {
			$session_sort = array( 'sort' => array( 'rarity' => get('rarity') ) );
		}

		$data['session_sort'] = $session_sort ? $session_sort : [];

		if(!empty($get_set_name)) {
			$data['set_name'] = $get_set_name->set_name;
		}

		if(!$data['search_name'] && !$data['cat_code']) show_404();

		$data['styles'] = array(
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/css/card_list.css">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/libraries/manareplacer/mana-symbols.css">'
		);
		$data['scripts'] = array(
			'<script type="text/javascript" src="'. base_url() .'assets/js/cards.js"></script>'
		);

		$data['content'] = 'pages/card_list';

		$variations = $this->Filter_model->get_where(array('main_filter' => 'Variation'));
		$colors = $this->Filter_model->get_where(array('main_filter' => 'Colors'));
		$types = $this->Filter_model->get_where(array('main_filter' => 'Type'));
		$rarity = $this->Filter_model->get_where(array('main_filter' => 'Rarity'));
		$promo = $this->Filter_model->get_where(array('main_filter' => 'Promo'));

		$data['filter_variations'] = json_decode($variations[0]['sub_filter']);
		$data['filter_colors'] = json_decode($colors[0]['sub_filter']);
		$data['filter_type'] = json_decode($types[0]['sub_filter']);
		$data['filter_rarity'] = json_decode($rarity[0]['sub_filter']);
		$data['filter_promo'] = json_decode($promo[0]['sub_filter']);
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$set_code_cond = get('cat') ? ' AND a.set_code = "' . get('cat') . '"' : '';
		$data['datestamp'] = $this->Product_model->get_sort('DISTINCT a.datestamp', 'WHERE a.category != "other products" AND a.promo ="datestamp" AND (a.regular_price != "" OR a.foil_price != "")'.$set_code_cond);

		$this->load->view('templates/main_template', $data);
	}

	public function view($id = null) {
		if($id == null) show_404();

		$language = get('lang') ? get('lang') : $this->session->userdata('language');
        $display_name = 'a.name as name,a.language, a.fully_handled as fully_handled, a.ability as ability, a.type as type, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw, a.category as category';
        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';

		$prod_tag = 'a.id as id, a.fully_handled as fully_handled, a.set_code as set_code, a.collector_number as collector_number, a.rarity as rarity, a.color as color, a.set as `set`, a.manacost as manacost, a.additional_img as additional_img, a.artist as artist, a.regular_price as regular_price, a.foil_price as foil_price, a.image as image, a.stock as stock, a.language as language, a.promo as promo, a.datestamp as datestamp, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color, ' . $display_name;
		$card = $this->Product_model->get_sort($prod_tag, 'WHERE a.id = ' . $id);
		if(!$card) show_404();
		$ability = addslashes($card[0]['ability']);
		$card[0]['ability'] = str_replace("£","<br>",  $card[0]['ability']);
		$card[0]['ability'] = str_replace("#_","<i>",  $card[0]['ability']);
		$card[0]['ability'] = str_replace("_#","</i>", $card[0]['ability']);
	    $img_src = getCardImage($card[0]);

	    $card[0]['img_src'] = $img_src;
	    $card_name = $card[0]['name'];
    
	    $other_card = $this->Product_model->get_other_product($card[0]); 
		$data['card'] = $card[0];
		$data['other_card'] = $other_card;

		$data['language'] = $language;

		$data['styles'] = array(
			'<link rel="stylesheet" type="text/css" href="' . styles_bundle("slick-theme.css") .'">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/libraries/treeview/css/bootstrap-treeview.css">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/css/card_view.css">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/libraries/manareplacer/mana-symbols.css">'
		);

		$data['scripts'] = array(
			'<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>',
			'<script type="text/javascript" src="'. base_url() .'assets/libraries/treeview/js/bootstrap-treeview.js"></script>',
			'<script type="text/javascript" src="'. scripts_bundle("card_view.js") .'"></script>'
		);

		$card_category = $this->Menu_model->getAll();
		foreach($card_category as $key => $cat){
			$subcat = json_decode($cat['sub_category']);
			$card_category[$key]['subcategory'] = $subcat;
		}
		$related_card = "";

		if(strpos($card_name, 'Token') !== false || ($card[0]['rarity'] == 'T' && strpos($card[0]['type'], 'Token') !== false)) {
			//remove (text here)
			$card_name_token = "";
			$re = '/(.*)(?:\(?\d*\/\d*\)?)/m';
			preg_match_all($re, $card_name, $matches, PREG_SET_ORDER, 0);
			if(!empty($matches[0][1])){
				$card_name_token =  $matches[0][1];
				$card_name_token = preg_replace('/\d/', '', $card_name_token );
			} else {
				$card_name_token =  $card_name;
			}

			$related_card = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND (a.ability LIKE "%' . $ability . '%" AND a.name LIKE "%' . $card_name_token . '%")' 
			. ' AND (a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');

			if($ability == "") {
				$related_card = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND (a.name LIKE "%' . $card_name_token . '%")' 
				. ' AND (a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');
			}

			if(count($related_card) < 12 ) {
				$rarity = $card[0]['rarity'];
				$color = $card[0]['color'];
				$set_code = $card[0]['set_code'];
				$additional_related_cards = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.set_code = "'.$set_code.'" AND (a.color = "'.$color.'") AND 
					(a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');
				$related_card =	array_merge($related_card, $additional_related_cards);
			}
	
			if(count($related_card) < 12 ) {
				$rarity = $card[0]['rarity'];
				$color = $card[0]['color'];
				$set_code = $card[0]['set_code'];
				$additional_related_cards = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND a.set_code = "'.$set_code.'" AND (a.rarity = "'.$rarity.'") AND 
					(a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');
				$related_card =	array_merge($related_card, $additional_related_cards);
			}
		} else {
			$related_card = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND (a.ability LIKE "%' . $ability . '%" AND a.name LIKE "%' . $card_name . '%")' 
			. ' AND (a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND stock > 0 LIMIT 12');
			if($ability == "") {
				$related_card = $this->Product_model->get_sort('*', 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND (a.name LIKE "%' . $card_name . '%")' 
				. ' AND (a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');
			}

			if(count($related_card) < 12 ) {
				$rarity = $card[0]['rarity'];
				$color = $card[0]['color'];
				$set_code = $card[0]['set_code'];
				$additional_related_cards = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND a.set_code = "'.$set_code.'" AND (a.color = "'.$color.'")AND 
					(a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');
				$related_card =	array_merge($related_card, $additional_related_cards);
			}
	
			if(count($related_card) < 12 ) {
				$rarity = $card[0]['rarity'];
				$color = $card[0]['color'];
				$set_code = $card[0]['set_code'];
				$additional_related_cards = $this->Product_model->get_sort($prod_tag, 'WHERE a.id !=' . $id . ' AND a.name != "'.$card[0]['name'].'" AND a.set_code = "'.$set_code.'" AND (a.rarity = "'.$rarity.'")AND 
					(a.regular_price != "" OR a.foil_price != "") AND fully_handled = "NM" AND a.stock > 0 LIMIT 12');
				$related_card =	array_merge($related_card, $additional_related_cards);
			}

			

		}
 
		$data['card_category'] = $card_category;
		
		$data['related_card'] = $related_card;
		$cards_list = $related_card;
		array_push($cards_list, $card[0]);
		if(sizeof($other_card)) {
			foreach ($other_card as $value) {
				array_push($cards_list, $value);
			}
		}
		$data['cards_list'] = $cards_list;

		if(is_logged() == 0){
			$data['watchlist'] = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist') : [];
		} else {
			$watchlist_data = $this->Global_model->fetch('watchlist', array('email'=>get_email()));
			$watchlist = $watchlist_data ? json_decode($watchlist_data[0]->watchlist, TRUE) : [];
			
			$data['watchlist'] = $watchlist;
		}

		$data['content'] = 'pages/card_view';
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');

		$this->load->view('templates/main_template', $data);
	}

}