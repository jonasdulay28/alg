<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class products_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Product_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}


	public function get_all_products(){
		$row = $this->Product_model->fetch_tag_row("set_code","products","","","limit 1","id desc");
		$category_code = isset($this->session->cat_code) ? $this->session->cat_code : $row->set_code;
		if($this->session->cat_code=="none")
			$category_code = "";
		$variation = isset($this->session->variation) ? $this->session->variation : "Regular";
		$product_name = isset($this->session->product_name) ? $this->session->product_name : "";
		$rarity = isset($this->session->rarity) ? $this->session->rarity : "";
		$card_category = isset($this->session->card_category) ? $this->session->card_category : "";
		$card_language = isset($this->session->card_language) ? $this->session->card_language : "";
		$filter = [];
		$c_rarity = "";
		if($rarity=="All")
			$c_rarity = "";
		elseif($rarity=="Mythic Rare")
			$c_rarity = "M";
		elseif($rarity=="Mythic")
			$c_rarity = "M";
		elseif($rarity=="Uncommon")
			$c_rarity = "U";
		elseif($rarity=="Rare")
			$c_rarity = "R";
		elseif($rarity=="Common")
			$c_rarity = "C";
		elseif($rarity=="Others")
			$c_rarity = "T";
		else 
			$c_rarity = "";

		if(strtolower($c_rarity) == 'all')
			$rarity = '';
		if(strtolower($card_language) == 'all')
			$card_language = '';

		// if($this->session->cat_code) 
		// 	$this->session->unset_userdata('cat_code');
		// if($this->session->variation) 
		// 	$this->session->unset_userdata('variation');
		// if($this->session->rarity) 
		// 	$this->session->unset_userdata('rarity');
		// if($this->session->product_name) 
		// 	$this->session->unset_userdata('product_name');
		// if($this->session->card_language) 
		// 	$this->session->unset_userdata('card_language');
		// if($this->session->card_category) 
		// 	$this->session->unset_userdata('card_category');
		$data['cat_code'] = $category_code;
		$data['variation'] = $variation;
		$data['rarity'] = $rarity;
		$data['product_name'] = $product_name;
		$data['card_category'] = $card_category;
		$data['card_language'] = $card_language;
		$data["products"] = $this->Product_model->fetch_products($category_code,$c_rarity,$variation,$product_name,$card_category,$card_language);
		echo json_encode($data);
	}

	public function updateStyle() {
		$style = post('style');
		//$style = implode(',',$style);
		$id = clean_data(post('id'));
		$datestamp = clean_data(post('datestamp'));
		$filter = ["id"=>$id];
		$data = ["style"=>$style,"promo"=>$style];
		if($style=="datestamp"){
			$data['datestamp'] = $datestamp;
		}

		$this->Product_model->update("products",$data,$filter);
		echo json_encode(["message"=>"success"]);
	}

	public function add()
	{
		$response = ["message"=>"success"];
		$product_data = json_decode(post('product_data'), true);
		if(!empty($_FILES['file']['name'])) {
			// where are you storing it
      $path = './uploads';
      // what are you naming it
      $new_name = 'prod-' . time();
      // start upload
      $input = 'file';
      $result = $this->upload_image($path, $new_name, $input);
      if($result == "success"){
      	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
      	$product_data['image'] = base_url()."uploads/".$new_name.".".$ext;
      } else {
      	$response["message"] = "failed";
      }
		} else {
			$response["message"] = "failed";
		}

		// $tags = $product_data['tag'];
		// $product_data['tag'] = $tags['content'];
		// $product_data['custom_tag'] = $tags['content'] == 'custom' ? $product_data['custom_tag'] : '';
		// $product_data['tag_color'] = $tags['text_color'];
		// $product_data['tag_bg_color'] = $tags['bg_color'];

		$product_data['tag'] = 'NM';
		$product_data['tag'] = '';
		$product_data['custom_tag'] = '';
		$product_data['tag_color'] = '';
		$product_data['tag_bg_color'] = '';

		$this->Product_model->insert('products',$product_data);
		echo json_encode($response);
	}

	public function change_card_language() {
		if(!$this->input->post()) show_404();

		$filter = ["id"=>post('id')];
		$product = $this->Product_model->fetch_data('products',$filter);

		if(!empty($product)) {
			$language = post('language');
			$product = $product[0];
			if($product->language != $language) {
				unset($product->id);
				$product->stock = '0';
				if($product->regular_price != '') {
					$product->regular_price = '0';
				} else if($product->foil_price != '') {
					$product->foil_price = '0';
				}
				$product->priority = '';
				$product->tag = '';
				$product->custom_tag = '';
				$product->promo = '';
				$product->datestamp = '';
				$product->tag_color = '';
				$product->tag_bg_color = '';
				$product->language = $language;

				$id = $this->Product_model->insert('products', $product);
				if($id) {
					$product->id = $id;
					echo json_encode(array(
						'status' => 'success',
						'product' => $product
					));
				} else {
					echo json_encode(array(
						'status' => 'fail'
					));
				}
			} else {
				echo json_encode(array(
					'status' => 'success',
					'product' => $product
				));
			}
			return;
		}
		echo json_encode(array(
			'status' => 'fail'
		));
	}

	public function duplicate_card_language() {
		if(!$this->input->post()) show_404();

		$filter = ["id"=>post('id')];
		$product = $this->Product_model->fetch_data('products',$filter);

		if(!empty($product)) {
			$language = strtolower(post('language'));
			$dc = 0;

			if(
				!in_array($language, array('tw', 'jp', 'ko'))
			) {
				echo json_encode(array(
					'status' => 'fail'
				));
				return;
			}

			$product = $product[0];
			if(strtolower($product->language) != $language) {
				unset($product->id);
				$product->stock = '0';
				if($product->regular_price != '') {
					$product->regular_price = '0';
				} else if($product->foil_price != '') {
					$product->foil_price = '0';
				}

				$product->language = $language;
				$product->priority = '';
				$product->tag = '';
				$product->custom_tag = '';
				$product->promo = '';
				$product->datestamp = '';
				$product->tag_color = '';
				$product->tag_bg_color = '';
				$product->fully_handled = 'NM';

				$id = $this->Product_model->insert('products', $product);
				if($id) {
					$product->id = $id;
					echo json_encode(array(
						'status' => 'success',
						'product' => $product
					));
				} else {
					echo json_encode(array(
						'status' => 'fail'
					));
				}
			} else {
				echo json_encode(array(
					'status' => 'success',
					'product' => $product
				));
			}
			return;
		}
		echo json_encode(array(
			'status' => 'fail'
		));
	}

	public function duplicate_card_condition() {
		if(!$this->input->post()) show_404();

		$filter = ["id"=>post('id')];
		$product = $this->Product_model->fetch_data('products',$filter);

		if(!empty($product)) {
			$fully_handled = strtoupper(post('fully_handled'));
			$dc = 0;

			if(
				!($fully_handled == 'SP' || $fully_handled == 'MP' || $fully_handled == 'HP')
			) {
				echo json_encode(array(
					'status' => 'fail'
				));
				return;
			}

			$product = $product[0];
			if(strtoupper($product->fully_handled) != $fully_handled) {
				unset($product->id);
				$product->stock = '0';
				if($product->regular_price != '') {
					$product->regular_price = getDiscountedPrice($fully_handled, $product->regular_price);
				} else if($product->foil_price != '') {
					$product->foil_price = getDiscountedPrice($fully_handled, $product->foil_price);
				}

				$product->priority = '';
				$product->tag = '';
				$product->custom_tag = '';
				$product->promo = '';
				$product->datestamp = '';
				$product->tag_color = '';
				$product->tag_bg_color = '';
				$product->fully_handled = $fully_handled;

				$id = $this->Product_model->insert('products', $product);
				if($id) {
					$product->id = $id;
					echo json_encode(array(
						'status' => 'success',
						'product' => $product
					));
				} else {
					echo json_encode(array(
						'status' => 'fail'
					));
				}
			} else {
				echo json_encode(array(
					'status' => 'success',
					'product' => $product
				));
			}
			return;
		}
		echo json_encode(array(
			'status' => 'fail'
		));
	}

	function upload_image($path, $new_name, $input) {
	    // define parameters
	    $config['upload_path'] = $path;
	    $config['allowed_types'] = 'jpg|png|pdf|docx';
	    $config['max_size'] = '0';
	    $config['max_width'] = '0';
	    $config['max_height'] = '0';
	    $config['file_name'] = $new_name;
	    $this->load->library('upload');
	    $this->upload->initialize($config);
	    // upload the image
	    if ($this->upload->do_upload($input)) {
	        return "success";
	    } else {
	        return "failed";
	    }
	}

	public function duplicateProduct() {
		$response = ["message"=>"success"];
		$selected_id = json_decode(post('selected_id'));
		$category_code = clean_data(post('category'));
		$variation = clean_data(post('variation'));
		$rarity = clean_data(post('rarity'));
		$fully_handled = clean_data(post('fully_handled'));
		$language = clean_data(post('language'));

		foreach($selected_id as $value){
			$filter = ["id"=>$value]; 
			$data = $this->Product_model->fetch_tag_row_array('*','products',$filter);
			if($data){
				if(
					in_array($fully_handled, array('SP', 'MP', 'HP'))
				) {
					$filter = [
						"name" => $data['name'], 
						'fully_handled' => $fully_handled, 
						'set_code' => $data['set_code'], 
						'language' => $data['language']
					];

					$card = $this->Product_model->fetch_tag_row_array('*','products',$filter);
					if(!$card){
						$insert_data = $data;

						$insert_data['stock'] = '0';
						if($insert_data['regular_price'] != '') {
							$insert_data['regular_price'] = getDiscountedPrice($fully_handled, $insert_data['regular_price']);
						} else if($insert_data['foil_price'] != '') {
							$insert_data['foil_price'] = getDiscountedPrice($fully_handled, $insert_data['foil_price']);
						}

						$insert_data['priority'] = '';
						$insert_data['tag'] = '';
						$insert_data['custom_tag'] = '';
						$insert_data['promo'] = '';
						$insert_data['datestamp'] = '';
						$insert_data['tag_color'] = '';
						$insert_data['tag_bg_color'] = '';
						$insert_data['fully_handled'] = $fully_handled;

						$insert_data['id'] = NULL;
						$this->Product_model->insert('products',$insert_data);
					}
				}

				if(
					in_array($language, array('tw', 'jp', 'ko'))
				) {

					$filter = [
						"name" => $data['name'], 
						'fully_handled' => 'NM',
						'set_code' => $data['set_code'], 
						'language' => $language
					];

					$card = $this->Product_model->fetch_tag_row_array('*','products',$filter);
					if(!$card){
						$insert_data = $data;

						$insert_data['stock'] = '0';
						if($insert_data['regular_price'] != '') {
							$insert_data['regular_price'] = '0';
						} else if($insert_data['foil_price'] != '') {
							$insert_data['foil_price'] = '0';
						}

						$insert_data['language'] = $language;
						$insert_data['priority'] = '';
						$insert_data['tag'] = '';
						$insert_data['custom_tag'] = '';
						$insert_data['promo'] = '';
						$insert_data['datestamp'] = '';
						$insert_data['tag_color'] = '';
						$insert_data['tag_bg_color'] = '';
						$insert_data['fully_handled'] = 'NM';

						$insert_data['id'] = NULL;
						$this->Product_model->insert('products',$insert_data);
					}
				}
			}
		}
		echo json_encode($response);
	}


	public function deleteAllProduct() {
		$response = ["message"=>"success"];
		$selected_id = json_decode(post('selected_id'));
		$category_code = clean_data(post('category'));
		$variation = clean_data(post('variation'));
		$rarity = clean_data(post('rarity'));
		foreach($selected_id as $value){
			$filter = ["id"=>$value]; 
			$data = $this->Product_model->fetch_tag_row_array('*','products',$filter);
			if($data){
				$filter = [
					"name" => $data['name'],
					'set_code' => $data['set_code'], 
					'language' => $data['language']
				];

				$card = $this->Product_model->fetch_tag_array('*','products',$filter);
				if($card){
					foreach($card as $value){
						$filter = ["id"=>$value['id']]; 
						$this->Product_model->delete('products',$filter);
					}
				}
			}
		}
		echo json_encode($response);
	}

	public function bulkEdit() {
		$response = ["message"=>"success"];
		$selected_id = json_decode(post('selected_id'));
		$num_stocks = clean_data(post('num_stocks'));
		$foil_price = clean_data(post('foil_price'));
		$regular_price = clean_data(post('regular_price'));
		$base_price = clean_data(post('base_price'));
		$category_code = clean_data(post('category'));
		$set_code = strtoupper(clean_data(post('set_code')));
		$variation = clean_data(post('variation'));
		$tag = clean_data(post('tag'));
		$tag = json_decode($tag, TRUE);
		$custom_tag = clean_data(post('custom_tag'));

		foreach($selected_id as $value){
			$filter = ["id"=>$value]; 
			$data = [];
			if($foil_price != '') {
				$data["foil_price"] = $foil_price != 0 ? $foil_price : "";
			} 
			if($regular_price != ''){
				$data['regular_price'] = $regular_price != 0 ? $regular_price : "";
			} 

			if($base_price != '') {
				$data['base_price'] = $base_price != 0 ? $base_price : "";
			} 
			if($num_stocks != '') {
				$data = ["stock"=>$num_stocks];
			}

			if($set_code != '') {
				$data = ["set_code"=>$set_code];
			}

			if($tag['content'] != '') {
				$data['tag'] = $tag['content'];
				if($tag['content'] == 'custom') $data['custom_tag'] = $custom_tag;
				if($tag['text_color'] != '') $data['tag_color'] = $tag['text_color'];
				if($tag['bg_color'] != '') $data['tag_bg_color'] = $tag['bg_color'];
			}

			$this->Product_model->update('products',$data,$filter);
		}
		echo json_encode($response);
	}

	public function get_product() {
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
		$data["product"] = $this->Product_model->fetch_data('products',$filter);
		echo json_encode($data);
	}

	public function getFilters() {
		/*$tmp_category = $this->Product_model->fetch_tag('sub_category_code,sub_category','menu');
		$data["categories"] = [];
		$arr_category = [];
		$arr_cat_code = [];
		foreach($tmp_category as $key=>$value ) {
			$category = json_decode($value->sub_category);
			$category_code = json_decode($value->sub_category_code); 
			foreach ($category as $key => $value) {
				array_push($arr_category,$value);
				array_push($arr_cat_code,$category_code[$key]);
			}
		}

		foreach($arr_category as $key=>$value) {
			$cat = ["category"=>$value];
			$cat_code = ["sub_category_code"=>$arr_cat_code[$key]];
			array_push($data['categories'],array_merge($cat, $cat_code));
		}*/
	$this->load->model("Allsets_model");
		$data['categories'] = $this->Allsets_model->get_sets();
		$data['filters'] = $this->Product_model->fetch_tag('main_filter,sub_filter','filters');

		foreach($data['filters'] as $key=>$value ) {
			$data['filters'][$key]->sub_filter = json_decode($value->sub_filter);
			$tmp = array();
			for($i = 0; $i < sizeof($data['filters'][$key]->sub_filter); $i++) {
				if(strtolower($data['filters'][$key]->sub_filter[$i]) != 'all') {
					array_push($tmp, $data['filters'][$key]->sub_filter[$i]);
				}
			}
			$data['filters'][$key]->sub_filter = $tmp;
		}
		$data['filters']['language'] = $this->Product_model->fetch('language');
		$tmp = array();
		for($i = 0; $i < sizeof($data['filters']['language']); $i++) {
			if(strtolower($data['filters']['language'][$i]->language) != 'all') {
				array_push($tmp, $data['filters']['language'][$i]);
			}
		}
		$data['filters']['language'] = $tmp;

		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$product_data = json_decode(post('product_data'), true);
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$category_code = $product_data['set_code'];
		$variation = $product_data['card_type'];
		$rarity = $product_data['rarity'];
		$this->session->cat_code = $category_code;
		$this->session->variation = $variation;
		$this->session->rarity = $rarity;

		$tags = $product_data['tag'];
		$product_data['tag'] = $tags['content'];
		$product_data['custom_tag'] = $tags['content'] == 'custom' ? $product_data['custom_tag'] : '';
		$product_data['tag_color'] = $tags['text_color'];
		$product_data['tag_bg_color'] = $tags['bg_color'];

		$this->Product_model->update('products',$product_data,$filter);
		echo json_encode($response);
	}

	public function update_product()
	{
		$response = ["message"=>"success"];
		$id = clean_data(post('id'));
		$col = clean_data(post('col'));
		$value = clean_data(post('value'));

		if($col == 'chinese_name') {
			$prod = $this->Product_model->fetch_tag('name', 'products', array('id' => $id));

			$card = !empty($prod) ? $this->Product_model->fetch_tag('id', 'cards', array('name' => $prod[0]->name)) : array();
			if(empty($card)){
				$this->Product_model->insert('cards', array('name' => $prod[0]->name, 'name_tw' => $value));
			} else {
				$this->Product_model->update('cards', array('name_tw' => $value), array('name' => $prod[0]->name));
			}
			return;
		}

		$filter = ["id"=>$id]; 
		$product_data = array();
		$product_data[$col] = $value;

		if($col == 'tag') {
			if($value != 'custom') {
				$product_data['custom_tag'] = '';
			}			
		} else if($col == 'promo') {
			if($value != 'datestamp') {
				$product_data['datestamp'] = '';
			}			
		}

		$this->Product_model->update('products',$product_data,$filter);
		echo json_encode($response);
	}

	public function add_duplicate_product(){
		$response = ["status"=>"","card"=>array()];
		$id = clean_data(post('id'));
		$fully_handled = clean_data(post('fully_handled')) . "";
		$fully_handled = strtoupper($fully_handled);

		$product = $this->Product_model->fetch_tag('*', 'products', array('id' => $id));

		if(!empty($product)) {
			$product_exist = $this->Product_model->fetch_tag('name', 'products', array('name' => $product[0]->name, 'fully_handled' => $fully_handled, 'set_code' => $product[0]->set_code));
			if(empty($product_exist)) {
				$product = $product[0];
				unset($product->id);
				$product->fully_handled = $fully_handled;
				$id = $this->Product_model->insert("products", $product);
				$product->id = -1;
				if($id) {
					$product->id = $id;
					$response = ["status"=>"success","card"=>$product];
				}
			}
		}

		echo json_encode($response);
	}

	public function change_card_name() 
	{
		$name = clean_data(post('name'));
		$actual_name = clean_data(post('actual_name'));
		$set_code = clean_data(post('set_code'));
		$language = strtolower(clean_data(post('language')));
		$product = $this->Product_model->fetch_tag('*', 'products', array('name' => $name, 'language'=>$language, 'set_code'=> $set_code));
		$card = $this->Product_model->fetch_tag('*', 'cards', array('name' => $name), 1);
		if(empty($product)) {
			$this->Product_model->update('products', array('name'=>$name), array('name'=>$actual_name, 'language'=>$language, 'set_code'=> $set_code));

			echo json_encode(array(
				'is_existing' => '0',
				'card' => $card
			));
			return;
		}
		echo json_encode(array(
			'is_existing' => '1',
			'card' => $card
		));
	}

	public function rename_category()
	{
		//if(!$this->input->post()) show_404();
		$response = ["message"=>"error"];
		if(post('new_category_name') != '') {
			$response = ["message"=>"success"];
			$data['set'] = clean_data(post('new_category_name'));
			$set_code = clean_data(post('set_code'));
			$filter = ["set_code"=>$set_code]; 
			$this->Product_model->update('products',$data,$filter);
			$category_code = clean_data(post('category'));
			$variation = clean_data(post('variation'));
			$rarity = clean_data(post('rarity'));
			$this->session->cat_code = $category_code;
			$this->session->variation = $variation;
			$this->session->rarity = $rarity;
		}
		echo json_encode($response);
	}

	public function move_product()
	{
		//if(!$this->input->post()) show_404();
		$response = ["message"=>"success"];
		$selected_id = json_decode(post('selected_id'));
		$category_code = clean_data(post('category'));
		$variation = clean_data(post('variation'));
		$rarity = clean_data(post('rarity'));
		foreach($selected_id as $value){
			$filter = ["id"=>$value]; 
			$data = $this->Product_model->fetch_tag_row_array('*','products',$filter);
			if($data){
				$filter = [
					"name" => $data['name'],
					'set_code' => $data['set_code'], 
					'language' => $data['language']
				];

				$card = $this->Product_model->fetch_tag_array('*','products',$filter);
				if($card){
					foreach($card as $value){
						$filter = ["id"=>$value['id']]; 
						$update_data['set_code'] = clean_data(post('move_category'));
						$update_data['set'] = clean_data(post('move_category_name'));
						$this->Product_model->update('products',$update_data,$filter);
					}
				}
			}
		}
		echo json_encode($response);
	}

	public function singleEdit()
	{
		if(!$this->input->post()) show_404();
		$response = ["message"=>"success"];
		$column = clean_data(post('column'));
		$new_value = clean_data(post('new_value'));
		$id = clean_data(post('id'));
		$data = [$column => $new_value];
		$filter = ["id"=>$id]; 
		$this->Product_model->update('products',$data,$filter);
		echo json_encode($response);
	}

	public function deleteProduct() {
		if(!$this->input->post()) show_404();
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$category_code = clean_data(post('category'));
		$variation = clean_data(post('variation'));
		$rarity = clean_data(post('rarity'));
		$this->Product_model->delete('products',$filter);
		$this->session->cat_code = $category_code;
		$this->session->variation = $variation;
		$this->session->rarity = $rarity;
		echo json_encode($response);
	}

	public function filterProduct() {
		if(!$this->input->post()) show_404();
		$data["message"] = "success";
		$product_name = clean_data(post('product_name'));
		$category_code = clean_data(post('category'));
		$variation = clean_data(post('variation'));
		$rarity = clean_data(post('rarity'));
		$card_category = clean_data(post('card_category'));
		$card_language = clean_data(post('card_language'));
		$this->session->cat_code = $category_code;
		$this->session->variation = $variation;
		$this->session->rarity = $rarity;
		$this->session->product_name = $product_name;
		$this->session->card_category = $card_category;
		$this->session->card_language = $card_language;
		$data['cat_code'] = $category_code;

		echo json_encode($data);
	}

	public function import_product() {
		if(isset($_FILES['spreadsheet'])){
			if($_FILES['spreadsheet']['tmp_name']){
				if(!$_FILES['spreadsheet']['error'])
				{
					require_once 'vendor/autoload.php';

				    $inputFile = $_FILES['spreadsheet']['tmp_name'];
				    $extension = strtoupper(pathinfo($_FILES['spreadsheet']['name'], PATHINFO_EXTENSION));
				    if($extension == 'XLSX'){

				        //Read spreadsheeet workbook
				        try {
				             $inputFileType = PHPExcel_IOFactory::identify($inputFile);
				             $objReader = PHPExcel_IOFactory::createReader($inputFileType);
				                 $objPHPExcel = $objReader->load($inputFile);
				        } catch(Exception $e) {
				                die($e->getMessage());
				        }

				        //Get worksheet dimensions
				        $sheet = $objPHPExcel->getSheet(0);
				        $highestRow = $sheet->getHighestRow(); 
				        $highestColumn = $sheet->getHighestColumn();
				        //Loop through each row of the worksheet in turn
				        for ($row = 5; $row <= $highestRow; $row++){  	
			                $id = $sheet->getCell('A' . $row)->getValue();
			                if($id != '') {
				                $data = array(
				                	'name' 				=> $sheet->getCell('B' . $row)->getValue(),
				                	'base_price' 		=> $sheet->getCell('E' . $row)->getValue(),
				                	'regular_price' 	=> $sheet->getCell('F' . $row)->getValue(),
				                	'foil_price' 		=> $sheet->getCell('G' . $row)->getValue(),
				                	'stock' 			=> $sheet->getCell('H' . $row)->getValue(),
				                	'rarity' 			=> $sheet->getCell('I' . $row)->getValue(),
				                	'image' 			=> $sheet->getCell('O' . $row)->getValue(),
				                	'collector_number'	=> $sheet->getCell('P' . $row)->getValue(),
				                	'ability' 			=> $sheet->getCell('Q' . $row)->getValue(),
				                	'type' 				=> $sheet->getCell('R' . $row)->getValue(),
				                	'flavor'			=> $sheet->getCell('S' . $row)->getValue()
				                );

				                $condition_list = array('NM', 'SP', 'MP', 'HP');
				                $fully_handled = strtoupper($sheet->getCell('J' . $row)->getValue());
				                if(in_array($fully_handled, $condition_list)) {
				                	$data['fully_handled'] = $fully_handled;
				                }

				                $language_list = array('eng', 'tw', 'jp');
				                $language = strtolower($sheet->getCell('K' . $row)->getValue());
				                if(in_array($language, $language_list)) {
				                	$data['language'] = $language;
				                }

				                $promo = $sheet->getCell('L' . $row)->getValue();
				                if(strtolower($promo) == 'planeswalker') {
				                	$data['promo'] = strtolower($promo);
				                	$data['datestamp'] = '';
				                } else {
					                $datestamp = $sheet->getCell('M' . $row)->getValue();
					                if($datestamp != '') {
					                	$data['promo'] = 'datestamp';
					                	$data['datestamp'] = $datestamp;
					                }
				                }

				                $tag = $sheet->getCell('N' . $row)->getValue();
				                $tag_list = array('', 'best_seller', 'great_value', 'limited_time', 'new_product', 'preorder', 'promotion', 'recommended', 'sale', 'selling_fast', 'special_order');

				                if(in_array(strtolower($tag), $tag_list)) {
				                	$data['tag'] = strtolower($tag);
				                }

				                $filter = array('id' => $id);
								$this->Product_model->update("products",$data,$filter);
							} else {
				                $data = array(
				                	'name' 				=> $sheet->getCell('B' . $row)->getValue(),
				                	'set'				=> $sheet->getCell('C' . $row)->getValue(),
				                	'set_code'			=> strtoupper($sheet->getCell('D' . $row)->getValue()),
				                	'base_price' 		=> $sheet->getCell('E' . $row)->getValue(),
				                	'regular_price' 	=> $sheet->getCell('F' . $row)->getValue(),
				                	'foil_price' 		=> $sheet->getCell('G' . $row)->getValue(),
				                	'stock' 			=> $sheet->getCell('H' . $row)->getValue(),
				                	'rarity' 			=> $sheet->getCell('I' . $row)->getValue(),
				                	'image' 			=> $sheet->getCell('O' . $row)->getValue(),
				                	'collector_number'	=> $sheet->getCell('P' . $row)->getValue(),
				                	'ability' 			=> $sheet->getCell('Q' . $row)->getValue(),
				                	'type' 				=> $sheet->getCell('R' . $row)->getValue(),
				                	'flavor'			=> $sheet->getCell('S' . $row)->getValue(),
				                	'category'			=> 'single cards'
				                );

				                $condition_list = array('NM', 'SP', 'MP', 'HP');
				                $fully_handled = strtoupper($sheet->getCell('J' . $row)->getValue());
				                if(in_array($fully_handled, $condition_list)) {
				                	$data['fully_handled'] = $fully_handled;
				                }

				                $language_list = array('eng', 'tw', 'jp');
				                $language = strtolower($sheet->getCell('K' . $row)->getValue());
				                if(in_array($language, $language_list)) {
				                	$data['language'] = $language;
				                }

				                $promo = $sheet->getCell('L' . $row)->getValue();
				                if(strtolower($promo) == 'planeswalker') {
				                	$data['promo'] = strtolower($promo);
				                	$data['datestamp'] = '';
				                } else {
					                $datestamp = $sheet->getCell('M' . $row)->getValue();
					                if($datestamp != '') {
					                	$data['promo'] = 'datestamp';
					                	$data['datestamp'] = $datestamp;
					                }
				                }

				                $tag = $sheet->getCell('N' . $row)->getValue();
				                $tag_list = array('', 'best_seller', 'great_value', 'limited_time', 'new_product', 'preorder', 'promotion', 'recommended', 'sale', 'selling_fast', 'special_order');

				                if(in_array(strtolower($tag), $tag_list)) {
				                	$data['tag'] = strtolower($tag);
				                }

								$this->Product_model->insert("products",$data);
							}

				        }

				        echo json_encode(array(
				        	'status' => 'success',
				        	'message' => 'Success'
				        ));
				    }
				    else{
				        echo json_encode(array(
				        	'status' => 'error',
				        	'message' => 'Please upload an XLSX file'
				        ));
				    }
				}
				else{
			        echo json_encode(array(
			        	'status' => 'error',
			        	'message' => $_FILES['spreadsheet']['error']
			        ));
				}
			}
		}
	}



	public function test(){
		$category = json_decode('["Standard","War of the Spark", "Ravnica Allegiance","Guilds of Ravnica"
,"Core Set 2019","Dominaria","Rivals of Ixalan","Ixalan"]');
		die(print_r($category));
		$category_code = json_decode('["","WAR","RNA","GRN","M19","DOM","RIX","XLN"]');
		$data['categories'] = [];
		for($x = 0; $x < count($category); $x++){
			$cat = ["category"=>$category[$x]];
			$cat_code = ["sub_category_code"=>$category_code[$x]];
			array_push($data['categories'],array_merge($cat, $cat_code));
		}
		print_r($data['categories']);
	}
}