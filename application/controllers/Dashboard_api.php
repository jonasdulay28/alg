<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}

	public function get_dashboard_data() {
		$filter = ["status" => 1];
		$data["num_orders"] = $this->Dashboard_model->count_rows("cart_order",$filter);
		$data["num_products"] = $this->Dashboard_model->count_rows("products");
		$data["num_sales"] = $this->Dashboard_model->get_total_sales();
		$data["num_users"] = $this->Dashboard_model->count_rows("users");
		echo json_encode($data);
	}
}