<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class homepage extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Global_model");
		$this->load->model("Product_model");
	}


	public function index()
	{
		$this->session->current_language =  $this->session->current_language  ? $this->session->current_language  : "traditional_chinese";
		if(isset($_GET['payment'])){
			setcookie('payment', $_GET['payment'], time() + 86400, "/");
		}

		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$data['banners'] = $this->Global_model->fetch('banners',"","","","order asc");
		$data['banners'] =  $data['banners'] ? $data['banners'] : [];
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['newest_expansion'] = $this->Global_model->fetch('newest_expansion',"","6","","id desc");

        $display_name = 'a.name as name, a.ability as ability, a.language as language, a.type as type, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw, a.category as category';
        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';
        $language = $this->session->userdata('language') ? $this->session->userdata('language') : '';
        
		// $sort_language = 'eng';
		// switch ($language) {
		// 	case 'tw':
		// 		$sort_language = post('language');
		// 		break;
		// 	default:
		// 		break;
		// }


        $prod_tag = 'a.id as id, a.fully_handled as fully_handled, a.card_type as card_type, a.set_code as set_code, a.collector_number as collector_number, a.rarity as rarity, a.color as color, a.set as `set`, a.manacost as manacost, a.additional_img as additional_img, a.category as category,a.image, a.artist as artist, a.regular_price as regular_price, a.foil_price as foil_price, a.image as img_src, a.stock as stock, a.datestamp as datestamp, a.promo as promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color, ' . $display_name;
		$data['top_card'] = $this->Product_model->get_sort_tag($prod_tag, 'WHERE priority = 1 AND stock > 0 LIMIT 15');
		$data['cards_list'] = $data['top_card'];
		$data['authentication_status'] = "login";
		if(isset($this->session->authentication_status) && $this->session->authentication_status == "registered") {
			$data['authentication_status'] = "registered";
		}
		unset($_SESSION['authentication_status']);
		$this->load->view('templates/homepage_template',$data);
	}

	public function test() {
		echo get_language('Newest Expansion','traditional_chinese');
	}

	public function cart_session_key() {
		echo $session_key = $this->session->userdata('cart_session_key');
	}

	public function check_login_status(){
		print_r($_SESSION);
		print_r($_COOKIE);
		die(var_dump(is_logged()));
	}

	public function set_menu_cat(){
		$categories = $this->Global_model->fetch('menu');
		foreach ($categories as $key) {
				$sub_category = json_decode($key->sub_category);
				$set_code = [];
				foreach($sub_category as $item){
					$filter = ["set"=>$item];
					$row = $this->Global_model->fetch_tag_row("set_code","products",$filter,1);
					$code = (empty($row) ? "" : strtolower($row->set_code));
					array_push($set_code, $code);
				}
				 $data = ["sub_category_code" => json_encode($set_code)];
				 $filter = ["main_category" => $key->main_category];
				 $this->Global_model->update("menu",$data,$filter);
				 die("success");
			
		}
	}

	public function change_language() {
		$language = clean_data(post('language'));
		$this->session->current_language = $language;
		echo json_encode(['message'=>"success"]);
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}