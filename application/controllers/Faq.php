<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class faq extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Global_model");
	}

	public function index()
	{
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');
		$this->load->view('templates/faq_template',$data);
	}
}
