<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class pages extends CI_Controller {
    
    	public function __construct() {
    		parent::__construct();
    		$this->load->model("Global_model");
    	}

    	public function index() {

    	}

        public function view($slug) {
            $data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
            $data['data']['categories'] = $this->Global_model->fetch('menu');
            $is_preview = isset($_GET['is_preview']) ? $_GET['is_preview'] : '';

            if($is_preview == 'on') {
                $data['data']['page_content'] = $this->Global_model->fetch('pages', array('slug'=> $slug));
            } else {
                $data['data']['page_content'] = $this->Global_model->fetch('pages', array('slug'=> $slug, 'status'=> 'Active'));
            }

            if(!$data['data']['page_content']) {
                show_404();
            }

            $data['content'] = 'pages/page_content';
            $this->load->view('templates/main_template', $data);
        }

    }
