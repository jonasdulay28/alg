<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cart extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
	}

	public function index() {
		$cart_data = getUpdatedCartList();
		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');

		$user = $this->Global_model->fetch_data('users', array('email' => get_email()));
		if(get_email()){
	  	$loyalty = $this->Global_model->fetch_data('loyalty_program', array('id' => $user[0]->user_loyalty), 1);
	  } else {
	  	$loyalty = $this->Global_model->fetch_data('loyalty_program', array('id' => 1), 1);
	  }

		$cart_list = [];
		$rebate = 0;
		$data['deduction'] = 0;
		if($cart_data) {
			foreach ($cart_data as $i => $value) {
				$img_src = getCardImage($cart_data[$i]['card'], 'json');
				$cart_data[$i]['card']->img_src = $cart_data[$i]['card']->img_src ? $cart_data[$i]['card']->img_src : $img_src;
				if($cart_data[$i]['card']->category == 'single cards') {
					$price = $cart_data[$i]['card']->regular_price ? $cart_data[$i]['card']->regular_price : $cart_data[$i]['card']->foil_price;
					$deduction = $loyalty[0] ? floatval(str_replace("%", "", $loyalty[0]->single_cards_rebate)) / 100 : 0;
					$data['deduction'] = $deduction;
					$rebate += $deduction * ($price * $cart_data[$i]['quantity']);
				} else {
					$price = $cart_data[$i]['card']->regular_price ? $cart_data[$i]['card']->regular_price : $cart_data[$i]['card']->foil_price;
					$deduction = $loyalty[0] ? floatval(str_replace("%", "", $loyalty[0]->other_products_rebate)) / 100 : 0;
					$data['deduction'] = $deduction;
					$rebate += $deduction * ($price * $cart_data[$i]['quantity']);
				}

			  array_push($cart_list, $cart_data[$i]);
			}
		}

		$data['rebate'] = $rebate;
		$data['single_card_rebate'] = floatval(str_replace("%", "", $loyalty[0]->single_cards_rebate)) / 100;
		$data['other_card_rebate'] = floatval(str_replace("%", "", $loyalty[0]->other_products_rebate)) / 100;

		$data['styles'] = array(
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/css/cart.css">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/libraries/manareplacer/mana-symbols.css">'
		);
		$data['scripts'] = array(
			'<script type="text/javascript" src="'. base_url() .'assets/js/cart.js"></script>'
		);

		if(is_logged() > 0) {
			$user = $this->Global_model->fetch_data('users', array('email' => get_email()));
			if($user){
				$loyalty = $this->Global_model->fetch_data('loyalty_program', array('lifetime_spend <=' => (int)$user[0]->user_exp), 1, "", "level_number DESC");
				$user_credits = $this->Global_model->fetch_data('user_credit', array('email'=> get_email()));

				if($user_credits) $data['credits'] = $user_credits[0]->credits;
				if($loyalty) $data['badge'] = $loyalty[0]->badge;
			}
		}

		$data['cart_list'] = $cart_list;
		$data['content'] = 'pages/cart';

		$this->load->view('templates/main_template', $data);
	}

	public function add_all_to_watchlist(){
		$cart_data = post("cart_data");
		$email = get_email();
		$watch_record = $this->Global_model->fetch_tag_row('*','watchlist', array('email'=>$email));
		$update = $watch_record ? true : false;
		$watchlist = $watch_record ? json_decode($watch_record->watchlist, TRUE): [];

		foreach ($cart_data as $key) {
			$id = $key['card']['id'];
			$watchlist[$id] = array('id'=>$id);
		}
		
		if($update) {
			$data = array(
				'watchlist'=>json_encode($watchlist)
			);
			$watch_record = $this->Global_model->update('watchlist', $data, array('email'=>$email));
		} else {
			$data = array(
				'watchlist'=>json_encode($watchlist),
				'email' => $email
			);
			$watch_record = $this->Global_model->insert('watchlist', $data);
		}
		echo json_encode(["message"=>"success"]);
		
	}

}