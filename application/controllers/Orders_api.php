<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class orders_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Cart_Order_model');
	$this->load->model('Global_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}

	public function get_order_details() {
		$id = get('id');
		if(!$id) show_404();

		$data = $this->Cart_Order_model->get($id);
		echo json_encode($data);
	}

	public function get_orders() {
		$sort = array();
		$sort['mode'] = 'Live';
		if(isset($_GET['status']) && $_GET['status'] != 'all') {
			$sort['admin_status'] = $_GET['status'];
		}
		if(isset($_GET['payment_status']) && $_GET['payment_status'] != 'all') {
			$sort['status'] = $_GET['payment_status'];
		}
		if(isset($_GET['payment_mode']) && $_GET['payment_mode'] != 'all') {
			if($_GET['payment_mode'] == 'paypal') $sort['payment_mode'] = 'Paypal';
			else if($_GET['payment_mode'] == 'bank_transfer') $sort['payment_mode'] = 'Bank Transfer';
			else if($_GET['payment_mode'] == 'pickup') $sort['payment_mode'] = 'Pickup';
		}
		if(isset($_GET['mode']) && $_GET['mode'] != 'Live') {
			$sort['mode'] = 'Test';
		}

		if(empty($sort)){
			$data['orders'] = $this->Cart_Order_model->get();
		} else {
			$data['orders'] = $this->Global_model->fetch_tag_array('*', 'cart_order', $sort);
			$data['orders'] = $data['orders'] ? $data['orders'] : [];
			$data['sort'] = $sort;
		}
		echo json_encode($data);
	}

	public function change_status() {
		if(!$this->input->post()) show_404();

		$status = post('status');
		$id = post('id');
		$data = array('admin_status' => $status);
		if($status == '3') {
			$data['status'] = 1;
			$row = $this->Global_model->fetch_tag_row("invoice_number,points_earned,payment_mode,email","cart_order",array('id' => $id));
			$invoice_number = $row->invoice_number;
			$points_earned = $row->points_earned;
			$payment_mode = $row->payment_mode;
			$email = $row->email;
			audit_purrcoins("Earn purrcoins","Order ".$invoice_number." has been completed through ".$payment_mode,"purrcoins collected",$points_earned,$email);
		}
		$status_name = "Pending";
		if($status == '0') {
      $status_name = 'Pending';
    } elseif($status == '1') {
      $status_name = 'Processing';
    } elseif($status == '2') {
      $status_name = 'On Hold';
    } elseif($status == '3') {
      $status_name = 'Completed';
    } elseif ($status == '4') {
      $status_name = 'Cancelled';
    } elseif ($status == '5') {
      $status_name = 'Refunded';
    }

		audit("Order Status Changed","Order ".$invoice_number." status changed to ".$status_name,"change order status","success");

		$this->Global_model->update('cart_order', $data, array('id' => $id));
	}

	public function grant_user_points() {
		if(!$this->input->post()) show_404();
		$points = post('points');
		$id = post('id');

		$data = $this->Cart_Order_model->get($id);

		if($data[0]) {
			if($data[0]['is_points_earned'] == 0) {
				$this->Global_model->update('cart_order', array('is_points_earned' => 1), array('id' => $id));

				$user = $this->Global_model->fetch('users', array('email' => $data[0]['email']));
				$points_earn = floatval($data[0]['points_earned']);
				$exp = floatval($user[0]->user_exp);
				$update_exp = $exp + floatval($data[0]['price']);

				$loyalty = $this->Global_model->fetch_data('loyalty_program', array('lifetime_spend <=' => $update_exp), 1, "", "level_number DESC");
				$user_loyalty = $user[0]->user_loyalty;
				if($loyalty[0]->id != $user[0]->user_loyalty) {
					$user_loyalty = $loyalty[0]->id;
				}
				$update_data = array('user_exp' => $update_exp, 'user_loyalty' => $user_loyalty);
				$this->Global_model->update('users', $update_data, array('id' => $user[0]->id));
        audit("logs","grant_user_points: ".$data[0]['email'],json_encode($update_data),"success");
				
				$user_credit = $this->Global_model->fetch('user_credit', array('email' => $user[0]->email));
				if($user_credit[0]) {
					$credits = floatval($points_earn) + floatval($user_credit[0]->credits);
					$this->Global_model->update('user_credit', array('credits' => $credits), array('email' => $user[0]->email));
				} else {
					$credits = $points_earn;
					$this->Global_model->insert('user_credit', array('credits' => $credits, 'email' => $user[0]->email));
				}
			}
		}
	}

	public function save_session(){
		if(!$this->input->post()) show_404();
		require_once 'vendor/autoload.php';

		$filename = 'order_' . time() . '.xlsx';
		$filepath = 'assets/file/excel/'.$filename;
		$this->session->set_userdata(array('export_excel'=>$filename));
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowCount = 2;
		$orders = json_decode(post('orders'));

		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);

		$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);
		$objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);
		$objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);
		$objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);
		$objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);
		$objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);
		$objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray(['fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID,'color' =>['rgb' => '05A9E2']]]);

		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Date Ordered');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, 'Invoice Number');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, 'Price');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, 'Payment Mode');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, 'Status');
		$rowCount++;

		foreach($orders as $order) {
			$user = $this->Global_model->fetch('users', array('email' => $order->email));
			$name = '';

			if(!empty($user)) {
				$middle_name = $user[0]->middle_name ? $user[0]->middle_name . ' ' : '';
				$name = $user[0]->first_name . ' ' . $middle_name . $user[0]->last_name;
			}
			$status = $order->status == 1 ? "Paid" : "Unpaid";
			 
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $order->created_at);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, '#' . $order->invoice_number);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $name);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $order->email);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $order->price . '฿');
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $order->payment_mode);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $status);
			$rowCount++;
		}
		$style = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );
		$objPHPExcel->getActiveSheet()->getStyle('B2:'.'G'.$rowCount)->applyFromArray($style); 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($filepath);
		echo 'success';
	}

    public function export_to_excel(){
    	$filename = $this->session->userdata('export_excel');
    	if(!$filename) {
			echo 'Please try again.';
			exit();
    	}

		$filepath = 'assets/file/excel/'.$filename;

		if(!file_exists($filepath)) {
			echo 'Please try again.';
			exit();
		}

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filepath));
		flush(); // Flush system output buffer
		readfile($filepath);

		unlink($filepath);
    }

}