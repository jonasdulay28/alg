<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pages_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Pages_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}

	public function add()
	{
		if(!$this->input->post()) show_404();
		$response = ["message"=>"success"];
		$page_data = json_decode(post('page_data'));
		
		$data = array("name"=>$page_data->page_name,"content"=>$page_data->content,"slug"=>$page_data->page_slug,"status"=>$page_data->status);
		$this->Pages_model->insert("pages",$data);
		echo json_encode($response);
	}

	public function edit()
	{
		if(!$this->input->post()) show_404();
		$response = ["message"=>"success"];
		$page_data = json_decode(post('page_data'));
		
		$data = array("name"=>$page_data->page_name,"content"=>$page_data->content,"slug"=>$page_data->page_slug,"status"=>$page_data->status);
		$this->Pages_model->update("pages",$data, array('id'=>post('id')));
		echo json_encode($response);
	}

	public function check_slug()
	{
		if(!$this->input->post()) show_404();
		$slug = post('slug');
		if(post('id')) {
			$page = $this->Pages_model->fetch("pages", array('slug'=>$slug,'id !='=>post('id')));
		} else {
			$page = $this->Pages_model->fetch("pages", array('slug'=>$slug));
		}

		if($page) {
			echo json_encode(array('is_exist'=>true));
		} else {
			echo json_encode(array('is_exist'=>false));
		}
	}
	
	public function get_pages() {
		$data['pages'] = $this->Pages_model->fetch('pages');
		echo json_encode($data);
	}

	public function get_page(){
		if(!$this->input->post()) show_404();

		$data['page'] = $this->Pages_model->fetch('pages', array('id'=>post('id')));
		echo json_encode($data);
	}

	public function delete()
	{
		if(!$this->input->post()) show_404();
		$response = ["message"=>"success"];
		$id = post('id');
		
		$this->Pages_model->delete("pages", array('id'=>$id));
		echo json_encode($response);
	}

}