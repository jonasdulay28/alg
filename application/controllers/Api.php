<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class api extends CI_Controller {  

	public function __construct() {
		parent::__construct();
		$this->load->model("User_model");
		$this->load->model("Product_model");
		$this->load->model("Global_model");
	}

	public function index() {

	}


	public function update_user_name() {
		$users = $this->db->select('email')->from('users')->get()->result();
		foreach ($users as $key) {
			$email = $key->email;
			$row = $this->db->select('billing_info')->from('cart_order')->where('email',$email)->limit(1)->get()->row();
			if(!empty($row)) {
				$billing_info = json_decode($row->billing_info);
				$firstname = $billing_info->firstname;
				$lastname = $billing_info->lastname;
				$data = ["first_name" => $firstname, "last_name"=>$lastname];
				$filter = ["email"=>$email];
				$this->db->update('users',$data,$filter);
			}
		}
		
	}

	public function apply_checkout() {
		if(!$this->input->post()) show_404();
		$coupon = post('coupon');
		$credits = post('credits');

		$cart_data = getUpdatedCartList();

		$total_price = 0;

		foreach ($cart_data as $i => $value) {
			$price = $cart_data[$i]['card']->regular_price ? $cart_data[$i]['card']->regular_price : $cart_data[$i]['card']->foil_price;
			$total_price += floatval($price) * $cart_data[$i]['quantity'];
		}

		$status = 'Success';
		$message = '';

		$now = strtotime("now");
		$this->session->unset_userdata('coupons');
		$this->session->unset_userdata('purrcoins');

		if($coupon){
			$user_coupon = $this->Global_model->fetch('coupons', array('coupon_code' => $coupon, 'status' => 1, 'num_limit >'=> 0));
			if(!$user_coupon) {
				$status = 'Failed';
				$message = 'Invalid coupon code';
			} else {
				$coupon_limit = floatval($user_coupon[0]->limit);
				if($coupon_limit > 0 && $coupon_limit > $total_price){
					$status = 'Failed';
					$message = 'Invalid coupon code';
				} else {
					$expiration = strtotime($user_coupon[0]->expiration);
					if($now > $expiration){
						$status = 'Failed';
						$message = 'Invalid coupon code';
					} else {
						$this->session->set_userdata(array('coupons' => $user_coupon[0]));
					}
				}
			}
		}	

		if($status == 'Failed') {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		}
		if($credits){
			$user_credit = $this->Global_model->fetch('user_credit', array('email' => get_email(), 'credits >=' => floatval($credits)));

			$cart_list = getUpdatedCartList();
			$items = array();
			$total_amount = 0;
			if(!$cart_list) {
				echo json_encode(array('status' => 'fail', 'message'=> 'Cart is empty'));
				return;
			}

			foreach($cart_list as $card) {
				$total_amount += ($card['card']->regular_price ? $card['card']->regular_price : $card['card']->foil_price) * $card['quantity'];
			}

			if($credits > $total_amount) {
				$credits = $total_amount;
			}

			foreach($cart_list as $card) {
				$total_amount += $card['card']->regular_price * $card['quantity'];
			}

			if(!$user_credit) {
				$status = 'Failed';
				$message = 'Invalid purrcoins';
			} else {
				$this->session->set_userdata(array('purrcoins' => $credits));
			}
		}

		if($status == 'Failed') {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		}

		echo json_encode(array('status' => $status, 'message' => $message));
	}

	public function register_guest() {
		if(!$this->input->post()) show_404();

		$billing_info = json_decode(post('billing_info'));
		$shipping_info = json_decode(post('shipping_info'));

		$email = clean_data(get_email());
		$password = generateRandomString(6);

		if($this->Global_model->check_exist('users', array('email' => $email)) && is_logged() < 1) {
			echo json_encode(array('status' => 'fail', 'message' => 'Email is already taken'));
			return;
		}

		if(is_logged() == 0) {
			$insert_data = array(
				'first_name' => $billing_info->firstname,
				'last_name' => $billing_info->lastname,
				'country' => $billing_info->country,
				'state' => $billing_info->state,
				'street_address' => $billing_info->street,
				'postal_zip' => $billing_info->zip,
				'town_city' => $billing_info->city,
				'phone' => $billing_info->phone,
				'email' => $email
			);
			$this->Global_model->insert('billing_address', $insert_data);

			$insert_data = array(
				'first_name' => $shipping_info->firstname,
				'last_name' => $shipping_info->lastname,
				'country' => $shipping_info->country,
				'state' => $shipping_info->state,
				'street_address' => $shipping_info->street,
				'postal_zip' => $shipping_info->zip,
				'town_city' => $shipping_info->city,
				'phone' => $shipping_info->phone,
				'email' => $email
			);
			$this->Global_model->insert('shipping_address', $insert_data);

			$data = array("email"=>$email,"password"=>$password,"reg_type"=>"Manual");
			$this->User_model->insert("users",$data);
			//set session
			$ses_key = encrypt($email);
			$sessdata = array('ses_key'  => $ses_key); 
			$this->session->set_userdata($sessdata);
			echo json_encode(array('status' => 'success'));
		} else {
			$update_data = array(
				'first_name' => $billing_info->firstname,
				'last_name' => $billing_info->lastname,
				'country' => $billing_info->country,
				'state' => $billing_info->state,
				'street_address' => $billing_info->street,
				'postal_zip' => $billing_info->zip,
				'town_city' => $billing_info->city,
				'phone' => $billing_info->phone,
				'email' => $email
			);
			if($this->Global_model->check_exist('billing_address', array('email' => get_email()))){
				$this->Global_model->update('billing_address', $update_data, array('email' => get_email()));
			} else {
				$this->Global_model->insert('billing_address', $update_data);
			}

			$update_data = array(
				'first_name' => $shipping_info->firstname,
				'last_name' => $shipping_info->lastname,
				'country' => $shipping_info->country,
				'state' => $shipping_info->state,
				'street_address' => $shipping_info->street,
				'postal_zip' => $shipping_info->zip,
				'town_city' => $shipping_info->city,
				'phone' => $shipping_info->phone,
				'email' => $email
			);

			if($this->Global_model->check_exist('shipping_address', array('email' => get_email()))){
				$this->Global_model->update('shipping_address', $update_data, array('email' => get_email()));		
			} else {
				$this->Global_model->insert('shipping_address', $update_data);
			}
			echo json_encode(array('status' => 'success'));
		}
	}

	public function get_all_products() {
		$regular_prod = $this->Global_model->fetch_tag_array('*, regular_price as price' ,'products', array('regular_price !='=> ''));
		$foil_prod = $this->Global_model->fetch_tag('*, foil_price as price' ,'products', array('foil_price !='=> ''));
		$regular_prod = $regular_prod ? $regular_prod : [];
		$foil_prod = $foil_prod ? $foil_prod : [];
		$products = array_merge($regular_prod, $foil_prod);
		echo json_encode($products);
	}

	public function get_product() {
		if(!$this->input->post()) show_404();

		$name = post('search_name');
		$cat_code = post('cat_code');
		$product = [];
		if($cat_code){
			$regular_prod = $this->Global_model->fetch_tag_array('*, regular_price as price' ,'products', array('regular_price !='=> '', 'set_code' => strtoupper($cat_code)));
			$foil_prod = $this->Global_model->fetch_tag('*, foil_price as price' ,'products', array('foil_price !='=> '', 'set_code' => strtoupper($cat_code)));
			$regular_prod = $regular_prod ? $regular_prod : [];
			$foil_prod = $foil_prod ? $foil_prod : [];
			$products = array_merge($regular_prod, $foil_prod);
		} else if($name) {
			$regular_prod = $this->Product_model->get_search_product('*, regular_price as price' ,$name, 'regular_price != ""');
			$foil_prod = $this->Product_model->get_search_product('*, foil_price as price', $name, 'foil_price != ""');
			$regular_prod = $regular_prod ? $regular_prod : [];
			$foil_prod = $foil_prod ? $foil_prod : [];
			$products = array_merge($regular_prod, $foil_prod);
		}

		echo json_encode($products);
	}

	public function check_cart() {
		if(!$this->input->post()) show_404();

		$cart_list = getUpdatedCartList();
		$out_of_stock = array();
		foreach($cart_list as $i => $card) {
			if($card['card']->stock < $card['quantity']){
				$card['card_name'] = getCardName($card['card'], 'json');
				array_push($out_of_stock, $card);
			}
		}
		echo json_encode($out_of_stock);
	}

	public function get_cart() {
		if(!$this->input->post()) show_404();
		$cart_list = getUpdatedCartList();
		$cart = array();
		foreach($cart_list as $i => $card) {
			array_push($cart, $card);
		}
		echo json_encode($cart);	
	}

	public function get_cart_total() {
		if(!$this->input->post()) show_404();

		$data = array();		
		$single_only = true;

		$cart_list = getUpdatedCartList();
		$items = array();
		$total_amount = 0;
		if(!$cart_list) {
			echo json_encode(array('status' => 'fail', 'message'=> 'Cart is empty'));
			return;
		}
		$base_total = 0;
		foreach($cart_list as $i => $card) {
			$cart_list[$i]['card_name'] = getCardName($card['card'], 'json');
			$cart_list[$i]['card_price'] = $card['card']->regular_price ? $card['card']->regular_price : $card['card']->foil_price;
			$cart_list[$i]['card_total_amount'] = $cart_list[$i]['card_price'] * $card['quantity'];
			$total_amount += ($card['card']->regular_price ? $card['card']->regular_price : $card['card']->foil_price) * $card['quantity'];
			$base_total += floatval($card['card']->base_price);
			array_push($items, $card);
			if($card['card']->category != 'single cards') $single_only = false;
		}
		$data['base_total'] = $base_total;
	  	$data['sub_total'] = $total_amount;

	  	if($single_only) {
		  	$shipping_fee = array(35, 70);
	  		$free_shipping_fee = array(1000, 1500);
	  	} else {
		  	$shipping_fee = array(70, 70);
	  		$free_shipping_fee = array(1500, 1500);
	  	}
	  	$selected_opt = intval($this->input->post('selected_opt'));
  		$data['shipping_fee'] = $selected_opt < 2 ? $shipping_fee[$selected_opt] : $shipping_fee[0];
  		$data['free_shipping_fee'] = $selected_opt < 2 ? $free_shipping_fee[$selected_opt] : $free_shipping_fee[0];
  		$data['local_shipping_list'] = $shipping_fee;
  		$data['local_free_shipping_list'] = $free_shipping_fee;

	  	$int_shipping_fee = array(90, 600, 2100);
	  	$free_int_shipping_fee = array(3000, 12000, 45000);
  		$data['int_shipping_fee'] = $selected_opt < 2 ? $int_shipping_fee[$selected_opt] : $int_shipping_fee[0];
  		$data['free_int_shipping_fee'] = $selected_opt < 2 ? $free_int_shipping_fee[$selected_opt] : $free_int_shipping_fee[0];
  		$data['int_shipping_list'] = $int_shipping_fee;
  		$data['int_free_shipping_list'] = $free_int_shipping_fee;

  	$data['purrcoins'] = floatval($this->session->userdata("purrcoins") ? $this->session->userdata("purrcoins") : 0);
 		$data['coupon_discount'] = '0.' . floatval($this->session->userdata("coupons") ? $this->session->userdata("coupons")->discount_type == 'percentage' ? pad($this->session->userdata("coupons")->discount , 2) : 0 : 0);
  	$data['coupon_value'] = floatval($this->session->userdata("coupons") ? $this->session->userdata("coupons")->discount_type != 'percentage' ? $this->session->userdata("coupons")->discount : 0 : 0);

		$user = $this->Global_model->fetch_data('users', array('email' => get_email()));
  	$loyalty = $this->Global_model->fetch_data('loyalty_program', array('id' => $user[0]->user_loyalty), 1);

		if($loyalty[0]){
	  	$data['single_card_rebate'] = floatval('0.' . pad(str_replace("%", "", $loyalty[0]->single_cards_rebate), 2));
	  	$data['other_card_rebate'] = floatval('0.' . pad(str_replace("%", "", $loyalty[0]->other_products_rebate), 2));
	  } else {
	  	$data['single_card_rebate'] = 0;
	  	$data['other_card_rebate'] = 0;
	  }
		echo json_encode(array('status' => 'success', 'message'=> '', 'total_amount' => $total_amount, 'items' => $items, 'data'=>$data, 'cart_item'=> $cart_list));
	}

	public function get_suggestion() {
		if(!$this->input->post()) show_404();

		$name = str_replace('’', "'", post('name'));
		$name = addslashes($name);
		echo json_encode($this->Product_model->get_suggestion($name));
	}

	public function get_suggestion_2() {
		//dito mo nalang i search
		$name = post('name');
		$name = str_replace('’', "'", $name);
		$name = addslashes($name);
		$data = $this->Product_model->get_available_products($name);
		
		die(json_encode($data));
	}

	public function sort_other_product() {
		if(!$this->input->post()) show_404();
		$products = array();
		$product_type = post('product_type');
		$product_brand = post('product_brand');
		$product_franchise = post('product_franchise');
		$product_price = post('product_price');
		$show_stock = post('show_stock');
		$sort_data = '';
		$search_name = post('search_name');

		switch ($product_price) {
			case 'Less than 100<thb>':
				$sort_data .= 'WHERE regular_price < 100';
				break;
			case '100<thb> - 249<thb>':
				$sort_data .= 'WHERE regular_price >= 100 AND regular_price < 250';
				break;
			case '250<thb> - 499<thb>':
				$sort_data .= 'WHERE regular_price >= 250 AND regular_price < 500';
				break;
			case '500<thb> - 999<thb>':
				$sort_data .= 'WHERE regular_price >= 500 AND regular_price < 1000';
				break;
			case '1000<thb> - 1999<thb>':
				$sort_data .= 'WHERE regular_price >= 1000 AND regular_price < 2000';
				break;
			case 'Over 2000<thb>':
				$sort_data .= 'WHERE regular_price >= 2000';
				break;
			default:
				$sort_data .= 'WHERE regular_price > 0';
				break;
		}

		if($search_name) {
			if($sort_data) {
				$sort_data = $sort_data . ' AND name LIKE "%'. $search_name .'%"';
			} else {
				$sort_data = 'WHERE name LIKE "%'. $search_name .'%"';
			}
		}
		
		if($product_type != '' && $product_type != 'all') {
			$sort_data .= ' AND lower(type) = "'. $product_type .'"';
		}
		if($product_brand != '' && $product_brand != 'all') {
			$sort_data .= ' AND lower(brand) = "'. $product_brand .'"';
		}
		if($product_franchise != '' && $product_franchise != 'all') {
			$sort_data .= ' AND lower(franchise) = "'. $product_franchise .'"';
		}
		$sort_regular_prod = $sort_data;
		$sort_regular_prod .= ' AND (regular_price != "" OR regular_price != null) AND category = "other products"';
		if($show_stock) {
			$sort_regular_prod .= ' AND stock > 0';
		}
		$sort_regular_prod .= ' GROUP BY id';
		$regular_prod = $this->Product_model->get_other_sort('*, regular_price as price', $sort_regular_prod);

		$products = $regular_prod;

		echo json_encode($products);
	}

	public function sort_product() {
		if(!$this->input->post()) show_404();

		$sort_variation = json_decode(post('filter_variation'));
		$sort_rarity 		= get('rarity') ? clean_data(get('rarity')) : json_decode(post('filter_rarity'));
		$sort_color 		= json_decode(post('filter_color'));
		$sort_type 			= json_decode(post('filter_type'));
		$sort_promo 		= json_decode(post('filter_promo'));
		$sort_language		= json_decode(post('sort_language'));
		$search_name		= post('search_name');
		$cat_code				= post('cat_code');
		$show_stock 		= post('show_stock');

		$sort_data = '';
		if($sort_rarity){
			if(!in_array('all', $sort_rarity) && sizeof($sort_rarity) > 0) {
				$sort_data = $sort_data ? $sort_data : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_rarity); $ctr++){
					$sort_data .= 'a.rarity LIKE "%' . $sort_rarity[$ctr] . '%" ';
					if($ctr != sizeof($sort_rarity) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		// if($sort_variation){
		// 	if(!in_array('all', $sort_variation) && sizeof($sort_variation) > 0) {
		// 		$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
		// 		for($ctr = 0; $ctr < sizeof($sort_variation); $ctr++){
		// 			if($sort_variation[$ctr] == 'regular') {
		// 				$sort_data .= 'regular_price != "" ';
		// 			} else if($sort_variation[$ctr] == 'foil') {
		// 				$sort_data .= 'foil_price != "" ';
		// 			}

		// 			if($ctr != sizeof($sort_variation) - 1) {
		// 				$sort_data .= ' OR ';
		// 			}
		// 		}
		// 		$sort_data .= ')';
		// 	}
		// }

		if($sort_color){
			if(!in_array('all', $sort_color) && sizeof($sort_color) > 0) {
				$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_color); $ctr++){
					if($sort_color[$ctr] == 'Multi') {
						$sort_data .= '(LENGTH(a.color) = 2 AND (a.color NOT LIKE"%A%" OR a.color NOT LIKE"%L%" OR a.color NOT LIKE"%K%"))';
						$sort_data .= ' OR (LENGTH(a.color) > 2 AND a.color != "KMC") ';
					} else if($sort_color[$ctr] == 'Colorless') {
						$sort_data .= 'a.color = "A" OR a.color = "L" OR a.color="KMC" OR a.color="C" OR a.color="KA" OR a.color="KAB" ';
					} else {
						$sort_data .= '(a.color = "'. $sort_color[$ctr] .'")';
						$sort_data .= ' OR (a.color LIKE "%'. $sort_color[$ctr] .'%" AND LENGTH(a.color) = 2 AND (a.color LIKE "%K%" OR a.color LIKE "%A%")) ';
					}
 
					if($ctr != sizeof($sort_color) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		if($sort_language){
			if(!in_array('all', $sort_language) && sizeof($sort_language) > 0) {
				$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_language); $ctr++){
					$sort_data .= 'a.language LIKE "%'. $sort_language[$ctr] .'%" ';
					if($ctr != sizeof($sort_language) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		if($sort_type){
			if(!in_array('all', $sort_type) && sizeof($sort_type) > 0) {
				$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_type); $ctr++){
					$sort_data .= 'a.type LIKE "%'. $sort_type[$ctr] .'%" ';
					if($ctr != sizeof($sort_type) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		if($sort_promo){
			if(!in_array('all', $sort_promo) && sizeof($sort_promo) > 0) {
				$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_promo); $ctr++){
					if(strtolower($sort_promo[$ctr]) == 'promo') {
						$sort_data .= 'a.tag = "Promo" ';
					} else {
						$sort_data .= 'a.promo = "'. $sort_promo[$ctr] .'" ';
						if($ctr != sizeof($sort_promo) - 1) {
							$sort_data .= ' OR ';
						}
					}
				}
				$sort_data .= ')';
			}
		}
		if(post('datestamp') != '') {
			array_push($sort_variation, 'all');
		}

		if($show_stock == 'true') {
			$sort_data = $sort_data ? $sort_data . ' AND a.stock != 0 ' : 'WHERE a.stock != 0 ';
		} else {
			$sort_data = $sort_data ? $sort_data . ' AND a.stock >= 0 ' : 'WHERE a.stock >= 0 ';			
		}

		$sort_data = $search_name ? $sort_data ? $sort_data . ' AND a.name LIKE "%'. $search_name .'%"' : 'WHERE a.name LIKE "%'. $search_name .'%"' : $sort_data;
		$sort_data = $cat_code ? $sort_data ? $sort_data . ' AND a.set_code = "'. strtoupper($cat_code) .'"' : 'WHERE a.set_code = "'. strtoupper($cat_code) .'"' : $sort_data;

        $display_name = 'a.name as name,a.language, a.image, a.collector_number, a.ability as ability, a.type as type, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw, a.category as category';
        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';

		$this->session->set_userdata(array('language'=>post('language')));
		$sort_language = '';
		switch (post('language')) {
			case 'eng': 
			   $sort_language = post('language');
				break;
			case 'tw':
				$sort_language = post('language');
				break;
			case 'jp':
				$sort_language = post('language');
				break;
			case 'ko':
				$sort_language = post('language');
				break;
			default:
				break;
		}

		if(in_array('all', $sort_variation) || in_array('regular', $sort_variation)){
			$sort_regular_prod = $sort_data . ' AND (a.regular_price != "" OR a.regular_price != null) AND a.category != "other products"';
			if($sort_language != '') {
				$sort_regular_prod .= ' AND a.language = "'.$sort_language.'"';

				$sort_regular_prod .= ' GROUP BY a.id ';
				$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number, a.image
					, a.set_code as set_code, a.rarity as rarity, a.fully_handled as fully_handled, a.regular_price as price
					, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color';

				$regular_prod = $this->Product_model->get_sort($product_tag, $sort_regular_prod);
			} else {
				$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number, a.image
					, a.set_code as set_code, a.rarity as rarity, a.fully_handled as fully_handled, a.regular_price as price
					, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color';

				$regular_prod = array_merge(
					$this->Product_model->get_sort($product_tag, $sort_regular_prod . " AND a.language = 'eng' GROUP BY id "), 
					$this->Product_model->get_sort($product_tag, $sort_regular_prod . " AND a.language = 'tw' GROUP BY id "), 
					$this->Product_model->get_sort($product_tag, $sort_regular_prod . " AND a.language = 'jp' GROUP BY id "), 
					$this->Product_model->get_sort($product_tag, $sort_regular_prod . " AND a.language = 'ko' GROUP BY id "));
			}

		} else {
			$regular_prod = [];
		}
		if(in_array('all', $sort_variation) || in_array('foil', $sort_variation)){
			$sort_foil_prod = $sort_data . ' AND (a.foil_price != "" OR a.foil_price != null) AND a.category != "other products"';
			if($sort_language != '') {
				$sort_foil_prod .= 'AND a.language = "'.$sort_language.'"';

				$sort_foil_prod .= ' GROUP BY a.id ';
				$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number, a.set_code as set_code,a.image
					, a.rarity as rarity, a.fully_handled as fully_handled, a.foil_price as price
					, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color';
				$foil_prod = $this->Product_model->get_sort($product_tag, $sort_foil_prod);
			} else {
				$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number, a.set_code as set_code,a.image
					, a.rarity as rarity, a.fully_handled as fully_handled, a.foil_price as price
					, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color';
					
				$foil_prod = array_merge(
					$this->Product_model->get_sort($product_tag, $sort_foil_prod . " AND a.language = 'eng' GROUP BY id "), 
					$this->Product_model->get_sort($product_tag, $sort_foil_prod . " AND a.language = 'tw' GROUP BY id "), 
					$this->Product_model->get_sort($product_tag, $sort_foil_prod . " AND a.language = 'jp' GROUP BY id "), 
					$this->Product_model->get_sort($product_tag, $sort_foil_prod . " AND a.language = 'ko' GROUP BY id "));
			}
		} else {
			$foil_prod = [];
		}

		$session_sort = array(
			'cat' => $cat_code ? $cat_code : '', 
			'search_name' => $search_name ? $search_name : '',
			'sort' => json_decode(post('filter_data')),
			'show_stock' => $show_stock
		);

		// OVERWRITE SESSION SORT
		$filter_data = json_decode(post('filter_data'), true);
		$session_sort = array(
			'cat' => $cat_code ? $cat_code : '', 
			'search_name' => $search_name ? $search_name : '',
			'datestamp' => post('datestamp'),
			'sort' => json_decode(post('filter_data')),
		);	
		$this->session->set_userdata('session_sort', $session_sort);

		//$this->session->set_userdata('session_sort', $session_sort);

		$products = array_merge($regular_prod, $foil_prod);
		if(empty($products)) {
			//$products = $this->sort_product_chinese();
			echo json_encode($products);
		} else {
			echo json_encode($products);
		}
		
	}

	public function sort_product_chinese() {
		if(!$this->input->post()) show_404();

		$sort_variation = json_decode(post('filter_variation'));
		$sort_rarity 		= get('rarity') ? clean_data(get('rarity')) : json_decode(post('filter_rarity'));
		$sort_color 		= json_decode(post('filter_color'));
		$sort_type 			= json_decode(post('filter_type'));
		$search_name		= post('search_name');
		$cat_code				= post('cat_code');
		$show_stock 		= post('show_stock');

		$sort_data = '';
		if($sort_rarity){
			if(!in_array('all', $sort_rarity) && sizeof($sort_rarity) > 0) {
				$sort_data = $sort_data ? $sort_data : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_rarity); $ctr++){
					$sort_data .= 'a.rarity LIKE "%' . $sort_rarity[$ctr] . '%" ';
					if($ctr != sizeof($sort_rarity) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		// if($sort_variation){
		// 	if(!in_array('all', $sort_variation) && sizeof($sort_variation) > 0) {
		// 		$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
		// 		for($ctr = 0; $ctr < sizeof($sort_variation); $ctr++){
		// 			if($sort_variation[$ctr] == 'regular') {
		// 				$sort_data .= 'regular_price != "" ';
		// 			} else if($sort_variation[$ctr] == 'foil') {
		// 				$sort_data .= 'foil_price != "" ';
		// 			}

		// 			if($ctr != sizeof($sort_variation) - 1) {
		// 				$sort_data .= ' OR ';
		// 			}
		// 		}
		// 		$sort_data .= ')';
		// 	}
		// }

		if($sort_color){
			if(!in_array('all', $sort_color) && sizeof($sort_color) > 0) {
				$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_color); $ctr++){
					if($sort_color[$ctr] == 'Multi') {
						$sort_data .= '(LENGTH(a.color) = 2 AND (a.color NOT LIKE"%A%" OR a.color NOT LIKE"%L%" OR a.color NOT LIKE"%K%"))';
						$sort_data .= ' OR (LENGTH(a.color) > 2 AND a.color != "KMC") ';
					} else if($sort_color[$ctr] == 'Colorless') {
						$sort_data .= 'a.color = "A" OR a.color = "L" OR a.color="KMC" OR a.color="C" OR a.color="KA" OR a.color="KAB" ';
					} else {
						$sort_data .= '(a.color = "'. $sort_color[$ctr] .'")';
						$sort_data .= ' OR (a.color LIKE "%'. $sort_color[$ctr] .'%" AND LENGTH(a.color) = 2 AND (a.color LIKE "%K%" OR a.color LIKE "%A%")) ';
					}
 
					if($ctr != sizeof($sort_color) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		if($sort_type){
			if(!in_array('all', $sort_type) && sizeof($sort_type) > 0) {
				$sort_data = $sort_data ? $sort_data . ' AND ( ' : 'WHERE ( ';
				for($ctr = 0; $ctr < sizeof($sort_type); $ctr++){
					$sort_data .= 'a.type LIKE "%'. $sort_type[$ctr] .'%" ';
					if($ctr != sizeof($sort_type) - 1) {
						$sort_data .= ' OR ';
					}
				}
				$sort_data .= ')';
			}
		}

		if($show_stock == 'true') {
			$sort_data = $sort_data ? $sort_data . ' AND a.stock != 0 ' : 'WHERE a.stock != 0 ';
		} else {
			$sort_data = $sort_data ? $sort_data . ' AND a.stock >= 0 ' : 'WHERE a.stock >= 0 ';			
		}

		$sort_data = $search_name ? $sort_data ? $sort_data . ' AND b.name_tw LIKE "%'. $search_name .'%"' : 'WHERE b.name_tw LIKE "%'. $search_name .'%"' : $sort_data;
		$sort_data = $cat_code ? $sort_data ? $sort_data . ' AND a.set_code = "'. strtoupper($cat_code) .'"' : 'WHERE a.set_code = "'. strtoupper($cat_code) .'"' : $sort_data;

        $display_name = 'a.name as name, a.ability as ability, a.type as type, a.image as image, a.flavor as flavor, b.name_tw as name_tw, b.ability_tw as ability_tw, b.type_tw as type_tw, b.flavor_tw as flavor_tw,a.image,a.collector_number, a.category as category';
        $display_name .= ', name_jp, type_jp, ability_jp, flavor_jp';
        $display_name .= ', name_ko, type_ko, ability_ko, flavor_ko';
        
		$this->session->set_userdata(array('language'=>post('language')));
		$sort_language = '';
		switch (post('language')) {
			case 'eng': 
			   $sort_language = post('language');
				break;
			case 'tw':
				$sort_language = post('language');
				break;
			case 'jp':
				$sort_language = post('language');
				break;
			case 'ko':
				$sort_language = post('language');
				break;
			default:
				break;
		}

		if(in_array('all', $sort_variation) || in_array('regular', $sort_variation)){
			$sort_regular_prod = $sort_data . ' AND (a.regular_price != "" OR a.regular_price != null) AND a.category != "other products"';
			if($sort_language != '') {
				$sort_regular_prod .= 'AND a.language = "'.$sort_language.'"';
			}
			$sort_regular_prod .= ' GROUP BY id ';
			$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number,a.image as image
				, a.set_code as set_code, a.rarity as rarity, a.fully_handled as fully_handled, a.regular_price as price
				, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color';
			$regular_prod = $this->Product_model->get_sort($product_tag, $sort_regular_prod);
		} else {
			$regular_prod = [];
		}
		if(in_array('all', $sort_variation) || in_array('foil', $sort_variation)){
			$sort_foil_prod = $sort_data . ' AND (a.foil_price != "" OR foil_price != null) AND a.category != "other products"';
			if($sort_language != '') {
				$sort_foil_prod .= 'AND a.language = "'.$sort_language.'"';
			}
			$sort_foil_prod .= ' GROUP BY a.id ';
			$product_tag = 'a.id as id, '.$display_name.', a.collector_number as collector_number, a.set_code as set_code,a.image as image
				, a.rarity as rarity, a.fully_handled as fully_handled, a.foil_price as price
				, a.regular_price as regular_price, a.foil_price as foil_price, a.stock as stock, a.datestamp, a.promo, a.tag, a.custom_tag, a.tag_color, a.tag_bg_color';
			$foil_prod = $this->Product_model->get_sort($product_tag, $sort_foil_prod);
		} else {
			$foil_prod = [];
		}

		$session_sort = array(
			'cat' => $cat_code ? $cat_code : '', 
			'search_name' => $search_name ? $search_name : '',
			'sort' => json_decode(post('filter_data')),
			'show_stock' => $show_stock
		);

		//$this->session->set_userdata('session_sort', $session_sort);

		$products = array_merge($regular_prod, $foil_prod);
		return $products;
		
	}


	public function add_item_to_wishlist() {
		$data = $this->input->post();
		if(!$data) return;

		$id = post('id');
		$language = post('language');
		
		if(is_logged() == 0) {
			$watchlist = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist') : [];

			$watchlist[$id] = array('id'=>$id);
			$this->session->set_userdata(array('watchlist' => $watchlist));
			return;
		}

		$email = get_email();
		$watch_record = $this->Global_model->fetch('watchlist', array('email'=>$email));
		if(!$watch_record) return;
		$watchlist = json_decode($watch_record[0]->watchlist, TRUE);
		$watchlist[$id] = array('id'=>$id);
		
		$update_data = array(
			'watchlist'=>json_encode($watchlist)
		);
		$watch_record = $this->Global_model->update('watchlist', $update_data, array('email'=>$email));
	}

	public function check_wishlist() {
		$data = $this->input->post();
		if(!$data) return;

		if(is_logged() == 0) {
			$watchlist = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist') : [];
		} else{
			$email = get_email();
			$watch_record = $this->Global_model->fetch('watchlist', array('email'=>$email));
			if(!$watch_record) return;
			$watchlist = json_decode($watch_record[0]->watchlist, TRUE);
		}
		
		$cart_list = getUpdatedCartList();
		$out_of_stock = array();
		if(sizeof($watchlist) <= 0) return;
		foreach($watchlist as $i => $value) {
			if(!isset($cart_list[$i])) {
				$card = $this->Global_model->fetch('products', array('id'=>$i));
				if(!$card) return;
				$card = $card[0];
				if($card->stock <= 0){
					$card_name = getCardName($card, 'json');
					array_push($out_of_stock, array('card_name'=>$card_name,'set_name'=>$card->set));
				}
			}
		}

		echo json_encode($out_of_stock);
	}

	public function move_wishlist_to_cart(){
		$data = $this->input->post();
		if(!$data) return;

		if(is_logged() == 0) {
			$watchlist = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist') : [];
		} else{
			$email = get_email();
			$watch_record = $this->Global_model->fetch('watchlist', array('email'=>$email));
			if(!$watch_record) return;
			$watchlist = json_decode($watch_record[0]->watchlist, TRUE);
		}
		
		$cart_list = getUpdatedCartList();
		if(sizeof($watchlist) <= 0) return;
		foreach($watchlist as $i => $value) {
			if(!isset($cart_list[$i])) {
				$card = $this->Global_model->fetch('products', array('id'=>$i));
				if(!$card) return;
				$card = $card[0];
				if($card->stock > 0){
					$img_src = getCardImage($card, 'json');
					$card->img_src = $img_src;
					$variation = $card->regular_price ? 'Regular' : 'Foil';
					$card->price = $variation == 'Regular' ? $card->regular_price : $card->foil_price;
					$cart_list[$i] = array('card' => array('id'=>$card->id), 'quantity' => 1, 'variation' => $variation);
				}
			}
		}

		updateCartList($cart_list);
		$cart_session_list = getUpdatedCartList();
		$cart_list = array();
		foreach($cart_session_list as $cart){
			array_push($cart_list, $cart);
		}

		echo json_encode(array('status' => 'success', 'message' => 'success', 'cart_list' => $cart_list));
	}

	public function remove_item_to_wishlist() {
		$data = $this->input->post();
		if(!$data) return;

		$id = post('id');
		if(is_logged() == 0) {
			$watchlist = $this->session->userdata('watchlist') ? $this->session->userdata('watchlist') : [];

			unset($watchlist[$id]);
			$this->session->set_userdata(array('watchlist' => $watchlist));
			return;
		}

		$email = get_email();
		$watch_record = $this->Global_model->fetch('watchlist', array('email'=>$email));
		if(!$watch_record) return;
		$watchlist = json_decode($watch_record[0]->watchlist, TRUE);
		unset($watchlist[$id]);

		$update_data = array(
			'watchlist'=>json_encode($watchlist)
		);
		$watch_record = $this->Global_model->update('watchlist', $update_data, array('email'=>$email));
	}

	public function add_item_to_cart() {
		$data = $this->input->post();
		if(!$data) return;

		$language = post('language') ? post('language') : '';
		$card_id = $data['card_id'];
		$card = $this->Global_model->fetch_tag('image as img_src,language, regular_price,foil_price,base_price,name,set_code,stock,collector_number,id,products.set,category,rarity,type','products', array('id'=> $card_id));
		if(!$card) return;
		$card = $card[0];
		$cart_session_list = getUpdatedCartList();
		$price = $card->regular_price ? $card->regular_price : $card->foil_price;
		if($data['quantity'] == 0){
			unset($cart_session_list[$card->id]);
		} else if($data['quantity'] > $card->stock){
			echo json_encode(array('status' => 'fail', 'message' => 'Invalid quantity'));
		} else {			
			$img_src = getCardImage($card, 'json');
			$card->img_src = $img_src;
			$variation = $card->regular_price ? 'Regular' : 'Foil';
			$card->price = $variation == 'Regular' ? $card->regular_price : $card->foil_price;
			$language = isset($cart_session_list[$card->id]['language']) ? $cart_session_list[$card->id]['language'] : $language;
			$cart_session_list[$card->id] = array('card' => array('id'=>$card->id), 'quantity' => $data['quantity'], 'variation' => $variation);
		}

		updateCartList($cart_session_list);
		$cart_session_list = getUpdatedCartList();
		$cart_list = array();
		foreach($cart_session_list as $cart){
			array_push($cart_list, $cart);
		}


		echo json_encode(array('status' => 'success', 'message' => 'success', 'cart_list' => $cart_list));
	}

	public function clear_cart() {
		$this->session->unset_userdata('cart_list');
		redirect(base_url());
	}

	public function remove_item_cart() {
		$data = $this->input->post();
		if(!$data) return;
		$user = $this->Global_model->fetch_data('users', array('email' => get_email()));
		if(get_email()){
	  		$loyalty = $this->Global_model->fetch_data('loyalty_program', array('id' => $user[0]->user_loyalty), 1);
		} else {
		  	$loyalty = $this->Global_model->fetch_data('loyalty_program', array('id' => 1), 1);
		}

		$cart_session_list = getUpdatedCartList();
		
		unset($cart_session_list[$data['id']]);
		$subtotal = 0;
		$rebate = 0;
		foreach ($cart_session_list as $key => $value) {
			if($value['card']->category == 'single cards'){
				$price = $value['card']->regular_price ? $value['card']->regular_price : $value['card']->foil_price;
				$deduction = $loyalty[0] ? floatval(str_replace("%", "", $loyalty[0]->single_cards_rebate)) / 100 : 0;
			} else {
				$price = $value['card']->regular_price ? $value['card']->regular_price : $value['card']->foil_price;
				$deduction = $loyalty[0] ? floatval(str_replace("%", "", $loyalty[0]->single_cards_rebate)) / 100 : 0;
			}
			$rebate += $deduction * ($price * $value['quantity']);
			$subtotal += ($price * $value['quantity']);
		
		}

		updateCartList($cart_session_list);
		$cart_list = array();
		foreach($cart_session_list as $cart){
			array_push($cart_list, $cart);
		}

		if(get_email()){
			if(empty($cart_list)) {
				$filter = ["email"=>get_email()];
				$this->Global_model->delete('tmp_cart',$filter);
			}
		}

		echo json_encode(array('status' => 'success', 'status' => 'success', 'cart_list' => $cart_list));
		// $rebate = number_format(floatval($rebate),2);
		// $this->session->set_userdata($cart_list);
		// $return_data = array(
		// 	'subtotal' => $subtotal,
		// 	'rebate' => $rebate
		// );
		// echo json_encode($return_data);
	}


	public function get_states() {
		$country = clean_data(get('q'));
		$filter = ["country_id"=>$country];
		$data['states'] = $this->Global_model->fetch_tag('id,name','states',$filter);
		echo json_encode($data);
	}

	public function get_town_city() {
		$country = clean_data(get('q_country'));
		$state = clean_data(get('q_state'));
		$filter = ["country_id"=>$country,"state_id"=>$state];
		$data['cities'] = $this->Global_model->fetch_tag('id,name','cities',$filter);
		echo json_encode($data);
	}

	public function get_user_billing_address(){
		if($_GET){
			$email = get_email();
			$filter = ["email" => $email];
			$data['billing_address'] = $this->Global_model->fetch_tag('*','billing_address',$filter);
			echo json_encode($data);
		}
	}

	public function get_user_shipping_address(){
		if($_GET){
			$email = get_email();
			$filter = ["email" => $email];
			$data['shipping_address'] = $this->Global_model->fetch_tag('*','shipping_address',$filter);
			echo json_encode($data);
		}
	}

	public function get_user_information(){
		if($_GET){
			$email = get_email();
			$filter = ["email" => $email];
			$data['user_information'] = $this->Global_model->fetch_tag('*','users',$filter);
			echo json_encode($data);
		}
	}

}