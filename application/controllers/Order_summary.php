<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class order_summary extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Cart_Order_model');
	}

	public function index() {
	}

	public function cart() {
		$cart_list = getUpdatedCartList();

		$data['content'] = 'pages/cart_view';

		$this->load->view('templates/main_template', $data);
	}

	public function view($id = null) {
		if($id == null) show_404();

		$data['content'] = 'pages/order_summary_view';
		$order = $this->Cart_Order_model->get($id);
		$data['order'] = $order[0];
		$data['order_list'] = json_decode($order[0]['cart_list']);

		$this->load->view('templates/main_template', $data);
	}

}