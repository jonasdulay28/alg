<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class accessories extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("User_model");
		$this->load->model("Product_model");
		$this->load->model("Menu_model");
		$this->load->model("Filter_model");
		$this->load->model("Global_model");
	}

	public function index() {

		$data['search_name'] = trim(addslashes(urldecode(get('name'))));
		$filter = ["set_code"=>clean_data(get('cat'))];
		$get_set_name = $this->Global_model->fetch_tag_row("products.set as set_name","products",$filter,1);
		$data['set_name'] = 'Accessories';

		$session_sort = $this->session->userdata('session_sort');
		if($session_sort){
			if($session_sort['search_name'] != $data['search_name']) {
				$session_sort = [];
				$this->session->unset_userdata('session_sort');
			}
		}
		$data['session_sort'] = $session_sort ? $session_sort : [];

		$data['styles'] = array(
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/css/card_list.css">',
			'<link rel="stylesheet" type="text/css" href="' . base_url() .'assets/libraries/manareplacer/mana-symbols.css">'
		);
		$data['scripts'] = array(
			'<script type="text/javascript" src="'. base_url() .'assets/js/accessories.js"></script>'
		);

		$data['content'] = 'pages/accessories';

		$accessories = $this->Filter_model->get_where(array('main_filter' => 'Accessories'));
		$brand = $this->Filter_model->get_where(array('main_filter' => 'Brand'));
		$franchise = $this->Filter_model->get_where(array('main_filter' => 'Franchise'));
		$by_price = $this->Filter_model->get_where(array('main_filter' => 'Shop by Price'));

		$accessories = json_decode($accessories[0]['sub_filter']);
		$brand = json_decode($brand[0]['sub_filter']);
		$franchise = json_decode($franchise[0]['sub_filter']);
		$by_price = json_decode($by_price[0]['sub_filter']);

		$tmp = array();
		for($i = 0; $i < sizeof($accessories); $i++) {
			$key = $accessories[$i];
			if($key != '' && strtolower($key) != 'all') {
				$prod = $this->Product_model->check_other_sort('*, regular_price as price', 'WHERE (regular_price != 0 OR regular_price != "") AND stock > 0 AND lower(type) = "'. strtolower($key) .'" AND category = "other products"');

				if(!empty($prod)){
					array_push($tmp, $key);
				}
			} else {
				array_push($tmp, $key);
			}
		}
		$accessories = $tmp;

		$tmp = array();
		for($i = 0; $i < sizeof($brand); $i++) {
			$key = $brand[$i];
			if($key != '' && strtolower($key) != 'all') {
				$prod = $this->Product_model->check_other_sort('*, regular_price as price', 'WHERE (regular_price != 0 OR regular_price != "") AND stock > 0 AND lower(brand) = "'. strtolower($key) .'" AND category = "other products"');
				
				if(!empty($prod)){
					array_push($tmp, $key);
				}
			} else {
				array_push($tmp, $key);
			}
		}
		$brand = $tmp;

		$tmp = array();
		for($i = 0; $i < sizeof($franchise); $i++) {
			$key = $franchise[$i];
			if($key != '' && strtolower($key) != 'all') {
				$prod = $this->Product_model->check_other_sort('*, regular_price as price', 'WHERE (regular_price != 0 OR regular_price != "") AND stock > 0 AND lower(franchise) = "'. strtolower($key) .'" AND category = "other products"');
				
				if(!empty($prod)){
					array_push($tmp, $key);
				}
			} else {
				array_push($tmp, $key);
			}
		}
		$franchise = $tmp;

		$data['filter_accessories'] = $accessories;
		$data['filter_brand'] = $brand;
		$data['filter_franchise'] = $franchise;
		$data['filter_by_price'] = $by_price;

		$data['data']['currencies'] = $this->Global_model->fetch('currencies');
		$data['data']['pages'] = $this->Global_model->fetch('pages', array('status'=>'Active'));
		$data['data']['categories'] = $this->Global_model->fetch('menu');

		$this->load->view('templates/main_template', $data);
	}

}