<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class coupons_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Coupons_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}
	
	public function get_all_coupons() {
		$data['coupons'] = $this->Coupons_model->fetch('coupons');
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$coupons_data = json_decode(post('coupons_data'));
		$this->Coupons_model->insert('coupons',$coupons_data);
		echo json_encode($response);
	}

	public function get_coupon(){
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
		$data["coupon"] = $this->Coupons_model->fetch_data('coupons',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$coupons_data = json_decode(post('coupons_data'));
		if(isset($coupons_data->password)) {
			$coupons_data->password = hash_password($coupons_data->password);
		}
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Coupons_model->update('coupons',$coupons_data,$filter);
		echo json_encode($response);
	}

	public function delete()
	{
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Coupons_model->delete('coupons',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$id = clean_data(post('id'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status"=>$status];
		$filter = ["id"=>$id]; 
		$this->Coupons_model->update('coupons',$data,$filter);
		echo json_encode($response);
	}
}