<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['homepage'] = 'Homepage';
$route['authentication'] = 'Authentication';
$route['cards'] = 'Cards';
$route['cart'] = 'Cart';
$route['checkout'] = 'Checkout';
$route['coupons_api'] = 'Coupons_api';
$route['purrcoins_api'] = 'Purrcoins_api';
$route['dashboard_api'] = 'Dashboard_api';
$route['faq'] = 'Faq';
$route['functions'] = 'Functions';
$route['loyalty_program'] = 'Loyalty_program';
$route['loyalty_program_api'] = 'Loyalty_program_api';
$route['my_account'] = 'My_account';
$route['order'] = 'Order';
$route['order_summary'] = 'Order_summary';
$route['our_store'] = 'Our_store';
$route['privacy_policy'] = 'Privacy_policy';
$route['products_api'] = 'Products_api';
$route['widgets_api'] = 'Widgets_api';
$route['secure_admin'] = 'Secure_admin';
$route['shipping'] = 'Shipping';
$route['thank_you'] = 'Thank_you';
$route['users_api'] = 'Users_api';
$route['secure_login'] = 'Secure_login';
$route['thank_you'] = 'Thank_you';
$route['audits_api'] = 'Audits_api';
$route['tos'] = 'Tos';
$route['run_product'] = 'Run_product';
$route['contact'] = 'Contact';
$route['visit_us'] = 'Visit_us';
$route['allsets'] = 'Allsets';
$route['currency_api'] = 'Currency_api';
$route['pages/(:any)'] = 'Pages/view/$1';

$route['default_controller'] = 'homepage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
